% ========= Syntax =========

metavar x, y, z, X, Y, Z ::= {{ com Variables }} {{ repr-locally-nameless }}
metavar nat ::= {{ coq nat }} {{ lex numeral }}

grammar
sort, s :: 'srt_' ::= {{ com Sort }} {{ coq sort }}
 | srt nat ::   :: level {{ com Sort Level }} {{ coq (srt_level ([[nat]])) }}
 | *       :: M :: star  {{ com Star }} {{ coq (srt_level 0) }}
 | BOX     :: M :: box   {{ com Box }} {{ coq (srt_level 1) }}

trm, e, A, B, C :: 'trm_' ::= {{ com Expression }}
 | x                     ::   :: var   {{ com Variable }}
 | s                     ::   :: sort  {{ com Sort }}
 | e1 e2                 ::   :: app   {{ com Application }}
 | \ x : A . e           ::   :: abs   (+ bind x in e +) {{ com Abstraction }}
 | Pi x : A . B          ::   :: prod  (+ bind x in B +) {{ com Product }}
 | / x : A . e           ::   :: mu    (+ bind x in e +) {{ com Recursion }}
 | castup [ A ] e        ::   :: castup {{ com Up Cast }}
 | castdn [ A ] e        ::   :: castdn {{ com Down Cast }}
 | ( e )                 :: S :: parens {{ coq ([[e]]) }}
 | e1 [ x := e2 ]        :: M :: subst {{ coq (open_trm_wrt_trm [[x e1]] [[e2]]) }}
 | A -> B                :: M :: Arrow {{ coq (trm_prod ([[A]]) ([[B]])) }}

substitutions
  single e x :: subst

freevars
  e x :: fv

grammar
env, G {{ tex \Gamma }} :: 'ctx_' ::= {{ com Context }} {{ coq LibEnv.env trm }}
 | nil                  ::   :: Nil     {{ com Empty }} {{ coq empty }}
 | G , x : A            ::   :: Binding {{ com Binding }} {{ coq ([[G]] & [[x]]~[[A]]) }}
 | G1 , G2              :: M :: Concat {{ coq ([[G1]] & [[G2]]) }}
 | G [ x := e ]         :: M :: Subst {{ coq (map (subst [[x]] ([[e]])) ([[G]])) }}


formula :: 'formula_' ::=
 | judgement            ::   :: judgement
 | \\ formula           ::   :: newline {{ coq [[formula]] }}
 | ( formula )          ::   :: parens {{ coq [[formula]] }}
 | x : A in G           ::   :: binds {{ coq binds ([[x]]) ([[A]]) ([[G]]) }}
 | e closed             ::   :: close {{ coq (lc_trm ([[e]])) }} {{ tex }}
 | x # G                ::   :: fresh {{ coq ([[x]] # ([[G]])) }} {{ tex [[x]] \text{~fresh in~} [[G]] }}
 | Rel s1 s2 s3         ::   :: relation
   {{ tex ([[s1]],[[s2]],[[s3]]) \in \mathcal{R} }}
   {{ coq (Rel ([[s1]]) ([[s2]]) ([[s3]]) ) }}
 | Ax s1 s2             ::   :: axiom
   {{ tex ([[s1]],[[s2]]) \in \mathcal{A} }}
   {{ coq (Ax ([[s1]]) ([[s2]]) ) }}
 | A ~p> B              ::   :: fullred {{ coq (C.dpara (erasure ([[A]])) (erasure ([[B]]))) }}

terminals :: 'terminals_' ::=
 | \      ::   :: lambda    {{ tex \lambda }}
 | /      ::   :: mu        {{ tex \mu }}
 | Pi     ::   :: pi        {{ tex \Pi }}
 | .      ::   :: dot       {{ tex .~ }}
 | ]      ::   :: rbracket  {{ tex ]~ }}
 | *      ::   :: star      {{ tex \star }}
 | BOX    ::   :: box       {{ tex \Box }}
 | nil    ::   :: nil       {{ tex \varnothing }}
 | ->     ::   :: to        {{ tex \rightarrow }}
 | :=     ::   :: subst     {{ tex \mapsto }}
 | |-     ::   :: turnstile {{ tex \vdash }}
 | ==     ::   :: eq        {{ tex \equiv }}
 | in     ::   :: in        {{ tex \in }}
 | val    ::   :: val       {{ tex ~\mathsf{val} }}
 | castup ::   :: castup    {{ tex \mathsf{castup} }}
 | castdn ::   :: castdn    {{ tex \mathsf{castdn} }}
 | -->    ::   :: red       {{ tex \hookrightarrow }}
 | \\     ::   :: newline   {{ tex \ottlinebreak }}

% ========= PTS Definition =========

embed {{ coq

(* General PTS *)
Definition Ax := pts_axiom PTS_Spec.PTS.
Definition Rel := pts_rules PTS_Spec.PTS.

(* Singly Sorted PTS *)
Definition AxUniq := pts_axuniq PTS_Spec.PTS.
Definition RelUniq := pts_reluniq PTS_Spec.PTS.

(* Decidable PTS *)
Definition AxDec := pts_axdec PTS_Spec.PTS.
Definition RelDec := pts_reldec PTS_Spec.PTS.

}}

% ========= Erasure =========

embed {{ coq

Require PTSMu_ott.
Module C := PTSMu_ott.

Fixpoint erasure (s : trm) {struct s} : C.trm :=
  match s with
  | trm_var_b i      => C.trm_var_b i 
  | trm_var_f x      => C.trm_var_f x
  | trm_sort s       => C.trm_sort s
  | trm_app t1 t2    => C.trm_app (erasure t1) (erasure t2)
  | trm_abs t1 t2    => C.trm_abs (erasure t1) (erasure t2)
  | trm_prod t1 t2   => C.trm_prod (erasure t1) (erasure t2)
  | trm_mu t1 t2     => C.trm_mu (erasure t1) (erasure t2)
  | trm_castup t1 t2 => erasure t2
  | trm_castdn t1 t2 => erasure t2
  end.

}}

% ========= Static Semantics =========

defns 
Jtyping :: '' ::=

defn 
G |- e : A ::   :: typing :: 't_' {{ com Typing }}
by

|- G
Ax s1 s2
------------ :: ax
G |- s1 : s2

|- G
x : A in G
---------- :: var
G |- x : A

G |- A : s1
G, x:A |- e : B
G, x:A |- B : s2
Rel s1 s2 s3
-------------------------- :: abs
G |- \x:A . e : Pi x:A . B

G |- e1 : Pi x : A . B
G |- e2 : A
------------------------ :: app
G |- e1 e2 : B [x := e2]

G |- A : s1
G, x:A |- B : s2
Rel s1 s2 s3
-------------------- :: prod
G |- Pi x:A . B : s3

G |- A : s
G, x:A |- e : A
----------------- :: mu
G |- /x:A . e : A

G |- B : s
G |- e : A
B ~p> A
--------------------- :: castup
G |- castup[B] e : B

G |- B : s
G |- e : A
A ~p> B
-------------------- :: castdn
G |- castdn[B] e : B

defn 
|- G ::   :: wf :: 'wf_' {{ com Well-formedness }}
by

------ :: nil
|- nil

G |- A : s
x # G      
------------ :: cons
|- G, x : A
