Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_ott LamI_Infra LamI_Proofs.
Require LibReflect.
Implicit Types x : var.

Definition decidable (P : Prop) := (P \/ ~ P).

(* ********************************************************************** *)
(** Lemmas of decidability *)

(** Equality of terms *)
Lemma eq_typ_dec : forall (T T' : trm),
  sumbool (T = T') (T <> T').
Proof.
  decide equality.
  decide equality.
  tests EQ: (x5 = x0); auto.
  induction s; induction s0; decide equality.
  decide equality.
Qed.

(** Bindings lookup *)
Lemma binds_lookup_dec : forall (x : var) (E : env),
    decidable (exists a, binds x a E).
Proof.
  intros. destruct (LibReflect.decidable_sumbool (indom_decidable E x)).
  left. destruct (get_some m). exists x0. unfold binds. auto.
  right. intros [a P]. unfold binds in P. pose (get_some_inv P). auto.
Qed.

(* ********************************************************************** *)
(** Typing is unique for singly sorted PTS *)

Lemma typing_unique : forall E e T1 T2,
  typing E e T1 ->
  typing E e T2 ->
  T1 = T2.
Proof.
  intros E e T1 T2 Typ1 Typ2.
  generalize dependent T2.
  induction Typ1; intros T' Typ'; inversion Typ'; subst; eauto.
  lets: AxUniq H0 H4. fequal~.
  eapply binds_func; eauto.
  f_equal; eauto.
  pick_fresh x.
  apply open_var_inj with (x := x); eauto.
  pose (IHTyp1_1 (trm_prod A0 B0) H2).
  injection e.
  intros. subst.
  auto.
  asserts* W1: (trm_sort s1 = trm_sort s4).
  pick_fresh y. lets: H0 y __. auto.
  asserts* W2: (trm_sort s2 = trm_sort s5).
  inversions W1. inversions W2.
  lets: RelUniq H1 H8. fequal~.
  pose (IHTyp1 _ H1).
  rewrite <- e0 in H3.
  pose (reduct_determ H H3).
  auto.
Qed.

(** A term is a value *)
Lemma value_dec : forall E t T,
    typing E t T ->
    decidable (value t).
Proof.
  introv Typ. lets Typ': Typ. inductions Typ.
  left*.
  right*. intros P. inversion P.
  left*.
  right*. intros P. inversion P.
  left*.
  right*. intros P. inversion P.
  left*.
  right. intros P. inversions P. 
Qed.

(** A term can be reduced *)
Lemma reduct_dec : forall E t T,
    typing E t T ->
    decidable (exists t', t --> t').
Proof.
  introv Typ. lets Typ': Typ. unfold decidable.
  inductions Typ; try solve [right; intros [S J]; inversion J].
  
  destruct (value_dec Typ1) as [Val | Q].
  inversions Typ1; inversions Val. left. exists* (e ^^ e2).
  inversions H1.
  destruct (IHTyp1 Typ1).
  destruct H as [e1' ?].
  left. exists* (trm_app e1' e2).
  unfold not in *.
  right. intros. destruct H0 as [t' ?].
  inversions H0. apply~ Q.
  apply~ H. exists* e1'.

  left. exists* (e ^^ trm_mu A e).
  
  destruct (value_dec Typ) as [Val | Q].
  inversions Typ; inversions Val.
  inversion H. inversion H. inversion H.
  left. exists* e0.
  destruct (IHTyp Typ) as [[t' L] | R].
  left. exists* (trm_castdn t').
  right. intros [t' P]. inversions P. destruct R.
  exists* e'. destruct Q. apply* value_castup.
Qed.

(* ********************************************************************** *)
(** Lemmas of typing renaming *)

Lemma typing_rename_eq : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  typing (E & (x ~ T1)) (e ^ x) T2 ->
  typing (E & (y ~ T1)) (e ^ y) T2.
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  tests EQ: (x = y).
  subst; eauto.
  assert (wf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  assert ([x ~> trm_var_f y] T2 = T2).
  apply* subst_fresh.
  rewrite <- H0.
  apply_empty* (@typing_substitution T1).
  apply~ (@typing_weaken).
  apply wf_cons with (s := s); auto.
  apply_empty~ typing_weaken.
  apply wf_cons with (s := s); auto.
Qed.

Lemma typing_rename : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  typing (E & (x ~ T1)) (e ^ x) (T2 ^ x) ->
  typing (E & (y ~ T1)) (e ^ y) (T2 ^ y).
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  tests EQ: (x = y).
  subst; eauto.
  assert (wf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  rewrite* (@subst_intro x T2).
  apply_empty* (@typing_substitution T1).
  apply~ (@typing_weaken).
  apply wf_cons with (s := s); auto.
  apply_empty~ typing_weaken.
  apply wf_cons with (s := s); auto.
Qed.


(* ********************************************************************** *)
(** Type checking is decidable *)

Lemma typing_decidable : forall E e,
  lc_trm e -> wf E -> decidable (exists T, typing E e T).
Proof.
  intros E e Trm Wf. gen E.
  induction Trm; intros E Wf.

  (* Case Var *)
  destruct (@binds_lookup_dec x5 E) as [[T H] | H].
  left; eauto. right. intros [T J]. inversion J; subst; eauto.

  (* Case Ax *)
  destruct (AxDec s) as [[s' H] | H].
  left. exists* (trm_sort s').
  unfold not in *. right. unfold not; intros. destruct H0 as [T H1].
  inversions H1. apply~ H. exists* s2.
  
  (* Case App *)
  destruct (IHTrm1 E Wf) as [[T H] | H]; eauto.
  destruct (IHTrm2 E Wf) as [[S J] | J]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_prod ?P1 ?P2) |-
        decidable (exists Q, typing ?E (trm_app ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_app ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E t1 (trm_prod ?U ?T) |- _] =>
          assert (K: P = trm_prod U T); eauto using typing_unique;
            inversion K
        end
    end.
  
    destruct (eq_typ_dec T1 S). subst. left; eauto.
    right. intros [S' J'].
    inversion J'; subst.
    lets: typing_unique J H5.
    lets: typing_unique H H3.
    inversion H1; subst; eauto using typing_unique.
    
    right. intros [S' J']. inversion J'; subst; eauto.
    right. intros [S' J']. inversion J'; subst; eauto.

  (* Case Lam *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_abs ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_abs ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E t1 (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end.

    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    destruct (typing_wf_from_typing J) as [s2 [K | K]].
    subst.
    destruct (AxDec s2) as [[s3 P] | P].
    destruct (RelDec s s3) as [[s4 Q] | Q].
    left. exists* (trm_prod A (trm_sort s2)).
    apply_fresh* t_abs as y.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W. apply_empty* typing_weaken.
    
    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ Q. exists s4.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & y ~ A) (e ^ y) (trm_sort s2 ^ y)).
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    lets: typing_unique H1 M. rewrite H4 in H3.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W in H3. inversions H3.
    lets: AxUniq P H13. subst~.

    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ P. exists s0.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & y ~ A) (e ^ y) (trm_sort s2 ^ y)).
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    lets: typing_unique H1 M. rewrite H4 in H3.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W in H3. inversions H3.
    auto.

    destruct (RelDec s s2) as [[s4 Q] | Q].
    left. exists (trm_prod A (close_var x S)).
    apply_fresh* t_abs as y.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    apply* close_var_fresh. rewrite* <- (@close_var_open x).
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite <- W.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    apply* close_var_fresh. notin_simpl; simpl; auto.
    apply~ fv_open_var. rewrite* <- (@close_var_open x).
    rewrite* <- (@close_var_open x).
    
    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ Q. exists s3.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & x ~ A) (e ^ x) (B ^ x)).
    apply* (@typing_rename y).
    lets: typing_unique J M.
    asserts* M2: (typing (E & x ~ A) (B ^ x) (trm_sort s0)).
    apply* (@typing_rename_eq y). notin_simpl; simpl; auto.
    notin_simpl; simpl; auto.
    lets: typing_fresh x H3.
    apply* fv_open_var.
    rewrite H4 in K.
    lets: typing_unique K M2. inversions H7. auto.
    
    right. intros [S K]. inversion K; subst.
    apply J.
    exists* (B ^ x).
    pick_fresh y. lets: H5 y __. auto.
    apply* (@typing_rename y).

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s1).

  (* Case Pi *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        assert (K: P = (trm_sort s1)); eauto using typing_unique;
          inversion K
    end.
  
    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    asserts* W3: (lc_trm S).
    induction W3;
      match goal with
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) (trm_sort ?s1) |-
          decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] => auto
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) ?P |-
          decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] =>
        right; intros [S' J']; inversion J'; subst;
          match goal with
          | [ H6: forall _, _ -> typing _ _ (trm_sort ?s1) |- _ ] =>
            pick_fresh y; assert (W: (typing (E & x ~ t1) (t2 ^ x) (trm_sort s1))) by
                (apply* (@typing_rename_eq y); notin_simpl; simpl; auto);
            lets W2: typing_unique W J; inversion W2
          end
      end.

      destruct (RelDec s s0) as [[s4 Q] | Q].
      left. exists (trm_sort s4).
      apply_fresh* t_prod as y.
      apply* (@typing_rename_eq x). notin_simpl; simpl; auto.
      right. unfold not in *. intros.
      destruct H1 as [T ?].
      inversions H1. apply~ Q. exists s3.
      lets: typing_unique H2 H5. inversions H1.
      pick_fresh y. lets: H7 y __. auto.
      asserts* M2: (typing (E & x ~ A) (B ^ x) (trm_sort s2)).
      apply* (@typing_rename_eq y). notin_simpl; simpl; auto.
      lets: typing_unique J M2. inversions H3. auto.

    right. intros [S K]. inversion K; subst.
    apply J.
    exists* (trm_sort s2).
    pick_fresh y. lets: H6 y __. auto.
    apply* (@typing_rename_eq y). notin_simpl; simpl; auto.

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s1).

  (* Case Mu *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_mu ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_mu ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E S' (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end.
    
    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    destruct (eq_typ_dec A S). subst.
    left. exists S. apply_fresh~ t_mu as y.
    instantiate (1 := s). auto.
    apply~ (@typing_rename_eq x).
    right. intros [S' K]. inversion K; subst.
    pick_fresh y.
    lets: H7 y __. auto.
    assert (typing (E & x ~ S') (e ^ x) S').
    apply~ (@typing_rename_eq y).
    lets: typing_unique J H3. false*.
    
    right. intros [S K]. inversion K; subst.
    apply J. exists S.
    pick_fresh y.
    lets: H7 y __. auto.
    apply~ (@typing_rename_eq y).

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s).

  (* Case CastUp *)
  destruct (IHTrm1 E Wf) as [[T H] | H]; eauto.
  destruct (IHTrm2 E Wf) as [[S J] | J]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_castup ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_castup ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E _ (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end. 
    
    destruct (reduct_dec H) as [[t1' L] | R].
    destruct (eq_typ_dec S t1').
    left. subst.
    exists A. apply t_castup with (A := t1') (s := s).
    auto. auto. auto.
    right. intros [T' P]. destruct (typing_castup_inv P) as [A' [B [C [D [? F]]]]].
    subst. assert (S = A') by (apply* typing_unique).
    subst. assert (t1' = A') by (apply~ (reduct_determ L F)).
    auto.
    right. intros [T' P]. destruct (typing_castup_inv P) as [A' [B [C [D F]]]]. destruct R. exists* A'.
    right. intros [S' J']. inversion J'; subst; eauto.
    right. intros [S' J']. inversion J'; subst; eauto.

  (* Case CastDn *)
  destruct (IHTrm E Wf) as [[T H] | H]; eauto.
  destruct (typing_wf_from_typing H) as [s [J | J]].
  subst. right. unfold not. intros.
  destruct H0 as [T K].
  inversions K.
  lets: typing_unique H H1. subst. inversion H3.
  destruct (reduct_dec J) as [[t' A] | B].
  left. exists* t'.
  right. intros [T0 P]. inversions P. destruct B.
  assert (T = A) by (apply* typing_unique).
  subst. exists* T0.
  right. intros [T P]. inversions P. destruct H.
  exists* A.
Qed.

