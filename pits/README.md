# Coq Formalization of "Pure Iso-Type Systems"

## Getting Started Guide

The repository consists of Coq proofs of the paper. Its correctness can
be checked by the Coq theorem prover (version 8.6). The repository
passes the test if all Coq files can be compiled without any error
messages, which indicates Coq proofs are machine-checked without
issues. The following guide is to prepare the environment to compile
(i.e. check) Coq files.

We put main lemmas proved across the Coq files altogether in the index
file [`Index.v`](Index.v) for easy searching. Notice that there is no `admit`
or `Admitted` tactics in our Coq proofs, which are used to skip proof
goals leading to incomplete proofs. This can be verified by executing
the following Unix shell command in the unpacked repository folder:

    ls *.v | xargs grep -y admit

There should be no output, which means there is no `admit` or
`Admitted` found in Coq proofs.

### Build Guide:

The dependencies for compilation are as follows:

- Coq 8.6 (*not 8.6.1 or 8.6.x*)
- GNU Make
- Perl 5
- Ott 0.25 (requires Git and OCaml)

We use the latest Debian stable release (9.0, Stretch) as an
example. Steps are similar for other Linux distros, Unix-like systems
(e.g., macOS and BSD) and Cygwin (for Windows).

1. Install prerequisites (Coq 8.6, GNU Make, Perl 5):

        sudo apt-get update
        sudo apt-get install coq=8.6-* make perl

2. Compile the Ott tool (requires OCaml and Git):

        sudo apt-get install ocaml-nox git
        git clone https://github.com/ott-lang/ott.git
        cd ott && git checkout 0.25 && make world
        sudo cp bin/ott /usr/local/bin/

    Ott tool is for generating Coq definition files (`*_ott.v`) from
    Ott source files (`*.ott`). The repository already contains
    pre-generated Coq definitions.

3. Unpack and test the repository:

        make

## Step by Step Instructions

The repository contains all proofs in the paper which are mechanized and
machine-checked in the Coq theorem prover.
The development is based on the Coq formalization of calculus of constructions
in [LN library][libln] by Arthur Charguéraud.
Main results of the paper
are organized in the index file [`Index.v`](Index.v). There are proofs for 3
variants of the PITS calculus and PTS with recursion:

* `LamI`: Call-by-name PITS (Section 4)
* `LamI_CBV`: Call-by-value PITS (Section 5)
* `LamI_Full`: Full PITS with parallel reduction (Section 6)
* `PTSMu`: PTS with recursion (Section 6)

For each type system, there are 1 Ott definition and 3 Coq files (`@` stands for
the name of variant):

* `PTS_Spec.v` contains the abstract definition of PTS (sorts, axioms and rules).
* `@.ott` is the Ott definition of type systems which is written in
  readable ASCII text, including syntax, operational
  semantics and static semantics. For
  details of Ott syntax, please refer to
  its [website](http://www.cl.cam.ac.uk/~pes20/ott/)
  or [developer page](https://github.com/ott-lang/ott).
* `@_ott.v` is the Coq definition file generated from `@.ott` file
  using *locally nameless representation*.
* `@_Infra.v` is the infrastructure file, which contains basic
  supporting lemmas for locally nameless representation.
* `@_Proofs.v` is proofs for the main metatheory for type safety, including subject reduction and progress lemmas.
* `@_Dec.v` contains the proofs related to decidability.

There are some extra notes for PTSMu:

* `PTSMu_CR.v` is the proof for Church-Rosser theorem.
* `PTSMu_Sound.v` is the type soundness proof, including subject reduction and progress lemmas.
* `PTS_Single.v` contains proofs for the completeness of Full PITS w.r.t. PTSStep (Section 6.3). Several lemma definitions are inspired by [CoreSpec][corespec] that formalizes System DC.

The folder `tlc` contains the "TLC" Coq library written by Arthur
Charguéraud, et al. for supporting locally nameless
representation. Detailed version and license information is included
in [`tlc/README.md`](tlc/README.md).

[libln]: http://www.chargueraud.org/softs/ln/
[corespec]: https://github.com/sweirich/corespec/blob/62dceb4/src/FcEtt/erase.v#L534
