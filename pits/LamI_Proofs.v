Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_ott LamI_Infra.
Implicit Types x : var.

Tactic Notation "apply_fresh" "~" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos~.

Lemma typing_weaken : forall G E F t T,
  typing (E & G) t T ->
  wf (E & F & G) ->
  typing (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto 4.
  (* case: var *)
  apply* t_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  instantiate (1 := s3). auto.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0. auto.
  (* case: trm_mu *)
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0.
Qed.

Definition red_out (R : relation) :=
  forall x u t t', lc_trm u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Hint Unfold red_out.

Lemma value_red_out :   forall x u t, lc_trm u -> value t -> 
  value ([x~>u]t).
Proof.
  intros_all. induction H0; simpl; auto.
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl; eauto 3.
  rewrite* subst_open.
  rewrite* subst_open.
  apply* r_mu. 
  apply* r_cast_elim.
Qed.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  typing E v V ->
  typing (E & x ~ V & F) t T ->
  typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) => 
      forall F, G = E & x ~ V & F -> 
      wf (E & (map (subst x v) F))); 
   intros; subst; simpls subst; eauto 4. 
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ t_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh~ (@t_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
   instantiate (1 := s3). auto.
  (* case: trm_app *)
  rewrite* subst_open.
  (* case: trm_prod *)
  apply_fresh~ (@t_prod) as y.
   cross; auto. apply_ih_map_bind* H0. auto.
  (* case: trm_prod *)
  apply_fresh~ (@t_mu) as y.
   cross; auto. apply_ih_map_bind* H0.
  (* case: redl *)
  apply~ (@t_castup). apply* reduct_red_out.
  (* case: redr *)
  apply~ (@t_castdn). apply* reduct_red_out.
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
Qed.

Lemma typing_abs_inv : forall E V t P,
  typing E (trm_abs V t) P -> exists T L,
  P = trm_prod V T /\
  forall x, x \notin L -> typing (E & x ~ V) (t ^ x) (T ^ x).
Proof.
  intros. inductions H; eauto 4.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> t --> t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv H2; inversions H2.
Qed.

Lemma reduct_determ : forall t t1 t2,
    reduct t t1 -> reduct t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv HH; inversions HH; auto.
  inversion H6.
  inversions H1.
  pose (IHreduct e1'0 H5). rewrite~ e.
  fequal*. inversions H1.
  inversions H2.
Qed.

Lemma typing_castup_inv : forall E t U V,
    typing E (trm_castup U t) V -> exists T s,
      typing E U s /\ typing E t T
      /\ U = V /\ U --> T.
Proof.
  introv Typ. gen_eq u: (trm_castup U t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* A.
Qed.

Lemma typing_preserve : forall E t t' T,
  typing E t T -> t --> t' -> typing E t' T.
Proof.
  introv Typ. gen t'.
  induction Typ; introv Red; inversions Red;
    try solve [eauto 4].

  (* case: reduct_beta *)
  destruct (typing_abs_inv Typ1) as [T' [L1 [EQ Typt2]]].
  inversions EQ.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  rewrite~ (@subst_intro x T').
  apply_empty~ (@typing_substitution A0).

  pick_fresh x.
  rewrite~ (@subst_intro x e).
  asserts W: ([x ~> trm_mu A e] A = A).
  apply~ subst_fresh.
  rewrite <- W at 2.
  apply_empty~ (@typing_substitution A).
  apply_fresh~ t_mu as y.
  instantiate (1 := s). auto.

  destruct (typing_castup_inv Typ) as [T' [s [TypU [Typt [Eq1 Red1]]]]].
  subst. pose (reduct_determ H Red1).
  rewrite e. auto.
Qed.

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  exists s, typing E U (trm_sort s).
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     exists s.
     apply_empty* typing_weaken.
    destruct (wf_push_inv W).
      destruct~ IHE. exists x1.
      apply_empty* typing_weaken.
Qed.

Lemma typing_prod_inv : forall E U T A,
  typing E (trm_prod U T) A -> 
  exists L s1 s2, typing E U (trm_sort s1) /\
  forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T).
  induction Typ; intros; subst; try solve [false | eauto 4].
  inversions EQP1. exists*.
Qed.

Lemma typing_wf_from_typing : forall E t T,
  typing E t T ->
  exists s, T = trm_sort s \/ typing E T (trm_sort s).
Proof.
  intros. induction H; eauto 3.
  destruct (typing_wf_from_context H0 H) as [s ?].
  exists* s.
  destruct IHtyping as [s [? | ?]].
  inversions H5. exists s3. right*.
  exists s3. right*.
  destruct IHtyping1 as [s [? | ?]].
  false*.
  destruct (typing_prod_inv H1) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. rewrite~ (@subst_intro x).
  exists s1. right.
  unsimpl ([x ~> e2](trm_sort s1)).
  apply_empty* (@typing_substitution A).
  destruct IHtyping as [s [? | ?]].
  subst. inversions H0.
  exists s. right.
  apply (typing_preserve H1). auto.
Qed.

Lemma typing_progress : forall t T, 
  typing empty t T ->
     value t 
  \/ exists t', t --> t'.
Proof.
  introv Typ. lets Typ': Typ. inductions Typ.
  left*.
  false* binds_empty_inv. 
  left*.
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1.
      exists* (e ^^ e2).
      inversions H1.
      exists* (trm_app t1' e2).
  left*.
  right. exists* (e ^^ (trm_mu A e)).
  left*.
  right. destruct~ IHTyp as [Val1 | [t1' Red1]].
      inversions Typ; inversions Val1. 
      inversion H. inversion H.
      inversion H. exists* e0.
      exists* (trm_castdn t1').
Qed.

