METALIB = ../tlc
OTT = ott
PERL = perl

COQPREFIX =
INCLUDES = -R $(METALIB) TLC
COQFLAGS = -w -deprecated-implicit-arguments,-notation-overridden,-local-declaration
COQC = $(COQPREFIX)coqc $(INCLUDES) $(COQFLAGS)
COQDEP = $(COQPREFIX)coqdep $(INCLUDES)

OTTFLAGS =

src_ott = $(wildcard *.ott)
src_coq = $(wildcard *.v) $(src_ott:.ott=_ott.v)
dep_src = $(src_coq) $(wildcard $(METALIB)/*.v)

.PHONY: all clean distclean
.SUFFIXES: .v .vo

all: $(src_coq:.v=.vo)

.v.vo : .depend
	@echo "Compiling $<"
	@$(COQC) $<

depend: .depend

.depend : $(dep_src) Makefile
	$(COQDEP) $(dep_src) > .depend

ifeq ($(findstring $(MAKECMDGOALS),depend clean distclean),)
include .depend
endif

clean:
	rm -f *.vo .depend *.deps *.dot *.glob .*.aux .lia.cache $(obj_ott)

distclean: clean
	@rm -f $(METALIB)/*.vo $(METALIB)/*.glob $(METALIB)/*.v.d $(METALIB)/.*.aux || echo skip
	rm -f *_ott.v

%_ott.v: %.ott
	$(OTT) $(OTTFLAGS) -o $@ $<
	@$(PERL) -pi -e 's/^Require Import Metatheory/Require Import LibLN PTS_Spec/' $@
	@$(PERL) -pi -e 's/^Definition x := var.//' $@
	@$(PERL) -pi -e 's/:x\)/:var\)/g' $@
	@$(PERL) -pi -e 's/: Set := LibEnv.env/:= LibEnv.env/g' $@
	@$(PERL) -pi -e 's/if \(k === nat\)/If (k = nat)/g' $@
	@$(PERL) -pi -e 's/=> \{}/=> \\{}/g' $@
	@$(PERL) -pi -e 's/=> \{\{(.*)}}/=> \\{$$1}/g' $@
	@$(PERL) -pi -e 's/if eq_var ([^\s]+)/If $$1 =/g' $@

