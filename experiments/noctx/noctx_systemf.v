Set Implicit Arguments.
Require Import Coq.Arith.Peano_dec.
Require Import Coq.Lists.List.
Require Import TLCTactics.

Inductive Typ :=
| Int : Typ
| TFVar : Typ
| TBVar : nat -> Typ
| MOpenTT : Typ -> list Typ -> Typ
| Fun : Typ -> Typ -> Typ
| Forall : Typ -> Typ.

Inductive Exp :=
| FVar : Typ -> Exp
| BVar : nat -> Exp
| Lit : nat -> Exp
| Lam : Typ -> Exp -> Exp
| TLam : Exp -> Exp
| MOpenEE : Exp -> list Exp -> Exp
| App : Exp -> Exp -> Exp
| TApp : Exp -> Typ -> Exp.

Fixpoint lookup (n : nat) (l : list Exp) : Exp :=
  match l with
    | nil => BVar n
    | x :: xs => match n with
                   | 0 => x
                   | S m => lookup m xs
                 end
  end.

Fixpoint lookupt (n : nat) (l : list Typ) : Typ :=
  match l with
    | nil => TBVar n
    | x :: xs => match n with
                   | 0 => x
                   | S m => lookupt m xs
                 end
  end.

Fixpoint mopentt (e : Typ) (T : list (Typ)) : Typ :=
  match e with
    | Int => Int
    | TFVar => TFVar
    | TBVar n => lookupt n T
    | MOpenTT e U => mopentt e (U ++ T)
    | Fun e1 e2 => Fun (mopentt e1 T) (mopentt e2 T)
    | Forall e => Forall (MOpenTT e T)
  end.


Fixpoint mopenee (e : Exp) (T : list (Exp)) : Exp :=
  match e with
    | FVar t => FVar t
    | Lit x => Lit x
    | BVar n => lookup n T
    | App e1 e2 => App (mopenee e1 T) (mopenee e2 T)
    | MOpenEE e U => mopenee e (U ++ T)
    | Lam t e => Lam t (MOpenEE e T)
    | TLam e => TLam (MOpenEE e T)
    | TApp e t => TApp (mopenee e T) t
  end.

Fixpoint mopenet (e : Exp) (T : list (Typ)) : Exp :=
  match e with
    | FVar t => FVar t
    | Lit x => Lit x
    | BVar n => BVar n
    | App e1 e2 => App (mopenet e1 T) (mopenet e2 T)
    | MOpenEE e U => mopenee (mopenet e T) U
    | Lam t e => Lam (mopentt t T) (mopenet e T)
    | TLam e => TLam (mopenet e T)
    | TApp e t => TApp (mopenet e T) (mopentt t T)
  end.

Definition openee (e : Exp) (v : Exp) : Exp := mopenee e (v :: nil).
Definition openet e v := mopenet e (v :: nil).
Definition opentt e v := mopentt e (v :: nil).

Inductive has_type : Exp -> Typ -> Prop :=
| TyFVar : forall t, has_type (FVar t) t
| TyInt : forall x, has_type (Lit x) Int
| TyApp : forall t1 t2 e1 e2,
    has_type e1 (Fun t1 t2) -> has_type e2 t1 -> has_type (App e1 e2) t2
| TyLam : forall e t1 t2,
    has_type (openee e (FVar t1)) t2 -> has_type (Lam t1 e) (Fun t1 t2)
| TyTApp : forall t1 t2 e1,
    has_type e1 (Forall t1) -> has_type (TApp e1 t2) (opentt t1 t2)
| TyTLam : forall e t,
    has_type (openet e TFVar) t -> has_type (TLam e) (Forall t).

Inductive stuck : Exp -> Prop :=
| SVar : forall t, stuck (FVar t)
| SApp : forall u e, stuck u -> stuck (App u e)
| STApp : forall u t, stuck u -> stuck (TApp u t).

Inductive value : Exp -> Prop :=
| VStuck : forall e, stuck e -> value e
| VLit : forall x, value (Lit x)
| VLam : forall t e, value (Lam t e)
| VTLam : forall e, value (TLam e).

Inductive red : Exp -> Exp -> Prop :=
| RApp1 : forall e1 e1' e2 , red e1 e1' -> red (App e1 e2) (App e1' e2)
| RApp2 : forall e e' v, red e e' -> value v -> red (App v e) (App v e')
| RBeta : forall t e1 e2, value e2 -> red (App (Lam t e1) e2) (openee e1 e2)
| RTApp : forall e1 e1' e2 , red e1 e1' -> red (TApp e1 e2) (TApp e1' e2)
| RTBeta : forall t e1, red (TApp (TLam e1) t) (openet e1 t).

Hint Constructors Typ Exp has_type red value stuck.

Lemma lookup_nil : forall n,
  lookup n nil = BVar n.
Proof.
  intros. induction n; simpl; auto.
Qed.

Lemma mopenee_lemma : forall {e T U t t1},
  has_type (mopenee e (T ++ FVar t1 :: U)) t ->
  forall e1, has_type e1 t1 ->
            has_type (mopenee e (T ++ e1 :: U)) t.
Proof.
  induction e; simpl; intros; try auto.
  generalize n H. clear H n.
  induction T; simpl in *; intros; auto.
  destruct n; simpl in *; auto.
  inversion H; subst. auto.
  destruct n; simpl in *; auto.
  inversion H; subst. clear H.
  apply TyLam. unfold openee in *. simpl in *.
  rewrite <- app_assoc in *.
  rewrite <- app_comm_cons in *.
  apply IHe with (t1 := t1).
  auto. auto.
  inversions H.
  apply* TyTLam.
  unfold openet in *. simpl in *.
  (* Problem here! *) admit.
  rewrite app_assoc in *.
  apply IHe with (t1 := t1). auto. auto.
  inversions H.
  apply TyApp with (t1 := t0).
  apply IHe1 with (t1 := t1); auto.
  apply IHe2 with (t1 := t1); auto.
  inversions H.
  apply* TyTApp.
Defined.

(* Lemma mopenet_subst : forall T U e t t1, *)
(*   has_type (mopenet e (T ++ TFVar :: U)) t1 -> *)
(*   has_type (mopenet e (T ++ t :: U)) (mopentt t1 (T ++ t :: U)). *)
(* Proof. *)
(*   intros. *)
(*   induction e; simpl; intros; try auto. *)
(*   generalize n H. clear H n. *)
(*   induction T; simpl in *; intros; auto. *)
(*   destruct n; simpl in *; auto. *)
(*   inversion H; subst. auto. *)
(*   destruct n; simpl in *; auto. *)
(*   inversion H; subst. clear H. *)
(*   apply TyLam. unfold openee in *. simpl in *. *)
(*   rewrite <- app_assoc in *. *)
(*   rewrite <- app_comm_cons in *. *)
(*   apply IHe with (t1 := t1). *)
(*   auto. auto. *)
(*   inversions H. *)
(*   apply* TyTLam. *)
(*   admit. *)
(*   rewrite app_assoc in *. *)
(*   apply IHe with (t1 := t1). auto. auto. *)
(*   inversions H. *)
(*   inversion H; subst. *)
(*   apply TyApp with (t1 := t0). *)
(*   apply IHe1 with (t1 := t1); auto. *)
(*   apply IHe2 with (t1 := t1); auto. *)
(*   inversions H. *)
(*   apply* TyTApp. *)


Lemma list_add_nil : forall (e : Exp), e :: nil = nil ++ e :: nil.
Proof.
  intros. simpl. auto. 
Qed.

Lemma subject_reduction : forall e e',
  red e e' -> forall t, has_type e t -> has_type e' t.
Proof.
  intros e e' r.
  induction r; intros.
  inversion H; subst.
  apply TyApp with (t1 := t1).
  apply (IHr _ H2). auto.
  inversion H0; subst.
  apply TyApp with (t1 := t1). auto.
  apply (IHr _ H5).
  inversion H0; subst. inversion H3; subst. clear H0 H3.
  unfold openee in *.
  rewrite (list_add_nil) in H2.
  rewrite (list_add_nil).
  apply (mopenee_lemma H2 H5).
  inversions H.
  apply* TyTApp.
  inversions H.
  inversions H3.
  unfold openet in *.
  unfold opentt in *.
  (* Need lemma! *) admit.
Defined.

Lemma progress_lem : forall e t,
  has_type e t ->
  value e \/ exists e', red e e'.
Proof.
  intros. induction H; subst; auto.
  inversion H; subst.
  left; eauto.
  destruct IHhas_type1.
  inversion H3; subst.
  left; eauto.
  destruct H3 as [e' ?].
  right. exists (App e' e2); eauto.
  destruct IHhas_type2.
  (* Not finished *)
Admitted.
  
  
