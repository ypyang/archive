Set Implicit Arguments.
Require Import Coq.Arith.Peano_dec.
Require Import Coq.Lists.List.

Inductive Typ :=
| Int : Typ
| Fun : Typ -> Typ -> Typ.

Inductive Bot :=.

Inductive Exp A :=
| FVar : A -> Exp A
| BVar : nat -> Exp A
| Lit : nat -> Exp A
| Lam : Typ -> Exp A -> Exp A
| MOpen : Exp A -> list (Exp A) -> Exp A
| App : Exp A -> Exp A -> Exp A.

Implicit Arguments BVar [A].
Implicit Arguments Lit [A].

Definition TExp := Exp Typ.

Fixpoint lookup {A} (n : nat) (l : list (Exp A)) : Exp A :=
  match l with
    | nil => BVar n
    | x :: xs => match n with
                   | 0 => x
                   | S m => lookup m xs
                 end
  end.

Fixpoint mopen {A} (e : Exp A) (T : list (Exp A)) : Exp A :=
  match e with
    | FVar t => FVar t
    | Lit x => Lit x
    | BVar n => lookup n T
    | App e1 e2 => App (mopen e1 T) (mopen e2 T)
    | MOpen e U => mopen e (U ++ T)
    | Lam t e => Lam t (MOpen e T)
  end.

Definition open {A} (e : Exp A) (v : Exp A) : Exp A := mopen e (v :: nil).

Inductive has_type : TExp -> Typ -> Prop :=
| TFVar : forall t, has_type (FVar t) t
| TInt : forall x, has_type (Lit x) Int
| TApp : forall t1 t2 e1 e2,
           has_type e1 (Fun t1 t2) -> has_type e2 t1 -> has_type (App e1 e2) t2
| TLam : forall e t1 t2,
           has_type (open e (FVar t1)) t2 -> has_type (Lam t1 e) (Fun t1 t2).

Inductive stuck {A} : Exp A -> Prop :=
| SVar : forall t, stuck (FVar t)
| SApp : forall u e, stuck u -> stuck (App u e).

Inductive value {A} : Exp A -> Prop :=
| VStuck : forall e, stuck e -> value e
| VLit : forall x, value (Lit x)
| VLam : forall t e, value (Lam t e).

Inductive red {A} : Exp A -> Exp A -> Prop :=
| RApp1 : forall e1 e1' e2 , red e1 e1' -> red (App e1 e2) (App e1' e2)
| RApp2 : forall e e' v, red e e' -> value v -> red (App v e) (App v e')
| RBeta : forall t e1 e2, value e2 -> red (App (Lam t e1) e2) (open e1 e2).

Hint Constructors Typ Exp has_type red value stuck.

Lemma mopen_lemma : forall {e T U t t1},
  has_type (mopen e (T ++ FVar t1 :: U)) t -> 
  forall e1, has_type e1 t1 -> 
            has_type (mopen e (T ++ e1 :: U)) t.
Proof.
  induction e; simpl; intros; try auto.
  generalize n H. clear H n.
  induction T; simpl in *; intros; auto.
  destruct n; simpl in *; auto.
  inversion H; subst. auto.
  destruct n; simpl in *; auto.
  inversion H; subst. clear H.
  apply TLam. unfold open in *. simpl in *.
  rewrite <- app_assoc in *.
  rewrite <- app_comm_cons in *.
  apply IHe with (t1 := t1).
  auto. auto.
  rewrite app_assoc in *.
  apply IHe with (t1 := t1). auto. auto.
  inversion H; subst.
  apply TApp with (t1 := t0).
  apply IHe1 with (t1 := t1); auto.
  apply IHe2 with (t1 := t1); auto.
Defined.

Lemma subject_reduction : forall e e',
  red e e' -> forall t, has_type e t -> has_type e' t.
Proof.
  intros e e' r.
  induction r; intros.
  inversion H; subst.
  apply TApp with (t1 := t1).
  apply (IHr _ H2). auto.
  inversion H0; subst.
  apply TApp with (t1 := t1). auto.
  apply (IHr _ H5).
  inversion H0; subst. inversion H3; subst. clear H0 H3.
  unfold open in *.
  assert (forall (e : Exp Typ), e :: nil = nil ++ e :: nil).
  intros. simpl. auto. rewrite (H0 (FVar t1)) in H2.
  rewrite (H0 e2).
  apply (mopen_lemma H2 H5).
Defined.
  
Lemma progress_lem : forall (e : Exp Typ) t,
  has_type e t ->
  value e \/ exists e', red e e'.
Proof.
  intros. induction H; subst; auto.
  inversion H; subst.
  left; eauto.
  destruct IHhas_type1.
  inversion H3; subst.
  left; eauto.
  destruct H3 as [e' ?].
  right. exists (App e' e2); eauto.
  destruct IHhas_type2.
  right. exists (open e e2); eauto.
  destruct H1 as [e' ?].
  right. exists (App (Lam t1 e) e'); eauto.
Qed.
  
