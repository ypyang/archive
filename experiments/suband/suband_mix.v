Set Implicit Arguments.
Require Import LibLN.
Implicit Types x : var.

Inductive trm : Set :=
  | trm_bvar : nat -> trm
  | trm_fvar : var -> trm
  | trm_star : trm
  | trm_app  : trm -> trm -> trm
  | trm_abs  : trm -> trm -> trm
  | trm_prod : trm -> trm -> trm
  | trm_merge : trm -> trm -> trm
  | trm_and : trm -> trm -> trm.

Fixpoint open_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i     => If k = i then u else (trm_bvar i)
  | trm_fvar x     => trm_fvar x 
  | trm_star       => trm_star
  | trm_app e1 e2  => trm_app (open_rec k u e1) (open_rec k u e2)
  | trm_abs e1 e2  => trm_abs (open_rec k u e1) (open_rec (S k) u e2) 
  | trm_prod e1 e2 => trm_prod (open_rec k u e1) (open_rec (S k) u e2) 
  | trm_merge e1 e2 => trm_merge (open_rec k u e1) (open_rec k u e2) 
  | trm_and e1 e2  => trm_and (open_rec k u e1) (open_rec k u e2)
  end.

Definition open t u := open_rec 0 u t.

Notation "{ k ~> u } t" := (open_rec k u t) (at level 67).
Notation "t ^^ u" := (open t u) (at level 67).
Notation "t ^ x" := (open t (trm_fvar x)).

Inductive term : trm -> Prop :=
  | term_var : forall x, 
      term (trm_fvar x)
  | term_app : forall e1 e2,
      term e1 -> 
      term e2 -> 
      term (trm_app e1 e2)
  | term_star :
      term trm_star
  | term_abs : forall L e1 e2,
      term e1 ->
      (forall x, x \notin L -> term (e2 ^ x)) -> 
      term (trm_abs e1 e2)
  | term_prod : forall L e1 e2,
      term e1 ->
      (forall x, x \notin L -> term (e2 ^ x)) -> 
      term (trm_prod e1 e2)
  | term_merge : forall e1 e2,
      term e1 ->
      term e2 ->
      term (trm_merge e1 e2)
  | term_and : forall e1 e2,
      term e1 -> 
      term e2 -> 
      term (trm_and e1 e2).

Definition body t :=
  exists L, forall x, x \notin L -> term (t ^ x).

Definition env := LibEnv.env trm.

Inductive leaf : trm -> Prop :=
  | leaf_var : forall x, 
      leaf (trm_fvar x)
  | leaf_star :
      leaf trm_star.

Implicit Types G : env.

Inductive sub : env -> trm -> trm -> trm -> Prop :=
  | sub_star :
      sub empty trm_star trm_star trm_star
  | sub_var : forall G x A,
      sub G A A trm_star ->
      x # G ->
      sub (G & x ~ A) (trm_fvar x) (trm_fvar x) A
  | sub_weak : forall G x e A B,
      sub G e e B ->
      sub G A A trm_star ->
      leaf e ->
      x # G ->
      sub (G & x ~ A) e e B
  | sub_lam : forall L G e1 e2 A B,
      sub G A A trm_star ->
      (forall x, x \notin L ->
        sub (G & x ~ A) (B ^ x) (B ^ x) trm_star) -> 
      (forall x, x \notin L ->
        sub (G & x ~ A) (e1 ^ x) (e2 ^ x) (B ^ x)) ->
      sub G (trm_abs A e1) (trm_abs A e2) (trm_prod A B)
  | sub_app : forall B G e1 e2 A C,
      sub G A A B ->
      sub G e1 e2 (trm_prod B C) ->
      sub G (trm_app e1 A) (trm_app e2 A) (C ^^ A)
  | sub_pi : forall L G A B C D,
      sub G C A trm_star ->
      (forall x, x \notin L ->
        sub (G & x ~ C) (B ^ x) (D ^ x) trm_star) ->
      (forall x, x \notin L ->
        sub (G & x ~ A) (B ^ x) (B ^ x) trm_star) ->
      sub G (trm_prod A B) (trm_prod C D) trm_star
  | sub_and1 : forall G A B C D,
      sub G A C D ->
      sub G B B D ->
      sub G (trm_and A B) C D
  | sub_and2 : forall G A B C D,
      sub G B C D ->
      sub G A A D ->
      sub G (trm_and A B) C D
  | sub_and3 : forall G A B C D,
      sub G A B D ->
      sub G A C D ->
      sub G A (trm_and B C) D
  | sub_merge : forall G e1 e2 e3 e4 A B,
      sub G e1 e3 A ->
      sub G e2 e4 B ->
      sub G (trm_merge e1 e2) (trm_merge e3 e4) (trm_and A B)
  | sub_sub : forall A G e1 e2 B,
      sub G e1 e2 A ->
      sub G A B trm_star ->
      sub G e1 e2 B.

Inductive wf : env -> Prop :=
  | wf_nil : wf empty
  | wf_cons : forall G x A,
     x # G -> 
     sub G A A trm_star -> 
     wf (G & x ~ A).

Fixpoint fv (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i     => \{}
  | trm_fvar x     => \{x}
  | trm_star       => \{}
  | trm_app t1 t2  => (fv t1) \u (fv t2)
  | trm_abs t1 t2  => (fv t1) \u (fv t2)
  | trm_prod t1 t2 => (fv t1) \u (fv t2)
  | trm_merge t1 t2  => (fv t1) \u (fv t2)
  | trm_and t1 t2  => (fv t1) \u (fv t2)
  end.

Fixpoint subst (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i     => trm_bvar i 
  | trm_fvar x     => If x = z then u else (trm_fvar x)
  | trm_star     => trm_star
  | trm_app t1 t2  => trm_app (subst z u t1) (subst z u t2)
  | trm_abs t1 t2  => trm_abs (subst z u t1) (subst z u t2)
  | trm_prod t1 t2 => trm_prod (subst z u t1) (subst z u t2)
  | trm_merge t1 t2  => trm_merge (subst z u t1) (subst z u t2)
  | trm_and t1 t2  => trm_and (subst z u t1) (subst z u t2)
  end.

Notation "[ z ~> u ] t" := (subst z u t) (at level 68).

Fixpoint close_var_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i     => trm_bvar i 
  | trm_fvar x     => If x = z then (trm_bvar k) else (trm_fvar x)
  | trm_star       => trm_star
  | trm_app t1 t2  => trm_app (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_abs t1 t2  => trm_abs (close_var_rec k z t1) (close_var_rec (S k) z t2) 
  | trm_prod t1 t2 => trm_prod (close_var_rec k z t1) (close_var_rec (S k) z t2)
  | trm_merge t1 t2 => trm_merge (close_var_rec k z t1) (close_var_rec k z t2)                               
  | trm_and t1 t2  => trm_and (close_var_rec k z t1) (close_var_rec k z t2)
  end.

Definition close_var z t := close_var_rec 0 z t.

Definition contains_terms E :=
  forall x U, binds x U E -> term U.

Definition relation := trm -> trm -> Prop.

Definition simulated (R1 R2 : relation) := 
  forall (t t' : trm), R1 t t' -> R2 t t'.
 
Infix "simulated_by" := simulated (at level 69).


Definition red_regular (R : relation) :=
  forall t t', R t t' -> term t /\ term t'.

Definition red_refl (R : relation) :=
  forall t, term t -> R t t.

Definition red_in (R : relation) :=
  forall t x u u', term t -> R u u' ->
  R ([x ~> u]t) ([x ~> u']t).
  
Definition red_all (R : relation) :=
  forall x t t', R t t' -> 
  forall u u', R u u' -> 
  R ([x~>u]t) ([x~>u']t').

Definition red_out (R : relation) :=
  forall x u t t', term u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Definition red_rename (R : relation) :=
  forall x t t' y,
  R (t ^ x) (t' ^ x) -> 
  x \notin (fv t) -> x \notin (fv t') ->
  R (t ^ y) (t' ^ y).

Definition red_through (R : relation) :=
  forall x t1 t2 u1 u2, 
  x \notin (fv t1) -> x \notin (fv u1) ->
  R (t1 ^ x) (u1 ^ x) -> R t2 u2 ->
  R (t1 ^^ t2) (u1 ^^ u2).

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : trm => fv x) in
  let D := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D).

Ltac pick_fresh X :=
  let L := gather_vars in (pick_fresh_gen L X).

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

Ltac exists_fresh := 
  let L := gather_vars_with (fun x : vars => x) in exists L.

Hint Constructors leaf sub wf.
Hint Constructors term.

Lemma open_rec_term_core :forall e j v i u, i <> j ->
  {j ~> v}e = {i ~> u}({j ~> v}e) -> e = {i ~> u}e.
Proof.
  induction e; introv Neq Equ;
  simpl in *; inversion* Equ; f_equal*.  
  case_nat*. case_nat*.
Qed.

Lemma open_rec_term : forall t u,
  term t -> forall k, t = {k ~> u}t.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open. pick_fresh x.
   apply* (@open_rec_term_core e2 0 (trm_fvar x)).
  unfolds open. pick_fresh x. 
   apply* (@open_rec_term_core e2 0 (trm_fvar x)).
Qed.

Lemma subst_fresh : forall x t u, 
  x \notin fv t -> [x ~> u] t = t.
Proof.
  intros. induction t; simpls; fequals*.
  case_var*. 
Qed.

Lemma subst_open : forall x u t1 t2, term u -> 
  [x ~> u] (t1 ^^ t2) = ([x ~> u]t1) ^^ ([x ~> u]t2).
Proof.
  intros. unfold open. generalize 0.
  induction t1; intros; simpl; f_equal*.
  case_nat*. case_var*. apply* open_rec_term.
Qed.

Lemma subst_open_var : forall x y u e, y <> x -> term u ->
  ([x ~> u]e) ^ y = [x ~> u] (e ^ y).
Proof.
  introv Neq Wu. rewrite* subst_open.
  simpl. case_var*.
Qed.

Lemma subst_intro : forall x t u, 
  x \notin (fv t) -> term u ->
  t ^^ u = [x ~> u](t ^ x).
Proof.
  introv Fr Wu. rewrite* subst_open.
  rewrite* subst_fresh. simpl. case_var*.
Qed.

Ltac cross := 
  rewrite subst_open_var; try cross.

Tactic Notation "cross" "*" := 
  cross; autos*.

Lemma subst_term : forall t z u,
  term u -> term t -> term ([z ~> u]t).
Proof.
  induction 2; simple*.
  case_var*.
  apply_fresh* term_abs as y. rewrite* subst_open_var.
  apply_fresh* term_prod as y. rewrite* subst_open_var.
Qed.

Lemma open_term : forall t u,
  body t -> term u -> term (t ^^ u).
Proof.
  intros. destruct H. pick_fresh y.
  rewrite* (@subst_intro y). apply* subst_term.
Qed.

Hint Resolve subst_term open_term.

Lemma term_abs1 : forall t2 t1,
  term (trm_abs t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_abs2 : forall t1 t2,  
  term (trm_abs t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma term_prod1 : forall t2 t1,
  term (trm_prod t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_prod2 : forall t1 t2,  
  term (trm_prod t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Hint Extern 1 (term ?t) =>
match goal with
  | H: term (trm_abs t ?t2) |- _ => apply (@term_abs1 t2)
  | H: term (trm_prod t ?t2) |- _ => apply (@term_prod1 t2)
end.

Hint Extern 3 (body ?t) =>
  match goal with 
  | H: context [trm_abs ?t1 t] |- _ => apply (@body_abs2 t1)
  | H: context [trm_prod ?t1 t] |- _ => apply (@body_prod2 t1)
  end.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: context [t ^ _] |- _ =>
      let x := fresh in let Fr := fresh in
      let P := fresh in
      let L := gather_vars in exists L; intros x Fr; 
      lets P: H x __; [ notin_solve 
                      | try destructs P ]
  end.

Lemma term_abs_prove : forall t1 t2,
  term t1 -> body t2 -> term (trm_abs t1 t2).
Proof.
  intros. apply_fresh* term_abs as x.
Qed.

Lemma term_prod_prove : forall t1 t2,
  term t1 -> body t2 -> term (trm_prod t1 t2).
Proof.
  intros. apply_fresh* term_prod as x.
Qed.

Hint Resolve term_abs_prove term_prod_prove.

Lemma body_prove : forall L t,
  (forall x, x \notin L -> term (t ^ x)) -> body t.
Proof.
  intros. exists* L.
Qed.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: forall _, _ \notin ?L -> term (t ^ _)  |- _ =>
    apply (@body_prove L)
  end. 

Lemma body_subst : forall x t u,
  term u -> body t -> body ([x ~> u]t).
Proof.
  introv. intros Wu [L Bt].
  exists (\{x} \u L). intros y Fr. cross*.
Qed.

Hint Resolve body_subst.

Lemma open_var_inj : forall x t1 t2, 
  x \notin (fv t1) -> x \notin (fv t2) -> 
  (t1 ^ x = t2 ^ x) -> (t1 = t2).
Proof.
  intros x t1. unfold open. generalize 0.
  induction t1; intro k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal* 
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma close_var_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv t1) ->
    {i ~> trm_fvar y} ({j ~> trm_fvar z} (close_var_rec j x t1) )
  = {j ~> trm_fvar z} (close_var_rec j x ({i ~> trm_fvar y}t1) ).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ]. 
  case_var*. simpl. case_nat*. 
Qed.

Lemma close_var_fresh : forall x t,
  x \notin fv (close_var x t).
Proof.
  introv. unfold close_var. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Lemma close_var_body : forall x t,
  term t -> body (close_var x t).
Proof.
  introv W. exists \{x}. intros y Fr.
  unfold open, close_var. generalize 0. gen y.
  induction W; intros y Fr k; simpls.
  case_var; simple*. case_nat*.
  autos*.
  autos*.
  apply_fresh* term_abs as z.
   unfolds open. rewrite* close_var_rec_open.
  apply_fresh* term_prod as z.
   unfolds open. rewrite* close_var_rec_open.
  autos*.
  autos*.
Qed.

Lemma close_var_open : forall x t,
  term t -> t = (close_var x t) ^ x.
Proof.
  introv W. unfold close_var, open. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
Qed. 

Lemma close_var_spec : forall t x, term t -> 
  exists u, t = u ^ x /\ body u /\ x \notin (fv u).
Proof.
  intros. exists (close_var x t). splits 3.
  apply* close_var_open.
  apply* close_var_body.
  apply* close_var_fresh.
Qed. 

Hint Extern 1 (term (trm_abs ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: term (trm_abs t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_abs t1 t2)) end.

Lemma regular_sub : forall G e1 e2 A,
  sub G e1 e2 A ->
  (wf G /\ ok G /\ contains_terms G /\ term e1 /\ term e2 /\ term A). 
Proof.
  intros; inductions H; unfolds contains_terms; splits*.
  intros. false* binds_empty_inv.
  intros. destruct (binds_push_inv H1) as [[? ?]|[? ?]]; subst*.
  intros. destruct (binds_push_inv H3) as [[? ?]|[? ?]]; subst*.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: sub _ t _ _ |- _ => apply (proj31 (proj44 (regular_sub H)))
  | H: sub _ _ t _ |- _ => apply (proj32 (proj44 (regular_sub H)))
  | H: sub _ _ _ t |- _ => apply (proj33 (proj44 (regular_sub H)))
  end.

Lemma ok_from_wf : forall G, wf G -> ok G.
Proof.
  induction 1. auto. autos* (regular_sub H0).
Qed.

Hint Extern 1 (ok ?E) => match goal with
  | H: wf E |- _ => apply (ok_from_wf H)
  end.

Hint Extern 1 (wf ?E) => match goal with
  | H: sub E _ _ _ |- _ => apply (proj1 (regular_sub H))
  end.

Lemma wf_push_inv : forall G x A,
  wf (G & x ~ A) -> wf G /\ term A.
Proof.
  introv W. inversions W. 
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]]. subst~.
Qed.

Lemma term_from_binds_in_wf : forall x E U,
  wf E -> binds x U E -> term U.
Proof.
  introv W Has. gen E. induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv W). binds_push~ Has.
Qed.

Hint Extern 1 (term ?t) => match goal with
  H: binds ?x t ?E |- _ => apply (@term_from_binds_in_wf x E)
  end.

Lemma wf_left : forall E F : env,
  wf (E & F) -> wf E.
Proof.
  intros. induction F using env_ind.
  rewrite~ concat_empty_r in H.
  rewrite concat_assoc in H.
   inversions H. false (empty_push_inv H1).
   destruct (eq_push_inv H0) as [? [? ?]]. subst~. 
Qed.

Implicit Arguments wf_left [E F].

Lemma fv_open_var : forall y x t,
  x <> y -> x \notin fv (t ^ y) -> x \notin fv t.
Proof.
  introv Neq. unfold open. generalize 0. 
  induction t; simpl; intros; try notin_solve.
  specializes IHt1 n. auto. specializes IHt2 n. auto.
  specializes IHt1 n. auto. specializes IHt2 (S n). auto.
  specializes IHt1 n. auto. specializes IHt2 (S n). auto.
  specializes IHt1 n. auto. specializes IHt2 n. auto.
  specializes IHt1 n. auto. specializes IHt2 n. auto.
Qed.

Lemma fresh_false : forall G x A,
  x # G & x ~ A -> False.
Proof.
  intros. simpl_dom.
  destruct (notin_union x \{x} (dom G)) as [H1 _].
  lets: H1 H.
  destruct H0 as [H3 _].
  apply* notin_same.
Qed.

Lemma and_simpl : forall (A : Prop),
    A -> A /\ A.
Proof.
  intros. auto.
Qed.

Lemma sub_fresh : forall G A B x,
  sub G A B trm_star -> x # G -> (x \notin fv A) /\ (x \notin fv B).
Proof.
  introv Typ.
  induction Typ; simpls; intros; try (apply and_simpl).
  auto.
  rewrite notin_singleton. intro. subst. apply* (fresh_false H0).
  apply* IHTyp1.
  pick_fresh y. destruct (IHTyp H3) as [NIn _]. split.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H2 y). auto. auto. auto.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H2 y). auto. auto. auto.
  lets: (IHTyp1 H). lets: IHTyp2 H. split*.
  pick_fresh y. destruct IHTyp as [NIn1 NIn2]. auto. split.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H0 y). auto. auto. auto.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H0 y). auto. auto. auto.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
Qed.

Lemma notin_fv_from_wf : forall E F x V,
  wf (E & x ~ V & F) -> x \notin fv V.
Proof.
  intros.
  lets W: (wf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. subst~.
  apply* sub_fresh.
Qed.

Lemma notin_fv_from_binds : forall x y U E,
  wf E -> binds y U E -> x # E -> x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv H). binds_push H0.
    inversions H. false* (empty_push_inv H5).
     destruct (eq_push_inv H4) as [? [? ?]]. subst~. 
     apply* sub_fresh.
    autos*.
Qed.

Lemma notin_fv_from_binds' : forall E F x V y U,
  wf (E & x ~ V & F) -> binds y U E -> x \notin fv U.
Proof.
  intros. lets W: (wf_left H). inversions keep W.
  false (empty_push_inv H2). 
  destruct (eq_push_inv H1) as [? [? ?]]. subst~. 
  lets W': (wf_left W). apply* notin_fv_from_binds.
Qed.

Hint Extern 1 (?x \notin fv ?V) => 
  match goal with H: wf (?E & x ~ V & ?F) |- _ =>
    apply (@notin_fv_from_wf E F) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: wf (?E & x ~ ?V & ?F), B: binds ?y U ?E |- _ =>
    apply (@notin_fv_from_binds' E F x V y) end.

Lemma fv_open_term : forall A B x,
  x \notin fv A -> x \notin fv B ->
  x \notin fv (A ^^ B).
Proof.
  intros A. unfold open. generalize 0. 
  induction A; simpl; intros; try (notin_simpl).
  case_nat~. auto. auto.
  specializes IHA1 n. auto. specializes IHA2 n. auto.
  specializes IHA2 (S n). auto.
  specializes IHA1 n. auto. specializes IHA2 (S n). auto. 
  specializes IHA1 n. auto. specializes IHA2 n. auto.
  specializes IHA1 n. auto. specializes IHA2 n. auto.
Qed.

Lemma sub_fresh_all : forall C G A B x,
  sub G A B C -> x # G -> x \notin fv A /\ x \notin fv B /\ x \notin fv C.
Proof.
  introv Typ.
  induction Typ; simpls; intros; try (apply and_simpl).
  auto.
  splits*.
    rewrite notin_singleton. intro. subst. apply* (fresh_false H0).
    rewrite notin_singleton. intro. subst. apply* (fresh_false H0).
    asserts* W: (x # G). apply* sub_fresh.
  apply* IHTyp1.
  pick_fresh y. destruct (IHTyp H3) as [NIn _]. splits.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H2 y). auto. auto. auto.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H2 y). auto. auto. destruct H5. auto.
    notin_simpl. auto. apply* (@fv_open_var y).
    destruct (H2 y). auto. auto. destruct H5. auto.
  splits*. apply* fv_open_term.
  lets: (IHTyp H3). split*.
    destruct H4 as [? [? _]].
    notin_simpl. auto. pick_fresh y.
    apply* (@fv_open_var y). destruct (H0 y). auto. auto. auto.
    destruct H4 as [? [? _]]. splits*.
    notin_simpl. auto. pick_fresh y.
    apply* (@fv_open_var y). destruct (H0 y). auto. auto. jauto.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
  lets: (IHTyp1 H). lets: (IHTyp2 H). split*.
Qed.  

Lemma sub_star_alt : forall G,
  wf G -> sub G trm_star trm_star trm_star.
Proof.
  intros. induction G using env_ind.
  apply* sub_star.
  inversions H. false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as (? & ? & ?). subst.
  apply* sub_weak.
Qed.

Lemma sub_var_alt : forall G x A,
  wf G -> binds x A G -> sub G (trm_fvar x) (trm_fvar x) A.
Proof.
  intros. induction G using env_ind.
  false. apply* binds_empty_inv.
  inversions H. false. apply* empty_push_inv.
  destruct (binds_push_inv H0) as [[? ?] | [? ?]].
  subst. destruct (eq_push_inv H1) as (? & ? & ?).
  subst. apply* sub_var.
  destruct (eq_push_inv H1) as (? & ? & ?). subst.
  apply* sub_weak.
Qed.

Lemma sub_var_empty_inv : forall x A,
  sub empty (trm_fvar x) (trm_fvar x) A -> False.
Proof.
  intros. inductions H; try solve [apply* empty_push_inv].
  apply* IHsub1.
Qed.

Lemma wf_to_wt : forall G x A,
  wf (G & x ~ A) -> sub G A A trm_star.
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]; subst*.
Qed.

Hint Extern 1 (sub ?G ?A ?A trm_star) => match goal with
  | H: wf (G & _ ~ A) |- _ => apply (wf_to_wt H)
  end.


Lemma sub_refl : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G e1 e1 A /\ sub G e2 e2 A.
Proof.
  intros. inductions H; try solve [split; autos*].
  split.
    apply_fresh* (@sub_lam) as y. destruct (H3 y). auto. auto.
    apply_fresh* (@sub_lam) as y. destruct (H3 y). auto. auto.
  split.
    apply_fresh* (@sub_pi) as y. 
    apply_fresh* (@sub_pi) as y. destruct (H1 y). auto. auto.
    destruct (H1 y). auto. auto.
Qed.

Hint Extern 1 (sub ?G ?A ?A ?B) => match goal with
  | H: sub G A _ B |- _ => apply (proj1 (sub_refl H))
  | H: sub G _ A B |- _ => apply (proj2 (sub_refl H))
  end.

Lemma sub_weaken : forall G E F t1 t2 T,
  sub (E & G) t1 t2 T ->
  wf (E & F & G) ->
  sub (E & F & G) t1 t2 T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: star *)
  apply* sub_star_alt.
  (* case: var *)
  apply* sub_var_alt. apply* binds_weaken. rewrite <- EQ. apply* binds_tail.
  (* case: weak *)
  destruct (env_case G0); subst.
    rewrite concat_empty_r. rewrite concat_empty_r in EQ, W.
    destruct (env_case E); subst. false. apply* empty_push_inv.
    destruct H1 as (y & v & E' & EQ).
    destruct (eq_push_inv EQ) as (? & ? & ?); subst.
    assert (sub (E' & (y ~ v & F) & empty) e e B).
    apply* (@IHTyp1 E' (y ~ v & F) empty). rewrite~ concat_empty_r.
    rewrite concat_empty_r. rewrite~ concat_assoc.
    rewrite concat_empty_r in H1. rewrite~ concat_assoc in H1.
    destruct H1 as (y & v & G0' & EQ2).
    subst. rewrite concat_assoc in EQ, W. rewrite concat_assoc.
    destruct (eq_push_inv EQ) as (? & ? & ?); subst.
    assert (wf (E & F & G0')). apply* wf_left.
    assert (ok (E & F & G0' & y ~ v)) by auto.
    destruct (ok_push_inv H2) as [_ ?].
    apply* sub_weak. 
  (* case: trm_abs *)
  apply_fresh* (@sub_lam) as y.
  apply_ih_bind* H0.  apply_ih_bind* H2.
  (* case: trm_prod *)
  lets: IHTyp E F G0 W. auto.
  apply_fresh* (@sub_pi) as y. 
  apply_ih_bind* H0. apply_ih_bind* H2.
Qed.

Lemma sub_narrowing : forall F B x E A C e1 e2,
  sub (E & x ~ B & F) e1 e2 C ->
  sub E A B trm_star ->
  wf (E & x ~ A & F) ->
  sub (E & x ~ A & F) e1 e2 C.
Proof.
  intros. gen_eq G: (E & x ~ B & F). gen E F.
  induction H; intros; subst; eauto.
  (* case star *)
  false. apply* empty_middle_inv.
  (* case var *)
  destruct (env_case F); subst.
  rewrite concat_empty_r in EQG, H2.
  rewrite concat_empty_r.
  destruct (eq_push_inv EQG) as [? [? ?]]; subst.
  apply* (@sub_sub A). apply_empty* sub_weaken.
  destruct H3 as [y [v [F' Eq]]]; subst.
  rewrite concat_assoc.
  rewrite concat_assoc in H2, EQG.
  destruct (eq_push_inv EQG) as [? [? ?]]; subst.
  apply* sub_var_alt.
  (* case weak *)
  destruct (env_case F); subst.
    rewrite concat_empty_r in H4, EQG.
    rewrite concat_empty_r.
    destruct (eq_push_inv EQG) as [? [? ?]]; subst.
    apply_empty* sub_weaken.
    destruct H5 as [y [v [F' Eq]]]; subst.
     rewrite concat_assoc.
     rewrite concat_assoc in H4, EQG.
     destruct (eq_push_inv EQG) as [? [? ?]]; subst.
     assert (sub (E & x ~ A & F') e e B0).
     apply* IHsub1. apply* wf_left.
     apply_empty* sub_weaken.
  (* case lam *)
  apply_fresh* (@sub_lam) as y. 
  apply_ih_bind* H1. apply_ih_bind* H3.
  (* case pi *)
  lets: (IHsub _ H4 _ H5 eq_refl).
  apply_fresh* (@sub_pi) as y.
  apply_ih_bind* H1. apply_ih_bind* H3.
Qed.

Lemma sub_star_any_comm : forall G A B,
  sub G trm_star A B ->
  sub G A trm_star B.
Proof.
  intros. inductions H; autos*.
Qed.

Hint Extern 1 (sub ?G ?A trm_star ?B) => match goal with
  | H: sub G trm_star A B |- _ => apply (sub_star_any_comm H)
  end.

Lemma sub_prod_prod_inv : forall G A B B0 C,
   sub G (trm_prod A B) (trm_prod A B0) C ->
   (exists L, forall x, x \notin L ->
        sub (G & x ~ A) (B ^ x) (B0 ^ x) trm_star).
Proof.
  intros. inductions H; eauto.
  destruct (IHsub1 _ _ _ eq_refl eq_refl) as [L' T].
  exists (L' \u dom G \u \{x}). intros. 
  asserts* W: (x0 \notin L').
  lets: T x0 W.
  asserts* W2: (wf (G & x0 ~ A)).
  apply* sub_weaken.
  apply* wf_cons.
  apply_empty* sub_weaken.
Qed.

Lemma sub_prod_star_false : forall G A B C,
  sub G (trm_prod A B) trm_star C -> False.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_star_prod_false : forall G A B C,
  sub G trm_star (trm_prod A B) C -> False.
Proof.
  intros. inductions H; eauto.
Qed.
  
Lemma sub_star_and_left : forall G A B C,
  sub G trm_star (trm_and A B) C ->
  sub G trm_star A C.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_star_and_right : forall G A B C,
  sub G trm_star (trm_and A B) C ->
  sub G trm_star B C.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_substar_prod_false : forall G A B C D,
  sub G trm_star D trm_star ->
  sub G D (trm_prod A B) C ->
  False.
Proof.
  intros. inductions H0; eauto.
  inversions H1. apply* sub_prod_star_false.
  inversions H; eauto. 
  apply* IHsub1. apply* sub_star_and_left.
  apply* IHsub1. apply* sub_star_and_right.
Qed.

Lemma sub_and_star_inv : forall G A B C,
  sub G (trm_and A B) trm_star C ->
  sub G A trm_star C \/ sub G B trm_star C.
Proof.
  intros. inductions H; eauto.
  destruct (IHsub1 _ _ eq_refl eq_refl). left. apply* (@sub_sub A0).
  right. apply* (@sub_sub B0).
Qed.

Lemma sub_prod_supstar_false : forall G A B C D,
  sub G D trm_star trm_star ->
  sub G (trm_prod A B) D C ->
  False.
Proof.
  intros. inductions H0; eauto.
  inversions H1. apply* sub_prod_star_false.
  destruct (sub_and_star_inv H); autos*.
Qed.

Lemma middle_eq_inv : forall (G:env) (E:env) (F:env) x A V,
  G & x ~ A = E & x ~ V & F -> ok (E & x ~ V & F) ->
  G = E /\ A = V /\ F = empty.
Proof.
  intros. destruct (env_case F).
  subst. rewrite concat_empty_r in H.
  destruct~ (eq_push_inv H). splits*.
  destruct H1 as [y [T [F' Eq]]].
  subst. rewrite concat_assoc in H.
  destruct~ (eq_push_inv H).
  subst. lets: (ok_middle_inv_r H0).
  false. apply fresh_false with (G := F') (x := y) (A := T). auto.
Qed.

Lemma sub_substitution : forall V (F:env) v (E:env) x t1 t2 T,
  sub E v v V ->
  sub (E & x ~ V & F) t1 t2 T ->
  sub (E & (map (subst x v) F)) (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  inductions Typt; intros; simpls subst.
  false. apply* empty_middle_inv.
  case_var. 
    assert (wf (G & x ~ A)). apply* wf_cons.
    assert (binds x A (G & x ~ A)). apply* binds_tail.
    rename H1 into H2.
    rename H0 into H1. rename EQG into H0.
    rewrite H0 in H1. rewrite H0 in H2.
    binds_mid~.
    rewrite* subst_fresh. apply_empty* sub_weaken.
    destruct~ (middle_eq_inv H0) as [Eq1 [Eq2 Eq3]].
    subst. rewrite map_empty. rewrite concat_empty_r.
    rewrite concat_empty_r in H1.
    apply (wf_left H1).
    destruct (env_case F). subst. 
    rename EQG into H0.
    rewrite concat_empty_r in H0.
    destruct (eq_push_inv H0). false.
    rename H0 into H1. rename EQG into H0.
    destruct H1 as [y [T [F' Eq]]]. subst.
    rewrite concat_assoc in H0. destruct (eq_push_inv H0) as [Eq1 [Eq2 Eq3]].
    lets: (IHTypt F' Eq3).
    asserts* W: (wf (E & map (subst x v) F')). subst.
    apply* sub_var_alt. rewrite map_push. rewrite concat_assoc.
    apply* wf_cons.
  asserts* W : (wf G).
  lets: (sub_fresh_all Typt1 H0).
  asserts* NIn: (x0 \notin fv B).
  tests EQ: (x = x0).
    destructs H1. clear H1.
    rename EQG into H1.
    destruct~ (middle_eq_inv H1) as [Eq1 [Eq2 Eq3]].
    rewrite <- H1. apply* ok_push. subst.
    rewrite* subst_fresh.
    rewrite* subst_fresh.
    rewrite map_empty. rewrite concat_empty_r. auto.
    destruct (env_case F). subst. 
    destructs H1. clear H1.
    rename EQG into H1.
    rewrite concat_empty_r in H1.
    destruct (eq_push_inv H1). false.
    destruct H2 as [y [T [F' Eq]]]. subst.
    rewrite concat_assoc in EQG. destruct (eq_push_inv EQG) as [Eq1 [Eq2 Eq3]].
    lets Typ: (IHTypt1 F' Eq3).
    asserts* W2: (wf (E & map (subst x v) F')). subst.
    rewrite map_push. rewrite concat_assoc.
    apply_empty* sub_weaken.
  apply_fresh* (@sub_lam) as y.
   cross; auto. apply_ih_map_bind* H0. 
   rewrite EQG. rewrite* concat_assoc.
   cross; auto. apply_ih_map_bind* H2. 
   rewrite EQG. rewrite* concat_assoc.
  rewrite* subst_open.
  apply_fresh* (@sub_pi) as y.
   cross; auto. apply_ih_map_bind* H0. 
   rewrite EQG. rewrite* concat_assoc.
   cross; auto. apply_ih_map_bind* H2. 
   rewrite EQG. rewrite* concat_assoc.
  apply* (@sub_and1).
  apply* (@sub_and2).
  apply* (@sub_and3).
  apply* (@sub_merge).
  apply* (@sub_sub).
Qed.

Lemma sub_wf_from_sub : forall E t1 t2 T,
  sub E t1 t2 T ->
  sub E T T trm_star.
Proof.
  induction 1.
  auto.
  apply_empty* sub_weaken.
  apply_empty* sub_weaken.
  apply* sub_pi.
  destruct (sub_prod_prod_inv IHsub2) as [L T'].
  pick_fresh x. rewrite~ (@subst_intro x).
  unsimpl ([x ~> A](trm_star)).
  apply_empty* (@sub_substitution B).
  auto. auto. auto. auto.
  apply* sub_and3.
  auto.
Qed.

Lemma sub_mono : forall G A A' B C,
  sub G A B C -> sub G A' A' C ->
  sub G (trm_and A A') B C /\ sub G (trm_and A' A) B C.
Proof.
  intros. inductions H; splits*.
Qed.

Lemma sub_mono_left : forall G A A' B C,
  sub G A B C -> sub G A' A' C ->
  sub G (trm_and A A') B C.
Proof.
  intros. lets: (sub_mono H H0).
  auto.
Qed.

Lemma sub_mono_right : forall G A A' B C,
  sub G A B C -> sub G A' A' C ->
  sub G (trm_and A' A) B C.
Proof.
  intros. lets: (sub_mono H H0).
  auto.
Qed.

Lemma sub_and_and_inv : forall G A B T,
  sub G (trm_and A B) (trm_and A B) T ->
  sub G A A T /\ sub G B B T.
Proof.
  intros. gen_eq Q: (trm_and A B). gen A B.
  induction H; intros; subst; tryfalse.
  inversions H1.  apply* IHsub1.
  apply* IHsub1.
  inversions EQQ. auto.
  destruct~ (IHsub1 _ _ eq_refl).
  splits*.
Qed.

Lemma sub_type_keep : forall G e1 e2 A B,
  sub G e1 e1 A ->
  sub G e1 e2 B ->
  sub G e1 e2 A.
Proof.
  intros. gen A.
  inductions H0; intros; auto.
  inductions H4. inversions H4.
  apply_fresh* sub_lam as y.
  apply* (@sub_sub A0).
  inductions H. inversions H1.
  apply sub_app with (B := B0). auto. apply* IHsub2.
  apply* (@sub_sub A0).
  inductions H4. inversions H4.
  apply_fresh* sub_pi as y. apply* (@sub_sub A0).
  destruct~ (sub_and_and_inv H) as [P Q].
  destruct~ (sub_and_and_inv H) as [P Q].
  inductions H. inversions H1.
  apply* sub_merge. apply* (@sub_sub A0).
Qed.

Lemma sub_trans_aux : forall e2 G e1 e3 A B,
  sub G e1 e2 A -> sub G e2 e3 B -> sub G e1 e3 A.
Proof.
  introv S1. asserts* W: (term e2).
  gen G A e1 B e3. set_eq e2' eq: e2. gen e2' eq.
  induction W; intros e2' eq G A e1' S1;
    induction S1; try discriminate; inversions eq;
      intros B' e3' S2; try solve [eauto 3 using sub_type_keep].
  (* case: app *)
  inductions S2. inversions H.
  apply sub_app with (B := B). auto. apply* sub_type_keep.
  apply* sub_and3. apply* IHS2_1.
  (* case lam *)
  inductions S2. inversions H5.
  apply_fresh* sub_lam as y. apply* sub_and3.
  apply* IHS2_1.
  (* case pi *)
  inductions S2. inversions H5. clear IHS2 H8 H5 H4 IHS1 H2.
  apply_fresh sub_pi as y. apply* sub_type_keep.
  asserts* NIn: (y \notin L).
  lets: (H0 y NIn _ eq_refl). apply H2 with (B := trm_star).
  rewrite <- (concat_empty_r (G & y ~ C)).
  apply sub_narrowing with (B := e1).
  rewrite concat_empty_r.
  apply* H1.
  auto. rewrite* concat_empty_r.
  apply* H6.
  apply* H3.
  apply* sub_and3.
  apply* IHS2_1.
  (* case merge *)
  inductions S2. inversions H. apply* sub_and3.
  apply* sub_merge. apply* IHS2_1.
  (* case and *)
  inductions S2. inversions H.
  apply* IHW1.
  apply* IHW2.
  apply* sub_and3.
  apply* IHS2_1.
Qed.

Lemma sub_trans : forall e2 G e1 e3 A,
  sub G e1 e2 A -> sub G e2 e3 A -> sub G e1 e3 A.
Proof.
  intros. apply* sub_trans_aux.
Qed.

