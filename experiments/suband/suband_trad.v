Set Implicit Arguments.
Require Import LibLN suband_mix.
Implicit Types x : var.
Implicit Types G : env.

Inductive _typing : env -> trm -> trm -> Prop :=
  | _typing_star : forall G,
      _wf G -> _typing G trm_star trm_star
  | _typing_var : forall G x A,
      _wf G -> binds x A G -> _typing G (trm_fvar x) A
  | _typing_lam : forall L G e A B,
      _typing G A trm_star ->
      (forall x, x \notin L ->
                 _typing (G & x ~ A) (B ^ x) trm_star) ->
      (forall x, x \notin L ->
                 _typing (G & x ~ A) (e ^ x) (B ^ x)) ->
      _typing G (trm_abs A e) (trm_prod A B)
  | _typing_app : forall G e A B C,
      _typing G A B ->
      _typing G e (trm_prod B C) ->
      _typing G (trm_app e A) (C ^^ A)
  | _typing_pi : forall L G A B,
      _typing G A trm_star ->
      (forall x, x \notin L ->
        _typing (G & x ~ A) (B ^ x) trm_star) ->
      _typing G (trm_prod A B) trm_star
  | _typing_and : forall G A B C,
      _typing G A C ->
      _typing G B C ->
      _typing G (trm_and A B) C
  | _typing_merge : forall G e1 e2 A B,
      _typing G e1 A ->
      _typing G e2 B ->
      _typing G (trm_merge e1 e2) (trm_and A B)
  | _typing_sub : forall A G e B,
      _typing G e A ->
      _sub G A B ->
      _typing G e B

with _wf : env -> Prop :=
  | _wf_nil : _wf empty
  | _wf_cons : forall G x A,
      x # G -> _typing G A trm_star ->
      _wf (G & x ~ A)

with _sub : env -> trm -> trm -> Prop :=
  | _sub_star : forall G, _wf G -> _sub G trm_star trm_star
  | _sub_var : forall G x A,
      _wf G -> binds x A G -> _sub G (trm_fvar x) (trm_fvar x)
  | _sub_lam : forall L G e1 e2 A,
      _typing G A trm_star ->
      (forall x, x \notin L -> _sub (G & x ~ A) (e1 ^ x) (e2 ^ x)) ->
      _sub G (trm_abs A e1) (trm_abs A e2)
  | _sub_app : forall G e1 e2 A,
      term A ->
      _sub G e1 e2 -> _sub G (trm_app e1 A) (trm_app e2 A)
  | _sub_pi : forall L G A B C D,
      _typing G C trm_star ->
      _sub G C A ->
      (forall x, x \notin L -> _sub (G & x ~ C) (B ^ x) (D ^ x)) ->
      _sub G (trm_prod A B) (trm_prod C D)
  | _sub_and1 : forall G A B C,
      term B ->
      _sub G A C -> _sub G (trm_and A B) C
  | _sub_and2 : forall G A B C,
      term A ->
      _sub G B C -> _sub G (trm_and A B) C
  | _sub_and3 : forall G A B C,
      _sub G A B -> _sub G A C -> _sub G A (trm_and B C)
  | _sub_merge : forall G e1 e2 e3 e4,
      _sub G e1 e3 -> _sub G e2 e4 -> _sub G (trm_merge e1 e2) (trm_merge e3 e4).

Hint Constructors _typing _sub _wf.

Scheme typing_induct := Induction for _typing Sort Prop
  with twf_induct := Induction for _wf Sort Prop
  with tsub_induct := Induction for _sub Sort Prop.

Lemma regular_typing : forall E t T, _typing E t T ->
  (_wf E /\ ok E /\ contains_terms E /\ term t /\ term T). 
Proof.
  apply typing_induct with
  (P0 := fun E (_ : _wf E) => ok E /\ contains_terms E)
  (P1 := fun E t1 t2 (_: _sub E t1 t2) => _wf E /\ ok E /\ contains_terms E /\ term t2); 
   unfolds contains_terms; intros; splits*.
  intros. destruct (binds_push_inv H0) as [[? ?]|[? ?]]; subst*.
Qed.

Lemma regular_tsub : forall E t T, _sub E t T ->
  (_wf E /\ ok E /\ contains_terms E /\ term t /\ term T). 
Proof.
  apply tsub_induct with
  (P0 := fun E (_ : _wf E) => ok E /\ contains_terms E)
  (P := fun E t1 t2 (_: _typing E t1 t2) => _wf E /\ ok E /\ contains_terms E /\ term t1 /\term t2); 
   unfolds contains_terms; intros; splits*.
  intros. destruct (binds_push_inv H0) as [[? ?]|[? ?]]; subst*.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: _typing _ t _ |- _ => apply (proj32 (proj33 (regular_typing H)))
  | H: _typing _ _ t |- _ => apply (proj33 (proj33 (regular_typing H)))
  | H: _sub _ t _ |- _ => apply (proj32 (proj33 (regular_tsub H)))
  | H: _sub _ _ t |- _ => apply (proj33 (proj33 (regular_tsub H)))
  end.

Lemma ok_from_twf : forall E, _wf E -> ok E.
Proof.
  induction 1. auto. autos* (regular_typing H0).
Qed.

Hint Extern 1 (ok ?E) => match goal with
  | H: _wf E |- _ => apply (ok_from_twf H)
  end.

Hint Extern 1 (_wf ?E) => match goal with
  | H: _typing E _ _ |- _ => apply (proj1 (regular_typing H))
  | H: _sub E _ _ |- _ => apply (proj1 (regular_tsub H))
  end.

Lemma twf_push_inv : forall E x U,
  _wf (E & x ~ U) -> _wf E /\ term U.
Proof.
  introv W. inversions W. 
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]]. subst~.
Qed.

Lemma term_from_binds_in_twf : forall x E U,
  _wf E -> binds x U E -> term U.
Proof.
  introv W Has. gen E. induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (twf_push_inv W). binds_push~ Has.
Qed.

Hint Extern 1 (term ?t) => match goal with
  H: binds ?x t ?E |- _ => apply (@term_from_binds_in_twf x E)
  end.

Lemma twf_left : forall E F : env,
  _wf (E & F) -> _wf E.
Proof.
  intros. induction F using env_ind.
  rewrite~ concat_empty_r in H.
  rewrite concat_assoc in H.
   inversions H. false (empty_push_inv H1).
   destruct (eq_push_inv H0) as [? [? ?]]. subst~. 
Qed.

Implicit Arguments twf_left [E F].

Lemma typing_fresh : forall E T x,
  _typing E T trm_star -> x # E -> x \notin fv T.
Proof.
  introv Typ.
  induction Typ; simpls; intros.
  auto.
  rewrite notin_singleton. intro. subst. applys binds_fresh_inv H0 H1.
  pick_fresh y. notin_simpl. auto. apply* (@fv_open_var y).
  lets: (IHTyp1 H). lets: (IHTyp2 H). auto.
  pick_fresh y. lets: (IHTyp H1). notin_simpl. auto. apply* (@fv_open_var y).
  lets: (IHTyp1 H). lets: (IHTyp2 H). auto.
  lets: (IHTyp1 H). lets: (IHTyp2 H). auto.
  lets: (IHTyp H0). auto.
Qed.

Lemma notin_fv_from_twf : forall E F x V,
  _wf (E & x ~ V & F) -> x \notin fv V.
Proof.
  intros.
  lets W: (twf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. subst~.
  apply* typing_fresh.
Qed.

Lemma notin_fv_from_tbinds : forall x y U E,
  _wf E -> binds y U E -> x # E -> x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (twf_push_inv H). binds_push H0.
    inversions H. false* (empty_push_inv H5).
     destruct (eq_push_inv H4) as [? [? ?]]. subst~. 
     apply* typing_fresh.
    autos*.
Qed.

Lemma notin_fv_from_tbinds' : forall E F x V y U,
  _wf (E & x ~ V & F) -> binds y U E -> x \notin fv U.
Proof.
  intros. lets W: (twf_left H). inversions keep W.
  false (empty_push_inv H2). 
  destruct (eq_push_inv H1) as [? [? ?]]. subst~. 
  lets W': (twf_left W). apply* notin_fv_from_tbinds.
Qed.

Hint Extern 1 (?x \notin fv ?V) => 
  match goal with H: _wf (?E & x ~ V & ?F) |- _ =>
    apply (@notin_fv_from_twf E F) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: _wf (?E & x ~ ?V & ?F), B: binds ?y U ?E |- _ =>
    apply (@notin_fv_from_tbinds' E F x V y) end.

Lemma tsub_weaken : forall G E F t T,
  _sub (E & G) t T ->
  _wf (E & F & G) ->
  _sub (E & F & G) t T
with typing_weaken : forall G E F t T,
  _typing (E & G) t T ->
  _wf (E & F & G) -> 
  _typing (E & F & G) t T.
Proof.
  clear tsub_weaken.
  introv Typ. gen_eq Env: (E & G). gen G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var *)
  apply* _sub_var. apply* binds_weaken.
  (* case: trm_abs *)
  apply_fresh* (@_sub_lam) as y.
  apply_ih_bind* H1.
  (* case: trm_prod *)
  apply_fresh* (@_sub_pi) as y. apply_ih_bind* H1.

  clear typing_weaken.
  introv Typ. gen_eq Env: (E & G). gen G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var *)
  apply* _typing_var. apply* binds_weaken.
  (* case: trm_abs *)
  apply_fresh* (@_typing_lam) as y.
  forwards TypP: (IHTyp G0); auto.
  apply_ih_bind* H0. apply_ih_bind* H2.
  (* case: trm_prod *)
  apply_fresh* (@_typing_pi) as y. apply_ih_bind* H0.
Qed.

Lemma tsub_mono : forall G A A' B,
  _sub G A B -> term A' ->
  _sub G (trm_and A A') B /\ _sub G (trm_and A' A) B.
Proof.
  intros. inductions H; splits*.
Qed.

Lemma tsub_mono_left : forall G A' A B,
  _sub G A B -> term A' ->
  _sub G (trm_and A A') B.
Proof.
  intros. destruct (tsub_mono H H0). auto.
Qed.

Lemma tsub_mono_right : forall G A' A B,
  _sub G A B -> term A' ->
  _sub G (trm_and A' A) B.
Proof.
  intros. destruct (tsub_mono H H0). auto.
Qed.

Lemma tsub_refl : forall G t T,
  _typing G t T -> _sub G t t.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma typing_narrowing : forall F x E A B C e,
  _typing (E & x ~ B & F) e C ->
  _sub E A B ->
  _wf (E & x ~ A & F) ->
  _typing (E & x ~ A & F) e C
with tsub_narrowing : forall F x E A B e1 e2,
  _sub (E & x ~ B & F) e1 e2 ->
  _sub E A B ->
  _wf (E & x ~ A & F) ->
  _sub (E & x ~ A & F) e1 e2.
Proof.
  clear typing_narrowing.
  intros. gen_eq G: (E & x ~ B & F). gen E F.
  induction H; intros; subst; eauto.
  (* case var *)
  asserts W: (_wf (E & x ~ A)). apply* twf_left.
  destruct (binds_concat_inv H0).
  apply* _typing_var.
  destruct H3.
  destruct (binds_push_inv H4).
  inversions H5. apply* (@_typing_sub A).
  apply_empty* tsub_weaken. apply_empty* tsub_weaken.
  apply_empty* typing_weaken.
  (* case lam *)
  apply_fresh* (@_typing_lam) as y. 
  apply_ih_bind* H1. apply_ih_bind* H3.
  (* case pi *)
  apply_fresh* (@_typing_pi) as y.
  apply_ih_bind* H1.
  
  clear tsub_narrowing.
  intros. gen_eq G: (E & x ~ B & F). gen E F.
  induction H; intros; subst; eauto.
  (* case var *)
  asserts W1: (_wf (E & x ~ A)). apply* twf_left.
  destruct (binds_concat_inv H0).
  apply* _sub_var.
  destruct H3.
  destruct (binds_push_inv H4).
  inversions H5. apply_empty* tsub_weaken.
  apply_empty* tsub_weaken.
  (* case lam *)
  apply_fresh* (@_sub_lam) as y. 
  apply_ih_bind* H1.
  (* case pi *)
  apply_fresh* (@_sub_pi) as y.
  apply_ih_bind* H2.
Qed.

Lemma tsub_trans : forall G B A C,
  _sub G A B -> _sub G B C -> _sub G A C.
Proof.
  introv S1. asserts* W: (term B).
  gen G A C. set_eq B' eq: B. gen B' eq.
  induction W; intros B' eq G A S1;
    induction S1; try discriminate; inversions eq;
      intros C' S2; inversions keep S2; 
        try solve [eauto 2 |
                   apply* tsub_mono_left |
                   apply* tsub_mono_right].
  apply* _sub_app.
  apply _sub_and3.
    gen C. inductions H0; intros.
    apply* _sub_app. apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
    gen B. inductions H1; intros.
    apply* _sub_app. apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
  apply_fresh* (@_sub_lam) as y.
  apply _sub_and3.
    gen C. inductions H4; intros.
    apply_fresh* _sub_lam as y. apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
    gen B. inductions H5; intros.
    apply_fresh* _sub_lam as y. apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
  apply_fresh (@_sub_pi) as y. auto. apply* IHW.
  apply* H0. apply_empty* tsub_narrowing.
  apply _sub_and3.
    gen C. inductions H4; intros.
    apply_fresh _sub_pi as y. auto. apply* IHW.
    apply* H0. apply_empty* tsub_narrowing.
    apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
    gen B. inductions H5; intros.
    apply_fresh _sub_pi as y. auto. apply* IHW.
    apply* H0. apply_empty* tsub_narrowing.
    apply _sub_and3.
    apply* IH_sub1. apply* IH_sub2.
  apply _sub_and3.
    gen C. inductions H; intros.
    apply _sub_and3. apply* IH_sub1. apply* IH_sub2.
    apply* _sub_merge.
    gen B. inductions H0; intros.
    apply _sub_and3. apply* IH_sub1. apply* IH_sub2.
    apply* _sub_merge.
  apply* _sub_merge.
  apply _sub_and3.
  gen_eq P: (trm_and e1 e2). gen C.
  induction H; intros; tryfalse.
  inversions H2. apply* (IHW1 e1).
  inversions H2. apply* (IHW2 e2).
  apply _sub_and3. apply* IH_sub1. apply* IH_sub2.
  gen_eq P: (trm_and e1 e2). gen B.
  induction H0; intros; tryfalse.
  inversions H2. apply* (IHW1 e1).
  inversions H2. apply* (IHW2 e2).
  apply _sub_and3. apply* IH_sub1. apply* IH_sub2.
Qed.

Hint Resolve tsub_trans.

Lemma typing_prod_inv : forall E U T P,
  _typing E (trm_prod U T) P ->
     _sub E trm_star P
  /\ _typing E U trm_star
  /\ exists L, forall x, x \notin L -> _typing (E & x ~ U) (T ^ x) trm_star.
Proof.
  introv Typ. gen_eq P1: (trm_prod U T).
  induction Typ; intros; subst; tryfalse.
  inversions H1. splits*. 
  destruct~ (IHTyp eq_refl) as [EQi [TypU [L P]]].
  splits*.
Qed.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  _typing E v V ->
  _typing (E & x ~ V & F) t T ->
  _typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : _typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      _typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:_wf G) => 
      forall F, G = E & x ~ V & F -> 
      _wf (E & (map (subst x v) F)))
   (P1 := fun (G:env) t T (Subt : _sub G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      _sub (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T)); 
   intros; subst; simpls subst. 
  (* case: trm_star *)
  autos*.
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ _typing_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh* (@_typing_lam) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
  (* case: trm_app *)
  rewrite* subst_open.
  (* case: trm_prod *)
  apply_fresh* (@_typing_pi) as y.
   cross; auto. apply_ih_map_bind* H0. 
  (* case: trm_and *)
  autos*.
  (* case: trm_merge *)
  autos*.
  (* case: trm_sub *)
  apply* (@_typing_sub).
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@_wf_cons). 
  (* case sub_star *)
  autos*.
  (* case sub_var *)
  case_var.
    binds_mid~. apply_empty* tsub_weaken. apply* tsub_refl.
    apply _sub_var with (A := [x ~> v] A). auto.
    destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
    rewrite* subst_fresh.
  (* case: sub_abs *)
  apply_fresh* (@_sub_lam) as y.
   cross; auto. apply_ih_map_bind* H0.
  (* case: sub_app *)
  autos*.
  (* case: sub_prod *)
  apply_fresh* (@_sub_pi) as y.
   cross; auto. apply_ih_map_bind* H1. 
  (* case: sub_and *)
  apply* tsub_mono_left.
  apply* tsub_mono_right.
  autos*.
  (* case: trm_merge *)
  autos*.
  (* case: conclusion *)
  auto.
Qed.

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  _wf E -> 
  _typing E U trm_star.
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     apply_empty* typing_weaken.
    destruct (twf_push_inv W).
     apply_empty* typing_weaken.
Qed.

Lemma typing_from_sub : forall G e1 e2 T,
  _typing G e1 T ->
  _sub G e1 e2 ->
  _typing G e2 T.
Proof.
  intros. gen T. inductions H0; intros.
  auto.
  auto.
  inductions H2. apply_fresh* _typing_lam as y.
  apply* (@_typing_sub A0).
  inductions H1. apply* _typing_app.
  apply* (@_typing_sub A0).
  inductions H3. apply_fresh* _typing_pi as y.
  apply* H2. apply_empty* typing_narrowing.
  apply* (@_typing_sub A0).
  inductions H1. apply* IH_sub.
  apply* (@_typing_sub A0).
  inductions H1. apply* IH_sub.
  apply* (@_typing_sub A0).
  apply* _typing_and.
  inductions H. apply* _typing_merge.
  apply* (@_typing_sub A).
Qed.

Lemma typing_wf_from_typing : forall E t T,
  _typing E t T ->
  _typing E T trm_star.
Proof.
  induction 1.
  auto.
  destruct* (typing_wf_from_context H0).
  apply_fresh* (@_typing_pi) as y.
  destruct (typing_prod_inv IH_typing2) as [Le [Typ [L P]]].
  pick_fresh x. rewrite~ (@subst_intro x).
  unsimpl ([x ~> A](trm_star)).
  apply_empty* (@typing_substitution B).
  auto. auto.
  apply* (_typing_and).
  apply (typing_from_sub IH_typing). auto.
Qed.

Lemma typing_abs_inv : forall E V t P,
  _typing E (trm_abs V t) P -> exists T L,
     _typing E V trm_star
  /\ (forall x, x \notin L -> _typing (E & x ~ V) (T ^ x) trm_star)
  /\ (_sub E (trm_prod V T) P)
  /\ (forall x, x \notin L -> _typing (E & x ~ V) (t ^ x) (T ^ x)).
Proof.
  introv Typ. gen_eq u: (trm_abs V t).
  induction Typ; intros; subst; tryfalse.
  inversions H3. exists B L. splits*. apply* tsub_refl.
  destruct (IHTyp eq_refl) as [T0 [L [TypE [TypT0 [Le P2]]]]].
  exists T0 L. splits*.
Qed.

Lemma tsub_prod_any_inv : forall G A B C,
  _sub G (trm_prod A B) C ->
  (exists L A' B', C = trm_prod A' B' /\
                 _sub G A' A /\
                 (forall x, x \notin L ->
                            _sub (G & x ~ A') (B ^ x) (B' ^ x))) \/
  (exists A' B', C = trm_and A' B').
Proof.
  intros. inductions H.
  left. exists L C D. splits*.
  right. exists* B0 C.
Qed.

Lemma tsub_prod_star_false : forall G A B,
  _sub G (trm_prod A B) trm_star -> False.
Proof.
  intros. destruct (tsub_prod_any_inv H).
  destruct H0 as [? [? [? [? _]]]]. false.
  destruct H0 as [? [? ?]]. false.
Qed.

Lemma typing_app_inv : forall G e A D,
  _typing G (trm_app e A) D ->
  exists B C, _typing G A B /\
              _typing G e (trm_prod B C) /\
              _sub G (C ^^ A) D.
Proof.
  intros. gen_eq Q: (trm_app e A).
  induction H; intros; subst; tryfalse.
  inversions H1. exists* B C. splits*.
  asserts* Typ2: (_typing G (trm_prod B C) trm_star).
  apply* typing_wf_from_typing.
  destruct (typing_prod_inv Typ2) as [? [? [L P]]].
  assert (_typing G (C ^^ A) trm_star).
  pick_fresh y. rewrite* (@subst_intro y).
  unsimpl([y ~> A] trm_star).
  asserts* Eq: (trm_star = trm_star ^ y).
  rewrite Eq.
  apply_empty* (@typing_substitution B).
  apply* tsub_refl.
  destruct~ IH_typing as [B' [C' [? [? ?]]]].
  exists* B' C'.
Qed.

Lemma typing_and_inv : forall G A B C,
  _typing G (trm_and A B) C ->
  exists C', _typing G A C' /\
             _typing G B C' /\
             _sub G C' C.
Proof.
  intros. gen_eq Q: (trm_and A B). gen A B.
  induction H; intros; subst; tryfalse.
  inversions H1. exists C. splits*. apply* tsub_refl. apply* typing_wf_from_typing.
  destruct~ (IH_typing A0 B0) as [? [? [? ?]]].
  exists x. splits*.
Qed.

Lemma typing_merge_inv : forall G e1 e2 A,
  _typing G (trm_merge e1 e2) A ->
  (exists T1 T2, _typing G e1 T1 /\
                 _typing G e2 T2 /\
                 _sub G (trm_and T1 T2) A).
Proof.
  intros. gen_eq Q: (trm_merge e1 e2). gen e1 e2.
  induction H; intros; subst; tryfalse.
  inversions H1. exists* A B. splits*. apply* tsub_refl. apply* _typing_and.
  apply* typing_wf_from_typing.
  apply* typing_wf_from_typing.
  destruct~ (IH_typing e1 e2) as [T1 [T2 [? [? ?]]]].
  exists T1 T2. splits*.
Qed.
                                               
Lemma tsub_star_any_comm : forall G A,
  _sub G trm_star A ->
  _sub G A trm_star.
Proof.
  intros. inductions H; autos*.
Qed.

Lemma typing_var_inv : forall G x A,
  _typing G (trm_fvar x) A ->
  exists A', binds x A' G /\ _sub G A' A.
Proof.
  intros.
  inductions H; tryfalse.
  exists* A. splits*.
  asserts* T: (_typing G (trm_fvar x) A).
  apply* tsub_refl. apply* typing_wf_from_typing.
  destruct IH_typing as [A' [? ?]].
  exists A'. splits*.
Qed.

Lemma twf_to_wt : forall x A G,
  _wf (G & x ~ A) -> _typing G A trm_star.
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]; subst*.
Qed.

Lemma sub_to : forall G e1 e2 A,
  sub G e1 e2 A ->
  _typing G e1 A /\
  _typing G e2 A /\
  _sub G e1 e2.
Proof.
  intros. induction H; splits.
  (* star *)
  auto. auto. auto.
  (* var *)
  apply* _typing_var.
  apply* _typing_var.
  apply* tsub_refl.
  apply_empty* typing_weaken.
  apply_empty* typing_weaken.
  apply_empty* tsub_weaken.
  (* lam *)
  apply_fresh* _typing_lam as y.
  destruct~ (H1 y) as [? [? ?]].
  destruct~ (H3 y) as [? [? ?]].
  apply_fresh* _typing_lam as y.
  destruct~ (H1 y) as [? [? ?]].
  destruct~ (H3 y) as [? [? ?]].
  apply_fresh* _sub_lam as y.
  destruct~ (H1 y) as [? [? ?]].
  destruct~ (H3 y) as [? [? ?]].
  (* app *)
  apply* _typing_app.
  apply* _typing_app.
  apply* _sub_app.
  (* pi *)
  apply_fresh* _typing_pi as y.
  destruct~ (H3 y) as [? [? ?]].
  apply_fresh* _typing_pi as y.
  destruct~ (H1 y) as [? [? ?]].
  apply_fresh* _sub_pi as y.
  destruct~ (H1 y) as [? [? ?]].
  (* and *)
  apply* _typing_and.
  destruct~ IHsub1 as [? [? ?]].
  apply* tsub_mono_left.
  apply* _typing_and.
  destruct~ IHsub1 as [? [? ?]].
  apply* tsub_mono_right.
  destruct~ IHsub1 as [? [? ?]].
  apply* _typing_and.
  destruct~ IHsub1 as [? [? ?]].
  destruct~ IHsub2 as [? [? ?]].
  (* merge *)
  apply* _typing_merge.
  apply* _typing_merge.
  apply* _sub_merge.
  (* sub *)
  apply* (@_typing_sub A).
  apply* (@_typing_sub A).
  destruct~ IHsub1 as [? [? ?]].
Qed.

Lemma sub_wf_to : forall G,
  wf G -> _wf G.
Proof.
  intros. inductions H.
  auto. destruct (sub_to H0) as [? [? ?]].
  apply* _wf_cons.
Qed.

Lemma sub_from_aux : forall G e1 e2 A,
  sub G e1 e1 A ->
  _sub G e1 e2 ->
  sub G e1 e2 A.
Proof.
  intros. gen A.
  apply tsub_induct with
   (P := fun (G:env) e A (Typ: _typing G e A) => sub G e e A)
   (P0 := fun (G:env) (W:_wf G) => wf G)
   (P1 := fun (G:env) e1 e2 (W:_sub G e1 e2) =>
            forall A (S1: sub G e1 e1 A),
            sub G e1 e2 A);
    intros; auto.

  apply* sub_star_alt.
  apply* sub_var_alt.
  apply_fresh* sub_lam as y.
  apply* sub_app.
  apply_fresh* sub_pi as y.
  apply* (@sub_sub A).
  assert (W: _typing G0 A trm_star) by (apply* typing_wf_from_typing).
  apply* H1. apply* sub_wf_from_sub.

  inductions S1. inversions H2.
  apply_fresh* sub_lam as y. apply* (@sub_sub A0).
  inductions S1. inversions H1.
  apply* sub_app. apply* (@sub_sub A0).
  inductions S1. inversions H4.
  apply_fresh* sub_pi as y. apply* H2. apply_empty* sub_narrowing.
  apply* (@sub_sub A0).
  destruct~ (sub_and_and_inv S1) as [P Q].
  destruct~ (sub_and_and_inv S1) as [P Q].
  inductions S1. inversions H2.
  apply* sub_merge. apply* (@sub_sub A).
Qed.

Lemma sub_from : forall G e1 e2 A,
  _typing G e1 A ->
  _sub G e1 e2 ->
  sub G e1 e2 A.
Proof.
  intros. gen e2.
  apply typing_induct with
   (P := fun (G:env) e1 A (Typ: _typing G e1 A) =>
           forall e2 (Sub: _sub G e1 e2), sub G e1 e2 A)
   (P0 := fun (G:env) (W:_wf G) => wf G)
   (P1 := fun (G:env) e1 e2 (W:_sub G e1 e2) => wf G); intros; auto.
  (* star *)
  inductions Sub. apply* sub_star_alt.
  apply* sub_and3.
  (* var *)
  inductions Sub.
  apply* sub_var_alt.
  apply* sub_and3.
  (* lam *)
  inductions Sub.
  apply_fresh* sub_lam as z.
  apply* H0. apply* tsub_refl.
  apply* H1. apply* tsub_refl.
  apply sub_and3. apply* IHSub1. apply* IHSub2.
  (* app *)
  inductions Sub.
  apply* (@sub_app B).
  apply* H0. apply* tsub_refl.
  apply* sub_and3.
  (* pi *)
  apply* sub_from_aux.
  apply_fresh* sub_pi as y.
  apply* H0. apply* tsub_refl.
  apply* H1. apply* tsub_refl.
  apply* H1. apply* tsub_refl.
  (* and *)
  inductions Sub.
  apply* sub_mono_left. apply* H1. apply* tsub_refl.
  apply* sub_mono_right. apply* H0. apply* tsub_refl.
  apply* sub_and3.
  (* merge *)
  apply* sub_from_aux.
  apply* sub_merge. apply* H0. apply* tsub_refl.
  apply* H1. apply* tsub_refl.
  (* sub *)
  apply* (@sub_sub A0). apply* sub_from_aux.
  apply* sub_wf_from_sub.
  (* cons *)
  apply* wf_cons.
  apply* H0. apply* tsub_refl.
  asserts* W: (_sub G0 A0 A0).
  apply* tsub_refl.
  lets: (H0 _ W).
  auto.
Qed.

