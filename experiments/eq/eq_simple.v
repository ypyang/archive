Set Implicit Arguments.
Require Import LibLN.
Implicit Types x : var.

(* Syntax *)

Inductive trm : Set :=
  | trm_bvar   : nat -> trm
  | trm_fvar   : var -> trm
  | trm_star   : trm
  | trm_app    : trm -> trm -> trm
  | trm_abs    : trm -> trm -> trm
  | trm_prod   : trm -> trm -> trm.

Fixpoint open_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i         => If k = i then u else (trm_bvar i)
  | trm_fvar x         => trm_fvar x 
  | trm_star           => trm_star
  | trm_app t1 t2      => trm_app (open_rec k u t1) (open_rec k u t2)
  | trm_abs t1 t2      => trm_abs (open_rec k u t1) (open_rec (S k) u t2) 
  | trm_prod t1 t2     => trm_prod (open_rec k u t1) (open_rec (S k) u t2) 
  end.

Definition open t u := open_rec 0 u t.

Notation "{ k ~> u } t" := (open_rec k u t) (at level 67).
Notation "t ^^ u" := (open t u) (at level 67).
Notation "t ^ x" := (open t (trm_fvar x)).

Fixpoint fv (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i         => \{}
  | trm_fvar x         => \{x}
  | trm_star           => \{}
  | trm_app t1 t2      => (fv t1) \u (fv t2)
  | trm_abs t0 t1      => (fv t0) \u (fv t1)
  | trm_prod t0 t1     => (fv t0) \u (fv t1)
  end.

Inductive term : trm -> Prop :=
  | term_var : forall x, 
      term (trm_fvar x)
  | term_app : forall t1 t2,
      term t1 -> term t2 -> 
      term (trm_app t1 t2)
  | term_star : term trm_star
  | term_abs : forall L t1 t2,
      term t1 ->
      (forall x, x \notin L -> term (t2 ^ x)) -> 
      term (trm_abs t1 t2)
  | term_prod : forall L t1 t2,
      term t1 ->
      (forall x, x \notin L -> term (t2 ^ x)) -> 
      term (trm_prod t1 t2).

Definition body t :=
  exists L, forall x, x \notin L -> term (t ^ x). 

Definition relation := trm -> trm -> Prop.

(* reduction relation *)

Inductive value : trm -> Prop :=
  | value_star : value trm_star
  | value_abs : forall A e2,
      term (trm_abs A e2) -> value (trm_abs A e2)
  | value_prod : forall A B,
      term (trm_prod A B) -> value (trm_prod A B).

Inductive reduct : trm -> trm -> Prop :=
  | reduct_beta : forall A e1 e2,
      term (trm_abs A e1) ->
      term e2 ->
      reduct (trm_app (trm_abs A e1) e2) (e1 ^^ e2)
  | reduct_app : forall e1 e1' e2,
      term e2 -> reduct e1 e1' ->
      reduct (trm_app e1 e2) (trm_app e1' e2).

Notation "t ~~> t'" := (reduct t t') (at level 67).

(* Typing *)

Definition env := LibEnv.env trm.

Implicit Types E : env.

Inductive dir := aeq.

Inductive deq : env -> dir -> trm -> trm -> trm -> Prop :=
  | deq_star : forall E d,
      wf E -> deq E d trm_star trm_star trm_star
  | deq_var : forall E d x A,
      wf E ->
      binds x A E ->
      deq E d (trm_fvar x) (trm_fvar x) A
  | deq_abs : forall L E d A e2 e2' B,
      deq E d A A trm_star ->
      (forall x, x \notin L ->
        deq (E & x ~ A) d (B ^ x) (B ^ x) trm_star) ->
      (forall x, x \notin L ->
        deq (E & x ~ A) d (e2 ^ x) (e2' ^ x) (B ^ x)) ->
      deq E d (trm_abs A e2) (trm_abs A e2') (trm_prod A B)
  | deq_app : forall E d e1 e2 A B C,
      deq E d A A B ->
      deq E d e1 e2 (trm_prod B C) ->
      deq E d (trm_app e1 A) (trm_app e2 A) (C ^^ A)
  | deq_prod : forall L E d A B A' B',
      deq E d A A' trm_star ->
      (forall x, x \notin L ->
        deq (E & x ~ A) d (B ^ x) (B' ^ x) trm_star) ->
      (forall x, x \notin L ->
        deq (E & x ~ A') d (B' ^ x) (B' ^ x) trm_star) ->
      deq E d (trm_prod A B) (trm_prod A' B') trm_star
  | deq_conv : forall A E d e1 e2 B,
      deq E d e1 e2 A ->
      deq E d A B trm_star ->
      deq E d e1 e2 B
with wf : env -> Prop :=
  | wf_nil : wf empty
  | wf_cons : forall E d x A,
      x # E ->
      deq E d A A trm_star ->
      wf (E & x ~ A).

Scheme deq_induct := Induction for deq Sort Prop
  with wf_induct := Induction for wf Sort Prop.

(* infrastructure *)

Fixpoint subst (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i        => trm_bvar i 
  | trm_fvar x        => If x = z then u else (trm_fvar x)
  | trm_star          => trm_star
  | trm_app t1 t2      => trm_app (subst z u t1) (subst z u t2)
  | trm_abs t1 t2      => trm_abs (subst z u t1) (subst z u t2)
  | trm_prod t1 t2     => trm_prod (subst z u t1) (subst z u t2)
  end.

Notation "[ z ~> u ] t" := (subst z u t) (at level 68).

Fixpoint close_var_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i        => trm_bvar i 
  | trm_fvar x        => If x = z then (trm_bvar k) else (trm_fvar x)
  | trm_star          => trm_star
  | trm_app t1 t2     => trm_app (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_abs t1 t2  => trm_abs (close_var_rec k z t1) (close_var_rec (S k) z t2) 
  | trm_prod t1 t2 => trm_prod (close_var_rec k z t1) (close_var_rec (S k) z t2) 
  end.

Definition close_var z t := close_var_rec 0 z t.

Definition contains_terms E :=
  (forall x T, binds x T E -> term T).

Definition simulated (R1 R2 : relation) := 
  forall (t t' : trm), R1 t t' -> R2 t t'.
 
Infix "simulated_by" := simulated (at level 69).


Definition red_regular (R : relation) :=
  forall t t', R t t' -> term t /\ term t'.

Definition red_refl (R : relation) :=
  forall t, term t -> R t t.

Definition red_in (R : relation) :=
  forall t x u u', term t -> R u u' ->
  R ([x ~> u]t) ([x ~> u']t).
  
Definition red_all (R : relation) :=
  forall x t t', R t t' -> 
  forall u u', R u u' -> 
  R ([x~>u]t) ([x~>u']t').

Definition red_out (R : relation) :=
  forall x u t t', term u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Definition red_rename (R : relation) :=
  forall x t t' y,
  R (t ^ x) (t' ^ x) -> 
  x \notin (fv t) -> x \notin (fv t') ->
  R (t ^ y) (t' ^ y).

Definition red_through (R : relation) :=
  forall x t1 t2 u1 u2, 
  x \notin (fv t1) -> x \notin (fv u1) ->
  R (t1 ^ x) (u1 ^ x) -> R t2 u2 ->
  R (t1 ^^ t2) (u1 ^^ u2).

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : trm => fv x) in
  let D := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D).

Ltac pick_fresh X :=
  let L := gather_vars in (pick_fresh_gen L X).

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

Ltac exists_fresh := 
  let L := gather_vars_with (fun x : vars => x) in exists L.

Hint Constructors value reduct deq wf.

Hint Constructors term.

Lemma open_rec_term_core :forall e j v i u, i <> j ->
  {j ~> v}e = {i ~> u}({j ~> v}e) -> e = {i ~> u}e.
Proof.
  induction e; introv Neq Equ;
  simpl in *; inversion* Equ; f_equal*.  
  case_nat*. case_nat*.
Qed.

Lemma open_rec_term : forall t u,
  term t -> forall k, t = {k ~> u}t.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open. pick_fresh x.
   apply* (@open_rec_term_core t2 0 (trm_fvar x)).
  unfolds open. pick_fresh x. 
   apply* (@open_rec_term_core t2 0 (trm_fvar x)).
Qed.

Lemma subst_fresh : forall x t u, 
  x \notin fv t -> [x ~> u] t = t.
Proof.
  intros. induction t; simpls; fequals*.
  case_var*. 
Qed.

Lemma subst_open : forall x u t1 t2, term u -> 
  [x ~> u] (t1 ^^ t2) = ([x ~> u]t1) ^^ ([x ~> u]t2).
Proof.
  intros. unfold open. generalize 0.
  induction t1; intros; simpl; f_equal*.
  case_nat*. case_var*. apply* open_rec_term.
Qed.

Lemma subst_open_var : forall x y u e, y <> x -> term u ->
  ([x ~> u]e) ^ y = [x ~> u] (e ^ y).
Proof.
  introv Neq Wu. rewrite* subst_open.
  simpl. case_var*.
Qed.

Lemma subst_intro : forall x t u, 
  x \notin (fv t) -> term u ->
  t ^^ u = [x ~> u](t ^ x).
Proof.
  introv Fr Wu. rewrite* subst_open.
  rewrite* subst_fresh. simpl. case_var*.
Qed.

Ltac cross := 
  rewrite subst_open_var; try cross.

Tactic Notation "cross" "*" := 
  cross; autos*.

Lemma subst_term : forall t z u,
  term u -> term t -> term ([z ~> u]t).
Proof.
  induction 2; simple*.
  case_var*.
  apply_fresh* term_abs as y. rewrite* subst_open_var.
  apply_fresh* term_prod as y. rewrite* subst_open_var.
Qed.

Lemma open_term : forall t u,
  body t -> term u -> term (t ^^ u).
Proof.
  intros. destruct H. pick_fresh y.
  rewrite* (@subst_intro y). apply* subst_term.
Qed.

Hint Resolve subst_term open_term.

Lemma term_abs1 : forall t2 t1,
  term (trm_abs t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_abs2 : forall t1 t2,  
  term (trm_abs t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma term_prod1 : forall t2 t1,
  term (trm_prod t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_prod2 : forall t1 t2,  
  term (trm_prod t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Hint Extern 1 (term ?t1) =>
  match goal with
  | H: term (trm_abs t1 ?t2) |- _ => apply (@term_abs1 t2)
  | H: term (trm_prod t1 ?t2) |- _ => apply (@term_prod1 t2)
  end.

Hint Extern 3 (body ?t) =>
  match goal with 
  | H: context [trm_abs ?t1 t] |- _ => apply (@body_abs2 t1)
  | H: context [trm_prod ?t1 t] |- _ => apply (@body_prod2 t1)
  end.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: context [t ^ _] |- _ =>
      let x := fresh in let Fr := fresh in
      let P := fresh in
      let L := gather_vars in exists L; intros x Fr; 
      lets P: H x __; [ notin_solve 
                      | try destructs P ]
  end.

Lemma term_abs_prove : forall t1 t2,
  term t1 -> body t2 -> term (trm_abs t1 t2).
Proof.
  intros. apply_fresh* term_abs as x.
Qed.

Lemma term_prod_prove : forall t1 t2,
  term t1 -> body t2 -> term (trm_prod t1 t2).
Proof.
  intros. apply_fresh* term_prod as x.
Qed.

Hint Resolve term_abs_prove term_prod_prove.

Lemma body_prove : forall L t,
  (forall x, x \notin L -> term (t ^ x)) -> body t.
Proof.
  intros. exists* L.
Qed.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: forall _, _ \notin ?L -> term (t ^ _)  |- _ =>
    apply (@body_prove L)
  end. 

Lemma body_subst : forall x t u,
  term u -> body t -> body ([x ~> u]t).
Proof.
  introv. intros Wu [L Bt].
  exists (\{x} \u L). intros y Fr. cross*.
Qed.

Hint Resolve body_subst.

Lemma open_var_inj : forall x t1 t2, 
  x \notin (fv t1) -> x \notin (fv t2) -> 
  (t1 ^ x = t2 ^ x) -> (t1 = t2).
Proof.
  intros x t1. unfold open. generalize 0.
  induction t1; intro k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal* 
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma close_var_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv t1) ->
    {i ~> trm_fvar y} ({j ~> trm_fvar z} (close_var_rec j x t1) )
  = {j ~> trm_fvar z} (close_var_rec j x ({i ~> trm_fvar y}t1) ).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ]. 
  case_var*. simpl. case_nat*. 
Qed.

Lemma close_var_fresh : forall x t,
  x \notin fv (close_var x t).
Proof.
  introv. unfold close_var. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Lemma close_var_body : forall x t,
  term t -> body (close_var x t).
Proof.
  introv W. exists \{x}. intros y Fr.
  unfold open, close_var. generalize 0. gen y.
  induction W; intros y Fr k; simpls.
  case_var; simple*. case_nat*.
  autos*.
  autos*.
  apply_fresh* term_abs as z.
   unfolds open. rewrite* close_var_rec_open.
  apply_fresh* term_prod as z.
   unfolds open. rewrite* close_var_rec_open.
Qed.

Lemma close_var_open : forall x t,
  term t -> t = (close_var x t) ^ x.
Proof.
  introv W. unfold close_var, open. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
Qed. 

Lemma close_var_spec : forall t x, term t -> 
  exists u, t = u ^ x /\ body u /\ x \notin (fv u).
Proof.
  intros. exists (close_var x t). splits 3.
  apply* close_var_open.
  apply* close_var_body.
  apply* close_var_fresh.
Qed. 

Lemma value_is_term : forall t, value t -> term t.
Proof.
  intros_all. induction* H.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: value t |- _ => apply (value_is_term H) end.

Lemma red_regular_reduct : red_regular reduct.
Proof.
  intros_all. induction* H.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: reduct t _ |- _ => apply (proj1 (red_regular_reduct H))
  | H: reduct _ t |- _ => apply (proj2 (red_regular_reduct H))
  end.

Hint Extern 1 (term (trm_abs ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: term (trm_abs t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_abs t1 t2)) end.

Hint Extern 1 (term (trm_prod ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: term (trm_prod t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_prod t1 t2)) end.

Lemma regular_deq : forall G d e1 e2 A,
  deq G d e1 e2 A ->
  (wf G /\ ok G /\ contains_terms G /\ term e1 /\ term e2 /\ term A). 
Proof.
  apply deq_induct with
  (P0 := fun E (_ : wf E) => ok E /\ contains_terms E);
    unfolds contains_terms; intros; splits*.
  intros. false* binds_empty_inv.
  intros.
    destruct (binds_push_inv H0) as [[? ?]|[? ?]].
    subst. destructs~ H.
  destructs H. apply* H4.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: deq _ _ t _ _ |- _ => apply (proj31 (proj44 (regular_deq H)))
  | H: deq _ _ _ t _ |- _ => apply (proj32 (proj44 (regular_deq H)))
  | H: deq _ _ _ _ t |- _ => apply (proj33 (proj44 (regular_deq H)))
  end.

Lemma ok_from_wf : forall G, wf G -> ok G.
Proof.
  induction 1. auto.
  autos* (regular_deq H0).
Qed.

Hint Extern 1 (ok ?E) => match goal with
  | H: wf E |- _ => apply (ok_from_wf H)
  end.

Hint Extern 1 (wf ?E) => match goal with
  | H: deq E _ _ _ _ |- _ => apply (proj1 (regular_deq H))
  end.

Lemma wf_push_inv : forall G x A,
  wf (G & x ~ A) -> wf G /\ term A.
Proof.
  introv W. inversions W. 
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]]. subst~.
Qed.

Lemma term_from_binds_in_wf : forall x E T,
  wf E -> binds x T E -> term T.
Proof.
  introv W Has. gen E. induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv W). 
  destruct (binds_push_inv Has) as [[? ?] | [? ?]].
  subst~.
  apply* IHE.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: binds ?x t ?E |- _ => apply (@term_from_binds_in_wf x E)
  end.

Lemma wf_left : forall E F : env,
  wf (E & F) -> wf E.
Proof.
  intros. induction F using env_ind.
  rewrite~ concat_empty_r in H.
  rewrite concat_assoc in H.
   inversions H. false (empty_push_inv H1).
   destruct (eq_push_inv H0) as [? [? ?]]. subst~. 
Qed.

Implicit Arguments wf_left [E F].

Lemma fv_open_var : forall y x t,
  x <> y -> x \notin fv (t ^ y) -> x \notin fv t.
Proof.
  introv Neq. unfold open. generalize 0. 
  induction t; simpl; intros; try notin_solve; autos*.
Qed.

Lemma and_simpl : forall (A : Prop),
    A -> A /\ A.
Proof.
  intros. auto.
Qed.

Lemma deq_fresh : forall G d A B C x,
  deq G d A B C -> x # G -> (x \notin fv A) /\ (x \notin fv B).
Proof.
  introv Typ.
  induction Typ; simpls; intros.
  auto.
  apply and_simpl. 
  rewrite notin_singleton. intro. subst. applys binds_fresh_inv H0 H1.
  destruct (IHTyp H3).
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  lets: H2 y __. auto. apply* H6.
  pick_fresh y. apply* (@fv_open_var y).
  lets: H2 y __. auto. apply* H6.
  destruct (IHTyp1 H).
  destruct (IHTyp2 H).
  split; notin_simpl; auto.
  destruct (IHTyp H3).
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  lets: H0 y __. auto. apply* H0.
  pick_fresh y. apply* (@fv_open_var y).
  lets: H0 y __. auto. apply* H0.
  autos*.
Qed.

Lemma notin_fv_from_wf : forall E F x T,
  wf (E & x ~ T & F) -> x \notin fv T.
Proof.
  intros.
  lets W: (wf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. subst~.
  apply* deq_fresh.
Qed.

Lemma notin_fv_from_binds : forall x y U E,
  wf E -> binds y U E -> x # E -> x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv H).
    destruct (binds_push_inv H0) as [[? ?] | [? ?]].
    subst~. inversions H. false* (empty_push_inv H5).
     destruct (eq_push_inv H4) as [? [? ?]]. subst~.
     apply* deq_fresh.
     apply* IHE.
Qed.

Lemma notin_fv_from_binds' : forall E F x V y U,
  wf (E & x ~ V & F) -> 
  binds y U E ->
  x \notin fv U.
Proof.
  intros.
  lets W: (wf_left H). inversions keep W.
  false (empty_push_inv H2). 
  destructs (eq_push_inv H1). subst~.
  apply* notin_fv_from_binds.
Qed.

Hint Extern 1 (?x \notin fv ?V) => 
  match goal with H: wf (?E & x ~ V & ?F) |- _ =>
    apply (@notin_fv_from_wf E F) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: wf (?E & x ~ ?V & ?F), B: binds ?y U ?E |- _ =>
    apply (@notin_fv_from_binds' E F x V y) end.

Lemma wf_to_wt_typ : forall G x A,
  wf (G & x ~ A) -> exists d, deq G d A A trm_star.
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]. subst~. 
  exists~ d.
Qed.

Lemma deq_prod_prod_inv1 : forall G d A A0 B B0 C,
   deq G d (trm_prod A B) (trm_prod A0 B0) C ->
   deq G d A A0 trm_star.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma deq_refl : forall G d e1 e2 A,
  deq G d e1 e2 A ->
  deq G d e1 e1 A /\ deq G d e2 e2 A.
Proof.
  intros. inductions H; try solve [split; eauto 2].
  split.
    apply_fresh* (@deq_abs) as y. apply* H3.
    apply_fresh* (@deq_abs) as y. apply* H3.
  split;
    apply* deq_app.
  split.
    apply_fresh* (@deq_prod) as y. apply* H1. apply* H1.
    apply_fresh* (@deq_prod) as y.
  splits*.
Qed.

Lemma deq_weaken : forall G d E F t1 t2 T,
  deq (E & G) d t1 t2 T ->
  wf (E & F & G) ->
  deq (E & F & G) d t1 t2 T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var *)
  apply* deq_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E0 F G eq_refl W).
  apply_fresh* (@deq_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  (* case: trm_bprod *)
  apply_fresh* (@deq_prod) as y;
    lets: (IHTyp E0 F G eq_refl W);
    lets: (deq_refl H3).
  apply_ih_bind* H0.
  apply_ih_bind* H2.
Qed.

Lemma deq_context : forall F d A' A E B Z S T,
  deq (E & Z ~ A & F) d S T B ->
  deq E d A' A trm_star ->
  deq E d A' A' trm_star ->
  deq (E & Z ~ A' & F) d S T B.
Proof.
  introv Sa Sb Sc.
  gen_eq G: (E & Z ~ A & F). gen F.
  apply deq_induct with
    (P := fun G d S T B (Sub: deq G d S T B) =>
            forall F, G = E & Z ~ A & F ->
                      deq (E & Z ~ A' & F) d S T B)
    (P0 := fun G (W: wf G) =>
            forall F, G = E & Z ~ A & F ->
                      wf (E & Z ~ A' & F));
    intros; subst; simpls subst.
   (* case: ax *)
  apply* deq_star.
  (* case: var refl *)
  tests EQ: (x = Z).
    asserts~ N1: (wf (E & Z ~ A' & F)).
    asserts~ N': (ok (E & Z ~ A & F)).
    lets: (binds_middle_eq_inv b N').
    destruct (ok_middle_inv N').
    subst.
    apply (@deq_conv A').
    apply deq_var. auto.
    apply* binds_middle_eq.
    asserts~ N3: (d = d0).
    induction d. induction d0. auto. subst.
    apply_empty* deq_weaken.
    apply_empty* deq_weaken.
    asserts* W1: (x # (Z ~ A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply deq_var. auto.
    apply binds_weaken. auto. auto.
  (* case: abs *)
  apply_fresh* deq_abs as Y.
  apply_ih_bind* H0.
  apply_ih_bind* H1.
  (* case: app *)
  autos*.
  (* case: prod *)
  apply_fresh* (@deq_prod) as Y.
  apply_ih_bind* H0.
  apply_ih_bind* H1.
  (* case: conv *)
  autos*.
  (* case: wf nil *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  destruct (env_case F).
    subst. rewrite concat_empty_r.
    rewrite concat_empty_r in H0.
    destruct (eq_push_inv H0) as [? [? ?]]. subst.
    apply* wf_cons.
    destruct H1 as [x' [v [E' ?]]].
    subst.
    rewrite concat_assoc.
    rewrite concat_assoc in H0.
    destruct (eq_push_inv H0) as [? [? ?]]. subst~.
    apply* wf_cons.
  (* conclusion *)
  auto.
Qed.

Hint Extern 1 (deq ?G ?d ?A ?A ?B) => match goal with
  | H: deq G d A _ B |- _ => apply (proj1 (deq_refl H))
  | H: deq G d _ A B |- _ => apply (proj2 (deq_refl H))
  end.

Lemma deq_sym : forall E d e1 e2 A,
  deq E d e1 e2 A -> deq E d e2 e1 A.
Proof.
  intros. induction H; eauto 2.
  apply_fresh* deq_prod as y.
  apply_empty* deq_context.
  lets: H0 y __. auto. auto.
Qed.

Hint Extern 1 (deq ?G ?d ?A ?B ?C) => match goal with
  | H: deq G d B A C |- _ => apply ((deq_sym H))
  end.

Lemma deq_substitution : forall V (F:env) d v (E:env) x t1 t2 T,
  deq E d v v V ->
  deq (E & x ~ V & F) d t1 t2 T ->
  deq (E & (map (subst x v) F)) d (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F.
  apply deq_induct with
   (P := fun (G:env) d t1 t2 T (Typt : deq G d t1 t2 T) =>
      forall (F:env), G = E & x ~ V & F ->
      deq (E & map (subst x v) F) d ([x ~> v]t1) ([x ~> v]t2) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) =>
      forall F, G = E & x ~ V & F ->
      wf (E & (map (subst x v) F)));
   intros; subst; simpls subst.
  (* case: ax *)
  autos*.
  (* case: var refl *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* deq_weaken.
    assert (d = d0). induction d. induction d0. auto. subst~.
    apply* deq_var.
    destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
    rewrite* subst_fresh.
  (* case: abs *)
  apply_fresh* (@deq_abs) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  (* case: app *)
  rewrite* subst_open.
  (* case: prod *)
  apply_fresh* (@deq_prod) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  (* case: sub *)
  autos*.
  (* case: wf empty *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. inversions H2.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
  (* case: conclusion *)
  auto.
Qed.

Lemma deq_trans : forall e2 G d e1 e3 A B,
  deq G d e1 e2 A -> deq G d e2 e3 B -> deq G d e1 e3 A.
Proof.
  introv S1. asserts* W: (term e2).
  gen G A e1 B e3. set_eq e2' eq: e2. gen e2' eq.
  induction W; intros e2' eq G A e1' S1;
    induction S1; try discriminate; inversions eq;
      intros B' e3' S2; try solve [eauto 3].
  (* case: var *)
  inductions S2; eauto 3.
  (* case: app *)
  inductions S2.
  apply deq_app with (B := B). auto. apply* IHW1.
  lets P: IHS2_1 t2 __. auto.
  lets P1: P IHW2 __. auto. lets P2: P1 t1 __. auto.
  apply* P2.
  (* case: star *)
  inductions S2; eauto 3.
  (* case lam *)
  inductions S2. 
  pick_fresh y.
  apply (@deq_abs (L \u L0 \u L1)). auto. auto. intros.
  lets P: H0 x __. auto. lets P2: P (t2 ^ x) __. auto.
  lets Q: H3 x __. auto.
  lets Q2: H8 x __. auto. lets: P2 Q Q2. auto.
  apply* IHS2_1.
  (* case pi *)
  inductions S2. clear H8 H6 H4 IHS1 H2 IHS2.
  pick_fresh y.
  apply (@deq_prod (L \u L0 \u L1)).
  apply* IHW. intros.
  lets: H0 x __. auto. lets: H4 (t2 ^ x) __. auto.
  lets: H1 x __. auto. lets: H6 H8. 
  lets: H7 x __. auto.
  apply H9 with (B0 := trm_star).
  apply_empty* (@deq_context).
  intros. apply* H5.
  lets P: IHS2_1 t2 __. auto. 
  lets P1: P IHS1 __. auto. auto. auto. auto.
  auto. auto. auto. apply* P1.
Qed.


