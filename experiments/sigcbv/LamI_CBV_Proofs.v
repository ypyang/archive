Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_CBV_ott LamI_CBV_Infra.
Implicit Types x : var.

Lemma typing_weaken : forall G E F t T,
  typing (E & G) t T ->
  wf (E & F & G) ->
  typing (E & F & G) t T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto 4.
  (* case: var *)
  apply* t_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  instantiate (1 := s3). auto.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0. auto.
  (* case: trm_mu *)
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0.
  apply* t_castup_app.
  apply* t_castup_pair.
  apply* t_castup_app_con.
  (* case: trm_sigma *)
  apply_fresh* (@t_sigma) as y. apply_ih_bind* H0.
  (* case: trm_pair *)
  apply* (@t_pair).
  apply* (@t_pair_v).
Qed.

Definition red_out (R : relation) :=
  forall x u t t', value u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Hint Unfold red_out.

Lemma value_red_out :   forall x u t, value u -> value t -> 
  value ([x~>u]t).
Proof.
  intros_all. induction H0; simpl; auto.
  case_if~.
  apply_fresh* value_mu as y.
  rewrite* subst_open_var.
Qed.

Lemma fv_subst : forall e x y u,
  x \in fv e -> x <> y ->
  x \in fv ([y ~> u] e).
Proof.
  intros.
  induction e; simpls~; try solve [
                       rewrite~ in_union in H;
                       rewrite in_union; destruct H; try solve [left~ | right~]
                     ].
  case_if~. subst. rewrite in_singleton in H. false.
    repeat (rewrite in_union in *). repeat (destruct H; eauto).
    repeat (rewrite in_union in *). repeat (destruct H; eauto).
    repeat (rewrite in_union in *). repeat (destruct H; eauto).
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl; eauto 3.
  rewrite* subst_open.
  apply* r_beta. apply* value_red_out.
  rewrite* subst_open. apply_fresh* r_beta_mu as z. rewrite~ subst_open_var. apply* value_red_out. apply* value_red_out. 
  apply* r_app_r. apply* value_red_out.
  rewrite* subst_open. apply_fresh* r_castdn_mu as z. rewrite~ subst_open_var. apply* value_red_out.
  apply* r_cast_elim. apply* value_red_out.
  apply* r_castup_app_r. apply* value_red_out.
  apply* r_castup_app_con_elim.
  apply* r_castup_pair.
  apply* r_castup_pair_elim. apply* value_red_out.
  apply* r_castup_app_elim. apply* value_red_out. apply* value_red_out.
  apply* r_pair1.
  apply* r_pair2. apply* value_red_out.
  apply* r_proj1. apply* value_red_out. apply* value_red_out.
  apply* r_proj2. apply* value_red_out. apply* value_red_out.
  apply* r_proj2_v. apply* value_red_out. apply* value_red_out.
  instantiate(1:=L \u \{x} \u fv u \u fv B).
  intros. lets: H2 x5 __. auto.
  rewrite* subst_open_var.
  apply* fv_subst.
  rewrite* subst_open. apply_fresh* r_proj1_mu as z. rewrite~ subst_open_var. apply* value_red_out.
  rewrite* subst_open. apply_fresh* r_proj2_mu as z. rewrite~ subst_open_var. apply* value_red_out.
  rewrite* subst_open.
  apply_fresh* r_proj2_v_mu as z. rewrite~ subst_open_var. apply* value_red_out. 
  rewrite~ subst_open_var. 
  apply~ fv_subst.
Qed.

Lemma typing_substitution : forall V (F:env) v (E:env) x t T,
  typing E v V ->
  typing (E & x ~ V & F) t T ->
  value v ->
  typing (E & (map (subst x v) F)) (subst x v t) (subst x v T).
Proof.
  introv Typv Typt Val.
  gen_eq G: (E & x ~ V & F). gen F. 
  apply typing_induct with
   (P := fun (G:env) t T (Typt : typing G t T) => 
      forall (F:env), G = E & x ~ V & F -> 
      typing (E & map (subst x v) F) ([x ~> v]t) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) => 
      forall F, G = E & x ~ V & F -> 
      wf (E & (map (subst x v) F))); 
   intros; subst; simpls subst; eauto 4. 
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* typing_weaken.
    apply~ t_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: trm_abs *)
  apply_fresh~ (@t_abs) as y.
   cross; auto. apply_ih_map_bind* H0.
   cross; auto. apply_ih_map_bind* H1.
   instantiate (1 := s3). auto.
  (* case: trm_app *)
  apply~ t_app.
  rewrite* subst_open.
  apply~ t_app_v.
  apply~ value_red_out.
  (* case: trm_prod *)
  apply_fresh~ (@t_prod) as y.
   cross; auto. apply_ih_map_bind* H0. auto.
  (* case: trm_prod *)
  apply_fresh~ (@t_mu) as y.
   cross; auto. apply_ih_map_bind* H0.
   rewrite~ subst_open_var. apply* value_red_out. 
  (* case: castup *)
  apply~ (@t_castup). apply* reduct_red_out.
  (* case: castup_app *)
  apply~ (@t_castup_app).
  instantiate(1:=L \u \{x} \u fv v \u fv B).
  intros. lets: m x5 __. auto.
  rewrite* subst_open_var.
  apply* fv_subst.
  apply~ (@t_castup_pair).
  instantiate(1:=L \u \{x} \u fv v \u fv B).
  intros. lets: m x5 __. auto.
  rewrite* subst_open_var.
  apply* fv_subst.
  apply~ (@t_castup_app_con). apply* reduct_red_out.
  instantiate(1:=L \u \{x} \u fv v \u fv B).
  intros. lets: m x5 __. auto.
  rewrite* subst_open_var.
  apply* fv_subst.
  (* case: castdn *)
  apply~ (@t_castdn). apply* reduct_red_out.
  (* case: trm_sigma *)
  apply_fresh* (@t_sigma) as y.
  cross; auto. apply_ih_map_bind* H0.
  (* case: trm_pair *)
  apply* t_pair.
  apply* t_pair_v.
  rewrite* <- subst_open. apply* value_red_out.
  (* case: trm_snd *)
  apply* t_snd_v. apply* value_red_out.
  instantiate(1:=L \u \{x} \u fv v \u fv B).
  intros. lets: m x5 __. auto.
  rewrite* subst_open_var.
  apply* fv_subst.
  (* case: wf nil *)
  false (empty_middle_inv H).
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
Qed.

Lemma typing_abs_inv : forall E V t P,
  typing E (trm_abs V t) P -> exists T L,
  P = trm_prod V T /\
  forall x, x \notin L -> typing (E & x ~ V) (t ^ x) (T ^ x).
Proof.
  intros. inductions H; eauto 4.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> t --> t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv HH; inversions HH.
  apply* IHvalue1.
  apply* IHvalue2.
  apply* IHvalue.
  apply* IHvalue1.
  apply* IHvalue2.
Qed.

Lemma reduct_determ : forall t t1 t2,
    reduct t t1 -> reduct t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv HH; inversions HH;
  match goal with
  | [ H : trm_abs _ _ --> _ |- _ ] => inversion H
  | [ H : trm_mu _ _ --> _ |- _ ] =>
    inversion H; pick_fresh z;
      match goal with
      | [ H1: forall x, x \notin ?L -> value (?e ^ x),
          H2 : forall y, y \notin ?L0 -> ?e ^ y --> _ ^ _ |- _] =>
          lets P1: H1 z __; auto;
            lets P2: H2 z __; auto;
              false (value_cannot_red P1 P2)   
      end
  | [ H : trm_pair ?e1 ?e2 ?A ?B --> _ |- _] => asserts~ Val: (value (trm_pair e1 e2 A B)); false* (@value_cannot_red (trm_pair e1 e2 A B))                                               
  | [ H1 : value ?e2, H2 : ?e2 --> _ |- _ ] => false* (@value_cannot_red e2)
  | _ => fequal~
  end.
  inversions H1. false* (@value_cannot_red t2).
  inversions H1.
  inversions H1. false* (@value_cannot_red v1).
  false* (@value_cannot_red v2).
  false* (@value_cannot_red (trm_castup A v)).
  fequals~.
  fequals~.
  fequals~.
  inversions H3.
  fequals~.
  inversions H2. false* (@value_cannot_red v1).
    false* (@value_cannot_red v2).
  pick_fresh z. false* (@closed_not_open z B).
  pick_fresh z. false* (@closed_not_open z B).
  pick_fresh z. false* (@closed_not_open z B).
  pick_fresh z. false* (@closed_not_open z B).
Qed.

Lemma typing_castup_inv : forall E t U V,
    typing E (trm_castup U t) V -> exists T s,
      typing E U s /\ typing E t T
      /\ U = V /\ U --> T.
Proof.
  introv Typ. gen_eq u: (trm_castup U t).
  induction Typ; intros; subst; tryfalse.
  inversions EQu. exists* A.
Qed.

Lemma typing_preserve : forall E t t' T,
  typing E t T -> t --> t' -> typing E t' T.
Proof.
  introv Typ. gen t'.
  induction Typ; introv Red; inversions Red;
    try solve [eauto 4].

  destruct (typing_abs_inv Typ1) as [T' [L1 [EQ Typt2]]].
  inversions EQ.
  pick_fresh x.
  rewrite~ (@subst_intro x e).
  asserts P: (T' = T' ^ x).
  apply* open_rec_term.
  asserts Q: ([x~>e2] T' = T').
  apply* subst_fresh.
  rewrite <- Q. clear Q.
  rewrite P. clear P.
  apply_empty~ (@typing_substitution A0).

  inversions Typ1.
  pick_fresh x.
  asserts Q: (B ^^ e2 = B).
  rewrite~ (@subst_intro x B).
  asserts P: (B = B ^ x).
  apply* open_rec_term.
  rewrite <- P.
  apply* subst_fresh.
  rewrite <- Q at 2.
  apply* t_app. rewrite Q.
  gen_eq N: ((trm_prod A B)).
  intros.
  gen_eq M: (trm_mu N v1).
  intros.
  rewrite~ (@subst_intro x v1).
  asserts P: (N = N ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] N = N).
  apply* subst_fresh. rewrite EQN. simpl; auto.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution N).
  subst. apply_fresh* t_mu as y.
  subst; auto.
  apply_fresh* value_mu as z.
  subst; auto.

  destruct (typing_abs_inv Typ1) as [T' [L1 [EQ Typt2]]].
  inversions EQ.
  pick_fresh x.
  rewrite~ (@subst_intro x e0).
  rewrite~ (@subst_intro x T').
  apply_empty~ (@typing_substitution A0).

  inversions Typ1.
  gen_eq N: ((trm_prod A B)). intros.
  gen_eq M: (trm_mu N v1). intros.
  apply* t_app_v. rewrite <- EQN.
  pick_fresh x.
  rewrite~ (@subst_intro x v1).
  asserts P: (N = N ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] N = N).
  apply* subst_fresh.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution N).
  subst. apply_fresh* t_mu as y.
  subst; auto.
  apply_fresh* value_mu as z.
  subst; auto.
  
  false* (@value_cannot_red v).

  destruct (regular_typing Typ2) as (_&_&_&_&W).
  inversions W.
  asserts* V: (value (trm_abs A B)).
  apply* t_castup_pair.

  destruct (regular_typing Typ2) as (_&_&_&_&W).
  inversions W.
  asserts* V: (value (trm_abs A B)).
  apply* t_pair_v.

  inversions Typ.
  apply t_castdn with (A := A).
  gen_eq M: (trm_mu A v). intros.
  pick_fresh x.
  rewrite~ (@subst_intro x v).
  asserts P: (A = A ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] A = A).
  apply* subst_fresh.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution A).
  subst. apply_fresh* t_mu as y.
  subst; auto.
  apply_fresh* value_mu as z.
  subst; auto. auto.

  destruct (typing_castup_inv Typ) as [T' [s [TypU [Typt [Eq1 Red1]]]]].
  subst. pose (reduct_determ H Red1).
  rewrite e. auto.

  inversions Typ.
  inversions H. false* (@value_cannot_red e3). inversions H11.
  lets: reduct_determ H11 H10. subst.
  apply* t_castup_app.

  inversions Typ.
  inversions H.
  apply* t_app_v. inversions H10.
  false* (@value_cannot_red v2).

  false* (@value_cannot_red v).
  inversions~ Typ.

  inversions Typ.
  apply t_fst with (B := B).
  gen_eq M: (trm_mu (trm_sigma A B) v). intros.
  pick_fresh x.
  rewrite~ (@subst_intro x v).
  asserts P: (A = A ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] (trm_sigma A B) = (trm_sigma A B)).
  apply* subst_fresh. simpls~.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution (trm_sigma A B)).
  subst. apply_fresh* t_mu as y.
  subst; auto.
  apply_fresh* value_mu as z.
  subst; auto.

  inversions~ Typ.
  pick_fresh x.
  asserts Q: (B ^^ v1 = B).
  rewrite~ (@subst_intro x B).
  asserts P: (B = B ^ x).
  apply* open_rec_term.
  rewrite <- P.
  apply* subst_fresh.
  rewrite <- Q. auto.

  inversions Typ. pick_fresh z. false* (@closed_not_open z B).
  pick_fresh z. false* (@closed_not_open z B).

  inversions Typ.
  apply t_snd with (A := A).
  gen_eq M: (trm_mu (trm_sigma A B) v). intros.
  pick_fresh x.
  rewrite~ (@subst_intro x v).
  asserts P: (A = A ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] (trm_sigma A B) = (trm_sigma A B)).
  apply* subst_fresh. simpls~.
  rewrite <- Q1.
  apply_empty~ (@typing_substitution (trm_sigma A B)).
  subst. apply_fresh* t_mu as y.
  subst; auto. 
  apply_fresh* value_mu as z.
  subst; auto. auto.

  inversions Typ. pick_fresh z. false* (@closed_not_open z B).
  false* (@value_cannot_red v).

  inversions Typ1. pick_fresh z. false* (@closed_not_open z B).
  pick_fresh z. false* (@closed_not_open z B).

  inversions keep Typ1.
  pick_fresh z. false* (@closed_not_open z B).
  inversions keep H14.
  apply* t_castup.

  inversions Typ1. pick_fresh z. false* (@closed_not_open z B).
  inversions keep Typ1.
  gen_eq M: (trm_mu (trm_sigma A B) v0). intros.
  apply t_castup with (s := s) (A := (trm_app (trm_abs A B) (trm_fst (v0 ^^ M)))).
  apply* t_app.
  apply_fresh* t_snd_v as y.
  pick_fresh x.
  rewrite~ (@subst_intro x v0).
  asserts P: (A = A ^ x).
  apply* open_rec_term.
  asserts Q1: ([x~>M] A = A).
  apply* subst_fresh.
  asserts Q2: ([x~>M] B = B).
  apply* subst_fresh.
  rewrite <- Q1.
  rewrite <- Q2.
  asserts Q3: (trm_sigma ([x ~> M] A) ([x ~> M] B) = [x ~> M] trm_sigma A B).
  simpls~.
  rewrite Q3.
  apply_empty~ (@typing_substitution (trm_sigma A B)).
  pick_fresh x.
  rewrite~ (@subst_intro x v0).
  apply* value_red_out.
  subst; eauto 5.
Qed.

Lemma typing_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  exists s, typing E U (trm_sort s).
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     exists s.
     apply_empty* typing_weaken.
    destruct (wf_push_inv W).
      destruct~ IHE. exists x1.
      apply_empty* typing_weaken.
Qed.

Lemma typing_prod_inv : forall E U T A,
  typing E (trm_prod U T) A -> 
  exists L s1 s2, typing E U (trm_sort s1) /\
  forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_prod U T).
  induction Typ; intros; subst; try solve [false | eauto 4].
  inversions EQP1. exists*.
Qed.

Lemma fv_open_term_rev : forall B x y e1,
  x \notin fv (B ^^ e1) ->
  x <> y -> x \notin fv (B ^ y).
Proof.
  intros B. unfold open_trm_wrt_trm. generalize 0. 
  induction B; simpl; intros; try (notin_simpl); autos*.
  case_if~. simpl. auto.
Qed.
  
Lemma typing_fresh' : forall x G e A,
  typing G e A -> x # G -> x \notin fv A.
Proof.
  intros. induction H; simpls; eauto 4.
  apply* notin_fv_from_binds.
  notin_simpl. apply* typing_fresh.
  pick_fresh y. apply* (@fv_open_var y).
  lets: IHtyping1 H0. auto.
  apply* fv_open_term. apply* typing_fresh.
  apply* typing_fresh.
  apply* typing_fresh.
  notin_simpl; autos.
  lets: IHtyping1 __. auto. auto. 
  apply* typing_fresh.
  notin_simpl; autos.
  lets: IHtyping2 __. auto. auto. 
  notin_simpl; autos.
  lets: IHtyping1 __. auto. auto. 
  apply* typing_fresh.
  apply reduct_fresh with (A := A). auto. auto.
  notin_simpl; autos.
  lets: IHtyping2 H0.
  pick_fresh y. apply* (@fv_open_var y).
  apply* fv_open_term_rev.
  lets: IHtyping __. auto. auto.
  lets: IHtyping __. auto. auto.
  lets: IHtyping1 __. auto.
  notin_simpl; auto.
  apply* typing_fresh.
Qed.

Lemma typing_strengthen : forall F E x A t T,
  typing (E & x ~ A & F) t T ->
  x \notin fv t ->
  wf (E & F) ->
  typing (E & F) t T.
Proof.
  introv Typ N W.
  gen_eq G: (E & x ~ A & F). gen F.
  induction Typ; intros; subst; simpls; eauto 4.
  rewrite notin_singleton in *.
  destruct (binds_middle_inv H0) as [? | [? | ?]].
  apply~ t_var.
  destructs H1. false.
  destructs H1. apply~ t_var.
  (* case: trm_abs *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh* (@t_abs) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  asserts TypS: (typing (E & F & y ~ A0) (e ^ y) (B ^ y)).
  apply_ih_bind* H0; apply* fv_close_var.
  apply_ih_bind* H2.
  apply (typing_fresh' TypS).
  lets W1: H y __. auto.
  lets W2: proj1 (regular_typing W1).
  lets W3: wf_left W2.
  lets W4: ok_from_wf W3.
  destructs (ok_middle_inv W4).
  auto.
  (* case: trm_prod *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh~ (@t_prod) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  auto.
  (* case: trm_mu *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh~ (@t_mu) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  (* case: trm_castup_app *)
  apply* (@t_castup_app).
  instantiate(1:=s).
  apply* IHTyp3.
  lets P: IHTyp1 __. auto. 
  lets P1: P F W __. auto.
  apply (typing_fresh' P1).
  lets W2: proj1 (regular_typing Typ1).
  lets W4: ok_from_wf W2.
  destructs (ok_middle_inv W4). auto.
  apply* (@t_castup_pair).
  apply* (@t_castup_app_con).
  instantiate(1:=s).
  apply* IHTyp4.
  lets P: IHTyp1 __. auto. 
  lets P1: P F W __. auto.
  apply (typing_fresh' P1).
  lets W2: proj1 (regular_typing Typ1).
  lets W4: ok_from_wf W2.
  destructs (ok_middle_inv W4). auto.
  (* case: trm_sigma *)
  lets Q: IHTyp __. auto.
  lets: (Q F W eq_refl). clear Q.
  apply_fresh~ (@t_sigma) as y.
  apply_ih_bind* H0; apply* fv_close_var.
  auto.
  (* case: trm_pair *)
  apply~ (@t_pair).
  apply~ (@t_pair_v).
  (* case: trm_snd *)
  apply_fresh* (@t_snd_v) as z.
  instantiate(1:=s).
  apply* IHTyp2.
  lets P: IHTyp1 N F W __. auto.
  apply (typing_fresh' P).
  lets W2: proj1 (regular_typing Typ1).
  lets W4: ok_from_wf W2.
  destructs (ok_middle_inv W4).
  auto.
Qed.

Lemma typing_sigma_inv : forall E U T A,
  typing E (trm_sigma U T) A -> 
  exists L s1 s2, typing E U (trm_sort s1) /\
  forall x, x \notin L -> typing (E & x ~ U) (T ^ x) (trm_sort s2).
Proof.
  introv Typ. gen_eq P1: (trm_sigma U T).
  induction Typ; intros; subst; try solve [false | eauto 4].
  inversions EQP1. exists*.
Qed.

Lemma typing_wf_from_typing : forall E t T,
  typing E t T ->
  exists s, T = trm_sort s \/ typing E T (trm_sort s).
Proof.
  intros. induction H; eauto 3.
  destruct (typing_wf_from_context H0 H) as [s ?].
  exists* s.
  destruct IHtyping as [s [? | ?]].
  inversions H5. exists s3. right*.
  exists s3. right*.
  destruct IHtyping1 as [s [? | ?]].
  false*.
  destruct (typing_prod_inv H2) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. lets: TypT x __. auto.
  asserts E: (B = B ^ x).
  apply* open_rec_term.
  exists s1.
  rewrite E. right. apply_empty* typing_strengthen.
  rewrite <- E. auto.
  destruct IHtyping1 as [s [? | ?]]. false.
  destruct (typing_prod_inv H2) as [TypU [L [s1 [s2 TypT]]]].
  pick_fresh x. rewrite~ (@subst_intro x).
  exists s1. right.
  unsimpl ([x ~> v](trm_sort s1)).
  apply_empty* (@typing_substitution A).
  
  destruct IHtyping1 as [s' [? | ?]]. false.
  exists s. right.
  apply* t_app.
  destruct IHtyping1 as [s' [? | ?]]. false.
  exists s. right.
  apply* t_app.

  destruct IHtyping as [s [? | ?]].
  subst. inversions H0.
  exists s. right.
  apply (typing_preserve H1). auto.
  destruct IHtyping as [s [? | ?]].
  false*. 
  destruct (typing_sigma_inv H0) as [TypU [L [s1 [s2 TypT]]]].
  exists L. right~.
  destruct IHtyping as [s [? | ?]].
  false*. 
  inversions H1.
  exists s2. right~. 
  pick_fresh x. lets: H7 x __. auto.
  asserts E: (B = B ^ x).
  apply* open_rec_term.
  rewrite E. apply_empty* typing_strengthen.
  rewrite <- E. auto.
  destruct IHtyping1 as [s' [? | ?]].
  false*.
  destruct (typing_sigma_inv H3) as [L' [s1 [s2 [TypU TypT]]]].
  exists s. right.
  apply t_app with (A := A). auto.
  apply t_fst with (B := B). auto. auto.
Qed.

Lemma typing_progress : forall t T, 
  typing empty t T ->
     value t 
  \/ exists t', t --> t'.
Proof.
  introv Typ. lets Typ': Typ. inductions Typ.
  left*.
  false* binds_empty_inv. 
  left*.
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1.
      false* binds_empty_inv. 
      destruct~ IHTyp2. exists* (e ^^ e2).
      destruct H0 as [t' ?].
      exists* (trm_app (trm_abs A e) t').
      destruct~ IHTyp2. exists* (trm_app (v ^^ trm_mu (trm_prod A B) v) e2).
      destruct H3 as [t' ?].
      exists* (trm_app (trm_mu (trm_prod A B) v) t').
      inversions H2.
      exists* (trm_app t1' e2).
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
      inversions Typ1; inversions Val1.
      false* binds_empty_inv. 
      destruct~ IHTyp2. exists* (e0 ^^ v).
      destruct H0 as [t' ?]. false* (@value_cannot_red v).
      destruct~ IHTyp2. exists* (trm_app (v0 ^^ trm_mu (trm_prod A B) v0) v).
      destruct H3 as [t' ?]. false* (@value_cannot_red v).
      inversions H2. 
      exists* (trm_app t1' v).
  left*.
  left*.
  destruct~ IHTyp2 as [Val2 | [t2' Red2]].
  right. exists* (trm_castup B t2').
  destruct~ IHTyp1 as [Val1 | [t1' Red1]].
  destruct~ IHTyp2 as [Val2 | [t2' Red2]].
  right. exists* (trm_castup_con e2  e1 t2').
  right. exists* (trm_castup_app t1' e2).
  destruct (regular_typing Typ') as (_&_&_&W&_).
  inversions W.
  right. destruct~ IHTyp1 as [Val1 | [t1' Red1]].
    exists* (trm_pair e1 (trm_castdn e2) A B).
    exists* (trm_castup_pair t1' (trm_castdn e2) A B).
  left*.
  right. destruct~ IHTyp as [Val1 | [t1' Red1]].
      inversions Typ; inversions Val1; try solve [inversion H].
      false* binds_empty_inv. 
      exists* (trm_castdn (v ^^ trm_mu A v)).
      exists* e0.
      exists* (trm_app e1 e2).
      exists* (trm_castup_app e1 e2).
      exists* (trm_castdn t1').
  left*.
  destruct~ IHTyp1; destruct~ IHTyp2.
      destruct H0 as [e2' P]. right. exists~ (trm_pair e1 e2' A B).
      destruct H as [e1' P]. right. exists~ (trm_pair e1' e2 A B).
      destruct H as [e1' P]. right. exists~ (trm_pair e1' e2 A B).
  destruct~ IHTyp2.
      destruct H0 as [e2' P]. right. exists~ (trm_pair v e2' A B).
  destruct~ IHTyp as [Val | [e' Red]].
      inductions Val; inversions Typ.
      false* binds_empty_inv.
      right. exists* (trm_fst (v ^^ (trm_mu (trm_sigma A B) v))).
      right. exists~ v1.
      right. exists~ v1.
      inversions H6.
      right. exists~ (trm_fst e').
  destruct~ IHTyp as [Val | [e' Red]].
      inductions Val; inversions Typ.
      false* binds_empty_inv.
      right. exists* (trm_snd (v ^^ (trm_mu (trm_sigma A B) v))).
      right. exists~ v2.
      right. exists~ v2.
      inversions H7.
      right. exists~ (trm_snd e').
  destruct~ IHTyp1 as [Val | [e' Red]].
      inductions Val; inversions Typ1.
      false* binds_empty_inv.
      right. exists~ (trm_castup (trm_app  ( (trm_abs A B) )   ( (trm_fst  ( (trm_mu  ( (trm_sigma A B) )  v) ) ) ) )  ( (trm_snd  (  (open_trm_wrt_trm  v   (trm_mu  ( (trm_sigma A B) )  v) )  ) ) ) ).
      apply_fresh* r_proj2_v_mu as z.
      false. pick_fresh z. false* (@closed_not_open z B).
      right. exists* ((trm_castup (trm_app  ( (trm_abs A B) )   ( (trm_fst  ( (trm_pair v1 v2 A B) ) ) ) )  ( (trm_castup (trm_app  ( (trm_abs A B) )  v1) v2) ) )).
      inversions H8.
      right. exists~ (trm_snd e').
Qed.

