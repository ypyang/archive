Set Implicit Arguments.
Require Import LibLN.
Require Import LamI_CBV_ott LamI_CBV_Infra LamI_CBV_Proofs.
Require LibReflect.
Implicit Types x : var.

Definition decidable (P : Prop) := (P \/ ~ P).

(* ********************************************************************** *)
(** Lemmas of decidability *)

(** Equality of Vars *)
Lemma var_dec : forall (x y : var),
  decidable (x = y).
Proof.
  intros.
  tests EQ: (x = y).
  left~. right~.
Qed.

(** Equality of terms *)
Lemma eq_typ_dec : forall (T T' : trm),
  sumbool (T = T') (T <> T').
Proof.
  decide equality.
  decide equality.
  tests EQ: (x5 = x0); auto.
  induction s; induction s0; decide equality.
  decide equality.
Qed.

(** Bindings lookup *)
Lemma binds_lookup_dec : forall (x : var) (E : env),
    decidable (exists a, binds x a E).
Proof.
  intros. destruct (LibReflect.decidable_sumbool (indom_decidable E x)).
  left. destruct (get_some m). exists x0. unfold binds. auto.
  right. intros [a P]. unfold binds in P. pose (get_some_inv P). auto.
Qed.

(* ********************************************************************** *)
(** Typing is unique for singly sorted PTS *)

Lemma typing_unique : forall E e T1 T2,
  typing E e T1 ->
  typing E e T2 ->
  T1 = T2.
Proof.
  intros E e T1 T2 Typ1 Typ2.
  gen T2.
  induction Typ1; intros T' Typ'; inversion Typ'; subst; eauto 2.
  lets: AxUniq H0 H4. fequal~.
  eapply binds_func; eauto.
  f_equal; eauto.
  pick_fresh x.
  apply open_var_inj with (x := x); eauto.
  lets W: IHTyp1_1 H2. inversions~ W.
  lets W: IHTyp1_1 H2. inversions~ W.
  lets: IHTyp1_1 H2. inversions~ H0.
  lets: IHTyp1_1 H2. inversions~ H0.
  asserts* W1: (trm_sort s1 = trm_sort s4).
  pick_fresh y. lets: H0 y __. auto.
  asserts* W2: (trm_sort s2 = trm_sort s5).
  inversions W1. inversions W2.
  lets: RelUniq H1 H8. fequal~.
  lets: IHTyp1_1 H2. inversions~ H0.
  lets W: IHTyp1_1 H4. inversions~ W.
  pose (IHTyp1 _ H1).
  rewrite <- e0 in H3.
  pose (reduct_determ H H3). auto.
  asserts* W1: (trm_sort s1 = trm_sort s4).
  pick_fresh y. lets: H0 y __. auto.
  asserts* W2: (trm_sort s2 = trm_sort s5).
  inversions W1. inversions W2.
  lets: SigUniq H1 H8. fequal~.
  lets W: IHTyp1 H1. inversions~ W.
  lets W: IHTyp1 H1. inversions~ W.
  lets W: IHTyp1 H1. inversions~ W.
    pick_fresh z. false* (@closed_not_open z B0).
  lets W: IHTyp1_1 H2. inversions~ W.
    pick_fresh z. false* (@closed_not_open z T').
  lets W: IHTyp1_1 H2. inversions~ W.
Qed.

(** A term is a value *)
Lemma value_dec : forall E t T,
    typing E t T ->
    decidable (value t).
Proof.
  introv Typ. lets Typ': Typ. 
  inductions Typ; try solve [left* | right*; intros P; inversion P].
  destruct~ IHTyp2.
  left*. right; intros P; inversion P. false.
  destruct~ IHTyp1; destruct~ IHTyp2; try solve [
  left* | right; intros P; inversion P; false].
  destruct~ IHTyp1; destruct~ IHTyp2; try solve [
  left* | right; intros P; inversion P; false].
  destruct~ IHTyp1; destruct~ IHTyp2; try solve [
  left* | right; intros P; inversion P; false].
Qed.

(** A term can be reduced *)
Lemma reduct_dec : forall E t T,
    typing E t T ->
    decidable (exists t', t --> t').
Proof.
  introv Typ. lets Typ': Typ. unfold decidable.
  inductions Typ; try solve [right; intros [S J]; inversion J].
  
  destruct (value_dec Typ1) as [Val | Q].
  inversions Typ1; inversions Val. 
  destruct~ IHTyp1. destruct H2. inversion H2.
  destruct~ IHTyp2. left. destruct H3 as [t' ?].
  exists* (trm_app (trm_var_f x5) t').
  right. unfold not in *. intros.
  inversion H4 as [t' R]. inversions R.
  inversion H9. apply* H3.
  destruct~ IHTyp2.
  destruct H0 as [t' ?]. left.
  exists* (trm_app (trm_abs A e) t').
  destruct (value_dec Typ2) as [Val | Q].
  left. exists* (e ^^ e2).
  right. unfold not in *. intros.
  destruct H1 as [t' ?].
  inversions H1. false. inversion H12. apply H0. exists* e'.

  destruct (value_dec Typ2) as [Val | Q].
  left. exists* (trm_app (v ^^ trm_mu (trm_prod A B) v) e2).
  destruct~ IHTyp2.
  destruct H3 as [t' ?].
  left. exists* (trm_app (trm_mu (trm_prod A B) v) t').
  right. unfold not in *. intros.
  destruct H4 as [t' ?]. inversions H4. false. inversion H11.
  apply* H3.

  inversions H2.
  destruct~ IHTyp1.
  destruct H0 as [e1' ?]. left. exists* (trm_app e1' e2).
  right. unfold not in *. intros. destruct H1 as [t' ?].
  inversions H1; try solve [apply* Q]. apply* H0.

  destruct (value_dec Typ1) as [Val | Q].
  destruct~ IHTyp2. false.
  destruct H0 as [e2' ?]. apply* (@value_cannot_red v).
  inversions Typ1; inversions Val. 
  destruct~ IHTyp1. destruct H3. inversion H3.
  right. unfold not in *. intros.
  inversion H4 as [t' R]. inversions R.
  inversion H9. apply* H0.
  left. exists* (e0 ^^ v).
  left. exists* (trm_app (v0 ^^ trm_mu (trm_prod A B) v0) v).
  inversions H3.
  destruct~ IHTyp1.
  destruct H0 as [e1' ?].
  left. exists* (trm_app e1' v).
  right. unfold not in *. intros. destruct H1 as [t' ?].
  inversions H1; try solve [apply* Q]. apply* H0.

  destruct (value_dec Typ2) as [Val | Q].
  right*. intros [P Q]. inversions Q. apply* (@value_cannot_red e).
  destruct~ IHTyp2. destruct H0 as [e' ?].
  left. exists* (trm_castup B e').
  right. unfold not in *. intros. destruct H1 as [t' ?]. inversions H1.
  apply* H0.

  destruct (value_dec Typ1) as [Val | Q].
  destruct (IHTyp2 Typ2) as [[e2' HL2] | HR2];
  try solve [left*]. right. intros [t' M]. inversions M.
  apply* (@value_cannot_red e1). false*.
  destruct (IHTyp1 Typ1) as [[e1' HL1] | HR1].
  left*. right. intros [t' M]. inversions M; false*.

  destruct (regular_typing Typ') as (_&_&_&W&_).
  inversions W.
  destruct (IHTyp1 Typ1) as [[e1' HL1] | HR1]. left*.
  destruct (value_dec Typ1) as [Val | Q].
  left. eexists. apply* r_castup_pair_elim.
  right. intros [t' M]. inversions M; false*.

  destruct (IHTyp Typ) as [[t' L] | R].
  left. exists* (trm_castdn t').
  destruct (value_dec Typ) as [Val | Q].
  inversions Val; inversions Typ; try solve [inversion H].
  right. intros [t' P]. inversions P. destruct R. inversions H2.
  left. exists* (trm_castdn (v ^^ trm_mu A v)).
  left. exists* v.
  left*. left*.
  right. intros [t' P]. inversions P; false*.

  destruct (value_dec Typ1) as [Val | Q].
  destruct (IHTyp2 Typ2) as [[e2' HL2] | HR2];
  try solve [left*]. right. intros [t' M]. inversions M.
  apply* (@value_cannot_red e1). false*.
  destruct (IHTyp1 Typ1) as [[e1' HL1] | HR1].
  left*. right. intros [t' M]. inversions M; false*.

  destruct (IHTyp2 Typ2) as [[e2' HL2] | HR2];
  try solve [left*]. 
  destruct (value_dec Typ2) as [Val | Q].
  right. intros [t' M]. inversions M; false*.
  apply* (@value_cannot_red v).
  right. intros [t' M]. inversions M; false*.
  apply* (@value_cannot_red v).
  
  destruct (IHTyp Typ) as [[t' L] | R].
  left*.
  destruct (value_dec Typ) as [Val | Q].
  inversions Val; inversions Typ; try solve [inversion H].
  right. intros [t' P]. inversions P. destruct R. inversions H1.
  left. eexists. apply* r_proj1_mu.
  left*. left*. inversions H7.
  right. intros [t' P]. inversions P; false*.

  destruct (IHTyp Typ) as [[t' L] | R].
  left*.
  destruct (value_dec Typ) as [Val | Q].
  inversions Val; inversions Typ; try solve [inversion H].
  right. intros [t' P]. inversions P. destruct R. inversions H2.
  left. eexists. apply* r_proj2_mu.
  left*. left*. inversions H8.
  right. intros [t' P]. inversions P; false*.

  inversions H; inversions Typ1.
  right. intros [t' P]. inversions P. inversions H2.
  left. eexists. apply_fresh* r_proj2_v_mu as z.
  left*. left*. inversions H8.
Qed.

(* ********************************************************************** *)
(** Lemmas of typing renaming *)

Lemma typing_rename_eq : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  typing (E & (x ~ T1)) (e ^ x) T2 ->
  typing (E & (y ~ T1)) (e ^ y) T2.
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  destruct (var_dec x y) as [EQ | NEQ].
  subst; eauto.
  assert (wf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  assert ([x ~> trm_var_f y] T2 = T2).
  apply* subst_fresh.
  rewrite <- H0.
  apply_empty* (@typing_substitution T1).
  apply~ (@typing_weaken).
  apply wf_cons with (s := s); auto.
  apply_empty~ typing_weaken.
  apply wf_cons with (s := s); auto.
Qed.

Lemma typing_rename : forall x y E e T1 T2,
  x \notin (fv e \u fv T2) -> y \notin (dom E \u fv e) ->
  typing (E & (x ~ T1)) (e ^ x) (T2 ^ x) ->
  typing (E & (y ~ T1)) (e ^ y) (T2 ^ y).
Proof.
  intros x y E e T1 T2 Fr1 Fr2 H.
  destruct (var_dec x y) as [EQ | NEQ].
  subst; eauto.
  assert (wf (E & x ~ T1)) by auto.
  inversions H0. elimtype False. apply* empty_push_inv.
  destruct (eq_push_inv H1) as [Eq1 [Eq2 Eq3]].
  subst.
  rewrite* (@subst_intro x e).
  rewrite* (@subst_intro x T2).
  apply_empty* (@typing_substitution T1).
  apply~ (@typing_weaken).
  apply wf_cons with (s := s); auto.
  apply_empty~ typing_weaken.
  apply wf_cons with (s := s); auto.
Qed.

Lemma value_rename : forall x y e,
  x \notin (fv e) -> y \notin (fv e) ->
  value (e ^ x) ->
  value (e ^ y).
Proof.
  intros.
  destruct (var_dec x y) as [EQ | NEQ].
  subst; eauto.
  rewrite* (@subst_intro x e).
  apply* value_red_out.
Qed.

Lemma lc_trm_decidable : forall A x,
  lc_trm (A ^ x) -> decidable (lc_trm A).
Proof.
  intros.
  destruct (eq_typ_dec A (A ^ x)) as [EQ | NEQ].
  left. rewrite~ EQ.
  right. unfold not in *. intros.
  apply NEQ.
  apply* open_rec_term.
Qed.

Lemma is_sort_dec : forall A,
  decidable (exists s, A = trm_sort s).
Proof.
  intros.
  inductions A; try solve [right; intros [K P]; inversion P]. 
  left*.
Qed.

Lemma decidable_in_fv : forall x e,
  decidable (x \in fv e).
Proof.
  intros. inductions e; simpls~; try solve [
  destruct IHe1; destruct IHe2;
    try solve [left; rewrite~ in_union |
    right; intros P; rewrite in_union in P;
    destruct~ P]].
  right. unfold not. intros. apply* in_empty_elim. 
  destruct (var_dec x5 x) as [EQ | NEQ]. subst.
    left. apply in_singleton_self.
    right. intros_all. rewrite (in_singleton x x5) in H. false.
  right. apply* in_empty_elim.
  destruct IHe1; destruct IHe2;
  destruct IHe3; destruct IHe4;
    try solve [left; rewrite~ in_union |
    left; (do_rew_all in_union auto); auto |
    right; intros P; rewrite in_union in P;
    destruct~ P].
  right. intros P. rewrite in_union in P.
  rewrite in_union in P. rewrite in_union in P.
  destruct P as [? | [? | [? | ?]]]; autos*.

  destruct IHe1; destruct IHe2;
  destruct IHe3; destruct IHe4;
    try solve [left; rewrite~ in_union |
    left; (do_rew_all in_union auto); auto |
    right; intros P; rewrite in_union in P;
    destruct~ P].
  right. intros P. rewrite in_union in P.
  rewrite in_union in P. rewrite in_union in P.
  destruct P as [? | [? | [? | ?]]]; autos*.

  destruct IHe1; destruct IHe2;
  destruct IHe3;
    try solve [left; rewrite~ in_union |
    left; (do_rew_all in_union auto); auto |
    right; intros P; rewrite in_union in P;
    destruct~ P].
  right. intros P. rewrite in_union in P.
  rewrite in_union in P. 
  destruct P as [? | [? | ?]]; autos*.
Qed.

Lemma fv_notin_self : forall x e,
  x \notin fv(e ^ x) ->
  e ^ x = e.
Proof.
  unfolds open_trm_wrt_trm. generalize 0. intros n x e. gen n.
  induction e; intros; simpls; try solve [eauto | fequal*].
  case_if~. simpls. rewrite notin_singleton in H. false.
Qed.

Lemma fv_in_from_notin : forall x e,
  (x \notin fv e -> False) -> x \in fv e.
Proof.
  intros. unfolds notin. unfolds not.
  destruct~ (decidable_in_fv x e).
  false~.
Qed.

(* ********************************************************************** *)
(** Type checking is decidable *)

Lemma typing_decidable : forall E e,
  lc_trm e -> wf E -> decidable (exists T, typing E e T).
Proof.
  intros E e Trm Wf. gen E.
  induction Trm; intros E Wf.

  (* Case Var *)
  destruct (@binds_lookup_dec x5 E) as [[T H] | H].
  left; eauto. right. intros [T J]. inversion J; subst; eauto.

  (* Case Ax *)
  destruct (AxDec s) as [[s' H] | H].
  left. exists* (trm_sort s').
  unfold not in *. right. unfold not; intros. destruct H0 as [T H1].
  inversions H1. apply~ H. exists* s2.
  
  (* Case App *)
  destruct (IHTrm1 E Wf) as [[T H] | H]; eauto.
  destruct (IHTrm2 E Wf) as [[S J] | J]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_prod ?P1 ?P2) |-
        decidable (exists Q, typing ?E (trm_app ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_app ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E t1 (trm_prod ?U ?T) |- _] =>
          assert (K: P = trm_prod U T); eauto using typing_unique;
            inversion K
        end
    end.
  
    destruct (value_dec J).
    destruct (eq_typ_dec T1 S). subst. left; eauto.
    right. intros [S' J'].
    inversion J'; subst.
    lets: typing_unique J H5.
    lets: typing_unique H H3.
    inversion H2; subst; eauto using typing_unique.
    lets: typing_unique J H5.
    lets: typing_unique H H3.
    inversion H2; subst; eauto using typing_unique.

    asserts* M: (lc_trm (trm_prod T1 T2)).
    inversions M. pick_fresh y. lets M2: H4 y __. auto.
    clear H3 H4.
    destruct (lc_trm_decidable _ _ M2).
    destruct (eq_typ_dec T1 S). subst. left; eauto.
    right. intros [S' J'].
    inversion J'; subst.
    lets: typing_unique J H6.
    lets: typing_unique H H4.
    inversion H3; subst; eauto using typing_unique.
    lets: typing_unique J H6.
    lets: typing_unique H H4.
    inversion H3; subst; eauto using typing_unique.

    right. intros [S' J']. inversions J'.
    lets: typing_unique H H4.
    inversion H2; subst. false.
    false.
    
    right. intros [S' J']. inversion J'; subst; eauto.
    right. intros [S' J']. inversion J'; subst; eauto.

  (* Case Lam *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_abs ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_abs ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E t1 (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end.

    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    destruct (typing_wf_from_typing J) as [s2 [K | K]].
    subst.
    destruct (AxDec s2) as [[s3 P] | P].
    destruct (RelDec s s3) as [[s4 Q] | Q].
    left. exists* (trm_prod A (trm_sort s2)).
    apply_fresh* t_abs as y.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W. apply_empty* typing_weaken.
    
    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ Q. exists s4.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & y ~ A) (e ^ y) (trm_sort s2 ^ y)).
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    lets: typing_unique H1 M. rewrite H4 in H3.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W in H3. inversions H3.
    lets: AxUniq P H13. subst~.

    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ P. exists s0.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & y ~ A) (e ^ y) (trm_sort s2 ^ y)).
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    lets: typing_unique H1 M. rewrite H4 in H3.
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite W in H3. inversions H3.
    auto.

    destruct (RelDec s s2) as [[s4 Q] | Q].
    left. exists (trm_prod A (close_var x S)).
    apply_fresh* t_abs as y.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    apply* close_var_fresh. rewrite* <- (@close_var_open x).
    asserts* W: (trm_sort s2 ^ y = trm_sort s2).
    rewrite <- W.
    apply* (@typing_rename x). notin_simpl; simpl; auto.
    apply* close_var_fresh. notin_simpl; simpl; auto.
    apply~ fv_open_var. rewrite* <- (@close_var_open x).
    rewrite* <- (@close_var_open x).
    
    right. unfold not in *. intros.
    destruct H1 as [T ?].
    inversions H1. apply~ Q. exists s3.
    lets: typing_unique H2 H5. inversions H1.
    pick_fresh y. lets: H6 y __. auto.
    lets: H8 y __. auto.
    asserts* M: (typing (E & x ~ A) (e ^ x) (B ^ x)).
    apply* (@typing_rename y).
    lets: typing_unique J M.
    asserts* M2: (typing (E & x ~ A) (B ^ x) (trm_sort s0)).
    apply* (@typing_rename_eq y). notin_simpl; simpl; auto.
    notin_simpl; simpl; auto.
    lets: typing_fresh x H3.
    apply* fv_open_var.
    rewrite H4 in K.
    lets: typing_unique K M2. inversions H7. auto.
    
    right. intros [S K]. inversion K; subst.
    apply J.
    exists* (B ^ x).
    pick_fresh y. lets: H5 y __. auto.
    apply* (@typing_rename y).

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s1).

  (* Case Pi *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        assert (K: P = (trm_sort s1)); eauto using typing_unique;
          inversion K
    end.
  
    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    asserts* W3: (lc_trm S).
    induction W3;
      match goal with
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) (trm_sort ?s1) |-
          decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] => auto
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) ?P |-
          decidable (exists Q, typing ?E (trm_prod ?t1 ?t2) Q) ] =>
        right; intros [S' J']; inversion J'; subst;
          match goal with
          | [ H6: forall _, _ -> typing _ _ (trm_sort ?s1) |- _ ] =>
            pick_fresh y; assert (W: (typing (E & x ~ t1) (t2 ^ x) (trm_sort s1))) by
                (apply* (@typing_rename_eq y); notin_simpl; simpl; auto);
            lets W2: typing_unique W J; inversion W2
          end
      end.

      destruct (RelDec s s0) as [[s4 Q] | Q].
      left. exists (trm_sort s4).
      apply_fresh* t_prod as y.
      apply* (@typing_rename_eq x). notin_simpl; simpl; auto.
      right. unfold not in *. intros.
      destruct H1 as [T ?].
      inversions H1. apply~ Q. exists s3.
      lets: typing_unique H2 H5. inversions H1.
      pick_fresh y. lets: H7 y __. auto.
      asserts* M2: (typing (E & x ~ A) (B ^ x) (trm_sort s2)).
      apply* (@typing_rename_eq y). notin_simpl; simpl; auto.
      lets: typing_unique J M2. inversions H3. auto.

    right. intros [S K]. inversion K; subst.
    apply J.
    exists* (trm_sort s2).
    pick_fresh y. lets: H6 y __. auto.
    apply* (@typing_rename_eq y). notin_simpl; simpl; auto.

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s1).

  (* Case Mu *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_mu ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_mu ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E S' (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end.
    
    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    destruct (eq_typ_dec A S). subst.
    destruct (value_dec J).
    left. exists S. apply_fresh~ t_mu as y.
    instantiate (1 := s). auto.
    apply~ (@typing_rename_eq x).
    apply* (@value_rename x).
    right. intros [S' K].
    inversions K. pick_fresh y.
    lets: H9 y __. auto. apply H1.
    apply* (@value_rename y).
    right. intros [S' K]. inversion K; subst.
    pick_fresh y.
    lets: H6 y __. auto.
    assert (typing (E & x ~ S') (e ^ x) S').
    apply~ (@typing_rename_eq y).
    lets: typing_unique J H3. false*.
    
    right. intros [S K]. inversion K; subst.
    apply J. exists S.
    pick_fresh y.
    lets: H6 y __. auto.
    apply~ (@typing_rename_eq y).

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s).

  (* Case Sigma *)
  destruct (IHTrm E Wf) as [[T H2] | H2]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_sigma ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_sigma ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        assert (K: P = (trm_sort s1)); eauto using typing_unique;
          inversion K
    end.
  
    pick_fresh x.
    lets W1: H0 x __. auto.
    asserts* Wf2: (wf (E & x ~ A)).
    lets W2: W1 Wf2.
    destruct W2 as [[S J]|J].
    asserts* W3: (lc_trm S).
    induction W3;
      match goal with
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) (trm_sort ?s1) |-
          decidable (exists Q, typing ?E (trm_sigma ?t1 ?t2) Q) ] => auto
      | [ H: typing (?E & ?x ~ ?t1) (?t2 ^ ?x) ?P |-
          decidable (exists Q, typing ?E (trm_sigma ?t1 ?t2) Q) ] =>
        right; intros [S' J']; inversion J'; subst;
          match goal with
          | [ H6: forall _, _ -> typing _ _ (trm_sort ?s1) |- _ ] =>
            pick_fresh y; assert (W: (typing (E & x ~ t1) (t2 ^ x) (trm_sort s1))) by
                (apply* (@typing_rename_eq y); notin_simpl; simpl; auto);
            lets W2: typing_unique W J; inversion W2
          end
      end.

      destruct (SigDec s s0) as [[s4 Q] | Q].
      left. exists (trm_sort s4).
      apply_fresh* t_sigma as y.
      apply* (@typing_rename_eq x). notin_simpl; simpl; auto.
      right. unfold not in *. intros.
      destruct H1 as [T ?].
      inversions H1. apply~ Q. exists s3.
      lets: typing_unique H2 H5. inversions H1.
      pick_fresh y. lets: H7 y __. auto.
      asserts* M2: (typing (E & x ~ A) (B ^ x) (trm_sort s2)).
      apply* (@typing_rename_eq y). notin_simpl; simpl; auto.
      lets: typing_unique J M2. inversions H3. auto.

    right. intros [S K]. inversion K; subst.
    apply J.
    exists* (trm_sort s2).
    pick_fresh y. lets: H6 y __. auto.
    apply* (@typing_rename_eq y). notin_simpl; simpl; auto.

    right. intros [S K]. inversion K; subst.
    apply H2.
    exists* (trm_sort s1).

  (* Case Pair *)
  destruct (IHTrm1 E Wf) as [[T1 H21] | F].
  destruct (IHTrm2 E Wf) as [[T2 H22] | F].
  destruct (IHTrm3 E Wf) as [[T3 H23] | F].
  destruct (eq_typ_dec T1 A) as [? | F]. subst.
  destruct (is_sort_dec T3) as [[s1 ?] | F]. subst.
  pick_fresh z.
  lets P1: H0 z __. auto.
  lets P2: P1 (E & z ~ A) __. apply* wf_cons.
  destruct P2 as [[T4 ?] | F].
  destruct (is_sort_dec T4) as [[s2 ?] | F]. subst.
  destruct (SigDec s1 s2) as [[s3 ?] | F].
  asserts Typ1: (typing E (trm_sigma A B) (trm_sort s3)).
  apply_fresh* t_sigma as z.
  apply* (@typing_rename_eq z). simpls~.
  lets P: H z __. auto.
  destruct (@lc_trm_decidable B z P).
  destruct (eq_typ_dec T2 B) as [? | F]. subst.
  left. exists* (trm_sigma A B).
  right; intros [S K]; inversions K.
    inversions H12. lets: typing_unique H22 H11. false.
    inversions H12. lets: typing_unique H22 H11.
    lets EQ: lc_open_eq B e1 H3 __. auto. rewrite EQ in H4. false.
  destruct (eq_typ_dec T2 (B ^^ e1)) as [? | F]. subst.
  destruct (value_dec H21) as [? | F].
  left. exists* (trm_sigma A B).
  right; intros [S K]; inversions K.
    inversions H12. apply* H3. false.
  right; intros [S K]; inversions K.
    apply* H3. lets: typing_unique H22 H11. false.
  right; intros [S K]; inversions K.
    inversions H10. pick_fresh x. lets M: H7 x __. auto.
     asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s4)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. inversions H2.
    lets: typing_unique H23 H6. inversions H2.
    apply* F.
    inversions H10. pick_fresh x. lets M: H8 x __. auto.
     asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s4)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. inversions H2.
    lets: typing_unique H23 H6. inversions H2.
    apply* F.
  right; intros [S K]; inversions K.
    inversions H10. pick_fresh x. lets M: H7 x __. auto.
    asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s2)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. subst. apply* F.
    inversions H10. pick_fresh x. lets M: H8 x __. auto.
    asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s2)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. subst. apply* F.
  right; intros [S K]; inversions K.
    inversions H9. pick_fresh x. lets: H6 x __. auto.
    apply* F. exists (trm_sort s2).
    apply* (@typing_rename_eq x). simpls. auto.
    inversions H9. pick_fresh x. lets: H7 x __. auto.
    apply* F. exists (trm_sort s2).
    apply* (@typing_rename_eq x). simpls. auto.
  right; intros [S K]; inversions K.
    inversions H9. lets: typing_unique H23 H5. apply* F.
    inversions H9. lets: typing_unique H23 H5. apply* F.
  right; intros [S K]; inversions K.
    lets: typing_unique H21 H7. false.
    lets: typing_unique H21 H6. false.
  right; intros [S K]; inversions K; inversions H9; apply* F.
  right; intros [S K]; inversions K; apply* F.
  right; intros [S K]; inversions K; apply* F.

  (* Case Fst *)
  destruct (IHTrm _ Wf) as [[T H] | H].
  asserts Dec: (decidable (exists A B, T = trm_sigma A B)).
    inductions T; try solve [right; intros [? [? K]]; inversions K | left*].
  destruct Dec as [[A [B EQ]] | F]. subst.
  left*. right; intros [S K]; inversions K.
  lets: typing_unique H2 H. apply* F.
  right; intros [S K]; inversions K. apply* H.
  
  (* Case Snd *)
  destruct (IHTrm _ Wf) as [[T H] | H].
  asserts Dec: (decidable (exists A B, T = trm_sigma A B)).
    inductions T; try solve [right; intros [? [? K]]; inversions K | left*].
  destruct Dec as [[A [B EQ]] | F]. subst.
  destruct (typing_wf_from_typing H) as [s [? | P]]. false.
  inversions P.
  pick_fresh z. lets: H5 z __. auto.
  asserts* LcB: (lc_trm (B ^ z)).
  destruct (lc_trm_decidable _ _ LcB).
  left. exists* B.
  destruct (value_dec H) as [? | F].
  destruct (AxDec s2) as [[s3 ?] | F].
  destruct (RelDec s1 s3) as [[s4 ?] | F].
  left. exists (trm_app (trm_abs A B) (trm_fst e)).
  apply_fresh* t_snd_v as x. instantiate (1:=s2).
  apply_fresh* t_abs as x1.
  unfold open_trm_wrt_trm. simpl.
  apply* t_ax.
  apply~ fv_in_from_notin. intros P.
  lets P1: fv_notin_self P.
  lets: H5 x __. auto. asserts W: (lc_trm B). rewrite~ <- P1. false.
  right; intros [S K]; inversions K.
    lets P: typing_unique H8 H. inversions P.
    apply~ H1. inversions H9.
    lets P: typing_unique H H8. inversions P.
    lets P: typing_unique H4 H14. inversions P.
    pick_fresh x. lets: H16 x __. auto.
    unfold open_trm_wrt_trm in H7 at 2. simpl in H7.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H7.
    lets: H17 x __. auto.
    unfold open_trm_wrt_trm in H7. simpl in H7.
    inversions H7.
    lets: AxUniq H19 H3. subst.
    apply* F.
  right; intros [S K]; inversions K.
    lets: typing_unique H7 H. inversions H3.
    apply~ H1. inversions H8.
    lets P: typing_unique H H7. inversions P.
    lets P: typing_unique H4 H13. inversions P.
    pick_fresh x. lets: H15 x __. auto.
    unfold open_trm_wrt_trm in H3 at 2. simpl in H3.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H3.
    lets: H16 x __. auto.
    unfold open_trm_wrt_trm in H3. simpl in H3.
    inversions H3. apply* F.  
  right; intros [S K]; inversions K.
    lets: typing_unique H3 H. inversions H2.
    apply~ H1. false.
  right; intros [S K]; inversions K;
    lets: typing_unique H1 H; apply* F.
  right; intros [S K]; inversions K; apply* H.
  
  (* Case CastUp *)
  destruct (IHTrm1 E Wf) as [[T H] | H]; eauto.
  destruct (IHTrm2 E Wf) as [[S J] | J]; eauto.
  destruct T;
    match goal with
    | [ H: typing ?E ?t1 (trm_sort ?s1) |-
        decidable (exists Q, typing ?E (trm_castup ?t1 ?t2) Q) ] => auto
    | [ H: typing ?E ?t1 ?P |-
        decidable (exists Q, typing ?E (trm_castup ?t1 ?t2) Q) ] =>
      right; intros [S' J']; inversion J'; subst;
        match goal with
        | [H2: typing E _ (trm_sort ?s1) |- _] =>
          lets W: typing_unique H2 H; inversions W
        end
    end. 
    
    destruct (reduct_dec H) as [[t1' L] | R].
    destruct (eq_typ_dec S t1').
    left. subst.
    exists A. apply t_castup with (A := t1') (s := s).
    auto. auto. auto.
    right. intros [T' P]. destruct (typing_castup_inv P) as [A' [B [C [D [? F]]]]].
    subst. assert (S = A') by (apply* typing_unique).
    subst. assert (t1' = A') by (apply~ (reduct_determ L F)).
    auto.
    right. intros [T' P]. destruct (typing_castup_inv P) as [A' [B [C [D F]]]]. destruct R. exists* A'.
    right. intros [S' J']. inversion J'; subst; eauto.
    right. intros [S' J']. inversion J'; subst; eauto.

  (* Case Castup_app *)
  destruct (IHTrm1 _ Wf) as [[T H] | H].
  asserts Dec: (decidable (exists A B, T = trm_prod A B)).
    inductions T; try solve [right; intros [? [? K]]; inversions K | left*].
  destruct Dec as [[A [B EQ]] | F]. subst.
  destruct (typing_wf_from_typing H) as [s [? | P]]. false.
  inversions P.
  pick_fresh z. lets: H5 z __. auto.
  asserts* LcB: (lc_trm (B ^ z)).
  destruct (lc_trm_decidable _ _ LcB).
  right. intros [S P]. inversions P.
  lets P: typing_unique H H7. inversions P.
  pick_fresh x. lets M: H10 x __. auto.
  lets: closed_not_open M. auto. auto. auto.
  destruct (AxDec s2) as [[s3 ?] | F].
  destruct (RelDec s1 s3) as [[s4 ?] | F].
  destruct (IHTrm2 _ Wf) as [[TT HH] | HH].
  destruct (eq_typ_dec TT A) as [? | F]. subst.
  left. exists (trm_app (trm_abs A B) e2).
  apply_fresh* t_castup_app as x.
  apply~ fv_in_from_notin. intros P.
  lets P1: fv_notin_self P.
  lets: H5 x __. auto. asserts W: (lc_trm B). rewrite~ <- P1. false.
  instantiate (1:=s2).
  apply_fresh* t_abs as x1.
  unfold open_trm_wrt_trm. simpl.
  apply* t_ax.
  right; intros [S K]; inversions K.
    lets P: typing_unique H9 H. inversions P.
    lets P: typing_unique HH H10. inversions P. false.
  right; intros [S K]; inversions K. apply* HH.
  right; intros [S K]; inversions K.
    inversions H13.
    lets P: typing_unique H H8. inversions P.
    lets P: typing_unique H4 H12. inversions P.
    pick_fresh x. lets: H15 x __. auto.
    unfold open_trm_wrt_trm in H3 at 2. simpl in H3.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H3.
    lets: H16 x __. auto.
    unfold open_trm_wrt_trm in H3. simpl in H3.
    inversions H3.
    lets: AxUniq H18 H2. subst.
    apply* F.
  right; intros [S K]; inversions K.
    inversions H12.
    lets P: typing_unique H H7. inversions P.
    lets P: typing_unique H4 H11. inversions P.
    pick_fresh x. lets H3: H14 x __. auto.
    unfold open_trm_wrt_trm in H3 at 2. simpl in H3.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H3.
    lets H3: H15 x __. auto.
    unfold open_trm_wrt_trm in H3. simpl in H3.
    inversions H3. apply* F.  
  right; intros [S K]; inversions K;
    lets: typing_unique H2 H; apply* F.
  right; intros [S K]; inversions K; apply* H.

  (* Case Castup Pair *)
  destruct (IHTrm1 E Wf) as [[T1 H21] | F].
  destruct (IHTrm2 E Wf) as [[T2 H22] | F].
  destruct (IHTrm3 E Wf) as [[T3 H23] | F].
  destruct (eq_typ_dec T1 A) as [? | F]. subst.
  destruct (is_sort_dec T3) as [[s1 ?] | F]. subst.
  pick_fresh z.
  lets P1: H0 z __. auto.
  lets P2: P1 (E & z ~ A) __. apply* wf_cons.
  destruct P2 as [[T4 ?] | F].
  destruct (is_sort_dec T4) as [[s2 ?] | F]. subst.
  destruct (SigDec s1 s2) as [[s3 ?] | F].
  asserts Typ1: (typing E (trm_sigma A B) (trm_sort s3)).
  apply_fresh* t_sigma as z.
  apply* (@typing_rename_eq z). simpls~.
  lets P: H z __. auto.
  destruct (@lc_trm_decidable B z P).
  right. intros [S K]. inversions K.
    pick_fresh x. lets M: H12 x __. auto.
    lets: closed_not_open M. auto. auto. auto.
  destruct (eq_typ_dec T2 (trm_app (trm_abs A B) e1)) as [? | F]. subst.
  left. exists (trm_sigma A B).
    apply_fresh* t_castup_pair as x.
    apply~ fv_in_from_notin. intros MM.
    lets MM1: fv_notin_self MM.
    lets: H x __. auto. asserts W: (lc_trm B). rewrite~ <- MM1. false.
  right; intros [S K]; inversions K.
    lets: typing_unique H22 H11. false.
  right; intros [S K]; inversions K.
    inversions H11. pick_fresh x. lets M: H8 x __. auto.
     asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s4)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. inversions H2.
    lets: typing_unique H23 H6. inversions H2.
    apply* F.
  right; intros [S K]; inversions K.
    inversions H11. pick_fresh x. lets M: H8 x __. auto.
    asserts Typ': (typing (E & z ~ A) (B ^ z) (trm_sort s2)).
    apply* (@typing_rename_eq x z). simpls~.
    lets: typing_unique Typ' H1. subst. apply* F.
  right; intros [S K]; inversions K.
    inversions H10. pick_fresh x. lets: H7 x __. auto.
    apply* F. exists (trm_sort s2).
    apply* (@typing_rename_eq x). simpls. auto.
  right; intros [S K]; inversions K.
    inversions H10. lets: typing_unique H23 H5. apply* F.
  right; intros [S K]; inversions K.
    lets: typing_unique H21 H6. false.
  right; intros [S K]; inversions K. inversions H10; apply* F.
  right; intros [S K]; inversions K; apply* F.
  right; intros [S K]; inversions K; apply* F.

  (* Case Castup_con *)
  rename A into e3.
  destruct (IHTrm2 _ Wf) as [[T H] | H].
  asserts Dec: (decidable (exists A B, T = trm_prod A B)).
    inductions T; try solve [right; intros [? [? K]]; inversions K | left*].
  destruct Dec as [[A [B EQ]] | F]. subst.
  destruct (typing_wf_from_typing H) as [s [? | P]]. false.
  inversions P.
  pick_fresh z. lets: H5 z __. auto.
  asserts* LcB: (lc_trm (B ^ z)).
  destruct (lc_trm_decidable _ _ LcB).
  right. intros [S P]. inversions P.
  lets P: typing_unique H H8. inversions P.
  pick_fresh x. lets M: H14 x __. auto.
  lets: closed_not_open M. auto. auto. auto.
  destruct (AxDec s2) as [[s3 ?] | F].
  destruct (RelDec s1 s3) as [[s4 ?] | F].
  destruct (IHTrm3 _ Wf) as [[TT HH] | HH].
  destruct (eq_typ_dec TT A) as [? | F]. subst.
  destruct (IHTrm1 _ Wf) as [[TT HH2] | F].
  destruct (eq_typ_dec TT A) as [? | F]. subst.
  destruct (reduct_dec HH2) as [[e3' ?] | F].
  destruct (eq_typ_dec e3' e2) as [? | F]. subst.
  left. exists (trm_app (trm_abs A B) e3).
  apply_fresh* t_castup_app_con as x.
  apply~ fv_in_from_notin. intros P.
  lets P1: fv_notin_self P.
  lets: H5 x __. auto. asserts W: (lc_trm B). rewrite~ <- P1. false.
  instantiate (1:=s2).
  apply_fresh* t_abs as x1.
  unfold open_trm_wrt_trm. simpl.
  apply* t_ax.
  
  right; intros [S K]; inversions K.
    lets P: typing_unique H11 H. inversions P.
    lets P: reduct_determ H15 H7. false.
  right; intros [S K]; inversions K. apply* F.
  right; intros [S K]; inversions K.
    lets P: typing_unique H10 H. inversions P.
    lets P: typing_unique H12 HH2. false.
  right; intros [S K]; inversions K. apply* F.
  right; intros [S K]; inversions K.
    lets P: typing_unique H10 H. inversions P.
    lets P: typing_unique H11 HH. false.
  right; intros [S K]; inversions K. apply* HH.

  right; intros [S K]; inversions K.
    inversions H16.
    lets P: typing_unique H H9. inversions P.
    lets P: typing_unique H4 H12. inversions P.
    pick_fresh x. lets: H17 x __. auto.
    unfold open_trm_wrt_trm in H3 at 2. simpl in H3.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H3.
    lets: H18 x __. auto.
    unfold open_trm_wrt_trm in H3. simpl in H3.
    inversions H3.
    lets: AxUniq H20 H2. subst.
    apply* F.
  right; intros [S K]; inversions K.
    inversions H15.
    lets P: typing_unique H H8. inversions P.
    lets P: typing_unique H4 H11. inversions P.
    pick_fresh x. lets H3: H16 x __. auto.
    unfold open_trm_wrt_trm in H3 at 2. simpl in H3.
    asserts Typ': (typing (E & z ~ A0) (B0 ^ z) (trm_sort s0)).
    apply* (@typing_rename_eq x z). simpls~.
    lets P: typing_unique Typ' H0. inversions P. clear H3.
    lets H3: H17 x __. auto.
    unfold open_trm_wrt_trm in H3. simpl in H3.
    inversions H3. apply* F.  
  right; intros [S K]; inversions K.
    lets: typing_unique H3 H; apply* F.
  right; intros [S K]; inversions K; apply* H.

  (* Case CastDn *)
  destruct (IHTrm E Wf) as [[T H] | H]; eauto.
  destruct (typing_wf_from_typing H) as [s [J | J]].
  subst. right. unfold not. intros.
  destruct H0 as [T K].
  inversions K.
  lets: typing_unique H H1. subst. inversion H3.
  destruct (reduct_dec J) as [[t' A] | B].
  left. exists* t'.
  right. intros [T0 P]. inversions P. destruct B.
  assert (T = A) by (apply* typing_unique).
  subst. exists* T0.
  right. intros [T P]. inversions P. destruct H.
  exists* A.
Qed.

