Set Implicit Arguments.
Require Import LibLN.
Require Import Main_ott Main_Infra.
Implicit Types x : var.

Lemma wf_to_wt : forall G x A,
  wf (G & x ~ A) -> sub G A A (trm_star).
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]. subst*.
Qed.

Lemma sub_prod_prod_inv1 : forall G A B B0 C,
   sub G (trm_prod A B) (trm_prod A B0) C ->
   (sub G A A (trm_star)).
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_refl : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G e1 e1 A /\ sub G e2 e2 A.
Proof.
  intros. inductions H; splits*.
  apply_fresh* sub_abs as y.
  lets P: H1 y __. auto. destruct~ P.
  apply_fresh* sub_abs as y.
  lets P: H1 y __. auto. destruct~ P.
  apply_fresh* sub_prod as y.
  lets P: H3 y __. auto. destruct~ P.
  lets P: H3 y __. auto. destruct~ P.
Qed.

Hint Extern 1 (sub ?G ?A ?A ?B) => match goal with
  | H: sub G A _ B |- _ => apply (proj1 (sub_refl H))
  | H: sub G _ A B |- _ => apply (proj2 (sub_refl H))
  end.

Lemma sub_weaken : forall G E F t1 t2 T,
  sub (E & G) t1 t2 T ->
  wf (E & F & G) ->
  sub (E & F & G) t1 t2 T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var refl *)
  apply* sub_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh* (@sub_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh* (@sub_prod) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
Qed.

Lemma sub_narrowing : forall A' A F E B Z S T,
  sub (E & Z ~ A & F) S T B ->
  sub E A' A (trm_star) ->
  sub (E & Z ~ A' & F) S T B.
Proof.
  introv Sa Sb.
  gen_eq G: (E & Z ~ A & F). gen F.
  apply sub_induct with
    (P := fun G S T B (Sub: sub G S T B) =>
            forall F, G = E & Z ~ A & F ->
                      sub (E & Z ~ A' & F) S T B)
    (P0 := fun G (W: wf G) =>
            forall F, G = E & Z ~ A & F ->
                      wf (E & Z ~ A' & F));
    intros; subst; simpls subst; try solve [eauto 5].
  (* case: var refl *)
  rename x5 into x.
  tests EQ: (x = Z).
    asserts~ N1: (wf (E & Z ~ A' & F)).
    asserts~ N': (ok (E & Z ~ A & F)).
    lets: (binds_middle_eq_inv b N').
    destruct (ok_middle_inv N').
    inversions H0.
    apply (@sub_sub' A').
    apply~ sub_var.
    apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    asserts* W1: (x # (Z ~ A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply~ sub_var.
    apply~ binds_weaken.
  (* case: abs *)
  apply_fresh* sub_abs as Y.
  apply_ih_bind* H0.
  apply_ih_bind* H1.
  (* case: prod *)
  apply_fresh* (@sub_prod) as Y.
  apply_ih_bind* H0. apply_ih_bind* H1.
  (* case: wf nil *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  destruct (env_case F).
    subst. rewrite concat_empty_r.
    rewrite concat_empty_r in H0.
    destruct (eq_push_inv H0) as [? [? ?]].
    subst~.
    destruct H1 as [y [v' [E' Eq]]].
    subst.
    rewrite concat_assoc.
    rewrite concat_assoc in H0.
    destruct (eq_push_inv H0) as [? [? ?]].
    subst~.
Qed.

Lemma sub_any_star_inv : forall E A B,
  sub E A (trm_star) B ->
  A = trm_star.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_top_any_inv : forall E A B,
  sub E (trm_top) A B ->
  A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_trans_aux : forall e2 G e1 e3 A B,
  sub G e1 e2 A -> sub G e2 e3 B -> sub G e1 e3 B.
Proof.
  introv S1. asserts* W: (lc_trm e2).
  gen G A e1 B e3. set_eq e2' eq: e2. gen e2' eq.
  induction W; intros e2' eq G AA e1' S1;
    induction S1; try discriminate; inversions eq;
      intros B' e3' S2; try solve [eauto 2].
  (* case: top *)
  lets: sub_top_any_inv S2. subst~.
  inductions S2; eauto.
  (* case: app *)
  clear IHS1_1 IHS1_2 IHS1_3 W1 W2.
  inductions S2. apply~ sub_top. clear IHS2.
  inductions S2. 
    apply sub_app with (A := A0).
     asserts P: (sub G e0 e1 (trm_prod A0 trm_star)).
     apply* IHW1. auto. auto. auto.
    rewrite <- x. apply sub_app_v with (A := A0).
     asserts P: (sub G e0 e1 (trm_prod A0 B0)).
     apply* IHW1. auto. auto. auto.
    apply* IHS2_1. apply* sub_any_star_inv.
  apply sub_app with (A := A0).  apply* IHW1.
    auto. auto.
  apply sub_app_v with (A := A0). apply* IHW1.
    auto. auto.
  apply~ (@sub_sub' A0). apply* IHS2_1.
  (* case: app_v *)
  clear IHS1_1 IHS1_2 W1 W2.
  inductions S2. apply~ sub_top. clear IHS2.
  inductions S2. 
    apply sub_app with (A := A0).
     asserts P: (sub G e0 e1 (trm_prod A0 trm_star)).
     apply* IHW1. auto. auto. auto.
    rewrite <- x. apply sub_app_v with (A := A0).
     asserts P: (sub G e0 e1 (trm_prod A0 B0)).
     apply* IHW1. auto. auto. auto.
    apply* IHS2_1. apply* sub_any_star_inv.
  apply sub_app with (A := A0).  apply* IHW1.
    auto. auto.
  apply sub_app_v with (A := A0). apply* IHW1.
    auto. auto.
  apply~ (@sub_sub' A0). apply* IHS2_1.
  (* case lam *)
  clear IHS1 W H2 H4.
  inductions S2. 
  false. clear IHS2 H H0 IHW H3 H1 S1.
    inductions S2. apply* IHS2_1. apply* sub_any_star_inv.
  apply (@sub_abs (L \u L0 \u L1)). auto. intros.
  lets P1: H1 x5 __. auto.
  lets P2: H5 x5 __. auto.
  apply* H0.
  intros. apply* H6.
  apply~ (@sub_sub' A0). apply* IHS2_1.
  (* case pi *)
  clear IHS1 H2 W H4.
  inductions S2.
  apply~ sub_top. apply_fresh* sub_prod as y.
  apply (@sub_prod (L \u L0 \u L1)).
  apply* IHW. intros.
  lets P: H3 x5 __. auto. auto.
  intros.
  lets P1: H3 x5 __. auto.
  lets P2: H6 x5 __. auto.
  lets Q: H0 x5 __. auto.
  lets Q1: Q (B ^ x5) __. auto.
  asserts Q3: (sub (G & x5 ~ A2) (B1 ^ x5) (B ^ x5) trm_star).
  apply_empty~ (@sub_narrowing A2 A).
  lets Q2: Q1 Q3 P2. auto.
  apply~ (@sub_sub' A0). apply* IHS2_1.
  (* case: castup *)
  clear IHS1_1 IHS1_2 W.
  inductions S2. 
  false. clear IHS2. inductions S2. inversion H.
    apply* IHS2_1. apply* sub_any_star_inv.
  apply* sub_castup.
  apply~ (@sub_sub' A0). apply* IHS2_1.
  (* case: castdn *)
  clear IHS1.
  inductions S2. apply~ sub_top. clear IHS2.
  inductions S2.
    apply sub_castdn with (A := A0). auto.
    asserts P: (sub G e1 e A0). apply* IHW. auto. auto.
    lets: sub_any_star_inv S2_2. subst. apply* IHS2_1.
  apply* sub_castdn.
  apply~ (@sub_sub' A0).
  lets P: IHS2_1 H W IHW.
  apply~ P.
Qed.

Definition red_out (R : relation) :=
  forall x u t t', value u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Hint Unfold red_out.

Lemma value_red_out :   forall x u t, value u -> value t -> 
  value ([x~>u]t).
Proof.
  intros_all. induction H0; simpl; auto.
  case_if~.
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl; eauto 3.
  rewrite* subst_open. apply* reduct_beta. apply* value_red_out.
  apply* reduct_app_l. apply* value_red_out.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> t --> t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv HH; inversions HH.
Qed.

Lemma reduct_determ : forall t t1 t2,
    reduct t t1 -> reduct t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv HH; inversions HH;
  match goal with
  | [ H : trm_abs _ _ --> _ |- _ ] => inversion H
  | [ H : trm_castup _ --> _ |- _ ] => inversion H
  | [ H1 : value ?e2, H2 : ?e2 --> _ |- _ ] => false* (@value_cannot_red e2)
  | _ => fequal~
  end.
Qed.

Lemma sub_substitution : forall V (F:env) v (E:env) x t1 t2 T,
  sub E v v V ->
  sub (E & x ~ V & F) t1 t2 T ->
  value v ->
  sub (E & (map (subst x v) F)) (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  introv Typv Typt Val.
  gen_eq G: (E & x ~ V & F). gen F.
  apply sub_induct with
   (P := fun (G:env) t1 t2 T (Typt : sub G t1 t2 T) =>
      forall (F:env), G = E & x ~ V & F ->
      sub (E & map (subst x v) F) ([x ~> v]t1) ([x ~> v]t2) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) =>
      forall F, G = E & x ~ V & F ->
      wf (E & (map (subst x v) F)));
   intros; subst; simpls subst; eauto 5.
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* sub_weaken.
    apply~ sub_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: abs *)
  apply_fresh* (@sub_abs) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  (* case: app_v *)
  rewrite~ subst_open.
  apply~ sub_app_v.
  apply~ value_red_out.
  (* case: prod *)
  apply_fresh* (@sub_prod) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  (* case: castup *)
  apply* sub_castup. apply* reduct_red_out.
  (* case: castdn *)
  apply* sub_castdn. apply* reduct_red_out.
  (* case: wf empty *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
Qed.

Lemma sub_trans : forall e2 G e1 e3 A,
  sub G e1 e2 A -> sub G e2 e3 A -> sub G e1 e3 A.
Proof.
  intros. apply* (@sub_trans_aux e2).
Qed.

Lemma sub_wf_from_context : forall x U (E:env),
  binds x U E -> 
  wf E -> 
  sub E U U trm_star.
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     apply_empty* sub_weaken.
    destruct (wf_push_inv W).
      apply_empty* sub_weaken.
Qed.

Lemma sub_prod_prod_inv : forall G A B B0 C,
   sub G (trm_prod A B) (trm_prod A B0) C ->
   (exists L, forall x, x \notin L ->
        sub (G & x ~ A) (B ^ x) (B0 ^ x) (trm_star)).
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_abs_abs_inv : forall G A A0 B B0 C,
   sub G (trm_abs A B) (trm_abs A0 B0) C -> exists C',
   A = A0 /\
   sub G (trm_prod A C') C (trm_star) /\
   (exists L, forall x, x \notin L ->
        sub (G & x ~ A) (B ^ x) (B0 ^ x) (C' ^ x)).
Proof.
  intros. inductions H; eauto.
  exists B1. splits; auto.
  apply_fresh* sub_prod as y.
  exists L. intros. apply* H0.
  destruct (IHsub1 _ _ _ _ eq_refl eq_refl) as [C' [? [? ?]]].
  exists C'. splits*.
  apply* (@sub_trans_aux A1).
Qed.

Lemma sub_star_any_inv : forall E A B,
  sub E (trm_star) A B ->
  A = trm_star \/ A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_any_top_inv : forall E A B,
  sub E A (trm_top) B ->
  B = trm_star \/ B = trm_top.
Proof.
  intros. inductions H; auto.
  destruct IHsub1; auto.
  subst. apply* sub_star_any_inv.
  subst. lets: sub_top_any_inv H0.
  right~.
Qed.

Lemma sub_prod_prod_inv2 : forall G A A' B B0 C,
   sub G (trm_prod A B) (trm_prod A' B0) C ->
   ((C = trm_star) \/ C = trm_top) /\
   sub G A' A (trm_star) /\
   (exists L, forall x, x \notin L ->
        sub (G & x ~ A') (B ^ x) (B0 ^ x) (trm_star)).
Proof.
  intros. inductions H; eauto.
  destructs (IHsub1 _ _ _ _ eq_refl eq_refl).
  splits*.
  destruct H1.
  subst. destruct (sub_star_any_inv H0).
  left*. right*.
  subst. lets: sub_top_any_inv H0. right*.
Qed.

Lemma sub_star_star_inv : forall E A,
  sub E trm_star trm_star A ->
  sub E trm_star A trm_star.
Proof.
  intros. inductions H; auto.
  apply* (@sub_trans A).
Qed.

Lemma sub_star_star_inv2 : forall E A,
  sub E trm_star trm_star A ->
  A = trm_star \/ (A = trm_top).
Proof.
  intros. inductions H; auto.
  destruct (IHsub1 eq_refl eq_refl); subst.
  destruct (sub_star_any_inv H0); auto.
  right.
  destruct~ (sub_top_any_inv H0).
Qed.

Lemma sub_castup_inv : forall E t2 t2' B,
  sub E (trm_castup t2) (trm_castup t2') B ->
  exists A t1,sub E t2 t2' A /\
              reduct t1 A /\
              sub E t1 B trm_star.
Proof.
  intros. inductions H.
  exists A B. splits*.
  destruct (IHsub1 _ _ eq_refl eq_refl) as [A0 [t1 [? [? ?]]]].
  exists A0 t1. splits*.
  apply* (@sub_trans_aux A).
Qed.

Lemma sub_any_prod_inv : forall E P A B C,
  sub E P (trm_prod A B) C ->
  (exists x, P = trm_var_f x) \/
  (exists A' B', P = trm_prod A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_prod_any_inv : forall E P A B C,
  sub E (trm_prod A B) P C ->
  P = trm_top \/
  (exists A' B', P = trm_prod A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_abs_any_inv : forall E P A B C,
  sub E (trm_abs A B) P C ->
  exists A' B', P = trm_abs A' B'.
Proof.
  intros. inductions H; autos*.
  false.
  inductions H.
  lets: sub_any_star_inv H0. apply* IHsub1.
Qed.

Lemma sub_any_abs_inv : forall E P A B C,
  sub E P (trm_abs A B) C ->
  (exists x, P = trm_var_f x) \/
  (exists A' B', P = trm_abs A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_castup_any_inv : forall E P B C,
  sub E (trm_castup B) P C ->
  P = trm_top \/
  (exists B', P = trm_castup B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_any_castup_inv : forall E P B C,
  sub E P (trm_castup B) C ->
  (exists x, P = trm_var_f x) \/
  (exists B', P = trm_castup B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_star_prod_false : forall E A B C,
  sub E trm_star (trm_prod A B) C ->
  False.
Proof.
  intros. destruct (sub_any_prod_inv H).
  destruct H0 as [? ?]. inversions H0.
  destruct H0 as [? [? ?]]. inversions H0.
Qed.

Lemma sub_prod_star_false : forall E A B C,
  sub E (trm_prod A B) trm_star C ->
  False.
Proof.
  intros. destruct (sub_prod_any_inv H).
  inversions H0.
  destruct H0 as [? [? ?]]. inversions H0.
Qed.

Lemma reduce_sub_var_false : forall E e0 x A e1,
  sub E e0 (trm_var_f x) A ->
  e0 --> e1 -> False.
Proof.
  intros. inductions H; tryfalse.
  inversions H1.
  apply* IHsub1.
Qed.

Lemma sub_any_var_inv : forall E e x A,
  sub E e (trm_var_f x) A ->
  (exists y, e = trm_var_f y).
Proof.
  intros. inductions H.
  exists* x.
  apply* IHsub1.
Qed.

Lemma sub_any_app_inv : forall E P e1 A C,
  sub E P (trm_app e1 A) C ->
  (exists x, P = trm_var_f x) \/
  (exists e1' A', P = trm_app e1' A').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_app_inv : forall E e1 e2 A A' T,
  sub E (trm_app e1 A) (trm_app e2 A') T ->
  exists B C, A = A' /\ sub E A A B /\
                 sub E e1 e2 (trm_prod B C).
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_app_any_inv : forall E P e1 A C,
  sub E (trm_app e1 A) P C ->
  P = trm_top \/
  (exists e1', P = trm_app e1' A).
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_castdn_any_inv : forall E P A C,
  sub E (trm_castdn A) P C ->
  P = trm_top \/
  (exists A', P = trm_castdn A').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_any_castdn_inv : forall E P A C,
  sub E P (trm_castdn A) C ->
  (exists x, P = trm_var_f x) \/
  (exists A', P = trm_castdn A').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_castdn_inv : forall E t2 t2' B,
  sub E (trm_castdn t2) (trm_castdn t2') B ->
  exists A t1, sub E t2 t2' A /\
               reduct A t1.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_value1 : forall G e1 e2 A,
  sub G e1 e2 A -> value e1 -> value e2.
Proof.
  introv S V.
  induction S; try solve [eauto 3 | inversions V].
  apply~ value_abs. apply_fresh* lc_trm_abs as y.
  forwards~ P: H y.
  apply~ value_prod. apply_fresh* lc_trm_prod as y.
  forwards~ P: H1 y.
Qed.

Lemma sub_value2 : forall G e1 e2 A B,
  sub G e1 e2 (trm_prod A B) -> value e2 -> value e1.
Proof.
  introv S V.
  gen e1 A B G. inductions V; intros.
  lets: sub_any_star_inv S. subst~.
  lets: sub_any_var_inv S. destruct H as [y ?]. subst~.
  lets: sub_any_top_inv S. destruct H; false.
  lets: sub_any_abs_inv S. 
    destruct H1 as [[x ?] | [A' [B' ?]]]; subst~. 
    destruct (regular_sub S) as (_&_&_&M&_). inversions M.
    apply~ value_abs.
  lets: sub_any_prod_inv S.
    destruct H1 as [[x ?] | [A' [B' ?]]]; subst~. 
    destruct (regular_sub S) as (_&_&_&M&_). inversions M.
    apply~ value_prod.
  lets: sub_any_castup_inv S.
    destruct H0 as [[x ?] | [B' ?]]; subst~. 
    destruct (regular_sub S) as (_&_&_&M&_). inversions M.
    apply~ value_castup.
Qed.

Definition decidable (P : Prop) := (P \/ ~ P).

Hint Unfold decidable.

Lemma value_dec : forall E t T,
    sub E t t T ->
    decidable (value t).
Proof.
  introv Typ. lets Typ': Typ. 
  inductions Typ; try solve [left* | right*; intros P; inversion P].
  destruct~ IHTyp1.
Qed.

Definition SubLemma E t1 t2 A :=
  match t1,t2 with
    | trm_castup t1', trm_castup t2' => forall C, A --> C -> sub E t1' t2' C
    | _,_ => forall t1' t2', t1 --> t1' -> t2 --> t2' -> sub E t1' t2' A
  end.

Lemma preserve_not_cast : forall t1 t2 E A, SubLemma E t1 t2 A -> forall t1' t2', t1 --> t1' -> t2 --> t2' -> sub E t1' t2' A.
  intros.
  unfold SubLemma in H; destruct t1; auto.
  inversions H0.
Defined.

Lemma sub_reduct_middle : forall E A B C e1 e3 T U,
  sub E A B T ->
  B --> C ->
  sub E e1 A U ->
  e1 --> e3 ->
  exists D, A --> D.
Proof.
  intros. gen e3 B C T.
  inductions H1; introv R1 R2 S; try solve [inversion R1 | inversion R2].
  (* case: top *)
  lets: (sub_top_any_inv S). subst. inversions R2.
  (* case: app *)
  inversions R1.
  destruct (sub_app_any_inv S) as [? | [K1 ?]]; subst.
  inversions R2.
  destruct (sub_abs_any_inv H1_) as [J1 [J2 ?]]; subst.
  exists (J2 ^^ e3). apply* reduct_beta. apply* lc_trm_abs1.
  destruct (sub_app_any_inv S) as [? | [K1 ?]]; subst.
  inversions R2.
  destruct (sub_app_inv S) as [e3' [B' [C' (?&?)]]].
  inversions R2.
  destruct (sub_any_abs_inv H0) as [[x ?] | [J1 [J2 J3]]]; subst.
  destruct (sub_any_var_inv H1_); subst. inversions H1.
  destruct (sub_any_abs_inv H1_) as [[x ?] | [JJ1 [JJ2 JJ3]]]; subst; inversions H1.
  lets P1: IHsub1 H1 H5 H0. destruct P1 as [e2' ?].
  exists* (trm_app e2' e3).
  false* (@value_cannot_red e3).
  exists* (trm_app e2 e2').
  
  inversions R1.
  destruct (sub_app_any_inv S) as [? | [K1 ?]]; subst.
  inversions R2.
  destruct (sub_abs_any_inv H1_) as [J1 [J2 ?]]; subst.
  exists (J2 ^^ v). apply* reduct_beta. apply* lc_trm_abs1.
  destruct (sub_app_any_inv S) as [? | [K1 ?]]; subst.
  inversions R2.
  destruct (sub_app_inv S) as [e3' [B' [C' (?&?)]]].
  inversions R2.
  destruct (sub_any_abs_inv H1) as [[x ?] | [J1 [J2 J3]]]; subst.
  destruct (sub_any_var_inv H1_); subst. inversions H2.
  destruct (sub_any_abs_inv H1_) as [[x ?] | [JJ1 [JJ2 JJ3]]]; subst; inversions H2.
  lets P1: IHsub1 H2 H6 H1. destruct P1 as [e2' ?].
  exists* (trm_app e2' v).
  false* (@value_cannot_red v).
  false* (@value_cannot_red v).
  (* case: castdn *)
  destruct (sub_castdn_any_inv S) as [? | [K ?]]; subst.
  inversions R2. 
  destruct (sub_castdn_inv S) as [J1 [J2 (?&?)]].
  inversions R1; inversions R2.
  lets P3: IHsub H4 H5 H0.
  destruct P3 as [e2' ?]. exists* (trm_castdn e2').
  destruct (sub_any_castup_inv H0) as [[x ?] |  [M2 ?]]; subst.
  destruct (sub_any_var_inv H1) as [y ?]; subst. inversions H4.
  asserts* W: (lc_trm (trm_castup M2)). inversion W.
  exists* M2.
  destruct (sub_castup_any_inv H1) as [? | [M2 ?]]; subst.
  lets: (sub_top_any_inv H0). subst. inversions H5.
  asserts* W: (lc_trm (trm_castup M2)). inversion W.
  exists* M2.
  destruct (sub_castup_any_inv H1) as [? | [M2 ?]]; subst.
  lets: (sub_top_any_inv H0). inversions H3.
  asserts* W: (lc_trm (trm_castup M2)). inversion W.
  exists* M2.
  (* case: sub *)
  lets P1: IHsub1 R1 R2 S.
  auto.
Qed.

Lemma is_castup_dec : forall e,
  (exists B, e = trm_castup B) \/
  ~(exists B, e = trm_castup B).
Proof.
  intros. unfold not. 
  induction e;
    try solve [right; intro N;
               destruct N as [J1 Eq]; inversions Eq].
  left*.
Qed.

Lemma is_castup_dec' : forall e1 e2,
  (exists e, e1 = trm_castup e) /\ (exists e, e2 = trm_castup e) \/
 not ((exists e, e1 = trm_castup e) /\ (exists e, e2 = trm_castup e)).
Proof.
  intros. unfolds not.
  destruct (is_castup_dec e1);
    destruct (is_castup_dec e2);
    try solve [right* | left*].
Qed.

Lemma not_eq_castup_inv : forall e1 e2,
  ((exists e, e1 = trm_castup e) /\
   (exists e, e2 = trm_castup e) -> False) ->
  (((exists e, e1 = trm_castup e) -> False) /\
   ((exists e, e2 = trm_castup e) -> False)) \/
  ((exists e, e1 = trm_castup e) /\
   ((exists e, e2 = trm_castup e) -> False)) \/
  (((exists e, e1 = trm_castup e) -> False) /\
   (exists e, e2 = trm_castup e)).
Proof.
  intros.
  intros. unfolds not.
  destruct (is_castup_dec e1);
    destruct (is_castup_dec e2).
  false*.
  right. left*.
  right. right*.
  left*.
Qed.

Lemma sub_reduct_preserve1 : forall E t1 t2 A,
  sub E t1 t2 A -> SubLemma E t1 t2 A.
Proof.
  introv Typ.
  induction Typ; 
    try solve 
        [unfold SubLemma; introv Red1 Red2; inversions Red1].

  (* case: top *)
  unfold SubLemma; destruct e; intros; inversion H0.

  (* case: app *)
  unfold SubLemma; introv Red1 Red2.
  inversions keep Red1; inversions keep Red2; 
    eauto 3; try solve [false* (@value_cannot_red e3)].
  destruct (sub_abs_abs_inv Typ1) as [C' (? & ? & [L ?])]. subst.
  destruct (sub_prod_prod_inv2 H0) as (? & ? & (L' & ?)).
  pick_fresh x.
    asserts EQ: (B = B ^^ e3).
    rewrite~ (@subst_intro x B).
    asserts EQ2: (B = B ^ x). apply~ open_closed_eq.
    rewrite <- EQ2. rewrite* subst_fresh. rewrite EQ.
   rewrite* (@subst_intro x e).
   rewrite* (@subst_intro x e0).
   rewrite* (@subst_intro x B).
  apply_empty* (@sub_substitution A).
  lets: H6 x __. auto.
  apply* (@sub_sub' (C' ^ x)).
  apply_empty* (@sub_narrowing A A1).

  destruct (sub_abs_any_inv Typ1).
    destruct H as [? ?]. subst. inversions H3.

  destruct (sub_any_abs_inv Typ1).
    destruct H as [? ?]. subst. inversions H1.
    destruct H as [? [? ?]]. subst. inversions H1.

  apply* sub_app. apply* preserve_not_cast.

  lets: (reduct_determ H3 H5). subst.
  apply* sub_app. apply* preserve_not_cast.
  
  (* case: app_v *)
  unfold SubLemma; introv Red1 Red2.
  inversions keep Red1; inversions keep Red2; 
    eauto 3; try solve [false* (@value_cannot_red v)].
  destruct (sub_abs_abs_inv Typ1) as [C' (? & ? & [L ?])]. subst.
  destruct (sub_prod_prod_inv2 H1) as (? & ? & (L' & ?)).
  pick_fresh x.
   rewrite* (@subst_intro x e).
   rewrite* (@subst_intro x e0).
   rewrite* (@subst_intro x B).
  apply_empty* (@sub_substitution A).
  lets: H7 x __. auto.
  apply* (@sub_sub' (C' ^ x)).
  apply_empty* (@sub_narrowing A A1).

  destruct (sub_abs_any_inv Typ1).
    destruct H0 as [? ?]. subst. inversions H4.

  destruct (sub_any_abs_inv Typ1).
    destruct H0 as [? ?]. subst. inversions H2.
    destruct H0 as [? [? ?]]. subst. inversions H2.

  apply* sub_app_v. apply* preserve_not_cast.

  (* case: castup *)
  unfold SubLemma; intros.
  lets: reduct_determ H H0. subst~.

  (* case: castdn *)
  unfold SubLemma; introv Red1 Red2.
  inversions Red1; inversions Red2.
  apply sub_castdn with (A := A).
    auto. apply* preserve_not_cast. auto.
  lets: sub_any_castup_inv Typ.
    destruct H0 as [[x ?] | [B' ?]]; subst~;
    inversion H1.
  lets: sub_castup_any_inv Typ.
    destruct H0 as [? | [B' ?]]; subst;
    inversion H2.
  unfold SubLemma in IHTyp.
  destruct (sub_castup_inv Typ) as [A2 [t1 P]].
  destructs P. subst.
  lets*: IHTyp H.

  (* Last case *)
  assert ((exists e, e1 = trm_castup e) /\ (exists e, e2 = trm_castup e) \/ not ((exists e, e1 = trm_castup e) /\ (exists e, e2 = trm_castup e))).
  apply is_castup_dec'.
  destruct H. destruct H. destruct H. destruct H0. subst.
  unfold SubLemma. intros.
  unfold SubLemma in IHTyp1.
  destruct (sub_castup_inv Typ1) as [A' [t1 [? [? ?]]]].
  assert (forall t1' t2', A --> t1' -> B --> t2' -> sub G t1' t2' trm_star). (* B is reducible, so IHTyp2 should have this form *)
  apply* preserve_not_cast.
  (* assert (D = x2). apply reduct_determ with (t := x1); auto. subst. *)
  (*apply IHTyp1 with (D := x3). *)
  assert (exists D, A --> D).
  lets: sub_reduct_middle Typ2 H H2 H1. auto.
  destruct H4 as [D' ?].
  pose (IHTyp1 _ H4). pose (H3 _ _ H4 H).
  apply sub_sub with (A := D'). auto. auto.
  destruct (not_eq_castup_inv H) as [(?&?)|[(?&?)|(?&?)]].
  lets: preserve_not_cast IHTyp1.  
  induction e1; induction e2; simpl; intros; try solve [apply* (@sub_sub' A)].
  false*.
  lets: preserve_not_cast IHTyp1.  
  induction e1; induction e2; simpl; intros; try solve [apply* (@sub_sub' A)].
  false*.
  lets: preserve_not_cast IHTyp1.  
  induction e1; induction e2; simpl; intros; try solve [apply* (@sub_sub' A)].
  false*.
Qed.

Lemma sub_reduct_preserve : forall E t1 t2 t1' t2' A,
  sub E t1 t2 A -> 
  t1 --> t1' ->
  t2 --> t2' ->
  sub E t1' t2' A.
  intros.
  apply (@preserve_not_cast t1 t2). apply sub_reduct_preserve1. auto. auto. auto.
Defined.

Lemma subject_reduction_wh : forall E t t' T,
  sub E t t T -> t --> t' -> sub E t' t' T.
Proof.
  introv S R.
  lets: sub_reduct_preserve S R R.
  auto.
Qed.

Lemma sub_wf_from_sub : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G A A trm_star.
Proof.
  intros. inductions H; try solve [eauto 2].
  autos* (sub_wf_from_context H0).
  destruct (sub_prod_prod_inv IHsub1) as [L T'].
  pick_fresh x. rewrite~ (@subst_intro x).
  unsimpl ([x ~> v](trm_star)).
  apply_empty* (@sub_substitution A).
  apply~ subject_reduction_wh.
Qed.

Lemma sub_var_var_inv : forall E x1 x2 A,
  sub E (trm_var_f x1) (trm_var_f x2) A ->
  exists B, x1 = x2 /\
            binds x1 B E /\
            sub E B A trm_star.
Proof.
  intros. inductions H; eauto.
  exists A. splits~. apply* sub_wf_from_context.
  destruct (IHsub1 _ _ eq_refl eq_refl) as [B' P].
  destructs P.
  exists B'. splits*. apply* (@sub_trans A).
Qed.

Lemma progress_wh : forall t T,
  sub empty t t T ->
  value t \/ exists t', t --> t'.
Proof.
  intros. inductions H; auto.
  (* case: abs *)
  left. apply~ value_abs.
  apply_fresh~ lc_trm_abs as y.
  forwards~ P: H0 y.
  (* case: app *)
  destruct~ (IHsub1). inductions H2.
    lets: (sub_star_star_inv H). false* sub_star_prod_false.
    false. destruct (sub_var_var_inv H) as [? (?&?&?)]. false* binds_empty_inv. 
    false. destruct (sub_any_top_inv H); false.
    destruct~ IHsub2. right. exists* (e ^^ e3).
    destruct H4 as [e3' ?]. right. exists* (trm_app (trm_abs A0 e) e3').
    destruct (sub_prod_prod_inv2 H) as (? & ? & ? & ?).
    destruct H4; false.
    false. destruct (sub_castup_inv H) as [A' (? & ? & ? & ?)].
      destruct (sub_any_prod_inv H5). destruct H6; subst. inversions H4.
      destruct H6 as [? [? ?]]; subst. inversions H4.
    destruct~ IHsub2.
    destruct H2 as [e1' ?].  right. exists* (trm_app e1' e3).
    destruct H3 as [e3' ?].  right. exists* (trm_app e1 e3').
  (* case: app_v *)
  destruct~ (IHsub1). inductions H2.
    lets: (sub_star_star_inv H). false* sub_star_prod_false.
    false. destruct (sub_var_var_inv H) as [? (?&?&?)]. false* binds_empty_inv. 
    false. destruct (sub_any_top_inv H); false.
    destruct~ IHsub2. right. exists* (e ^^ v).
    destruct H4 as [e3' ?]. false* (@value_cannot_red v).
    destruct (sub_prod_prod_inv2 H) as (? & ? & ? & ?).
    destruct H4; false.
    false. destruct (sub_castup_inv H) as [A' (? & ? & ? & ?)].
      destruct (sub_any_prod_inv H5). destruct H6; subst. inversions H4.
      destruct H6 as [? [? ?]]; subst. inversions H4.
    destruct H2 as [e1' ?].  right. exists* (trm_app e1' v).
  (* case: prod *)
  left. apply~ value_prod.
  apply_fresh~ lc_trm_prod as y.
  forwards~ P: H0 y.
  (* case: castdn *)
  destruct~ (IHsub).
    rename H1 into H2. rename H0 into H1. rename H into H0.
    asserts~ H: (lc_trm e1).
    inductions H2.
    destruct (sub_star_star_inv2 H0); subst. inversions H1. 
    inversions H1.
    false. destruct (sub_var_var_inv H0) as [? (?&?&?)]. false* binds_empty_inv. 
    lets P: sub_any_top_inv H0. destruct P; subst; inversions H1.
    destruct (sub_abs_abs_inv H0) as [A1 P]. destructs P.
    destruct (sub_prod_any_inv H5); subst. 
    inversions H1. destruct H7 as [? [? ?]]; subst; inversions H1.
    destruct (sub_prod_prod_inv2 H0) as (? & ? & ? & L).
    destruct H4; subst; inversions H1.
    right. exists* e.
    right. destruct H1 as [t' ?]. exists* (trm_castdn t').
Qed.