Set Implicit Arguments.
Require Import LibLN.
Require Import Alg_ott Alg_Infra.
Implicit Types x : var.

Lemma dsub_refl : forall G e1 e2 d A,
  dsub G e1 e2 d A ->
  dsub G e1 e1 d A /\ dsub G e2 e2 d A.
Proof.
  intros. inductions H; splits*.
  apply_fresh* dsub_abs as y.
  lets P: H1 y __. auto. destruct~ P.
  apply_fresh* dsub_abs as y.
  lets P: H1 y __. auto. destruct~ P.
  apply_fresh* dsub_prod as y.
  lets P: H3 y __. auto. destruct~ P.
  lets P: H3 y __. auto. destruct~ P.
Qed.

Hint Extern 1 (dsub ?G ?A ?A ?d ?B) => match goal with
  | H: dsub G A _ d B |- _ => apply (proj1 (dsub_refl H))
  | H: dsub G _ A d B |- _ => apply (proj2 (dsub_refl H))
  end.

Lemma dsub_weaken : forall G E F t1 t2 d T,
  dsub (E & G) t1 t2 d T ->
  dwf (E & F & G) ->
  dsub (E & F & G) t1 t2 d T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var refl *)
  apply* dsub_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh* (@dsub_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh* (@dsub_prod) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
Qed.

Lemma era_value : forall v,
  value v -> C.value (era v).
Proof.
  intros. induction H; simpls~.
  apply* C.value_abs. 
  inversions H0.
  apply_fresh* C.lc_trm_abs as y.
  rewrite* <- era_open. apply* era_lc_trm.
  apply* C.value_prod. 
  inversions H0.
  apply_fresh* C.lc_trm_prod as y.
  rewrite* <- era_open. apply* era_lc_trm.
Qed.

(* soundness *)
Lemma dsub_to_sub : forall G e1 e2 d A,
  dsub G e1 e2 d A -> C.sub (cera G) (era e1) (era e2) (era A).
Proof.
  apply dsub_induct with
  (P0 := fun E (_ : dwf E) => C.wf (cera E));
    intros; unfold cera; simpls; eauto 3.
  apply_fresh* C.sub_abs as y;
    do_rew_all <- era_open autos~;
    rewrite <- map_push.
    apply* H0. apply* H1.
  unfold open_trm_wrt_trm. rewrite era_open_rec.
    apply* C.sub_app_v. apply* era_value.
  apply_fresh* C.sub_prod as y;
    do_rew_all <- era_open autos~;
    rewrite <- map_push.
    apply* H0. apply* H1.
  rewrite* map_empty.
  rewrite* map_push.
Qed.

Lemma binds_map_exists : forall A B (f : A -> B) x b0 (E:LibEnv.env A),
  binds x b0 (map f E) ->
  exists a, f a = b0 /\ binds x a E.
Proof.
  intros. induction E using env_ind.
  rewrite map_empty in H.
  false* binds_empty_inv.
  autorewrite with rew_env_map in H.
  destruct (binds_push_inv H) as [[? ?]|[? ?]]. 
  subst~. exists* v.
  destruct (IHE H1) as [a1 [? ?]].
  subst. exists* a1.
Qed.

Lemma close_erase_rec : forall k x a,
  C.close_var_rec k x (erasure a) = erasure (close_var_rec k x a).
Proof.
  intros. gen k x. induction a; eauto; simpls; intros; fequal~.
  case_if~.
Qed.
  
Lemma close_erase : forall x a,
  CI.close_var x (erasure a) = erasure (close_var x a).
Proof.
  intros; apply~ close_erase_rec.
Qed.

Lemma close_open_rec : forall k x a,
  x \notin CI.fv a ->
  C.close_var_rec k x (C.open_trm_wrt_trm_rec k (C.trm_var_f x) a) = a.
Proof.
  intros. gen k x. induction a; simpls; eauto; intros; fequal~.
  case_if~. subst~. simpls. case_if~.
  case_if~. subst. rewrite notin_singleton in H. false~.
Qed.

Notation "t @ x" := (C.open_trm_wrt_trm t (C.trm_var_f x)) (at level 67).
Notation "t @@ u" := (C.open_trm_wrt_trm t u) (at level 67).

Lemma close_open : forall x a,
  x \notin CI.fv a ->
  CI.close_var x (a @ x) = a.
Proof.
  intros. apply~ close_open_rec.
Qed.

Lemma open_erase_rec : forall k u a,
  C.open_trm_wrt_trm_rec k (erasure u) (erasure a) = erasure (open_trm_wrt_trm_rec k u a).
Proof.
  intros. gen k u. induction a; eauto; simpls; intros; fequal~.
  case_if~; case_if~.
Qed.

Lemma open_erase : forall u a,
  (erasure a) @@ (erasure u) = erasure (a ^^ u).
Proof.
  intros. apply~ open_erase_rec.
Qed.

Lemma erase_pi : forall G AB0 CD0 A B C D d S,
    erasure AB0 = C.trm_prod A B ->
    erasure CD0 = C.trm_prod C D ->
    dsub G AB0 CD0 d S ->
    exists A1 B0 C1 D0,
      erasure AB0 = erasure (trm_prod A1 B0) /\
      erasure CD0 = erasure (trm_prod C1 D0) /\
      erasure A1 = A /\ erasure B0 = B /\
      erasure C1 = C /\ erasure D0 = D /\
      dsub G (trm_prod A1 B0) (trm_prod C1 D0) d trm_star.
Proof.
Admitted.

Lemma erasure_sort : forall G a b d A,
  dsub G a b d A -> erasure A = C.trm_star ->
  exists a' b', erasure a = erasure a' /\ 
                erasure b = erasure b' /\
                dsub G a' b' d trm_star.
Proof.
Admitted.

Lemma erasure_cvt :  forall G a b d A,
  dsub G a b d A -> forall B, 
  dsub G B B d trm_star -> erasure A = erasure B ->
  exists a' b', erasure a = erasure a' /\ 
                erasure b = erasure b' /\
                dsub G a' b' d B.
Proof.
Admitted.

Lemma sub_to_dsub' :
  forall G a b A, C.sub G a b A ->
    forall G0, map erasure G0 = G -> dwf G0 ->
    exists a0 b0 A0 d,
      erasure a0 = a /\
      erasure b0 = b /\
      erasure A0 = A /\
      dsub G0 a0 b0 d A0.
Proof.
  intros. inductions H.
  (* case ax *)
  exists* trm_star trm_star trm_star dir_inf.
  (* case var *)
  rewrite <- H1 in H0.
  apply binds_map_exists in H0.
  destruct H0 as [a [? ?]]. subst.
  exists* (trm_var_f x5) (trm_var_f x5) a dir_inf.
  (* case top *)
  destruct (IHsub _ H0 H1) as [a1 [b1 [A1 [d1 [EQ1 [EQ2 [EQ3 ?]]]]]]].
  exists* a1 trm_top A1 dir_chk. splits*.
  (* Stuck here *)
Admitted.