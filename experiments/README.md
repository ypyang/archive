This directory contains some experimental Coq scripts.

Require Coq v8.6. 

Type `make` in each folder to compile scripts.

Type `make doc` in each folder to re-generate PDF documents (if any).

* `suband`: Unified subtyping with intersection types
* `eq`: Unified equality, using similar forms of unified subtyping
* `sigcbv`: Call-by-name PITS with sigma types and experiments to relax value restriction
* `subsup`: Unified subtyping with both subtype and supertype relations
* `bidir`: Bidirectional unified subtyping (WIP)
* `bidir2`: Another attempt of bidirectional unified subtyping (WIP)
* `noctx`: Formalizing calculi without contexts (WIP)
