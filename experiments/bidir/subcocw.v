Set Implicit Arguments.
Require Import LibLN.
Implicit Types x : var.

(* Syntax *)

Inductive trm : Set :=
  | trm_bvar   : nat -> trm
  | trm_fvar   : var -> trm
  | trm_star   : trm
  | trm_top    : trm
  | trm_app    : trm -> trm -> trm
  | trm_abs    : trm -> trm -> trm -> trm
  | trm_prod   : trm -> trm -> trm -> trm
  | trm_castup : trm -> trm -> trm
  | trm_castdn : trm -> trm
  | trm_anno : trm -> trm -> trm.

Fixpoint open_rec (k : nat) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i        => If k = i then u else (trm_bvar i)
  | trm_fvar x        => trm_fvar x 
  | trm_star          => trm_star
  | trm_top           => trm_top
  | trm_app t1 t2     => trm_app (open_rec k u t1) (open_rec k u t2)
  | trm_abs t0 t1 t2  => trm_abs (open_rec k u t0) (open_rec k u t1) (open_rec (S k) u t2) 
  | trm_prod t0 t1 t2 => trm_prod (open_rec k u t0) (open_rec k u t1) (open_rec (S k) u t2) 
  | trm_castup t1 t2  => trm_castup (open_rec k u t1) (open_rec k u t2)
  | trm_castdn t1     => trm_castdn (open_rec k u t1)
  | trm_anno t1 t2     => trm_anno (open_rec k u t1) (open_rec k u t2)
  end.

Definition open t u := open_rec 0 u t.

Notation "{ k ~> u } t" := (open_rec k u t) (at level 67).
Notation "t ^^ u" := (open t u) (at level 67).
Notation "t ^ x" := (open t (trm_fvar x)).

Inductive term : trm -> Prop :=
  | term_var : forall x, 
      term (trm_fvar x)
  | term_app : forall t1 t2,
      term t1 -> term t2 -> 
      term (trm_app t1 t2)
  | term_sort :
      term (trm_star)
  | term_top : term trm_top
  | term_abs : forall L t0 t1 t2,
      term t0 -> term t1 ->
      (forall x, x \notin L -> term (t2 ^ x)) -> 
      term (trm_abs t0 t1 t2)
  | term_prod : forall L t0 t1 t2,
      term t0 -> term t1 ->
      (forall x, x \notin L -> term (t2 ^ x)) -> 
      term (trm_prod t0 t1 t2)
  | term_castup : forall t1 t2,
      term t1 -> term t2 -> 
      term (trm_castup t1 t2)
  | term_castdn : forall t1,
      term t1 ->
      term (trm_castdn t1)
  | term_anno : forall t1 t2,
      term t1 -> term t2 -> 
      term (trm_anno t1 t2)
.

Definition body t :=
  exists L, forall x, x \notin L -> term (t ^ x). 

Definition relation := trm -> trm -> Prop.

(* reduction relation *)

Inductive ivalue : trm -> Prop :=
  | ivalue_top : ivalue trm_top
  | ivalue_app : forall u e,
      ivalue u -> term e -> ivalue (trm_app u e)
  | ivalue_castdn : forall u,
      ivalue u -> ivalue (trm_castdn u).

Inductive value : trm -> Prop :=
  | value_sort : value trm_star
  | value_ivalue : forall u, ivalue u -> value u
  | value_abs : forall e1 A e2,
      term (trm_abs e1 A e2) -> value (trm_abs e1 A e2)
  | value_prod : forall e1 A B,
      term (trm_prod e1 A B) -> value (trm_prod e1 A B)
  | value_castup : forall t v,
      term t -> value v -> value (trm_castup t v).

Inductive reduct : trm -> trm -> Prop :=
  | reduct_beta : forall e A e1 e2,
      term (trm_abs e A e1) ->
      term e2 ->
      reduct (trm_app (trm_abs e A e1) e2) (e1 ^^ e2)
  | reduct_app : forall e1 e1' e2,
      term e2 -> reduct e1 e1' ->
      reduct (trm_app e1 e2) (trm_app e1' e2)
  | reduct_castdn : forall e e',
      reduct e e' ->
      reduct (trm_castdn e) (trm_castdn e')
  | reduct_castup : forall t e e',
      term t -> reduct e e' ->
      reduct (trm_castup t e) (trm_castup t e')
  | reduct_castelim : forall t v,
      term t -> value v ->
      reduct (trm_castdn (trm_castup t v)) v.

Inductive castred : trm -> trm -> Prop :=
  | castred_beta : forall e A e1 e2,
      term (trm_abs e A e1) ->
      term e2 ->
      castred (trm_app (trm_abs e A e1) e2) (e1 ^^ e2)
  | castred_app : forall e1 e1' e2,
      term e2 -> castred e1 e1' ->
      castred (trm_app e1 e2) (trm_app e1' e2)
  | castred_castdn : forall e e',
      castred e e' ->
      castred (trm_castdn e) (trm_castdn e')
  | castred_castup : forall t e e',
      term t -> castred e e' ->
      castred (trm_castup t e) (trm_castup t e').

Notation "t --> t'" := (reduct t t') (at level 67).
Notation "t ~~> t'" := (castred t t') (at level 67).

(* Typing *)

Inductive env_typ : Type :=
  env_bnd : trm -> trm -> env_typ.

Definition env := LibEnv.env env_typ.

Implicit Types E : env.

Inductive sub : env -> trm -> trm -> trm -> Prop :=
  | sub_ax : forall E,
      wf E ->
      sub E trm_star trm_star trm_star
  | sub_var_refl : forall E x e A,
      wf E ->
      binds x (env_bnd e A) E ->
      sub E (trm_fvar x) (trm_fvar x) A
  | sub_var_trans : forall e1 E x e2 A,
      binds x (env_bnd e1 A) E ->
      sub E e1 e2 A ->
      sub E (trm_fvar x) e2 A
  | sub_top : forall E e A,
      sub E e e A ->
      sub E e trm_top A
  | sub_top_refl : forall E A,
      sub E A A trm_star ->
      sub E trm_top trm_top A
  | sub_abs : forall L E e1 A e2 e2' B,
      sub E e1 e1 A ->
      sub E A A (trm_star) ->
      (forall x, x \notin L ->
        sub (E & x ~ env_bnd e1 A) (B ^ x) (B ^ x) (trm_star)) ->
      (forall x, x \notin L ->
        sub (E & x ~ env_bnd e1 A) (e2 ^ x) (e2' ^ x) (B ^ x)) ->
      sub E (trm_abs e1 A e2) (trm_abs e1 A e2') (trm_prod e1 A B)
  | sub_app : forall E e3 e1 e2 A B C,
      sub E A e3 B ->
      sub E e1 e2 (trm_prod e3 B C) ->
      sub E (trm_app e1 A) (trm_app e2 A) (C ^^ A)
  | sub_prod : forall L E e A B A' B',
      sub E e e A' ->
      sub E A' A (trm_star) ->
      sub E A A (trm_star) ->
      (forall x, x \notin L ->
        sub (E & x ~ env_bnd e A) (B ^ x) (B ^ x) (trm_star)) ->
      (forall x, x \notin L ->
        sub (E & x ~ env_bnd e A') (B ^ x) (B' ^ x) (trm_star)) ->
      sub E (trm_prod e A B) (trm_prod e A' B') (trm_star)
  | sub_castup : forall E e1 e2 A B,
      sub E e1 e2 A ->
      sub E B B (trm_star) ->
      castred B A ->
      sub E (trm_castup B e1) (trm_castup B e2) B
  | sub_castdn : forall E e1 e2 A B,
      sub E e1 e2 A ->
      sub E B B (trm_star) ->
      castred A B ->
      sub E (trm_castdn e1) (trm_castdn e2) B
  | sub_sub : forall A E e1 e2 B,
      sub E e1 e2 A ->
      sub E A B (trm_star) ->
      sub E e1 e2 B
with wf : env -> Prop :=
  | wf_nil : wf empty
  | wf_cons : forall E x A B,
      x # E ->
      sub E A A B ->
      sub E B B (trm_star) ->
      wf (E & x ~ env_bnd A B).

Scheme sub_induct := Induction for sub Sort Prop
  with wf_induct := Induction for wf Sort Prop.

(* infrastructure *)

Fixpoint fv (t : trm) {struct t} : vars :=
  match t with
  | trm_bvar i        => \{}
  | trm_fvar x        => \{x}
  | trm_star          => \{}
  | trm_top           => \{}
  | trm_app t1 t2     => (fv t1) \u (fv t2)
  | trm_abs t0 t1 t2  => (fv t0) \u (fv t1) \u (fv t2)
  | trm_prod t0 t1 t2 => (fv t0) \u (fv t1) \u (fv t2)
  | trm_castup t1 t2  => (fv t1) \u (fv t2)
  | trm_castdn t1     => (fv t1)
  | trm_anno t1 t2    => (fv t1) \u (fv t2)
  end.

Fixpoint subst (z : var) (u : trm) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i        => trm_bvar i 
  | trm_fvar x        => If x = z then u else (trm_fvar x)
  | trm_star          => trm_star
  | trm_top           => trm_top
  | trm_app t1 t2     => trm_app (subst z u t1) (subst z u t2)
  | trm_abs t0 t1 t2  => trm_abs (subst z u t0) (subst z u t1) (subst z u t2)
  | trm_prod t0 t1 t2 => trm_prod (subst z u t0) (subst z u t1) (subst z u t2)
  | trm_castup t1 t2  => trm_castup (subst z u t1) (subst z u t2)
  | trm_castdn t1     => trm_castdn (subst z u t1)
  | trm_anno t1 t2     => trm_anno (subst z u t1) (subst z u t2)
  end.

Notation "[ z ~> u ] t" := (subst z u t) (at level 68).

Fixpoint close_var_rec (k : nat) (z : var) (t : trm) {struct t} : trm :=
  match t with
  | trm_bvar i        => trm_bvar i 
  | trm_fvar x        => If x = z then (trm_bvar k) else (trm_fvar x)
  | trm_star          => trm_star
  | trm_top           => trm_top
  | trm_app t1 t2     => trm_app (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_abs t0 t1 t2  => trm_abs (close_var_rec k z t0) (close_var_rec k z t1) (close_var_rec (S k) z t2) 
  | trm_prod t0 t1 t2 => trm_prod (close_var_rec k z t0) (close_var_rec k z t1) (close_var_rec (S k) z t2) 
  | trm_castup t1 t2  => trm_castup (close_var_rec k z t1) (close_var_rec k z t2)
  | trm_castdn t1     => trm_castdn (close_var_rec k z t1)
  | trm_anno t1 t2     => trm_anno (close_var_rec k z t1) (close_var_rec k z t2)
  end.

Definition close_var z t := close_var_rec 0 z t.

Definition contains_terms E :=
  forall x T U, binds x (env_bnd T U) E -> term T /\ term U.

Definition simulated (R1 R2 : relation) := 
  forall (t t' : trm), R1 t t' -> R2 t t'.
 
Infix "simulated_by" := simulated (at level 69).


Definition red_regular (R : relation) :=
  forall t t', R t t' -> term t /\ term t'.

Definition red_refl (R : relation) :=
  forall t, term t -> R t t.

Definition red_in (R : relation) :=
  forall t x u u', term t -> R u u' ->
  R ([x ~> u]t) ([x ~> u']t).
  
Definition red_all (R : relation) :=
  forall x t t', R t t' -> 
  forall u u', R u u' -> 
  R ([x~>u]t) ([x~>u']t').

Definition red_out (R : relation) :=
  forall x u t t', term u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Definition red_rename (R : relation) :=
  forall x t t' y,
  R (t ^ x) (t' ^ x) -> 
  x \notin (fv t) -> x \notin (fv t') ->
  R (t ^ y) (t' ^ y).

Definition red_through (R : relation) :=
  forall x t1 t2 u1 u2, 
  x \notin (fv t1) -> x \notin (fv u1) ->
  R (t1 ^ x) (u1 ^ x) -> R t2 u2 ->
  R (t1 ^^ t2) (u1 ^^ u2).

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : trm => fv x) in
  let D := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D).

Ltac pick_fresh X :=
  let L := gather_vars in (pick_fresh_gen L X).

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

Ltac exists_fresh := 
  let L := gather_vars_with (fun x : vars => x) in exists L.

Hint Constructors ivalue value reduct castred sub wf.

Hint Constructors term.

Lemma open_rec_term_core :forall e j v i u, i <> j ->
  {j ~> v}e = {i ~> u}({j ~> v}e) -> e = {i ~> u}e.
Proof.
  induction e; introv Neq Equ;
  simpl in *; inversion* Equ; f_equal*.  
  case_nat*. case_nat*.
Qed.

Lemma open_rec_term : forall t u,
  term t -> forall k, t = {k ~> u}t.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open. pick_fresh x.
   apply* (@open_rec_term_core t2 0 (trm_fvar x)).
  unfolds open. pick_fresh x. 
   apply* (@open_rec_term_core t2 0 (trm_fvar x)).
Qed.

Lemma subst_fresh : forall x t u, 
  x \notin fv t -> [x ~> u] t = t.
Proof.
  intros. induction t; simpls; fequals*.
  case_var*. 
Qed.

Lemma subst_open : forall x u t1 t2, term u -> 
  [x ~> u] (t1 ^^ t2) = ([x ~> u]t1) ^^ ([x ~> u]t2).
Proof.
  intros. unfold open. generalize 0.
  induction t1; intros; simpl; f_equal*.
  case_nat*. case_var*. apply* open_rec_term.
Qed.

Lemma subst_open_var : forall x y u e, y <> x -> term u ->
  ([x ~> u]e) ^ y = [x ~> u] (e ^ y).
Proof.
  introv Neq Wu. rewrite* subst_open.
  simpl. case_var*.
Qed.

Lemma subst_intro : forall x t u, 
  x \notin (fv t) -> term u ->
  t ^^ u = [x ~> u](t ^ x).
Proof.
  introv Fr Wu. rewrite* subst_open.
  rewrite* subst_fresh. simpl. case_var*.
Qed.

Ltac cross := 
  rewrite subst_open_var; try cross.

Tactic Notation "cross" "*" := 
  cross; autos*.

Lemma subst_term : forall t z u,
  term u -> term t -> term ([z ~> u]t).
Proof.
  induction 2; simple*.
  case_var*.
  apply_fresh* term_abs as y. rewrite* subst_open_var.
  apply_fresh* term_prod as y. rewrite* subst_open_var.
Qed.

Lemma open_term : forall t u,
  body t -> term u -> term (t ^^ u).
Proof.
  intros. destruct H. pick_fresh y.
  rewrite* (@subst_intro y). apply* subst_term.
Qed.

Hint Resolve subst_term open_term.

Lemma term_abs0 : forall t1 t2 t0,
  term (trm_abs t0 t1 t2) -> term t0.
Proof.
  intros. inversion* H.
Qed.

Lemma term_abs1 : forall t0 t2 t1,
  term (trm_abs t0 t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_abs2 : forall t0 t1 t2,  
  term (trm_abs t0 t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma term_prod0 : forall t1 t2 t0,
  term (trm_prod t0 t1 t2) -> term t0.
Proof.
  intros. inversion* H.
Qed.

Lemma term_prod1 : forall t0 t2 t1,
  term (trm_prod t0 t1 t2) -> term t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_prod2 : forall t0 t1 t2,  
  term (trm_prod t0 t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Hint Extern 1 (term ?t0) =>
  match goal with
  | H: term (trm_abs t0 ?t1 ?t2) |- _ => apply (@term_abs0 t1 t2)
  | H: term (trm_prod t0 ?t1 ?t2) |- _ => apply (@term_prod0 t1 t2)
  end.

Hint Extern 1 (term ?t1) =>
  match goal with
  | H: term (trm_abs ?t0 t1 ?t2) |- _ => apply (@term_abs1 t0 t2)
  | H: term (trm_prod ?t0 t1 ?t2) |- _ => apply (@term_prod1 t0 t2)
  end.

Hint Extern 3 (body ?t) =>
  match goal with 
  | H: context [trm_abs ?t0 ?t1 t] |- _ => apply (@body_abs2 t0 t1)
  | H: context [trm_prod ?t0 ?t1 t] |- _ => apply (@body_prod2 t0 t1)
  end.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: context [t ^ _] |- _ =>
      let x := fresh in let Fr := fresh in
      let P := fresh in
      let L := gather_vars in exists L; intros x Fr; 
      lets P: H x __; [ notin_solve 
                      | try destructs P ]
  end.

Lemma term_abs_prove : forall t0 t1 t2,
  term t0 -> term t1 -> body t2 -> term (trm_abs t0 t1 t2).
Proof.
  intros. apply_fresh* term_abs as x.
Qed.

Lemma term_prod_prove : forall t0 t1 t2,
  term t0 -> term t1 -> body t2 -> term (trm_prod t0 t1 t2).
Proof.
  intros. apply_fresh* term_prod as x.
Qed.

Hint Resolve term_abs_prove term_prod_prove.

Lemma body_prove : forall L t,
  (forall x, x \notin L -> term (t ^ x)) -> body t.
Proof.
  intros. exists* L.
Qed.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: forall _, _ \notin ?L -> term (t ^ _)  |- _ =>
    apply (@body_prove L)
  end. 

Lemma body_subst : forall x t u,
  term u -> body t -> body ([x ~> u]t).
Proof.
  introv. intros Wu [L Bt].
  exists (\{x} \u L). intros y Fr. cross*.
Qed.

Hint Resolve body_subst.

Lemma open_var_inj : forall x t1 t2, 
  x \notin (fv t1) -> x \notin (fv t2) -> 
  (t1 ^ x = t2 ^ x) -> (t1 = t2).
Proof.
  intros x t1. unfold open. generalize 0.
  induction t1; intro k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal* 
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma close_var_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv t1) ->
    {i ~> trm_fvar y} ({j ~> trm_fvar z} (close_var_rec j x t1) )
  = {j ~> trm_fvar z} (close_var_rec j x ({i ~> trm_fvar y}t1) ).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ]. 
  case_var*. simpl. case_nat*. 
Qed.

Lemma close_var_fresh : forall x t,
  x \notin fv (close_var x t).
Proof.
  introv. unfold close_var. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Lemma close_var_body : forall x t,
  term t -> body (close_var x t).
Proof.
  introv W. exists \{x}. intros y Fr.
  unfold open, close_var. generalize 0. gen y.
  induction W; intros y Fr k; simpls; try solve [autos*].
  case_var; simple*. case_nat*.
  apply_fresh* term_abs as z.
   unfolds open. rewrite* close_var_rec_open.
  apply_fresh* term_prod as z.
   unfolds open. rewrite* close_var_rec_open.
Qed.

Lemma close_var_open : forall x t,
  term t -> t = (close_var x t) ^ x.
Proof.
  introv W. unfold close_var, open. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open. rewrite* close_var_rec_open.
Qed. 

Lemma close_var_spec : forall t x, term t -> 
  exists u, t = u ^ x /\ body u /\ x \notin (fv u).
Proof.
  intros. exists (close_var x t). splits 3.
  apply* close_var_open.
  apply* close_var_body.
  apply* close_var_fresh.
Qed. 

Lemma ivalue_is_term : forall t, ivalue t -> term t.
Proof.
  intros_all. induction* H.
Qed.

Lemma value_is_term : forall t, value t -> term t.
Proof.
  intros_all. induction* H.
  apply* ivalue_is_term.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: ivalue t |- _ => apply (ivalue_is_term H)
  | H: value t |- _ => apply (value_is_term H) end.

Lemma red_regular_reduct : red_regular reduct.
Proof.
  intros_all. induction* H.
Qed.

Lemma red_regular_castred : red_regular castred.
Proof.
  intros_all. induction* H.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: reduct t _ |- _ => apply (proj1 (red_regular_reduct H))
  | H: reduct _ t |- _ => apply (proj2 (red_regular_reduct H))
  end.

Hint Extern 1 (term ?t) => match goal with
  | H: castred t _ |- _ => apply (proj1 (red_regular_castred H))
  | H: castred _ t |- _ => apply (proj2 (red_regular_castred H))
  end.

Hint Extern 1 (term (trm_abs ([?x ~> ?u]?t0) ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: term (trm_abs t0 t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_abs t0 t1 t2)) end.

Hint Extern 1 (term (trm_prod ([?x ~> ?u]?t0) ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: term (trm_prod t0 t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_prod t0 t1 t2)) end.

Lemma regular_sub : forall G e1 e2 A,
  sub G e1 e2 A ->
  (wf G /\ ok G /\ contains_terms G /\ term e1 /\ term e2 /\ term A). 
Proof.
  apply sub_induct with
  (P0 := fun E (_ : wf E) => ok E /\ contains_terms E);
    unfolds contains_terms; intros; splits*.
  destruct H as [? ?].
  lets: (H0 x e A b). jauto.
  intros. false* binds_empty_inv.
  intros. destruct (binds_push_inv H1) as [[? ?]|[? ?]].
    inversion H3. subst*.
    destruct H0 as (? & ? & HH & ?). apply* HH.
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: sub _ t _ _ |- _ => apply (proj31 (proj44 (regular_sub H)))
  | H: sub _ _ t _ |- _ => apply (proj32 (proj44 (regular_sub H)))
  | H: sub _ _ _ t |- _ => apply (proj33 (proj44 (regular_sub H)))
  end.

Lemma ok_from_wf : forall G, wf G -> ok G.
Proof.
  induction 1. auto. autos* (regular_sub H0).
Qed.

Hint Extern 1 (ok ?E) => match goal with
  | H: wf E |- _ => apply (ok_from_wf H)
  end.

Hint Extern 1 (wf ?E) => match goal with
  | H: sub E _ _ _ |- _ => apply (proj1 (regular_sub H))
  end.

Lemma wf_push_inv : forall G x A B,
  wf (G & x ~ env_bnd A B) -> wf G /\ term A /\ term B.
Proof.
  introv W. inversions W. 
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]]. inversion H3.
  subst~.
  destruct (eq_push_inv H) as [? [? ?]]. inversion H4.
  subst~.
Qed.

Lemma term_from_binds_in_wf : forall x E T U,
  wf E -> binds x (env_bnd T U) E -> term T /\ term U.
Proof.
  introv W Has. gen E. induction E using env_ind; intros.
  false* binds_empty_inv.
  induction v.
  destruct (wf_push_inv W). 
  destruct (binds_push_inv Has) as [[? ?] | [? ?]].
  inversion H2. subst*.
  apply* IHE.
Qed.

Lemma term_from_binds_in_wf_left : forall x E T U,
  wf E -> binds x (env_bnd T U) E -> term T.
Proof.
  intros. destruct~ (term_from_binds_in_wf H H0).
Qed.

Lemma term_from_binds_in_wf_right : forall x E T U,
  wf E -> binds x (env_bnd T U) E -> term U.
Proof.
  intros. destruct~ (term_from_binds_in_wf H H0).
Qed.

Hint Extern 1 (term ?t) => match goal with
  | H: binds ?x (env_bnd t ?t2) ?E |- _ => apply (@term_from_binds_in_wf_left x E)
  | H: binds ?x (env_bnd ?t2 t) ?E |- _ => apply (@term_from_binds_in_wf_right x E)
  end.

Lemma wf_left : forall E F : env,
  wf (E & F) -> wf E.
Proof.
  intros. induction F using env_ind.
  rewrite~ concat_empty_r in H.
  rewrite concat_assoc in H.
   inversions H. false (empty_push_inv H1).
   destruct (eq_push_inv H0) as [? [? ?]]. subst~. 
Qed.

Implicit Arguments wf_left [E F].

Lemma fv_open_var : forall y x t,
  x <> y -> x \notin fv (t ^ y) -> x \notin fv t.
Proof.
  introv Neq. unfold open. generalize 0. 
  induction t; simpl; intros; try notin_solve; autos*.
Qed.

Lemma fresh_false : forall G x (v:env_typ),
  x # G & x ~ v -> False.
Proof.
  intros. simpl_dom.
  destruct (notin_union x \{x} (dom G)) as [H1 _].
  lets: H1 H.
  destruct H0 as [H3 _].
  apply* notin_same.
Qed.

Lemma and_simpl : forall (A : Prop),
    A -> A /\ A.
Proof.
  intros. auto.
Qed.

Lemma fv_open_term : forall A B x,
  x \notin fv A -> x \notin fv B ->
  x \notin fv (A ^^ B).
Proof.
  intros A. unfold open. generalize 0. 
  induction A; simpl; intros; try (notin_simpl); autos*.
  case_nat~. 
Qed.

Lemma fv_close_var : forall y x t,
  x <> y -> x \notin fv t -> x \notin fv (t ^ y).
Proof.
  introv Neq. unfold open. generalize 0. 
  induction t; simpl; intros; try notin_solve; try autos*.
  case_nat~. simpl. auto.
Qed.

Lemma sub_fresh : forall G A B x,
  sub G A B (trm_star) -> x # G -> (x \notin fv A) /\ (x \notin fv B).
Proof.
  introv Typ.
  induction Typ; simpls; intros; try (apply and_simpl).
  auto.
  rewrite notin_singleton. intro. subst. applys binds_fresh_inv H0 H1.
  split.
    rewrite notin_singleton. intro. subst. applys binds_fresh_inv H H0.
    apply* IHTyp.
  splits*. 
  auto.
  destruct (IHTyp1 H3).
  destruct (IHTyp2 H3).
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y). auto. auto. auto.
  splits*.
  destruct (IHTyp1 H3).
  destruct (IHTyp2 H3).
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y). auto. auto. auto.
  splits*.
  splits*.
  auto.
Qed.

Lemma notin_fv_from_wf : forall E F x U T,
  wf (E & x ~ env_bnd U T & F) -> x \notin fv T.
Proof.
  intros.
  lets W: (wf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. inversion H5. subst~.
  apply* sub_fresh.
Qed.

Lemma notin_fv_from_binds_right : forall x y T U E,
  wf E -> binds y (env_bnd T U) E -> x # E -> x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  induction v.
  destruct (wf_push_inv H).
    destruct (binds_push_inv H0) as [[? ?] | [? ?]].
    inversion H5. subst. inversions H. false* (empty_push_inv H6).
     destruct (eq_push_inv H4) as [? [? ?]].  inversion H9. subst~. 
     apply* sub_fresh.
    autos*.
Qed.

Lemma sub_fresh_all : forall C G A B x,
  sub G A B C -> x # G -> x \notin fv A /\ x \notin fv B /\ x \notin fv C.
Proof.
  introv Typ.
  induction Typ; simpls; intros; try (apply and_simpl).
  auto.
  splits; try (rewrite notin_singleton).
    intro. subst. applys binds_fresh_inv H0 H1.
    intro. subst. applys binds_fresh_inv H0 H1.
    apply* notin_fv_from_binds_right.
  splits.
     rewrite notin_singleton. intro. subst. applys binds_fresh_inv H H0.
     apply* IHTyp.
     apply* IHTyp.
  splits*.
  splits*.
  destruct (IHTyp1 H3) as [? [? ?]].
  destruct (IHTyp2 H3) as [? [? ?]].
  splits; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y) as [? [? ?]]. auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y) as [? [? ?]]. auto. auto. auto.
  splits*.
  destruct (IHTyp1 H) as [? [? ?]].
  destruct (IHTyp2 H) as [? [? ?]].
  apply* fv_open_term.
  destruct (IHTyp1 H3) as [? [? ?]].
  destruct (IHTyp2 H3) as [? [? ?]].
  splits; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y) as [? [? ?]]. auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y) as [? [? ?]]. auto. auto. auto.
  splits*.
  splits*.
  splits*.
Qed.

Lemma notin_fv_from_binds : forall x y T U E,
  wf E -> binds y (env_bnd T U) E -> x # E -> x \notin fv T /\ x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  induction v.
  destruct (wf_push_inv H).
    destruct (binds_push_inv H0) as [[? ?] | [? ?]].
    inversion H5. subst. inversions H. false* (empty_push_inv H6).
     destruct (eq_push_inv H4) as [? [? ?]].  inversion H9. subst~. 
     apply* sub_fresh_all.
    autos*.
Qed.

Lemma notin_fv_from_binds' : forall E F x V1 V2 y U1 U2,
  wf (E & x ~ env_bnd V1 V2 & F) -> 
  binds y (env_bnd U1 U2) E ->
  x \notin fv U1 /\ x \notin fv U2.
Proof.
  intros. lets W: (wf_left H). inversions keep W.
  false (empty_push_inv H2). 
  destruct (eq_push_inv H1) as [? [? ?]]. inversion H5. subst~. 
  lets W': (wf_left W). apply* notin_fv_from_binds.
Qed.

Lemma notin_fv_from_binds'_left : forall E F x V y U1 U2,
  wf (E & x ~ V & F) -> 
  binds y (env_bnd U1 U2) E ->
  x \notin fv U1.
Proof.
  intros. induction V.
  destruct~ (notin_fv_from_binds' H H0).
Qed.
  
Lemma notin_fv_from_binds'_right : forall E F x V y U1 U2,
  wf (E & x ~ V & F) -> 
  binds y (env_bnd U1 U2) E ->
  x \notin fv U2.
Proof.
  intros. induction V.
  destruct~ (notin_fv_from_binds' H H0).
Qed.

Hint Extern 1 (?x \notin fv ?V) => 
  match goal with H: wf (?E & x ~ (env_bnd ?U V) & ?F) |- _ =>
    apply (@notin_fv_from_wf E F) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: wf (?E & x ~ ?V & ?F), B: binds ?y (env_bnd U ?U') ?E |- _ =>
    apply (@notin_fv_from_binds'_left E F x V y) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: wf (?E & x ~ ?V & ?F), B: binds ?y (env_bnd ?U' U) ?E |- _ =>
    apply (@notin_fv_from_binds'_right E F x V y) end.

Lemma sub_var_refl_empty_inv : forall x A,
  sub empty (trm_fvar x) (trm_fvar x) A -> False.
Proof.
  intros. asserts* W: (x # (empty : env)).
  destruct (sub_fresh_all H W) as [? [? ?]].
  simpl in H0.
  rewrite notin_singleton in H0.
  auto.
Qed.

Lemma sub_var_trans_empty_inv : forall x e A,
  sub empty (trm_fvar x) e A -> False.
Proof.
  intros. asserts* W: (x # (empty : env)).
  destruct (sub_fresh_all H W) as [? [? ?]].
  simpl in H0.
  rewrite notin_singleton in H0.
  auto.
Qed.

Lemma wf_to_wt : forall G x e A,
  wf (G & x ~ env_bnd e A) -> sub G e e A /\ sub G A A (trm_star).
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]. inversion H4. subst*.
Qed.

Lemma sub_prod_prod_inv1 : forall G e A B B0 C,
   sub G (trm_prod e A B) (trm_prod e A B0) C ->
   sub G e e A /\
   (sub G A A (trm_star)).
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_refl_left : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G e1 e1 A.
Proof.
  intros. inductions H; try solve [eauto 2].
  apply_fresh* (@sub_prod) as y.
Qed.

Hint Extern 1 (sub ?G ?A ?A ?B) => match goal with
  | H: sub G A _ B |- _ => apply ((sub_refl_left H))
  end.

Lemma sub_weaken : forall G E F t1 t2 T,
  sub (E & G) t1 t2 T ->
  wf (E & F & G) ->
  sub (E & F & G) t1 t2 T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var refl *)
  apply* sub_var_refl. apply* binds_weaken.
  (* case: var trans *)
  apply* sub_var_trans. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp1 E0 F G eq_refl W).
  lets: (IHTyp2 E0 F G eq_refl W).
  apply_fresh* (@sub_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  (* case: trm_prod *)
  apply_fresh* (@sub_prod) as y;
    lets: (IHTyp1 E0 F G eq_refl W);
    lets: (IHTyp2 E0 F G eq_refl W);
    lets: (sub_refl_left H4).
  apply_ih_bind* H0.
  apply_ih_bind* H2.
Qed.

Lemma sub_typekeep' : forall Q E T A B,
  sub E Q T B -> sub E Q Q A -> sub E Q T A.
Proof.
  intro Q. introv SsubQ QsubT. asserts* W: (term Q).
  gen E T B A. set_eq Q' EQ: Q. gen Q' EQ.
  induction W; intros Q' EQ E T A SsubQ;
    induction SsubQ; try discriminate; inversions EQ;
      intros BB QsubT'; eauto 2.
  inductions QsubT'.
    lets: (binds_func H H0).
    inversions H2. apply* (@sub_var_trans e).
    lets: (binds_func H H0).
    inversions H1. apply* (@sub_var_trans e0).
    autos*.
  inductions QsubT'.
    apply sub_app with (B := B0) (e3 := e0). auto.
    apply* IHW1.
    apply* (@sub_sub A).
  inductions QsubT'.
    apply_fresh* sub_abs as y.
    apply* (@sub_sub A).
  inductions QsubT'.
    apply_fresh* (@sub_prod) as y.
    apply* (@sub_sub A).
  inductions QsubT'.
    apply* sub_castup.
    apply* (@sub_sub A0).
  inductions QsubT'.
    apply sub_castdn with (A := A0).
    apply* IHW. auto. auto.
    apply* (@sub_sub A0).
Qed.

Lemma sub_typekeep : forall B G e1 e2 A,
  sub G e1 e1 A ->
  sub G e1 e2 B ->
  sub G e1 e2 A.
Proof.
  intros. apply* sub_typekeep'.
Qed.

Lemma sub_narrowing_typ : forall Q A' A F E B Z S T,
  sub (E & Z ~ env_bnd Q A & F) S T B ->
  sub E Q Q A' ->
  sub E A' A (trm_star) ->
  sub (E & Z ~ env_bnd Q A' & F) S T B.
Proof.
  introv Sa Sb Sc.
  gen_eq G: (E & Z ~ env_bnd Q A & F). gen F.
  apply sub_induct with
    (P := fun G S T B (Sub: sub G S T B) =>
            forall F, G = E & Z ~ env_bnd Q A & F ->
                      sub (E & Z ~ env_bnd Q A' & F) S T B)
    (P0 := fun G (W: wf G) =>
            forall F, G = E & Z ~ env_bnd Q A & F ->
                      wf (E & Z ~ env_bnd Q A' & F));
    intros; subst; simpls subst.
   (* case: ax *)
  apply* sub_ax.
  (* case: var refl *)
  tests EQ: (x = Z).
    asserts~ N1: (wf (E & Z ~ env_bnd Q A' & F)).
    asserts~ N': (ok (E & Z ~ env_bnd Q A & F)).
    lets: (binds_middle_eq_inv b N').
    destruct (ok_middle_inv N').
    inversions H0.
    apply (@sub_sub A').
    apply sub_var_refl with (e := Q). auto.
    apply* binds_middle_eq.
    apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    asserts* W1: (x # (Z ~ env_bnd Q A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply sub_var_refl with (e := e). auto.
    apply binds_weaken. auto. auto.
  (* case: var trans *)
  tests EQ: (x = Z).
    asserts~ WF: (wf (E & Z ~ env_bnd Q A & F)).
    asserts~ N: (ok (E & Z ~ env_bnd Q A & F)).
    destruct (ok_middle_inv N).
    lets: (binds_middle_eq_inv b N).
    inversions H2.
    apply (@sub_sub A').
    apply sub_var_trans with (e1 := Q). auto.
    lets: (H F eq_refl).
    lets: (@sub_typekeep' Q (E & Z ~ env_bnd Q A' & F) e2 A' A).
    apply H3. auto. apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    lets: (H F eq_refl). auto.
    asserts* W1: (x # (Z ~ env_bnd Q A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply sub_var_trans with (e1 := e1). auto.
    apply binds_weaken. auto. 
    asserts* WF: (wf (E & Z ~ env_bnd Q A' & F)). auto.
    auto.
  (* case: top *)
  autos*.
  (* case: top refl *)
  autos*.
  (* case: abs *)
  apply_fresh* sub_abs as Y.
  apply_ih_bind* H1.
  apply_ih_bind* H2.
  (* case: app *)
  autos*.
  (* case: prod *)
  apply_fresh* (@sub_prod) as Y.
  apply_ih_bind* H2. apply_ih_bind* H3.
  (* case: castup *)
  autos*.
  (* case: castdn *)
  autos*.
  (* case: sub *)
  autos*.
  (* case: wf nil *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  destruct (env_case F).
    subst. rewrite concat_empty_r.
    rewrite concat_empty_r in H1.
    destruct (eq_push_inv H1) as [? [? ?]].
    inversions H3. apply* wf_cons.
    destruct H2 as [y [v' [E' Eq]]].
    induction v'. subst.
    rewrite concat_assoc.
    rewrite concat_assoc in H1.
    destruct (eq_push_inv H1) as [? [? ?]].
    inversions H3. apply* wf_cons.
  (* conclusion *)
  auto.
Qed.

Definition substenv (x:var) (v:trm) (p:env_typ) :=
  match p with
  | env_bnd e A => env_bnd (subst x v e) (subst x v A)
  end.

Lemma substenv_eq : forall x v e1 e2,
  env_bnd ([x ~> v] e1) ([x ~> v] e2) = substenv x v (env_bnd e1 e2).
Proof.
  intros. simpls*.
Qed.

Lemma substenv_fresh : forall x t1 t2 u, 
  x \notin fv t1 -> x \notin fv t2 ->
  substenv x u (env_bnd t1 t2) = env_bnd t1 t2.
Proof.
  intros. unfolds substenv.
  rewrite* subst_fresh.
  rewrite* subst_fresh.
Qed.

Lemma notin_fv_from_wf_left : forall E F x U T,
  wf (E & x ~ env_bnd U T & F) -> x \notin fv U.
Proof.
  intros.
  lets W: (wf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. inversion H5. subst~.
  destruct (sub_fresh_all H2 H1).
  auto.
Qed.

Lemma map_substenv_fresh : forall x v F, 
  wf F -> x # F -> map (substenv x v) F = F.
Proof.
  intros. induction F using env_ind.
  apply* map_empty.
  rewrite* map_push.
  inductions v0.
  asserts* b: (binds x0 (env_bnd t t0) (F & x0 ~ env_bnd t t0)).
  lets: (notin_fv_from_binds H b H0).
  destruct H1.
  rewrite* (@substenv_fresh x t t0 v H1 H2).
  lets: wf_left H.
  lets: IHF H3 __. auto.
  rewrite* H4.
Qed.

Lemma ivalue_red_out : forall x u t,
  term u -> ivalue t -> ivalue ([x ~> u] t).
Proof.
  intros_all. induction H0; simpl.
  auto. apply* ivalue_app. apply* ivalue_castdn.
Qed.

Lemma value_red_out : forall x u t,
  term u -> value t -> value ([x ~> u] t).
Proof.
  intros_all. induction H0; simpl.
  auto. apply* value_ivalue.
  apply* ivalue_red_out.
  apply* value_abs. apply* value_prod.
  apply* value_castup.
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl.
  rewrite* subst_open.
  apply* reduct_app.
  apply* reduct_castdn.
  apply* reduct_castup.
  apply* reduct_castelim.
  apply* value_red_out.
Qed.

Lemma castred_red_out : red_out castred.
Proof.
  intros_all. induction H0; simpl.
  rewrite* subst_open.
  apply* castred_app.
  apply* castred_castdn.
  apply* castred_castup.
Qed.

Lemma ivalue_cannot_red : forall t t',
    ivalue t -> castred t t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv H2; inversions H2.
  inversions H1.
  apply* IHivalue.
  apply* IHivalue.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> castred t t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv H2;
    try solve [inversions H2; apply* ivalue_cannot_red].
  inversions H2.
  apply* IHvalue.
Qed.

Lemma castred_determ : forall t t1 t2,
    castred t t1 -> castred t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv H2; inversions H2.
  auto. inversion H6.
  inversions H1.
  pose (IHcastred e1'0 H6). rewrite e. auto.
  pose (IHcastred e'0 H0). rewrite e0. auto.
  pose (IHcastred e'0 H6). rewrite e0. auto.
Qed.

Definition transitivity_on Q := forall E S T A,
  sub E S Q A -> sub E Q T A -> sub E S T A.

Hint Unfold transitivity_on.

Lemma sub_substitution_aux : forall V e (F:env) v (E:env) x t1 t2 T,
  transitivity_on e ->
  sub E v e V ->
  sub (E & x ~ env_bnd e V & F) t1 t2 T ->
  sub (E & (map (substenv x v) F)) (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  introv TransE Typv Typt.
  gen_eq G: (E & x ~ env_bnd e V & F). gen F.
  apply sub_induct with
   (P := fun (G:env) t1 t2 T (Typt : sub G t1 t2 T) =>
      forall (F:env), G = E & x ~ env_bnd e V & F ->
      sub (E & map (substenv x v) F) ([x ~> v]t1) ([x ~> v]t2) ([x ~> v]T))
   (P0 := fun (G:env) (W:wf G) =>
      forall F, G = E & x ~ env_bnd e V & F ->
      wf (E & (map (substenv x v) F)));
   intros; subst; simpls subst.
  (* case: ax *)
  autos*.
  (* case: var refl *)
  case_var.
    binds_mid~. rename TEMP into H0.
    inversions H0. rewrite* subst_fresh. apply_empty* sub_weaken.
    apply* sub_fresh_all.
    apply sub_var_refl with (e := [x ~> v] e0). auto.
    rewrite substenv_eq.
    destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
    lets: (@notin_fv_from_binds' E F x e V x0 e0 A w) __. auto.
    rewrite* substenv_fresh.
  (* case: var trans *)
  case_var.
    binds_mid~. apply* ok_from_wf. rename TEMP into H0. inversions H0.
    lets: H F __. auto.
    apply* TransE.
    rewrite* subst_fresh.
    apply_empty* sub_weaken.
    asserts* W: (wf (E & x ~ env_bnd e V & F)).
    apply* notin_fv_from_wf.
    assert ([x ~> v] e = e).
    apply* subst_fresh.
    apply* notin_fv_from_wf_left.
    rewrite <- H1. auto.
    lets: H F __. auto.
    apply sub_var_trans with (e1 := ([x ~> v]e1)). auto.
    destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
    rewrite* substenv_eq.
    asserts* w: (wf (E & x ~ env_bnd e V & F)).
    lets: wf_left (wf_left w).
    asserts* N: (x # E).
    destruct~ (notin_fv_from_binds H1 K N).
    rewrite* subst_fresh.
    rewrite* subst_fresh.
    auto.
  (* case: top *)
  autos*.  
  (* case: top refl *)
  autos*.  
  (* case: abs *)
  apply_fresh* (@sub_abs) as y; rewrite* substenv_eq.
  cross; auto. apply_ih_map_bind* H1.
  cross; auto. apply_ih_map_bind* H2.
  (* case: app *)
  rewrite* subst_open.
  (* case: prod *)
  apply_fresh* (@sub_prod) as y; rewrite* substenv_eq.
  cross; auto. apply_ih_map_bind* H2.
  cross; auto. apply_ih_map_bind* H3.
  (* case: castup *)
  apply* sub_castup. apply* castred_red_out.
  (* case: castdn *)
  apply* sub_castdn. apply* castred_red_out.
  (* case: sub *)
  autos*.
  (* case: wf empty *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H1.
     destruct (eq_push_inv H1) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H1.
     destruct (eq_push_inv H1) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@wf_cons). 
  (* case: conclusion *)
  auto.
Qed.

Lemma sub_wf_from_context : forall x T U (E:env),
  binds x (env_bnd T U) E -> 
  wf E -> 
  sub E T T U /\
  sub E U U trm_star.
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    inductions v. inversions H0.
    inversions W. false (empty_push_inv H0).
     destruct (eq_push_inv H) as [? [? ?]]. inversions H4.
     splits. apply_empty* sub_weaken.
     apply_empty* sub_weaken.
    inductions v.
    destruct (wf_push_inv W). forwards~ [k P]: IHE.
     splits. apply_empty* sub_weaken.
     apply_empty* sub_weaken.
Qed.

Lemma sub_prod_prod_inv : forall G e A B B0 C,
   sub G (trm_prod e A B) (trm_prod e A B0) C ->
   (exists L, forall x, x \notin L ->
        sub (G & x ~ env_bnd e A) (B ^ x) (B0 ^ x) (trm_star)).
Proof.
  intros. inductions H; eauto.
Qed.

Lemma sub_narrowing_aux : forall Q F E A B Z P S T,
  transitivity_on Q ->
  sub (E & Z ~ env_bnd Q A & F) S T B ->
  sub E P Q A ->
  sub (E & Z ~ env_bnd P A & F) S T B.
Proof.
  introv TransQ SsubT PsubQ.
  gen_eq G: (E & Z ~ env_bnd Q A & F). gen F.
  apply sub_induct with
    (P := fun G S T B (Sub: sub G S T B) =>
            forall F, G = E & Z ~ env_bnd Q A & F ->
                      sub (E & Z ~ env_bnd P A & F) S T B)
    (P0 := fun G (W: wf G) =>
            forall F, G = E & Z ~ env_bnd Q A & F ->
                      wf (E & Z ~ env_bnd P A & F));
    intros; subst; simpls subst.
  (* case: ax *)
  apply* sub_ax.
  (* case: var refl *)
  tests EQ: (x = Z).
    asserts~ N: (ok (E & Z ~ env_bnd Q A & F)).
    destruct (ok_middle_inv N).
    lets: (binds_middle_eq_inv b N).
    inversions H2.
    apply* sub_var_refl.
    apply sub_var_refl with (e := e). auto.
    asserts* W1: (x # (Z ~ env_bnd Q A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply* binds_weaken.
  (* case: var trans *)
  tests EQ: (x = Z).
    asserts~ N: (ok (E & Z ~ env_bnd Q A & F)).
    apply* ok_from_wf.
    destruct (ok_middle_inv N).
    lets: (binds_middle_eq_inv b N).
    inversions H2.
    applys~ (@sub_var_trans P).
    apply TransQ.
    lets: H F __. auto.
    do_rew* concat_assoc (apply_empty* sub_weaken).
    autos*.
    apply* sub_var_trans.
    asserts* W1: (x # (Z ~ env_bnd Q A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply* binds_weaken.
    apply* ok_from_wf.
  (* case: top *)
  autos*.
  (* case: top refl *)
  autos*.
  (* case: abs *)
  apply_fresh* sub_abs as Y.
  apply_ih_bind* H1.
  apply_ih_bind* H2.
  (* case: app *)
  autos*.
  (* case: prod *)
  apply_fresh* (@sub_prod) as Y.
  apply_ih_bind* H2. apply_ih_bind* H3.
  (* case: castup *)
  autos*.
  (* case: castdn *)
  autos*.
  (* case: sub *)
  autos*.
  (* case: wf nil *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  destruct (env_case F).
    subst. rewrite concat_empty_r.
    rewrite concat_empty_r in H1.
    destruct (eq_push_inv H1) as [? [? ?]].
    inversions H3. apply* wf_cons.
    destruct H2 as [y [v' [E' Eq]]].
    induction v'. subst.
    rewrite concat_assoc.
    rewrite concat_assoc in H1.
    destruct (eq_push_inv H1) as [? [? ?]].
    inversions H3. apply* wf_cons.
  (* conclusion *)
  auto.
Qed.
  
Lemma sub_trans_aux : forall e2 G e1 e3 A B,
  sub G e1 e2 A -> sub G e2 e3 B -> sub G e1 e3 A.
Proof.
  introv S1. asserts* W: (term e2).
  gen G A e1 B e3. set_eq e2' eq: e2. gen e2' eq.
  induction W; intros e2' eq G A e1' S1;
    induction S1; try discriminate; inversions eq;
      intros B' e3' S2; try solve [eauto 3 using sub_typekeep].
  (* case: app *)
  inductions S2. clear IHW2. autos*.
  apply sub_app with (B := B) (e3 := e3). auto. 
  apply* IHW1.
  lets P: IHS2_1 IHW2 __. auto. auto. lets Q: P IHW1 __. auto.
  auto. apply* Q.
  (* case: top *)
  inductions S2; autos*.
  (* case lam *)
  inductions S2. apply sub_top.
  pick_fresh y.
  apply (@sub_abs L0). auto. auto. intros.
  apply* H1. intros. lets: (sub_refl_left (H3 x H5)). auto.
  apply_fresh sub_abs as y. auto. auto.
  apply* H1. lets: H0 y __. auto.
  lets: H9 (t2 ^ y) __. auto.
  apply* H10.
  apply* IHS2_1.
  (* case pi *)
  inductions S2. apply sub_top.
  pick_fresh y.
  lets: H1 y __. auto. asserts* W: (wf (E & y ~ env_bnd t0 A)).
  destruct (wf_to_wt W) as [? ?].
  apply (@sub_prod (L \u L0)). auto. auto. auto. intros.
  apply* H1. intros. apply* H1.
  pick_fresh y.
  lets: H5 y __. auto. asserts* W: (wf (E & y ~ env_bnd t0 A')).
  destruct (wf_to_wt W) as [? ?].
  apply (@sub_prod (L \u L0 \u L1)). auto.
  lets: IHW2 t1 __. auto.
  apply H12 with (B := (trm_star)).
  apply* sub_typekeep. auto. auto.
  intros. apply* H1. intros.
  lets: H0 x __. auto.  lets: H13 (t2 ^ x) __. auto.
  apply H14 with (B := (trm_star)).
  lets: H3 x __. auto.
  apply_empty* (@sub_narrowing_typ t0 A' t1).
  apply~ H8.
  lets P: IHS2_1 IHW2 __. auto. auto. auto. auto. auto.
  lets Q: P IHW1 __. auto. auto. auto.
  (* case: castup *)
  inductions S2. autos*. apply* sub_castup.
  apply* sub_typekeep.
  (* case: castdn *)
  inductions S2. autos*. apply* sub_castdn.
  apply* sub_typekeep.
Qed.

Lemma sub_trans : forall e2 G e1 e3 A,
  sub G e1 e2 A -> sub G e2 e3 A -> sub G e1 e3 A.
Proof.
  intros. apply* (@sub_trans_aux e2).
Qed.

Lemma sub_narrowing : forall Q F E A B Z P S T,
  sub (E & Z ~ env_bnd Q A & F) S T B ->
  sub E P Q A ->
  sub (E & Z ~ env_bnd P A & F) S T B.
Proof.
  intros.
  apply* sub_narrowing_aux.
  unfold transitivity_on.
  intros. apply* (@sub_trans Q).
Qed.

Lemma sub_substitution : forall V e (F:env) v (E:env) x t1 t2 T,
  sub E v e V ->
  sub (E & x ~ env_bnd e V & F) t1 t2 T ->
  sub (E & (map (substenv x v) F)) (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  intros.
  apply sub_substitution_aux with (V := V) (e := e).
  unfold transitivity_on.
  intros. apply* (@sub_trans e).
  auto. auto.
Qed.

Lemma sub_refl_right : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G e2 e2 A
with sub_wf_from_sub : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G A A trm_star.
Proof.
  clear sub_refl_right.
  introv H. inductions H; try solve [eauto 2].
  apply sub_top_refl. apply (sub_wf_from_sub E e e A H).

  clear sub_wf_from_sub.
  intros. inductions H; try solve [eauto 2].
  destruct* (sub_wf_from_context H0).
  destruct (sub_prod_prod_inv IHsub2) as [L T'].
  pick_fresh x. rewrite~ (@subst_intro x).
  unsimpl ([x ~> A](trm_star)).
  apply_empty* (@sub_substitution B e3).
Qed.

Hint Extern 1 (sub ?G ?A ?A ?B) => match goal with
  | H: sub G _ A B |- _ => apply ((sub_refl_right H))
  end.

Lemma sub_refl : forall G e1 e2 A,
  sub G e1 e2 A ->
  sub G e1 e1 A /\ sub G e2 e2 A.
Proof.
  intros. splits*.
Qed.

Lemma sub_abs_abs_inv : forall G e e0 A A0 B B0 C,
   sub G (trm_abs e A B) (trm_abs e0 A0 B0) C -> exists C',
   A = A0 /\ e = e0 /\
   sub G (trm_prod e A C') C (trm_star) /\
   (exists L, forall x, x \notin L ->
        sub (G & x ~ env_bnd e A) (B ^ x) (B0 ^ x) (C' ^ x)).
Proof.
  intros. inductions H; eauto.
  exists B1. splits*.
  lets P: IHsub1 e e0 A A0 B.
  lets Q: P B0.
  lets Q1: Q __. auto.
  lets Q2: Q1 __. auto.
  destruct Q2 as [C' [? [? ?]]].
  exists C'. splits*.
  apply* (@sub_trans_aux A1).
Qed.

Lemma sub_sort_any_inv : forall E A B,
  sub E (trm_star) A B ->
  A = trm_star \/ A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_top_any_inv : forall E A B,
  sub E trm_top A B ->
  A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_prod_prod_inv2 : forall G e e' A A' B B0 C,
   sub G (trm_prod e A B) (trm_prod e' A' B0) C ->
   ((C = trm_star) \/ C = trm_top) /\
   e = e' /\
   sub G e e A' /\
   sub G A' A (trm_star) /\
   (exists L, forall x, x \notin L ->
        sub (G & x ~ env_bnd e A') (B ^ x) (B0 ^ x) (trm_star)).
Proof.
  intros. inductions H; eauto.
  splits*. destructs (IHsub1 _ _ _ _ _ _ eq_refl eq_refl).
  splits*. destruct H1.
  subst. destruct (sub_sort_any_inv H0).
  left*. right*.
  subst. lets: sub_top_any_inv H0. right*.
Qed.

Lemma sub_star_star_inv : forall E A,
  sub E trm_star trm_star A ->
  sub E trm_star A trm_star.
Proof.
  intros. inductions H; auto.
  apply* (@sub_trans A).
Qed.

Lemma sub_star_any_inv : forall E A B,
  sub E trm_star A B ->
  A = trm_star \/ (A = trm_top).
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_star_star_inv2 : forall E A,
  sub E trm_star trm_star A ->
  A = trm_star \/ (A = trm_top).
Proof.
  intros. inductions H; auto.
  destruct IHsub1; subst.
  destruct (sub_star_any_inv H); auto. auto.
  destruct (sub_star_any_inv H0); auto. 
  right.
  destruct~ (sub_top_any_inv H0).
Qed.

Lemma sub_castup_inv : forall E t1 t2 t1' t2' B,
  sub E (trm_castup t1 t2) (trm_castup t1' t2') B ->
  exists A, t1 = t1' /\ sub E t2 t2' A /\
              castred t1 A /\
              sub E t1 B trm_star.
Proof.
  intros. inductions H.
  exists A. splits*.
  destruct (IHsub1 _ _ _ _ eq_refl eq_refl) as [A0 [? [? ?]]].
  exists A0. splits*.
  apply* (@sub_trans_aux A).
Qed.

Lemma sub_castdn_inv : forall E t2 B,
  sub E (trm_castdn t2) (trm_castdn t2) B ->
  exists A t1, sub E t2 t2 A /\
               castred A t1 /\
               sub E t1 B trm_star.
Proof.
  intros. inductions H.
  exists A B. splits*.
  destruct (IHsub1 _ eq_refl eq_refl) as [A0 [t1 [? ?]]].
  exists A0 t1. splits*.
  apply* (@sub_trans_aux A).
Qed.

Lemma sub_any_prod_inv : forall E P e1 A B C,
  sub E P (trm_prod e1 A B) C ->
  (exists x, P = trm_fvar x) \/
  (exists e1' A' B', P = trm_prod e1' A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_prod_any_inv : forall E P e1 A B C,
  sub E (trm_prod e1 A B) P C ->
  P = trm_top \/
  (exists e1' A' B', P = trm_prod e1' A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_abs_any_inv : forall E P e1 A B C,
  sub E (trm_abs e1 A B) P C ->
  P = trm_top \/
  (exists e1' A' B', P = trm_abs e1' A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_any_abs_inv : forall E P e1 A B C,
  sub E P (trm_abs e1 A B) C ->
  (exists x, P = trm_fvar x) \/
  (exists e1' A' B', P = trm_abs e1' A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_castup_any_inv : forall E P A B C,
  sub E (trm_castup A B) P C ->
  P = trm_top \/
  (exists A' B', P = trm_castup A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_any_castup_inv : forall E P A B C,
  sub E P (trm_castup A B) C ->
  (exists x, P = trm_fvar x) \/
  (exists A' B', P = trm_castup A' B').
Proof.
  intros. inductions H; autos*.
Qed.

Lemma sub_star_prod_false : forall E e A B C,
  sub E trm_star (trm_prod e A B) C ->
  False.
Proof.
  intros. destruct (sub_any_prod_inv H).
  destruct H0 as [? ?]. inversions H0.
  destruct H0 as [? [? [? ?]]]. inversions H0.
Qed.

Lemma sub_prod_star_false : forall E e A B C,
  sub E (trm_prod e A B) trm_star C ->
  False.
Proof.
  intros. destruct (sub_prod_any_inv H).
  inversions H0.
  destruct H0 as [? [? [? ?]]]. inversions H0.
Qed.

Lemma sub_castred_preserve : forall E t1 t2 t1' t2' A,
  sub E t1 t2 A -> 
  t1 ~~> t1' ->
  t2 ~~> t2' ->
  sub E t1' t2' A.
Proof.
  introv Typ. gen t1' t2'.
  induction Typ; introv Red1 Red2;
    try solve [inversions Red1 | inversions Red2 | eauto 3 using sub_sub].
  inversions Red1; inversions Red2.
  destruct (sub_abs_abs_inv Typ2) as [C' (? & ? & ? & [L ?])]. subst.
  destruct (sub_prod_prod_inv2 H4) as (? & ? & ? & ? & (L' & ?)). subst.
  pick_fresh x.
   rewrite* (@subst_intro x e0).
   rewrite* (@subst_intro x e4).
   rewrite* (@subst_intro x C).
  apply_empty* (@sub_substitution B e3).
  lets: H9 x __. auto.
  apply* (@sub_sub (C' ^ x)).
  apply_empty* (@sub_narrowing_typ e3 B A1).

  destruct (sub_abs_any_inv Typ2).
    subst. inversions H5.
    destruct H as [? [? [? ?]]]. subst. inversions H5.
  
  destruct (sub_any_abs_inv Typ2).
    destruct H as [? ?]. subst. inversions H3.
    destruct H as [? [? [? ?]]]. subst. inversions H3.
  
  apply sub_app with (e3 := e3) (B := B). auto.
  apply* IHTyp2.

  (* case: castup *)
  inversions Red1; inversions Red2.
  apply* sub_castup.

  (* case: castdn *)
  inversions Red1; inversions Red2.
  apply* sub_castdn.
Qed.

Lemma subject_reduction_wh : forall E t t' T,
  sub E t t T -> t --> t' -> sub E t' t' T.
Proof.
  introv Typ. gen t'.
  induction Typ; introv Red;
    try solve [ apply* sub_sub | apply* IHTyp ]; inversions Red.

  destruct (sub_refl Typ2).
  destruct (sub_abs_abs_inv H0) as [C' [? [? [? [L ?]]]]].
  destruct (sub_prod_prod_inv2 H5) as (? & ? & ? & ? & (L' & ?)).
  subst.
  pick_fresh x.
   rewrite* (@subst_intro x e0).
   rewrite* (@subst_intro x C).
  apply_empty (@sub_substitution B e3).
  auto.
  lets: H6 x __. auto.
  apply* (@sub_sub (C' ^ x)).
  apply_empty* (@sub_narrowing_typ e3 B A0).

  apply sub_app with (e3 := e3) (B := B). auto. auto.
  
  autos*.
  autos*.
  destruct (sub_refl Typ1).
  destruct (sub_castup_inv H3) as [A' [? [? [? ?]]]]. subst.
  lets: (sub_castred_preserve H7 H6 H).
  apply* (@sub_sub A').
Qed.

Lemma progress_wh : forall t T,
  sub empty t t T ->
  value t \/ exists t', t --> t'.
Proof.
  intros. inductions H; auto.
  (* case: var refl *)
  false* binds_empty_inv.
  (* case: var trans *)
  false* binds_empty_inv.
  (* case: abs *)
  left. apply value_abs. apply* term_abs.
  intros. lets: H3 x H5. auto.
  (* case: app *)
  destruct~ IHsub2. inductions H1.
    lets: (sub_star_star_inv H0). false* sub_star_prod_false.
    left. apply* value_ivalue.
    right. exists* (e2 ^^ A).
    destruct (sub_prod_prod_inv2 H0) as (? & ? & ? & ? & (L' & ?)).
    destruct H2; false.
    false. destruct (sub_castup_inv H0) as [A' (? & ? & ? & ?)].
      destruct (sub_any_prod_inv H6). destruct H7; subst. inversions H5.
      destruct H7 as [? [? [? ?]]]; subst. inversions H5.
    destruct H1 as [e1' ?].  right. exists* (trm_app e1' A).
  (* case: prod *)
  left. apply value_prod. apply* term_prod.
  intros. lets: H4 x H6. auto.
  (* case: castup *)
  destruct~ IHsub1. destruct H2 as [t' ?].
    right. exists (trm_castup B t'). auto.
  (* case: castdn *)
  destruct~ IHsub1. inductions H2.
    destruct (sub_star_star_inv2 H); subst. inversions H1. 
    inversions H1.
    left*.
    destruct (sub_abs_abs_inv H) as [C' [? [? [? [L ?]]]]].
    destruct (sub_prod_any_inv H5); subst. 
    inversions H1. destruct H7 as [? [? [? ?]]]; subst; inversions H1.
    destruct (sub_prod_prod_inv2 H) as (? & ? & ? & ? & (L' & ?)).
    destruct H3; subst. inversions H1. inversions H1.
    right. exists* v.
    right. destruct H2 as [t' ?]. exists* (trm_castdn t').
Qed.

