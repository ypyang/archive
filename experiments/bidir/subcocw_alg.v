Set Implicit Arguments.
Require Import LibLN subcocw.
Implicit Types x : var.

Fixpoint remove_anno (e : trm) {struct e} : trm :=
  match e with
  | trm_bvar i        => trm_bvar i 
  | trm_fvar x        => trm_fvar x
  | trm_star          => trm_star
  | trm_top           => trm_top
  | trm_app t1 t2     => trm_app (remove_anno t1) (remove_anno t2)
  | trm_abs t0 t1 t2  => trm_abs (remove_anno t0) (remove_anno t1) (remove_anno t2)
  | trm_prod t0 t1 t2 => trm_prod (remove_anno t0) (remove_anno t1) (remove_anno t2)
  | trm_castup t1 t2  => trm_castup (remove_anno t1) (remove_anno t2)
  | trm_castdn t1     => trm_castdn (remove_anno t1)
  | trm_anno t1 t2    => remove_anno t1
  end.

Notation "[# e #]" := (remove_anno e) (at level 2).

Definition rem_anno_env (p:env_typ) :=
  match p with
  | env_bnd e A => env_bnd (remove_anno e) (remove_anno A)
  end.

Lemma rem_anno_env_eq : forall e1 e2,
  env_bnd [#e1#] [#e2#] = rem_anno_env (env_bnd e1 e2).
Proof.
  intros. simpls*.
Qed.

Notation "{# E #}" := (map rem_anno_env E) (at level 2).

Inductive dir := inf | chk.

Inductive dsub : env -> dir -> trm -> trm -> trm -> Prop :=
  | dsub_ax : forall E,
      dwf E ->
      dsub E inf trm_star trm_star trm_star
  | dsub_var_refl : forall E x e A,
      dwf E ->
      binds x (env_bnd e A) E ->
      dsub E inf (trm_fvar x) (trm_fvar x) A
  | dsub_var_trans : forall e1 A E x e2,
      binds x (env_bnd e1 A) E ->
      dsub E chk e1 e2 A ->
      dsub E inf (trm_fvar x) e2 A
  | dsub_top : forall E e A,
      dsub E inf e e A ->
      dsub E inf e trm_top A
  | dsub_top_refl : forall E A,
      dsub E chk A A trm_star ->
      dsub E chk trm_top trm_top A
  | dsub_abs : forall L E e1 A e2 e2' B,
      dsub E chk e1 e1 A ->
      dsub E chk A A (trm_star) ->
      (forall x, x \notin L ->
        dsub (E & x ~ env_bnd e1 A) chk (B ^ x) (B ^ x) (trm_star)) ->
      (forall x, x \notin L ->
        dsub (E & x ~ env_bnd e1 A) inf (e2 ^ x) (e2' ^ x) (B ^ x)) ->
      dsub E inf (trm_abs e1 A e2) (trm_abs e1 A e2') (trm_prod e1 A B)
  | dsub_app : forall E e3 e1 e2 A B C,
      dsub E chk A e3 B ->
      dsub E inf e1 e2 (trm_prod e3 B C) ->
      dsub E inf (trm_app e1 A) (trm_app e2 A) (C ^^ A)
  | dsub_prod : forall L E e A B A' B',
      dsub E chk e e A' ->
      dsub E chk e e A ->
      dsub E chk A' A trm_star ->
      dsub E chk A A trm_star ->
      (forall x, x \notin L ->
        dsub (E & x ~ env_bnd e A) chk (B ^ x) (B ^ x) (trm_star)) ->
      (forall x, x \notin L ->
        dsub (E & x ~ env_bnd e A') chk (B ^ x) (B' ^ x) (trm_star)) ->
      dsub E inf (trm_prod e A B) (trm_prod e A' B') (trm_star)
  | dsub_castup : forall E e1 e2 A B,
      dsub E inf e1 e2 A ->
      dsub E chk B B (trm_star) ->
      castred [#B#] [#A#] ->
      dsub E inf (trm_castup B e1) (trm_castup B e2) B
  | dsub_castdn : forall E e1 e2 A B,
      dsub E inf e1 e2 A ->
      dsub E chk B B (trm_star) ->
      castred [#A#] [#B#] ->
      dsub E chk (trm_castdn e1) (trm_castdn e2) B
  | dsub_anno : forall E e1 e2 A,
      dsub E chk e1 e2 A ->
      dsub E inf (trm_anno e1 A) (trm_anno e2 A) A
  | dsub_sub : forall A E e1 e2 B,
      dsub E inf e1 e2 A ->
      dsub E chk A B (trm_star) ->
      dsub E chk e1 e2 B
with dwf : env -> Prop :=
  | dwf_nil : dwf empty
  | dwf_cons : forall E x A B,
      x # E ->
      dsub E chk A A B ->
      dsub E chk B B (trm_star) ->
      dwf (E & x ~ env_bnd A B).

Hint Constructors dsub dwf.

Scheme dsub_induct := Induction for dsub Sort Prop
  with dwf_induct := Induction for dwf Sort Prop.

Lemma open_rem_comm : forall A B,
  [# A ^^ B #] = [#A#] ^^ [#B#].
Proof.
  intros. unfolds open. generalize 0.
  induction A; intros; simpls; autos*.
  case_nat~.
  rewrite* (IHA1 n). rewrite* (IHA2 n).
  rewrite* (IHA1 n). rewrite* (IHA2 n). rewrite* (IHA3 (S n)).
  rewrite* (IHA1 n). rewrite* (IHA2 n). rewrite* (IHA3 (S n)).
  rewrite* (IHA1 n). rewrite* (IHA2 n).
  rewrite* (IHA n).
Qed.

Lemma dsub_regular : forall E d e1 e2 A,
  dsub E d e1 e2 A -> dwf E.
Proof.
  intros. inductions H; auto.
Qed.

(* Soundness *)
Lemma dsub_to_sub : forall E d e1 e2 A,
  dsub E d e1 e2 A ->
  sub {#E#} [#e1#] [#e2#] [#A#].
Proof.
  apply dsub_induct with
  (P0 := fun E (W: dwf E) => wf {#E#});
    intros; simpls; try solve [eauto 2].
  apply* (@sub_var_refl {#E#} x [#e#] [#A#]).
  rewrite* (rem_anno_env_eq).
  apply* (@sub_var_trans).
  rewrite* (rem_anno_env_eq).
  apply_fresh* (@sub_abs) as y;
    rewrite* rem_anno_env_eq.
  lets: H1 y __. auto.
  rewrite* map_push in H3.
  rewrite* open_rem_comm in H3.
  lets: H2 y __. auto.
  rewrite* map_push in H3.
  rewrite* open_rem_comm in H3.
  rewrite* open_rem_comm in H3.
  rewrite* open_rem_comm in H3.
  rewrite* open_rem_comm.
  apply_fresh* (@sub_prod) as y;
    rewrite* rem_anno_env_eq.
  lets P: H3 y __. auto.
  rewrite* map_push in P.
  rewrite* open_rem_comm in P.
  lets P: H4 y __. auto.
  rewrite* map_push in P.
  rewrite* open_rem_comm in P.
  rewrite* open_rem_comm in P.
  rewrite* map_empty.
  rewrite* map_push.
  apply* wf_cons.
Qed.


Lemma sub_to_dsub : forall E e1 e2 A,
  sub E e1 e2 A ->
  exists E' e1' e2' A', dsub E' inf e1' e2' A' /\
                        [#e1'#] = e1 /\ [#e2'#] = e2 /\
                        [#A'#] = A /\ {#E'#} = E.
Proof.
  apply sub_induct with
    (P0 := fun E (W: wf E) => exists E', dwf E' /\ {#E'#} = E); intros.
  destruct H as [E' [? ?]].
  exists E' trm_star trm_star trm_star. splits*.
  destruct H as [E' [? ?]].
  admit. admit.
  destruct H as (E'&e1'&e2'&A'&?&?&?&?&?).
  exists E' e1' trm_top A'. splits*.
  apply* dsub_top. admit.
  destruct H as (E'&e1'&e2'&A'&?&?&?&?&?).
  exists E' (trm_anno trm_top e1') (trm_anno trm_top e1') e1'. splits*.
  apply* dsub_anno. apply* dsub_top_refl.
  

Admitted.  
  
  

Inductive tr : Type :=
  | Tr : env -> trm -> trm -> trm -> tr.

Inductive trans : env -> trm -> trm -> trm -> tr -> Prop :=
  | trans_ax : forall E E',
      trenv E E' ->
      trans E trm_star trm_star trm_star (Tr E' trm_star trm_star trm_star)
  | trans_var_refl : forall E x A E' e e' A',
      trenv E E' ->
      binds x (env_bnd e' A') E' ->
      binds x (env_bnd e A) E ->
      trans E (trm_fvar x) (trm_fvar x) A (Tr E' (trm_fvar x) (trm_fvar x) A')
  | trans_var_trans : forall e1 A e1' A' E E' x e2 e2',
      binds x (env_bnd e1' A') E' ->
      binds x (env_bnd e1 A) E ->
      trans E e1 e2 A (Tr E' e1' e2' A') ->
      trans E (trm_fvar x) e2 A (Tr E' (trm_fvar x) e2' A')
  | trans_top : forall E e A E' e' A',
      trans E e e A (Tr E' e' e' A') ->
      trans E e trm_top A (Tr E' e' trm_top A')
  | trans_top_refl : forall E A E' A',
      trans E A A trm_star (Tr E' A' A' trm_star) ->
      trans E trm_top trm_top A (Tr E' (trm_anno trm_top A') (trm_anno trm_top A') A')
  | trans_abs : forall L E e1 A e2 e3 B E' e1' A' e2' e3' B',
      trans E e1 e1 A (Tr E' e1' e1' A') ->
      trans E A A (trm_star) (Tr E' A' A' trm_star) ->
      (forall x, x \notin L ->
        trans (E & x ~ env_bnd e1 A) (B ^ x) (B ^ x) (trm_star)
          (Tr (E' & x ~ env_bnd e1' A') (B' ^ x) (B' ^ x) trm_star)) ->
      (forall x, x \notin L ->
        trans (E & x ~ env_bnd e1 A) (e2 ^ x) (e3 ^ x) (B ^ x)
          (Tr (E' & x ~ env_bnd e1' A') (e2' ^ x) (e3' ^ x) (B' ^ x))) ->
      trans E (trm_abs e1 A e2) (trm_abs e1 A e3) (trm_prod e1 A B)
            (Tr E' (trm_abs e1' A' e2') (trm_abs e1' A' e3') (trm_prod e1' A' B'))
  | trans_app : forall E e3 e1 e2 A B C E' e3' e1' e2' A' B' C',
      trans E A e3 B (Tr E' A' e3' B') ->
      trans E e1 e2 (trm_prod e3 B C) (Tr E' e1' e2' (trm_prod e3' B' C'))->
      trans E (trm_app e1 A) (trm_app e2 A) (C ^^ A)
            (Tr E' (trm_app e1' A') (trm_app e2' A') (C' ^^ A'))
  | trans_prod : forall L E e A B A1 B1 E' e' A' B' A1' B1',
      trans E e e A1 (Tr E' e' e' A1') ->
      trans E e e A (Tr E' e' e' A') ->
      trans E A1 A (trm_star) (Tr E' A1' A' trm_star) ->
      trans E A A (trm_star) (Tr E' A' A' trm_star) ->
      (forall x, x \notin L ->
        trans (E & x ~ env_bnd e A) (B ^ x) (B ^ x) (trm_star)
              (Tr (E' & x ~ env_bnd e' A') (B' ^ x) (B' ^ x) trm_star)) ->
      (forall x, x \notin L ->
        trans (E & x ~ env_bnd e A1) (B ^ x) (B1 ^ x) (trm_star)
              (Tr (E' & x ~ env_bnd e' A1') (B' ^ x) (B1' ^ x) trm_star)) ->
      trans E (trm_prod e A B) (trm_prod e A1 B1) (trm_star)
            (Tr E' (trm_prod e' A' B') (trm_prod e' A1' B1') trm_star)
  | trans_castup : forall E e1 e2 A B E' e1' e2' A' B',
      trans E e1 e2 A (Tr E' e1' e2' A') ->
      trans E B B (trm_star) (Tr E' B' B' trm_star) ->
      castred B A ->
      trans E (trm_castup B e1) (trm_castup B e2) B
            (Tr E' (trm_castup B' e1') (trm_castup B' e2') B')
  | trans_castdn : forall E e1 e2 A B E' e1' e2' A' B',
      trans E e1 e2 A (Tr E' e1' e2' A') ->
      trans E B B (trm_star) (Tr E' B' B' trm_star) ->
      castred A B ->
      trans E (trm_castdn e1) (trm_castdn e2) B
            (Tr E' (trm_anno (trm_castdn e1') B') (trm_anno (trm_castdn e2') B') B')
  | trans_sub : forall A E e1 e2 B A' E' e1' e2' B',
      trans E e1 e2 A (Tr E' e1' e2' A') ->
      trans E A B (trm_star) (Tr E' A' B' trm_star)->
      trans E e1 e2 B (Tr E' (trm_anno e1' B') (trm_anno e2' B') B')
with trenv : env -> env -> Prop :=
  | trenv_nil : trenv empty empty
  | trenv_cons : forall E x A B E' A' B',
      x # E ->
      trans E A A B (Tr E' A' A' B') ->
      trans E B B (trm_star) (Tr E' B' B' trm_star)->
      trenv (E & x ~ env_bnd A B) (E' & x ~ env_bnd A' B').

Scheme trans_induct := Induction for trans Sort Prop
  with trenv_induct := Induction for trenv Sort Prop.

Hint Constructors trans trenv.

Lemma dbinds_func: forall x e A F e' A',
  binds x (env_bnd e A) {#F#} ->
  binds x (env_bnd e' A') F ->
  [#e'#] = e /\ [#A'#] = A.
Proof.
  intros. induction F using env_ind.
  rewrite map_empty in H.
  false* (binds_empty_inv).
  rewrite map_push in H.
  induction v.
  destruct (binds_push_inv H0) as [[? ?]|[? ?]];
  destruct (binds_push_inv H) as [[? ?]|[? ?]].
  rewrite <- rem_anno_env_eq in H4.
  inversions H2. inversions H4. splits*.
  false*. false*.
  destructs (IHF H4 H2). splits*.
Qed.

Lemma rem_not_fv : forall x e,
  x \notin fv e ->
  x \notin fv [#e#].
Proof.
  intros. inductions e; simpls; autos*.
Qed.

Lemma dbinds_rem : forall x e A E,
  binds x (env_bnd e A) E ->
  binds x (env_bnd [#e#] [#A#]) {#E#}.
Proof.
  intros. induction E using env_ind.
  false* (binds_empty_inv).
  induction v.
  destruct (binds_push_inv H) as [[? ?]|[? ?]].
  inversions H1. rewrite* rem_anno_env_eq.
  lets: (IHE H1).
  rewrite* rem_anno_env_eq.
Qed.

Lemma trans_regular : forall E e1 e2 A E' e1' e2' A',
  trans E e1 e2 A (Tr E' e1' e2' A') ->
  trenv E E'.
Proof.
  intros. inductions H; eauto.
Qed.

Lemma trans_to_sub : forall E e1 e2 A E' e1' e2' A',
  trans E e1 e2 A (Tr E' e1' e2' A') ->
  sub E e1 e2 A /\
  {#E'#} = E /\ [#e1'#] = e1 /\
  [#e2'#] = e2 /\ [#A'#] = A.
Proof.
  intros. gen_eq r: (Tr E' e1' e2' A').
  gen E' e1' e2' A'.
  apply trans_induct with
    (P := fun E e1 e2 A r (R: trans E e1 e2 A r) => 
          forall E' e1' e2' A', r = Tr E' e1' e2' A' ->
                                  sub E e1 e2 A /\
                                  {#E'#} = E /\ [#e1'#] = e1 /\
                                  [#e2'#] = e2 /\ [#A'#] = A)
    (P0 := fun E E' (W: trenv E E') => wf E /\ {#E'#} = E); intros; simpls.
  inversions H1. splits*.
  destructs H0. inversions H1. destruct (dbinds_func b0 b). splits*.
  destructs (H0 _ _ _ _ eq_refl). subst. inversions H1. splits*.
  destructs (H0 _ _ _ _ eq_refl). subst. inversions H1. splits*.
  destructs (H0 _ _ _ _ eq_refl). subst. inversions H1. splits*.
  (* case: abs *)
  destructs (H0 E' e1' e1' A'). auto. subst.
  destructs (H1 E' A' A' trm_star). auto.
  inversions H4. 
  pick_fresh z.
  lets: H3 z __. auto.
  destructs~ (H4 (E'0 & z ~ env_bnd e1' A') (e2' ^ z) (e3' ^ z) (B' ^ z)).
  rewrite open_rem_comm in H14,H15,H16. simpl in H14,H15,H16.
  splits; simpls.
  apply_fresh* sub_abs as y. 
  lets: H2 y __. auto.
  destructs~ (H17 (E'0 & y ~ env_bnd e1' A') (B' ^ y) (B' ^ y) trm_star).
  lets: H3 y __. auto.
  destructs~ (H17 (E'0 & y ~ env_bnd e1' A') (e2' ^ y) (e3' ^ y) (B' ^ y)).
  auto.
  fequals. apply* open_var_inj. apply* rem_not_fv.
  fequals. apply* open_var_inj. apply* rem_not_fv.
  fequals. apply* open_var_inj. apply* rem_not_fv.
  (* case: app *)
  destructs~ (H0 E' A' e3' B'). subst.
  destructs~ (H1 E' e1' e2' (trm_prod e3' B' C')). subst.
  inversions H2. splits*.
  rewrite open_rem_comm. fequals.
  simpl in H8. inversions H8. auto.
  (* case: prod *)
  destructs~ (H0 E' e' e' A1'). subst.
  destructs~ (H1 _ _ _ _ eq_refl). subst.
  destructs~ (H2 _ _ _ _ eq_refl). subst.
  destructs~ (H3 _ _ _ _ eq_refl). subst.
  inversions H6. 
  pick_fresh z.
  lets P: H5 z __. auto.
  destructs~ (P (E'0 & z ~ env_bnd e' A1') (B' ^ z) (B1' ^ z) trm_star).
  rewrite open_rem_comm in *. simpl in *.
  splits; simpls.
  apply_fresh* sub_prod as y. 
  lets P1: H4 y __. auto.
  destructs~ (P1 (E'0 & y ~ env_bnd e' A') (B' ^ y) (B' ^ y) trm_star).
  lets P2: H5 y __. auto.
  destructs~ (P2 (E'0 & y ~ env_bnd e' A1') (B' ^ y) (B1' ^ y) trm_star).
  auto.
  fequals. apply* open_var_inj. apply* rem_not_fv.
  fequals. apply* open_var_inj. apply* rem_not_fv.
  auto.
  (* case: castup *)
  destructs~ (H0 E' e1' e2' A'). subst.
  destructs~ (H1 E' B' B' trm_star). subst.
  inversions H2. splits*.
  (* case: castdn *)
  destructs~ (H0 E' e1' e2' A'). subst.
  destructs~ (H1 E' B' B' trm_star). subst.
  inversions H2. splits*.
  (* case: sub *)
  destructs~ (H0 E' e1' e2' A'). subst.
  destructs~ (H1 E' A' B' trm_star). subst.
  inversions H2. splits*.
  (* case: wf nil *)
  splits*. rewrite* map_empty.
  (* case: wf cons *)
  destructs~ (H0 E' A' A' B'). subst.
  destructs~ (H1 E' B' B' trm_star). subst.
  splits*. rewrite* map_push.
  (* case: conclusion *)
  auto.
Qed.

Lemma trans_env_fresh : forall x E e1 e2 A E' e1' e2' A',
  trans E e1 e2 A (Tr E' e1' e2' A') ->
  x # E -> x # E'.
Proof.
  intros. gen_eq r: (Tr E' e1' e2' A').
  gen E' e1' e2' A' x.
  apply trans_induct with
    (P := fun E e1 e2 A r (R: trans E e1 e2 A r) => 
          forall E' e1' e2' A' x, x # E -> 
                                  r = Tr E' e1' e2' A' -> x # E')
    (P0 := fun E E' (W: trenv E E') => forall x, x # E -> x # E')
    (e := E) (t := e1) (t0 := e2) (t1 := A) (t2 := r);
    intros; simpls.
  inversions~ H2.
  inversions~ H2.
  inversions H2. apply* H0.
  inversions H2. apply* H0.
  inversions H2. apply* H0.
  inversions H5. apply* H0.
  inversions H3. apply* H0.
  inversions H7. apply* H0.
  inversions H3. apply* H0.
  inversions H3. apply* H0.
  inversions H3. apply* H0.
  auto.
  tests EQ: (x = x0). false* (fresh_false H2).
  asserts: (x # E').
  apply* H0.
  rewrite* dom_push.
  auto.
Qed.

Lemma trans_to_dsub : forall E e1 e2 A E' e1' e2' A',
  trans E e1 e2 A (Tr E' e1' e2' A') ->
  dsub E' inf e1' e2' A'.
Proof.
  intros. gen_eq r: (Tr E' e1' e2' A').
  gen E' e1' e2' A'.
  apply trans_induct with
    (P := fun E e1 e2 A r (R: trans E e1 e2 A r) => 
          forall E' e1' e2' A', r = Tr E' e1' e2' A' ->
                                dsub E' inf e1' e2' A')
    (P0 := fun E E' (W: trenv E E') => dwf E')
    (e := E) (t := e1) (t0 := e2) (t1 := A) (t2 := r);
    intros; simpls.
  inversions H1. eauto.
  inversions H1. eauto.
  destructs (trans_to_sub t).
   inversions H1. apply* dsub_var_trans. admit.
  inversions H1. eauto.
  inversions H1. apply* dsub_anno. admit.
  (* case: abs *)
  lets: (H0 E' e1' e1' A' eq_refl).
  lets: (H1 E' A' A' trm_star eq_refl).
  inversions H4. 
  (* apply_fresh* dsub_abs as y. *)
  (* (* case: app *) *)
  (* inversions H2. eauto. *)
  (* (* case: prod *) *)
  (* lets: (H0 E' e' e' A1' eq_refl). *)
  (* lets: (H1 E' _ _ _ eq_refl). *)
  (* lets: (H2 E' _ _ _ eq_refl). *)
  (* lets: (H3 E' _ _ _ eq_refl). *)
  (* inversions H6.  *)
  (* apply_fresh* dsub_prod as y. *)
  (* (* case: castup *) *)
  (* destructs (trans_to_sub t). *)
  (* destructs (trans_to_sub t0). *)
  (* inversions H2. apply* dsub_castup. *)
  (* (* case: castdn *) *)
  (* destructs (trans_to_sub t). *)
  (* destructs (trans_to_sub t0). *)
  (* inversions H2. apply* dsub_anno. *)
  (* (* case: sub *) *)
  (* inversions H2. *)
  (* apply* dsub_anno. *)
  (* (* case: dwf nil *) *)
  (* auto. *)
  (* (* case: dwf cons *) *)
  (* lets: trans_env_fresh t n. *)
  (* apply* dwf_cons. *)
  (* (* conclusion *) *)
  (* auto. *)
Admitted.

Definition add_anno (d : dir) e A :=
  match d with
  | inf => e
  | chk => trm_anno e A
  end.

Lemma dsub_to_trans : forall E d e1 e2 A,
  dsub E d e1 e2 A ->
  exists e1' e2', trans {#E#} [#e1#] [#e2#] [#A#] (Tr E e1' e2' A).
Proof.
  apply dsub_induct with
  (P0 := fun E (W: dwf E) => trenv {#E#} E);
    intros; simpls; try solve [eauto 2].
  exists* trm_star trm_star.
  lets: dbinds_rem b.
  exists* (trm_fvar x) (trm_fvar x).
  lets: dbinds_rem b.
  destruct H as (e1'&e2'&?).
  exists* (trm_fvar x) e2'.
  destructs (trans_to_sub H).
  rewrite <- H4.
  apply trans_var_trans with (e1 := [#e1'#]) (e1' := e1').

Admitted.

Lemma dbinds_exists: forall x e A E E',
  trenv E E' ->
  binds x (env_bnd e A) E ->
  exists e' A', binds x (env_bnd e' A') E' /\
                [#A'#] = A /\ [#e'#] = e.
Proof.
  intros. gen E'. induction E using env_ind; intros.
  false* (binds_empty_inv).
  induction v.
  inversions H. false* (empty_push_inv).
  destruct (eq_push_inv H1) as (?&?&?).
  inversions H5.
  destruct (binds_push_inv H0) as [[? ?]|[? ?]].
  inversions H5.
  destructs (trans_to_sub H4).
  destructs (trans_to_sub H3).
  exists A' B'. splits*.
  destruct (IHE H5 E'0 (trans_regular H4)) as [e' [B ?]].
  exists* e' B.
Qed.

Lemma trans_rename : forall x y E e1 e2 e3 A B E' e1' e2' e3' A' B',
  x \notin (fv e1 \u fv e2 \u fv A \u fv e1' \u fv e2' \u fv e3' \u fv A') ->
  y \notin (dom E \u fv e1 \u fv e2 \u fv A \u fv e1' \u fv e2' \u fv e3' \u fv A') ->
  trans (E & x ~ env_bnd e3 B) (e1 ^ x) (e2 ^ x) (A ^ x)
        (Tr (E' & x ~ env_bnd e3' B') (e1' ^ x) (e2' ^ x) (A' ^ x)) ->
  trans (E & y ~ env_bnd e3 B) (e1 ^ y) (e2 ^ y) (A ^ y)
        (Tr (E' & y ~ env_bnd e3' B') (e1' ^ y) (e2' ^ y) (A' ^ y)).
Proof.
  introv NIx NIy H.
  (* destruct (eq_var_dec x y). subst. auto. *)
  (* lets: (trans_regular H). *)
  (* inversions H0. false* (empty_push_inv). *)
  (* destructs (eq_push_inv H1). *)
  (* destructs (eq_push_inv H2). *)
  (* inversions H6. inversions H9. *)
  (* clear H1 H2 H8. *)
  (* rewrite* (@subst_intro x e1). *)
  (* rewrite* (@subst_intro x e2). *)
  (* rewrite* (@subst_intro x A). *)
  (* rewrite* (@subst_intro x e1'). *)
  (* rewrite* (@subst_intro x e2'). *)
  (* rewrite* (@subst_intro x A'). *)
Admitted.
  
Lemma trans_eq_exists : forall E e A E' e1' e2' A',
  trans E e e A (Tr E' e1' e2' A') ->
  exists e', trans E e e A (Tr E' e' e' A').
Admitted.

Lemma trans_binds : forall E x e1 e2 A E' e1' e2' A',
  trans E e1 e2 A (Tr E' e1' e2' A') ->
  binds x (env_bnd e1 A) E ->
  binds x (env_bnd e1' A') E'.
Admitted.

Lemma sub_to_trans : forall E e1 e2 A,
  sub E e1 e2 A ->
  exists E' e1' e2' A', trans E e1 e2 A (Tr E' e1' e2' A').
Proof.
  apply sub_induct with
    (P0 := fun E (W: wf E) => exists E', trenv E E'); intros.
  destruct H as [E' ?]. exists* E' trm_star trm_star trm_star.
  destruct H as [E' ?]. destruct (dbinds_exists H b) as [e' [A' ?]].
    destructs H0.
    exists* E' (trm_fvar x) (trm_fvar x) A'.
  destruct H as (E'&e1'&e2'&A'&?).
    lets: trans_regular H.
    exists* E' (trm_fvar x) e2' A'.
    apply* trans_var_trans.
    apply* trans_binds.
  destruct H as (E'&e1'&e2'&A'&?). 
  destruct (trans_eq_exists H) as (e'&?).
  exists* E' e' trm_top A'.
  destruct H as (E'&e1'&e2'&A'&?). 
  destruct (trans_eq_exists H) as (At&?).
    exists* E' (trm_anno trm_top At) (trm_anno trm_top At) At.
    apply* trans_top_refl.
    admit.
Admitted.
  
  
    
  
  

(* Completeness *)
(* Lemma sub_to_dsub : forall E e1 e2 A, *)
(*   sub E e1 e2 A -> *)
(*   exists E' e1' e2' A', *)
(*     dsub E' inf e1' e2' A' /\ *)
(*     {#E'#} = E /\ [#e1'#] = e1 /\ *)
(*     [#e2'#] = e2 /\ [#A'#] = A. *)
(* Proof. *)
(* Admitted.   *)
  
  

Lemma trans_substitution_var : forall V e V' e' (F:env) (E:env) F' E' x y t1 t2 T t1' t2' T',
  y <> x ->
  trans (E & x ~ env_bnd e V & F) t1 t2 T 
        (Tr (E' & x ~ env_bnd e' V' & F') t1' t2' T')->
  trans (E & (map (substenv x (trm_fvar y)) F))
        (subst x (trm_fvar y) t1) (subst x (trm_fvar y) t2) (subst x (trm_fvar y) T)
        (Tr (E' & (map (substenv x (trm_fvar y)) F'))
            (subst x (trm_fvar y) t1') (subst x (trm_fvar y) t2') (subst x (trm_fvar y) T')).
Proof.
  introv N R.
  gen_eq G: (E & x ~ env_bnd e V & F).
  gen_eq G': (E' & x ~ env_bnd e' V' & F').
  gen_eq r: (Tr G' t1' t2' T'). gen F F'.
  apply trans_induct with
    (P := fun G t1 t2 T r (R: trans G t1 t2 T r) => 
          forall (F : env) (F' : LibEnv.env env_typ),
            r = Tr G' t1' t2' T' ->
            G' = E' & x ~ env_bnd e' V' & F' ->
            G = E & x ~ env_bnd e V & F ->
            trans (E & map (substenv x (trm_fvar y)) F) ([x ~> trm_fvar y] t1) 
                  ([x ~> trm_fvar y] t2) ([x ~> trm_fvar y] T)
                  (Tr (E' & map (substenv x (trm_fvar y)) F') ([x ~> trm_fvar y] t1')
                      ([x ~> trm_fvar y] t2') ([x ~> trm_fvar y] T')))
      (P0 := fun G G' (W: trenv G G') =>
               forall (F : env) (F' : LibEnv.env env_typ),
                 G' = E' & x ~ env_bnd e' V' & F' ->
                 G = E & x ~ env_bnd e V & F ->
                 trenv (E & map (substenv x (trm_fvar y)) F)
                       ((E' & map (substenv x (trm_fvar y)) F')));
    intros; subst; simpls subst.
    inversions H0. autos*.
    inversions H0. case_var.
      binds_mid~. admit. 
Admitted.

