Set Implicit Arguments.
Require Import LibLN.
Require Import SubSup_ott SubSup_Infra.
Implicit Types x : var.

Lemma wf_to_wt : forall G m x A,
  wf m (G & x ~ A) -> subsup G m A A (trm_star).
Proof.
  intros. inversions H.
  false. apply* empty_push_inv.
  destruct (eq_push_inv H0) as [? [? ?]]. subst*.
Qed.

Lemma sub_refl_right : forall G m e1 e2 A,
  subsup G m e1 e2 A ->
  subsup G m e2 e2 A.
Proof.
  intros. inductions H; eauto.
  apply_fresh* s_prod as y.
  induction m; simpls.
  apply* H3. apply* H3.
Qed.

Hint Extern 1 (subsup ?G ?m ?A ?A ?B) => match goal with
  | H: subsup G m A _ B |- _ => apply ((sub_refl_left H))
  | H: subsup G m _ A B |- _ => apply ((sub_refl_right H))
  end.

Lemma wf_any_mode : forall m1 m2 G,
  wf m1 G -> wf m2 G.
Proof.
  asserts* EQ1:(mode_neg m_sub = m_sup).
  asserts* EQ2:(mode_neg m_sup = m_sub).
  intros. induction m1; induction m2; auto.
  rewrite <- EQ1. apply* wf_neg.
  rewrite <- EQ2. apply* wf_neg.
Qed.

Lemma sub_type_any_mode : forall m1 m2 G e A,
  subsup G m1 e e A -> subsup G m2 e e A.
Proof.
  asserts* EQ1:(mode_neg m_sub = m_sup).
  asserts* EQ2:(mode_neg m_sup = m_sub).
  intros. induction m1; induction m2; auto.
  rewrite <- EQ1. apply* sub_neg.
  rewrite <- EQ2. apply* sub_neg.
Qed.

Hint Extern 1 (wf ?m1 ?G) => match goal with
  | H: wf ?m2 G |- _ => apply ((@wf_any_mode m2 m1 G))
  end.

Hint Extern 1 (subsup ?G ?m1 ?A ?A ?B) => match goal with
  | H: subsup G ?m2 A A B |- _ => apply (@sub_type_any_mode m2 m1 G A B)
  end.

Lemma sub_weaken : forall G m E F t1 t2 T,
  subsup (E & G) m t1 t2 T ->
  wf m (E & F & G) ->
  subsup (E & F & G) m t1 t2 T.
Proof.
  introv Typ. gen_eq Env: (E & G). gen E F G.
  induction Typ; introv EQ W; subst; eauto.
  (* case: var refl *)
  apply* s_var. apply* binds_weaken.
  (* case: trm_abs *)
  lets: (IHTyp E F G0 eq_refl W).
  apply_fresh* (@s_abs) as y.
  apply_ih_bind* H0.
  apply_ih_bind* H2.
  (* case: trm_prod *)
  lets: (IHTyp E F G0 eq_refl (wf_neg W)).
  apply_fresh* (@s_prod) as y.
  apply_ih_bind* H0.
  apply* w_cons. apply* sub_type_any_mode.
  apply_ih_bind* H2.
  apply* w_cons. apply* sub_type_any_mode.
  apply_ih_bind* H4.
  apply* w_cons.
  induction m; simpls; apply~ sub_type_any_mode.
Qed.

Lemma sub_narrowing : forall A' A F E m B Z S T,
  subsup (E & Z ~ A & F) m S T B ->
  subsup E m_sub A' A (trm_star) ->
  subsup (E & Z ~ A' & F) m S T B.
Proof.
  introv Sa Sb.
  gen_eq G: (E & Z ~ A & F). gen F.
  apply sub_induct with
    (P := fun G m S T B (Sub: subsup G m S T B) =>
            forall F, G = E & Z ~ A & F ->
                      subsup (E & Z ~ A' & F) m S T B)
    (P0 := fun m G (W: wf m G) =>
            forall F, G = E & Z ~ A & F ->
                      wf m (E & Z ~ A' & F));
    intros; subst; simpls subst; try solve [eauto 5].
  (* case: var refl *)
  rename x5 into x.
  tests EQ: (x = Z).
    asserts~ N1: (wf m0 (E & Z ~ A' & F)).
    asserts~ N': (ok (E & Z ~ A & F)).
    lets: (binds_middle_eq_inv b N').
    destruct (ok_middle_inv N').
    subst~.
    apply (@s_sub' A').
    apply~ s_var.
    apply_empty* sub_weaken.
    apply_empty* sub_weaken.
    asserts* W1: (x # (Z ~ A)).
    lets: (binds_remove b W1).
    lets: (H F eq_refl).
    apply~ s_var.
    apply~ binds_weaken.
  (* case: abs *)
  apply_fresh* s_abs as Y.
  apply_ih_bind* H0.
  apply_ih_bind* H1.
  (* case: prod *)
  apply_fresh* (@s_prod) as Y.
  apply_ih_bind* H0. apply_ih_bind* H1.
  apply_ih_bind* H2.
  (* case: wf nil *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  destruct (env_case F).
    subst. rewrite concat_empty_r.
    rewrite concat_empty_r in H0.
    destruct (eq_push_inv H0) as [? [? ?]].
    subst~. apply* w_cons. apply* sub_type_any_mode.
    destruct H1 as [y [v' [E' Eq]]].
    subst.
    rewrite concat_assoc.
    rewrite concat_assoc in H0.
    destruct (eq_push_inv H0) as [? [? ?]].
    subst~.
Qed.

Lemma sub_sym : forall G m e1 e2 A,
  subsup G m e1 e2 A ->
  subsup G (mode_neg m) e2 e1 A.
Proof.
  apply sub_induct with
    (P0 := fun m E (_ : wf m E) => wf (mode_neg m) E);
  intros; eauto.
  rewrite mode_double_neg in H.
  apply_fresh* s_prod as y.
  rewrite~ mode_double_neg.
  induction m; simpls.
  lets: H2 y __. auto. auto.
  lets: H2 y __. auto. auto.
Qed.

Lemma sub_any_star_inv : forall G A B,
  subsup G m_sub A (trm_star) B ->
  A = trm_star.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sup_any_star_inv : forall G A B,
  subsup G m_sup trm_star A B ->
  A = trm_star.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_top_any_inv : forall E A B,
  subsup E m_sub (trm_top) A B ->
  A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sup_top_any_inv : forall E A B,
  subsup E m_sup A (trm_top) B ->
  A = trm_top.
Proof.
  intros. inductions H; auto.
Qed.

Lemma sub_trans_aux : forall e2 G e1 e3 A B,
  subsup G m_sub e1 e2 A -> subsup G m_sub e2 e3 B -> subsup G m_sub e1 e3 B.
Proof.
  introv S1. asserts* W: (lc_trm e2).
  gen G A e1 B e3. set_eq e2' eq: e2. gen e2' eq.
  induction W; intros e2' eq G AA e1' S1;
    inductions S1; try discriminate; inversions eq;
      intros B' e3' S2; try solve [eauto 2].
  (* case: top *)
  lets: sub_top_any_inv S2. subst~.
  inductions S2; eauto.
  (* case: app *)
  clear IHS1_1 IHS1_2 W1 W2.
  inductions S2. apply~ s_top_sub. clear IHS2.
  inductions S2.
    rewrite <- x. apply s_app with (A := A0).
     asserts P: (subsup G m_sub e0 e1 (trm_prod A0 B0)).
     apply* IHW1. auto. auto.
    apply* IHS2_1. apply* sub_any_star_inv.
  apply s_app with (A := A0).  apply* IHW1.
    auto.
  apply~ (@s_sub' A0). apply* IHS2_1.
  (* case lam *)
  clear IHS1 W H2 H4.
  inductions S2. 
  false. clear IHS2 H H0 IHW H3 H1 S1.
    inductions S2. apply* IHS2_1. apply* sub_any_star_inv.
  apply (@s_abs (L \u L0 \u L1)). auto. intros.
  lets P1: H1 x5 __. auto.
  lets P2: H5 x5 __. auto.
  apply* H0.
  intros. apply* H6.
  apply~ (@s_sub' A0). apply* IHS2_1.
  (* case pi *)
  clear IHS1 H2 W H4.
  inductions S2.
  apply~ s_top_sub. apply_fresh* s_prod as y.
  lets PP1: sub_sym S2. rewrite mode_double_neg in PP1.
  lets PP2: sub_sym S1. rewrite mode_double_neg in PP2.
  apply (@s_prod (L \u L0 \u L1)).
  apply sub_sym.
  apply* IHW. intros.
  apply* H1. intros.
  lets P: H3 x5 __. auto. auto.
  intros. simpls.
  lets P1: H3 x5 __. auto.
  lets P2: H8 x5 __. auto.
  lets Q: H0 x5 __. auto.
  lets Q1: Q (B ^ x5) __. auto.
  asserts Q3: (subsup (G & x5 ~ A2) m_sub (B1 ^ x5) (B ^ x5) trm_star).
  apply_empty~ (@sub_narrowing A2 A).
  lets Q2: Q1 Q3 P2. auto.
  apply~ (@s_sub' A0). apply* IHS2_1.
  (* case: castup *)
  clear IHS1_1 IHS1_2 W1 W2 IHW1.
  inductions S2. 
  false. clear IHS2 IHW2. inductions S2. inversion H.
    apply* IHS2_1. apply* sub_any_star_inv.
  apply* s_castup.
  apply~ (@s_sub' A1). apply* IHS2_1.
  (* case: castdn *)
  clear IHS1.
  inductions S2. apply~ s_top_sub. clear IHS2.
  inductions S2.
    apply s_castdn with (A := A0). auto.
    asserts P: (subsup G m_sub e1 e A0). apply* IHW. auto. auto.
    lets: sub_any_star_inv S2_2. subst. apply* IHS2_1.
  apply* s_castdn.
  apply~ (@s_sub' A0).
  lets P: IHS2_1 H W IHW.
  apply~ P.
Qed.

Lemma sub_trans_sup : forall e2 G e1 e3 A B,
  subsup G m_sup e1 e2 A -> subsup G m_sup e2 e3 B -> subsup G m_sup e1 e3 A.
Proof.
  introv H1 H2. 
  lets P1: sub_sym H1.
  lets P2: sub_sym H2.
  simpls.
  lets P3: sub_trans_aux P2 P1.
  lets P4: sub_sym P3.
  simpl in P4. auto.
Qed.

Lemma sub_trans : forall e2 G m e1 e3 A,
  subsup G m e1 e2 A -> subsup G m e2 e3 A -> subsup G m e1 e3 A.
Proof.
  intros. induction m.
  apply* sub_trans_aux.
  apply* sub_trans_sup.
Qed.

Definition red_out (R : relation) :=
  forall x u t t', lc_trm u -> R t t' -> 
  R ([x~>u]t) ([x~>u]t').

Hint Unfold red_out.

Lemma value_red_out :   forall x u t, lc_trm u -> value t -> 
  value ([x~>u]t).
Proof.
  intros_all. induction H0; simpl; auto.
Qed.

Lemma reduct_red_out : red_out reduct.
Proof.
  intros_all. induction H0; simpl; eauto 3.
  rewrite* subst_open. 
  apply* r_cast_elim.
Qed.

Lemma value_cannot_red : forall t t',
    value t -> t --> t' -> False.
Proof.
  introv H1. gen t'.
  induction H1; introv HH; inversions HH.
Qed.

Lemma reduct_determ : forall t t1 t2,
    reduct t t1 -> reduct t t2 -> t1 = t2.
Proof.
  introv H1. gen t2. induction H1; introv HH; inversions HH;
  match goal with
  | [ H : trm_abs _ _ --> _ |- _ ] => inversion H
  | [ H : trm_castup _ _ --> _ |- _ ] => inversion H
  | [ H1 : value ?e2, H2 : ?e2 --> _ |- _ ] => false* (@value_cannot_red e2)
  | _ => fequal~
  end.
Qed.

Lemma sub_substitution : forall V (F:env) m v (E:env) x t1 t2 T,
  subsup E m v v V ->
  subsup (E & x ~ V & F) m t1 t2 T ->
  subsup (E & (map (subst x v) F)) m (subst x v t1) (subst x v t2) (subst x v T).
Proof.
  introv Typv Typt.
  gen_eq G: (E & x ~ V & F). gen F.
  apply sub_induct with
   (P := fun (G:env) m t1 t2 T (Typt : subsup G m t1 t2 T) =>
      forall (F:env), G = E & x ~ V & F ->
      subsup (E & map (subst x v) F) m ([x ~> v]t1) ([x ~> v]t2) ([x ~> v]T))
   (P0 := fun m (G:env) (W:wf m G) =>
      forall F, G = E & x ~ V & F ->
      wf m (E & (map (subst x v) F)));
   intros; subst; simpls subst; eauto 5.
  (* case: var *)
  case_var.
    binds_mid~. rewrite* subst_fresh. apply_empty* sub_weaken.
    apply~ s_var. destruct~ (binds_not_middle_inv b) as [K|[Fr K]].
      rewrite* subst_fresh.
  (* case: abs *)
  apply_fresh* (@s_abs) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  (* case: app_v *)
  rewrite* subst_open.
  (* case: prod *)
  apply_fresh* (@s_prod) as y.
  cross; auto. apply_ih_map_bind* H0.
  cross; auto. apply_ih_map_bind* H1.
  cross; auto. induction m0; simpls; apply_ih_map_bind* H2.
  (* case: castup *)
  apply* s_castup. apply* reduct_red_out.
  (* case: castdn *)
  apply* s_castdn. apply* reduct_red_out.
  (* case: wf empty *)
  false. apply* empty_middle_inv.
  (* case: wf cons *)
  change LibEnv.env with LibEnv.env in *.
  induction F using env_ind.
    rewrite concat_empty_r in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_empty. rewrite~ concat_empty_r.
    clear IHF. rewrite concat_assoc in H0.
     destruct (eq_push_inv H0) as [? [? ?]]. subst.
     rewrite map_push. rewrite concat_assoc. apply* (@w_cons). 
Qed.

Lemma sub_wf_from_context : forall x U (E:env) m,
  binds x U E -> 
  wf m E -> 
  subsup E m U U trm_star.
Proof.
  introv B W. induction E using env_ind. 
  false* binds_empty_inv. 
  destruct (binds_push_inv B) as [[? ?]|[? ?]]. 
    subst. inversions W. false (empty_push_inv H1).
     destruct (eq_push_inv H) as [? [? ?]]. subst.
     apply_empty* sub_weaken.
    destruct (wf_push_inv W).
      apply_empty* sub_weaken.
Qed.

Lemma sub_prod_prod_inv : forall G m A B B0 C,
   subsup G m (trm_prod A B) (trm_prod A B0) C ->
   (exists L, forall x, x \notin L ->
        subsup (G & x ~ A) m (B ^ x) (B0 ^ x) (trm_star)).
Proof.
  intros. inductions H; eauto.
  rewrite mode_sel_eq in H4. eauto.
Qed.

Lemma sub_abs_abs_inv : forall G m A A0 B B0 C,
   subsup G m (trm_abs A B) (trm_abs A0 B0) C -> exists C',
   A = A0 /\
   subsup G m_sub (trm_prod A C') C (trm_star) /\
   (exists L, forall x, x \notin L ->
        subsup (G & x ~ A) m (B ^ x) (B0 ^ x) (C' ^ x)).
Proof.
  intros. inductions H; eauto.
  exists B1. splits; auto.
  lets P: sub_type_any_mode m.
  apply~ P.
  apply_fresh* s_prod as y.
  rewrite* mode_sel_eq.
  exists L. intros. apply* H0.
  destruct (IHsubsup1 _ _ _ _ eq_refl eq_refl) as [C' [? [? ?]]].
  exists C'. splits*.
  apply* (@sub_trans A1).
Qed.

Lemma sub_sel_left : forall A G m1 m2 e1 e2 A' B,
  subsup G m1 e1 e2 (mode_sel m2 A B) ->
  subsup G m_sub A A' trm_star ->
  subsup G m1 e1 e2 (mode_sel m2 A' B).
Proof.
  induction m2; eauto.
Qed.

Lemma sub_sel_right : forall A G m1 m2 e1 e2 A' B,
  subsup G m1 e1 e2 (mode_sel m2 B A) ->
  subsup G m_sub A A' trm_star ->
  subsup G m1 e1 e2 (mode_sel m2 B A').
Proof.
  induction m2; eauto.
Qed.

(* Lemma sub_try : forall e2 G m e1 A B, *)
(*   subsup G m e1 e2 A -> subsup G m e2 e2 B -> subsup G m e1 e2 (mode_sel m A B). *)
(* Proof. *)
(*   introv SP SQ. *)
(*   induction SP; auto. *)
(*   (* star *) *)
(*   inductions SQ; simpl. rewrite* mode_sel_eq. *)
(*   apply* sub_sel_right. *)
(*   (* var *) *)
(*   inductions SQ; simpl. induction m; eauto. *)
(*   apply* sub_sel_right. *)
(*   (* top *) *)
(*   simpls. inductions SQ; eauto. *)
(*   induction m; eauto. *)
(*   (* abs *) *)
(*   induction m; simpls; eauto. *)
(*   inductions SQ; eauto. *)
  
  




(*Lemma sub_trans_try : forall e2 G m e1 e3 A,
  subsup G m e1 e2 A -> subsup G m e2 e3 A -> subsup G m e1 e3 A.
Proof.
   introv S1. asserts* W: (lc_trm e2). 
   gen G m A e1 e3. set_eq e2' eq: e2. gen e2' eq; 
   induction W; intros e2' eq G m AA e1' S1; 
     inductions S1; try discriminate; inversions eq; 
       intros e3' S2; try solve [eauto 2]. 
   induction m; simpl; eauto. 
  
  


  introv S. gen e3. induction S; intros; eauto.
   induction m; simpls; eauto. 
  - lets EQ: sub_top_any_inv H.
    subst~.
  - inductions H3.
    apply_fresh* s_abs as y.
     apply~ (@s_sub' A0).  
    apply* IHsubsup1.
Qed.

Abort.*)


