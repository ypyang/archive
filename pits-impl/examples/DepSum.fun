--> True
-- List
data List = Nil | Cons Int List;
defrec elem (x : Int) (xs : List) : Bool =
    case xs of Nil => False | Cons y ys => if (x == y) then True else elem x ys;

-- Dependent Sums
def Sig (A : *) (P : A -> *) = (z : *) -> ((x : A) -> P x -> z) -> z;
def pack (A : *) (P : A -> *) (e1 : A) (e2 : P e1) =
  (castup^2 (\(z : *) (f : (x : A) -> P x -> z). f e1 e2) :: Sig A P);
def unpack (A : *) (P : A -> *) (C : *) (e : Sig A P) (f : (x : A) -> P x -> C) =
  (castdn^2 e :: (z : *) -> ((x : A) -> P x -> z) -> z) C f;

-- Set
data SetRec (T : *) = SetR {empty: T, member: Int -> T -> Bool, insert: Int -> T -> T};
def Set = Sig * SetRec;
def ListSet = pack * SetRec List (SetR List Nil elem Cons);

-- Main
def f (s : Set) = unpack * SetRec Bool s (\(T : *) (r : SetRec T). member T r 3 (insert T r 3 (empty T r)));
f ListSet
