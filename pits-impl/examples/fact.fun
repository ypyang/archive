--> 120

letrec fact (x : Int) : Int = if x == 0 then 1 else x ** fact (x - 1) in fact 5
