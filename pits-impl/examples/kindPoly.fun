--> castup^4 [Mu * Listf Int] (\(_Mu : *) (_Roll : Listf (Mu * Listf) Int -> _Mu) . _Roll (Nil (Mu * Listf) Int))
{-

Higher-kinded fixpoint combinator

-}


data Mu (k : *) (f : (k -> *) -> k -> *) (a : k) =
  Roll (f (Mu k f) a);



data Listf (f : * -> *) (a : *) = Nil | Cons a (f a); 
def List = \(a : *) . Mu * Listf a;


def nil = Roll * Listf Int (Nil (Mu * Listf) Int);

nil
