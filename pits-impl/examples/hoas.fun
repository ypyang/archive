--> 42
{-

Higher-order abstract syntax, also define `show` function for printing `Value`

-}

data Exp = Num Int
         | Lam (Exp -> Exp)
         | App Exp Exp;
data Value = VI Int | VF (Value -> Value);
data Eval = Ev { eval' : Exp -> Value
                       , reify' : Value -> Exp }; 
defrec ev : Eval = Ev 
(\ e : Exp . case e of 
    Num n => VI n
  | Lam fun => VF (\e' : Value . eval' ev (fun (reify' ev e')))
  | App a b => case eval' ev a of 
                 VF fun => fun (eval' ev b))
(\v : Value . case v of 
    VI n => Num n
  | VF fun =>
       Lam (\e' : Exp . (reify' ev (fun (eval' ev e')))));
def eval = eval' ev;

def show = \(v : Value) . case v of 
    VI n => n
;

def expr = App (Lam (\ f : Exp . App f (Num 42)))
                      (Lam (\g : Exp. g));
show (eval expr)     -- return 42
