module Language.Fun.Typecheck (
  Ctx(..),
  emptyCtx,
  ctxMerge,
  conCheck,
  typeCheck,
  reduceCheck,
  ctxMap,
  ctxAddVar,
  ctxAddCon,
  ctxAddBnd,
  ctxAddVars,
  ctxAddCons,
  ctxAddBnds,
  M,
  runM,
  reduceInf,
  isEmptyCtx,
  ConCtx,
  BndCtx,
  ppConCtx,
  ppBndCtx,
  isValidVar,
  reduceStep,
  ctxKeepBnd,
  ) where

import           Control.Monad.Except
import           Control.Monad.Identity
import           Control.Monad.State
import           Control.Monad.Writer             hiding (All, Any, (<>))
import qualified Data.LinkedHashMap.IntMap        as HM
import qualified Data.Map.Strict                  as M
import           Language.Fun.Core                hiding (M, runM)
import qualified Language.Fun.Core                as C (M)
import           Language.Fun.Lit
import           Language.Fun.Pretty
import           Prelude                          hiding ((<$>))
import           Text.PrettyPrint.ANSI.Leijen     hiding (Pretty)
import           Unbound.Generics.LocallyNameless hiding (bind, subst)

type M = ExceptT Doc (WriterT [Doc] (StateT Cache (FreshMT Identity)))

type Cache = M.Map VarName Ty

emptyCache :: Cache
emptyCache = M.empty

cachePut :: Cache -> M ()
cachePut = lift . put

cacheGet :: M Cache
cacheGet = lift get

cacheAdd :: VarName -> Ty -> M ()
cacheAdd name ty = do
  c <- cacheGet
  cachePut $ M.insert name ty c

convCM :: C.M a -> M a
convCM = lift . lift . lift

type VarCtx = HM.LinkedHashMap VarName Ty
type ConCtx = [Con]
type BndCtx = HM.LinkedHashMap VarName Tm

data Ctx = Ctx {
  varCtx :: VarCtx,
  conCtx :: ConCtx,
  bndCtx :: BndCtx
  } deriving (Show)

ppConCtx :: ConCtx -> [Doc]
ppConCtx c = if null c then [] else [strsep ", " . map pp $ c]

ppMapCtx :: String -> VarCtx -> Doc
ppMapCtx s = strsep ", " . map (\(v, t) -> vardoc v <+>
                                           text s <+>
                                           pp t) . HM.toList

ppVarCtx :: VarCtx -> [Doc]
ppVarCtx c = if HM.null c then [] else [ppMapCtx ":" c]

ppBndCtx :: BndCtx -> [Doc]
ppBndCtx c = if HM.null c then [] else [ppMapCtx "=" c]

instance Pretty Ctx where
  ppr _ ctx = strsep "; " $ concat
    [ppVarCtx (varCtx ctx), ppConCtx (conCtx ctx)]

runM :: M a -> (Either Doc a, [Doc])
runM = fst . runIdentity . runFreshMT . (`runStateT` emptyCache) . runWriterT . runExceptT

trace :: Doc -> M ()
trace s = lift (tell [s])

emptyCtx :: Ctx
emptyCtx = Ctx {
  varCtx = HM.empty,
  conCtx = [],
  bndCtx = HM.empty
  }

isEmptyCtx :: Ctx -> Bool
isEmptyCtx c = HM.null (varCtx c) &&
  null (conCtx c) && HM.null (bndCtx c)

ctxMerge :: Ctx -> Ctx -> Ctx
ctxMerge x y = Ctx {
  varCtx = foldl (\c (k, v) -> HM.insert k v c) (varCtx x) (HM.toList $ varCtx y),
  conCtx = conCtx x ++ conCtx y,
  bndCtx = foldl (\c (k, v) -> HM.insert k v c) (bndCtx x) (HM.toList $ bndCtx y)
  }

ctxMap :: (VarCtx -> VarCtx) ->
          (ConCtx -> ConCtx) ->
          (BndCtx -> BndCtx) ->
          Ctx -> Ctx
ctxMap f1 f2 f3 ctx = Ctx {
  varCtx = f1 (varCtx ctx),
  conCtx = f2 (conCtx ctx),
  bndCtx = f3 (bndCtx ctx)
  }

isValidVar :: VarName -> Bool
isValidVar = not . isDummyVar

ctxAddVar :: Ctx -> VarName -> Ty -> Ctx
ctxAddVar ctx v t = ctxMap (if isValidVar v then HM.insert v t else id) id id ctx

ctxAddVars :: Ctx -> [Tele] -> Ctx
ctxAddVars = foldl (\c (v, t) -> ctxAddVar c v t)

ctxAddCon :: Ctx -> Con -> Ctx
ctxAddCon ctx c = ctxMap id (++ [c]) id ctx

ctxAddCons :: Ctx -> [Con] -> Ctx
ctxAddCons ctx cs = ctxMap id (++ cs) id ctx

ctxAddBnd :: Ctx -> VarName -> Tm -> Ctx
ctxAddBnd ctx v t = ctxMap id id (if isValidVar v then HM.insert v t else id) ctx

ctxAddBnds :: Ctx -> [Tele] -> Ctx
ctxAddBnds = foldl (\c (v, t) -> ctxAddBnd c v t)

ctxFetchVar :: Ctx -> VarName -> Maybe Tm
ctxFetchVar ctx v = HM.lookup v (varCtx ctx)

ctxFetchBnd :: Ctx -> VarName -> Maybe Tm
ctxFetchBnd ctx v = HM.lookup v (bndCtx ctx)

ctxSubst :: Ctx -> Tm -> Tm
ctxSubst c = bndsSubst (HM.toList . bndCtx $ c)

ctxKeepBnd :: Ctx -> Ctx
ctxKeepBnd c = ctxMap id id (\_ -> bndCtx c) emptyCtx

bndsSubst :: [Tele] -> Tm -> Tm
bndsSubst bnds tm =
  let mkSubs [] = []
      mkSubs (a@(x, e):as) = a : mkSubs (map (\(x', e') -> (x', vnSubst x e e')) as)
  in foldl (\v (x, e) -> vnSubst x e v) tm (mkSubs bnds)

andM :: M Bool -> M Bool -> M Bool
andM = liftM2 (&&)

allM ::[M Bool] -> M Bool
allM [] = return True
allM (mx:mxs) = do
  x <- mx
  if not x then return False
    else allM mxs

conCheck_ :: Ctx -> Con -> M Bool
conCheck_ ctx con = do
  let (Eq ty ty') = substCtxCon ctx con
  return $ ty `aeq` ty'
  where
    substCtxCon c (Eq ty1 ty2) = Eq (ctxSubst c ty1) (ctxSubst c ty2)

conCheck :: Ctx -> Con -> Doc -> M ()
conCheck ctx con d = do
  ret <- conCheck_ ctx con
  unless ret $ throwError d

kindCheck :: Ctx -> Tm -> M ()
kindCheck c tm = do
  ty <- typeCheck c tm
  let isValidKind = case ty of
        Star -> True
        Any -> True
        Unit -> True
        _ -> False
  unless isValidKind
    (throwError $ text "invalid kind" <+>
     squotes (pp ty) <> colon <+> pp tm)

conValid :: Ctx -> Con -> M ()
conValid ctx (Eq ty1 ty2) = do
  s1 <- typeCheck ctx ty1
  s2 <- typeCheck ctx ty2
  kindCheck ctx s1
  kindCheck ctx s2

consValid :: Ctx -> [Con] -> M ()
consValid ctx = mapM_ (conValid ctx)

litOpCheck :: (LitTy, LitTy) -> Ty -> (Tm, Tm) -> Ctx -> M LitTy
litOpCheck (inTy, outTy) ty (inp, e) ctx =
  case inTy of
    LTAny -> return outTy
    _ -> case ty of
      LTy lty -> if lty /= inTy then throwError (text "expect" <+> squotes (pp inTy) <+>
                                                 text "but got" <+> squotes (pp ty))
                 else return outTy
      _ -> do
        result <- conCheck_ ctx (Eq ty (LTy inTy))
        if result then return outTy else
          throwError (hang 2 $ text "literal operation type mismatch in" <+>
                      squotes (pp inp) <> colon <$>
                      text "expression" <+> squotes (pp e) <+>
                      text "has type" <+> squotes (pp ty) <$>
                      text "but requires type" <+> squotes (pp inTy))

typeCheck :: Ctx -> Tm -> M Ty
typeCheck ctx inp = do
  trace $ pp ctx <+> text "|-" <+> pp inp
  let raise = throwError . text
  case inp of
    Var v ->
      case ctxFetchVar ctx v of
        Just ty -> return ty
        _ -> case ctxFetchBnd ctx v of
          Just tm -> do
            cache <- cacheGet
            case M.lookup v cache of
              Just ty -> return ty
              _ -> do
                ty <- typeCheck ctx tm
                cacheAdd v ty
                return ty
          _ -> throwError (text "variable" <+>
                           squotes (vardoc v) <+>
                           text "not in the scope")
    Star -> return Star
    App e1 e2 -> do
      piTy1 <- typeCheck ctx e1
      ty3 <- typeCheck ctx e2
      (x, ty2, ty1) <- case piTy1 of
        Pi b -> do
          ((x, Embed ty1), ty2) <- unbind b
          kindCheck ctx ty1
          return (x, ty2, ty1)
        _ -> throwError $
          text "expect a pi type for" <+> squotes (pp e1) <+>
          text "but got" <+> squotes (pp piTy1)
      kindCheck ctx ty3
      conCheck ctx (Eq ty1 ty3) (hang 2 $ text "type of application mismatch in" <+>
                                 squotes (pp inp) <> colon <$>
                                 text "argument" <+> squotes (pp e2) <+>
                                 text "has type" <+> squotes (pp ty3) <$>
                                 text "but function requires type" <+> squotes (pp ty1))
      return (vnSubst x e2 ty2)
    Lam b -> do
      ((x, Embed t1), e) <- unbind b
      t2 <- typeCheck (ctxAddVar ctx x t1) e
      let piTy = Pi (vnbind x t1 t2)
      kindCheck ctx piTy
      return piTy
    Pi b -> do
      ((x, Embed t1), t2) <- unbind b
      kindCheck ctx t1
      kindCheck (ctxAddVar ctx x t1) t2
      return Star
    Mu b -> do
      ((x, Embed t1), e) <- unbind b
      t2 <- typeCheck (ctxAddVar ctx x t1) e
      kindCheck ctx t1
      kindCheck ctx t2
      conCheck ctx (Eq t1 t2) (hang 2 $ text "type of mu mismatch in" <+>
                               squotes (pp inp) <> colon <$>
                               text "body" <+> squotes (pp e) <+>
                               text "has type" <+> squotes (pp t2) <$>
                               text "but binder requires type" <+> squotes (pp t1))
      return t1
    Cast ud ann step mt e -> case mt of
      Nothing -> if ud == Up then raise "missing type annotation in cast"
                 else do
        t1 <- typeCheck ctx e
        reduceStep ctx step t1
      Just t1 -> do
        t2 <- typeCheck ctx e
        kindCheck ctx t2
        kindCheck ctx t1
        case ud of
          Up -> reduceCheck ann ctx step t1 t2 (pp inp)
          Down -> reduceCheck ann ctx step t2 t1 (pp inp)
        return t1
    ConPi{} -> raise "fatal core: conpi"
    ConLam{} -> raise "fatal core: conlam"
    ConApp{} -> raise "fatal core: conapp"
    Any -> return Any
    Unit -> return Unit
    Let b -> do
      ((x, Embed e1), e2) <- unbind b
      t1 <- typeCheck ctx e1
      typeCheck (ctxAddVar (ctxAddBnd ctx x e1) x t1) e2
    IfThenElse e1 e2 e3 -> do
      t1 <- typeCheck ctx e1
      case t1 of
        LTy LTBool -> do
          t2 <- typeCheck ctx e2
          t3 <- typeCheck ctx e3
          conCheck ctx (Eq t2 t3)
            (hang 2 $ text "if branches type mismatch in" <+>
             squotes (pp inp) <> colon <$>
             squotes (pp e2) <+> text "has type" <+> squotes (pp t2) <$>
             squotes (pp e3) <+> text "has type" <+> squotes (pp t3))
          return t2
        _ -> throwError $ text "if condition" <+> squotes (pp e1) <+>
                          text "is not a boolean, but" <+> squotes (pp t1)
    LTy _ -> return Star
    LVal val -> return (LTy (typeOfVal val))
    LUnary op tm -> do
      ty <- typeCheck ctx tm
      retTy <- litOpCheck (typeOfUOp op) ty (inp, tm) ctx
      return (LTy retTy)
    LBin op e1 e2 -> do
      let opTy = typeOfBOp op
      t1 <- typeCheck ctx e1
      t2 <- typeCheck ctx e2
      retTy <- litOpCheck opTy t1 (inp, e1) ctx
      _ <- litOpCheck opTy t2 (inp, e2) ctx
      return (LTy retTy)

reduceCheck :: CastAnn -> Ctx -> Int -> Tm -> Tm -> Doc -> M ()
reduceCheck ann ctx n e1 e2 d = do
  (eq, e2', _) <- reduceCheck_ ann ctx n e1 e2
  unless eq (throwError $ hang 2 (text "reduction failed in" <+> squotes d <> colon <$>
                text "expect" <+> squotes (pp e1 <+>
                                           text "==>" <> pp ann <> parens (int n) <+>
                                           pp e2) <>
                maybe empty (\e -> line <>
                                   text "but got" <+> pp e) e2'))

reduceCheck_ :: CastAnn -> Ctx -> Int -> Tm -> Tm -> M (Bool, Maybe Tm, Int)
reduceCheck_ ann inpctx n inp1 inp2 = do
  e1 <- erase inp1
  e2 <- erase inp2
  ctx <- do
    bndCtx' <- mapM erase (bndCtx inpctx)
    return Ctx {
      varCtx = HM.empty,
      conCtx = [],
      bndCtx = bndCtx'
      }
  case ann of
    Full -> do
      eq <- fullCheck n (ctxSubst ctx e1) (ctxSubst ctx e2)
      return (eq, Nothing, 0)
    Weak -> do (e2', n') <- case n of
                 0 -> return (e1, 0)
                 _ -> reduceStep_ ctx n e1
               eq <- conCheck_ ctx (Eq e2 e2')
               return (eq, Just e2', n')

fullCheck :: Int -> Tm -> Tm -> M Bool
fullCheck steps = fr
  where ctx = emptyCtx
        eq x y = return $ aeq x y
        fr tm1 tm2 = do
          trace $ text "<=-=-=-=-=-" <$>
            text "=>" <+> parens (int steps) <$>
            text "=>?" <+> pp tm1 <$>
            text "=>?" <+> pp tm2 <$>
            text "=-=-=-=-=->"
          res <- tm1 `eq` tm2
          if steps <= 0 then return res else
            if res then return True else fr' tm1 tm2

        frBind b b' = do
          ((x, Embed t), e) <- unbind b
          ((x', Embed t'), e') <- unbind b'
          andM (t `fr` t') (e `fr` vnSubst x' (Var x) e')

        weakCheck tm1 tm2 = do
          (tm1', n', reducible) <- reduce ctx (tm1, 0, True)
          if not reducible then eq tm1' tm2
            else fullCheck (steps - n') tm1' tm2

        fr' tm1@(App e1 e2) tm2@(App e1' e2') = do
          chk <- e1 `fr` e1'
          if chk then fr' e2 e2'
            else weakCheck tm1 tm2
        fr' (Lam b) (Lam b') = frBind b b'
        fr' (Pi b) (Pi b') = frBind b b'
        fr' tm1@(Mu b) tm2 = do
          (tm1', n', reducible) <- reduce ctx (tm1, 0, True)
          if not reducible then
            case tm2 of
              Mu b' -> frBind b b'
              _ -> eq tm1' tm2
            else fullCheck (steps - n') tm1' tm2
        fr' (LBin op e1 e2) (LBin op' e1' e2') =
          allM [return (op == op'), fr e1 e1', fr e2 e2']
        fr' tm1 tm2 = weakCheck tm1 tm2

reduceInf :: Ctx -> Tm -> M Tm
reduceInf c tm = liftM fst $ reduceInf_ c tm

reduceInf_ :: Ctx -> Tm -> M (Tm, Int)
reduceInf_ ctx e = reduceWhile ctx (const True) e 0

reduceStep :: Ctx -> Int -> Tm  -> M Tm
reduceStep c n tm = liftM fst $ reduceStep_ c n tm

reduceStep_ :: Ctx -> Int -> Tm  -> M (Tm, Int)
reduceStep_ ctx n tm = reduceWhile ctx (< n) tm 0

reduceWhile :: Ctx -> (Int -> Bool) -> Tm -> Int -> M (Tm, Int)
reduceWhile ctx f tm n | f n = do
                           (tm', n', reduced) <- reduce ctx (tm, n, False)
                           if reduced then reduceWhile ctx f tm' n'
                             else return (tm', n')
                       | otherwise = return (tm, n)

eraseBnd :: (Bnd -> Tm) -> Bnd -> M Tm
eraseBnd f b = do
  ((x, Embed t), e) <- unbind b
  t' <- erase t
  e' <- erase e
  return $ f (vnbind x t' e')

erase :: Tm -> M Tm
erase inp = case inp of
  Var{} -> return inp
  Star -> return inp
  App e1 e2 -> App `fmap` erase e1 <*> erase e2
  Lam b -> eraseBnd Lam b
  Pi b -> eraseBnd Pi b
  Mu b -> eraseBnd Mu b
  Cast _ _ _ _ e -> erase e
  ConPi _ ty -> erase ty
  ConLam _ tm -> erase tm
  ConApp _ tm -> erase tm
  Any -> return inp
  Unit -> return inp
  Let b -> eraseBnd Let b
  IfThenElse e1 e2 e3 -> IfThenElse `fmap` erase e1 <*> erase e2 <*> erase e3
  LTy{} -> return inp
  LVal{} -> return inp
  LUnary op tm -> LUnary op `fmap` erase tm
  LBin op e1 e2 -> LBin op `fmap` erase e1 <*> erase e2

reduce :: Ctx -> (Tm, Int, Bool) -> M (Tm, Int, Bool)
reduce ctx (tm, n, b) = do
  trace $ text "|->" <> parens (int n) <+> pp tm
  let raise = throwError . text
      retval = return (tm, n, False)
  case tm of
    Any -> raise "exception raised"
    Var v -> case ctxFetchBnd ctx v of
      Just e -> return (e, n, True)
      _ -> retval
    Star -> retval
    Unit -> retval
    Lam{} -> retval
    Pi{} -> retval
    ConPi{} -> retval
    ConLam{} -> retval
    LTy{} -> retval
    LVal{} -> retval

    App (Lam bnd) e' -> do
      ((x, _), e) <- unbind bnd
      return (vnSubst x e' e, n + 1, True)
    App e1 e2 -> do
      (e1', n', b') <- reduce ctx (e1, n, b)
      return (App e1' e2, n', b')
    Mu bnd -> do
      ((x, _), e) <- unbind bnd
      return (vnSubst x tm e, n + 1, True)
    Cast _ _ 0 _ e -> return (e, n, True)
    Cast Down Weak s1 ty1 (Cast Up Weak s2 ty2 e)
      | s2 == 0 -> return (Cast Down Weak s1 ty1 e, n, True)
      | s1 <= 0 || s2 < 0 -> raise "failsafe: invalid cast"
      | otherwise -> let castdown' = if s1 == 1 then id
                                     else Cast Down Weak (s1 - 1) ty1
                         castup' = if s2 == 1 then id
                                   else Cast Up Weak (s2 - 1) ty2
                     in return (castdown' (castup' e), n + 1, True)
    Cast _ Full _ _ e -> return (e, n, True)
    Cast ud ann s t e -> do
      (e', n', b') <- reduce ctx (e, n, b)
      return (Cast ud ann s t e', n', b')
    ConApp s (ConLam cons e)
      | null cons -> raise "failsafe: empty constraint"
      | s <= 0 -> throwError (text "invalid constraint reduction:" <+>
                              pp tm)
      | otherwise -> let cons' = tail cons
                         body' = if null cons' then e
                                 else ConLam cons' e
                         app' = if s == 1 then id
                                else ConApp (s - 1)
                     in return (app' body', n + 1, True)
    ConApp s e -> do
      (e', n', b') <- reduce ctx (e, n, b)
      return (ConApp s e', n', b')
    Let bnd -> do
      ((x, Embed e1), e2) <- unbind bnd
      return (vnSubst x e1 e2, n + 1, True)
    IfThenElse e@(LVal v) e1 e2 ->
      case v of
        LVBool cond -> return (if cond then e1 else e2, n + 1, True)
        _ -> throwError $ text "if condition" <+> squotes (pp e) <+>
                          text "is not a boolean"
    IfThenElse e1 e2 e3 -> do
      (e1', n', b') <- reduce ctx (e1, n, b)
      return (IfThenElse e1' e2 e3, n', b')
    LUnary op e -> do
      (e', n', b') <- reduce ctx (e, n, b)
      if b' then return (LUnary op e', n', b')
        else case op of
        LOpShow -> return (LVal $ LVStr (show (pp e)), n + 1, True)
        LOpNeg -> case e of
          LVal (LVInt i) -> return (LVal (LVInt (-i)), n + 1, True)
          _ -> throwError $ text "invalid literal operation:" <+> pp tm
        LOpNot -> case e of
          LVal (LVBool x) -> return (LVal (LVBool (not x)), n + 1, True)
          _ -> throwError $ text "invalid literal operation:" <+> pp tm
    LBin op (LVal v1) (LVal v2) -> do
      let fatal = throwError $ text "invalid literal operation:" <+> pp tm
          execute (LVInt i) (LVInt j) =
            case op of
              LOpAdd -> return $ LVInt (i + j)
              LOpMinus -> return $ LVInt (i - j)
              LOpMul -> return $ LVInt (i * j)
              LOpG -> return $ LVBool (i > j)
              LOpGE -> return $ LVBool (i >= j)
              LOpL -> return $ LVBool (i < j)
              LOpLE -> return $ LVBool (i <= j)
              LOpEq -> return $ LVBool (i == j)
              LOpNeq -> return $ LVBool (i /= j)
              _ -> fatal
          execute (LVStr s) (LVStr t) =
            case op of
              LOpConcat -> return $ LVStr (s ++ t)
              _ -> fatal
          execute (LVBool x) (LVBool y) =
            case op of
              LOpAnd -> return $ LVBool (x && y)
              LOpOr -> return $ LVBool (x || y)
              _ -> fatal
          execute _ _ = fatal
      v <- execute v1 v2
      return (LVal v, n + 1, True)
    LBin op e1@(LVal _) e2 -> do
      (e2', n', b') <- reduce ctx (e2, n, b)
      return (LBin op e1 e2', n', b')
    LBin op e1 e2 -> do
      (e1', n', b') <- reduce ctx (e1, n, b)
      return (LBin op e1' e2, n', b')
