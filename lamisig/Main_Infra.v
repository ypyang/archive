Set Implicit Arguments.
Require Import LibLN.
Require Import Main_ott.
Implicit Types x : var.

(* Syntax *)

Notation "{ k ~> u } t" := (open_trm_wrt_trm_rec k u t) (at level 67).
Notation "t ^^ u" := (open_trm_wrt_trm t u) (at level 67).
Notation "t ^ x" := (open_trm_wrt_trm t (trm_var_f x)).
Notation "t ^! x" := (open_trm_wrt_trm_1 t (trm_var_f x)) (at level 67).
Notation "t ^^! u" := (open_trm_wrt_trm_1 t u) (at level 67).

Definition body t :=
  exists L, forall x, x \notin L -> lc_trm (t ^ x). 

Definition relation := trm -> trm -> Prop.

Notation "t --> t'" := (reduct t t') (at level 67).

(* Typing *)

Implicit Types E : env.

Scheme sub_induct := Induction for sub Sort Prop
  with wf_induct := Induction for wf Sort Prop.

Lemma sub_sub' : forall A (G:env) (e1 e2 B:trm),
  sub G e1 e2 A ->
  sub G A B trm_star ->
  sub G e1 e2 B.
Proof.
  intros. eauto 2.
Qed.

(* infrastructure *)
Definition fv := fv_trm.
Definition subst z u t := subst_trm u z t.

Notation "[ z ~> u ] t" := (subst z u t) (at level 68).

Definition close_var z t := close_var_rec 0 z t.

Definition contains_terms E :=
  forall x T, binds x T E -> lc_trm T.

Definition red_regular (R : relation) :=
  forall t t', R t t' -> lc_trm t /\ lc_trm t'.

Ltac gather_vars :=
  let A := gather_vars_with (fun x : vars => x) in
  let B := gather_vars_with (fun x : var => \{x}) in
  let C := gather_vars_with (fun x : trm => fv x) in
  let D := gather_vars_with (fun x : env => dom x) in
  constr:(A \u B \u C \u D).

Ltac pick_fresh X :=
  let L := gather_vars in (pick_fresh_gen L X).

Tactic Notation "apply_fresh" constr(T) "as" ident(x) :=
  apply_fresh_base T gather_vars x.

Tactic Notation "apply_fresh" "*" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos*.

Tactic Notation "apply_fresh" "~" constr(T) "as" ident(x) :=
  apply_fresh T as x; autos~.

Ltac exists_fresh := 
  let L := gather_vars_with (fun x : vars => x) in exists L.

Lemma open_rec_term_core :forall e j v i u, i <> j ->
  {j ~> v}e = {i ~> u}({j ~> v}e) -> e = {i ~> u}e.
Proof.
  induction e; introv Neq Equ;
  simpl in *; inversion* Equ; f_equal*.  
  case_nat*. case_nat*.
Qed.

Lemma open_rec_term : forall t u,
  lc_trm t -> forall k, t = {k ~> u}t.
Proof.
  induction 1; intros; simpl; f_equal*.
  unfolds open_trm_wrt_trm. pick_fresh x.
   apply* (@open_rec_term_core e 0 (trm_var_f x)).
  unfolds open_trm_wrt_trm. pick_fresh x. 
   apply* (@open_rec_term_core B 0 (trm_var_f x)).
  unfolds open_trm_wrt_trm. pick_fresh x. 
   apply* (@open_rec_term_core B 0 (trm_var_f x)).
  unfolds open_trm_wrt_trm. pick_fresh x. 
   apply* (@open_rec_term_core B 0 (trm_var_f x)).
Qed.

Lemma subst_fresh : forall x t u, 
  x \notin fv t -> [x ~> u] t = t.
Proof.
  intros. induction t; simpls; fequals*.
  case_var*. 
Qed.

Lemma subst_open : forall x u t1 t2, lc_trm u -> 
  [x ~> u] (t1 ^^ t2) = ([x ~> u]t1) ^^ ([x ~> u]t2).
Proof.
  intros. unfold open_trm_wrt_trm. generalize 0.
  induction t1; intros; simpl; f_equal*.
  case_nat*. case_var*. apply* open_rec_term.
Qed.

Lemma subst_open_var : forall x y u e, y <> x -> lc_trm u ->
  ([x ~> u]e) ^ y = [x ~> u] (e ^ y).
Proof.
  introv Neq Wu. rewrite* subst_open.
  simpl. case_var*.
Qed.

Lemma subst_open_2 : forall x u t1 t2, lc_trm u -> 
  [x ~> u] (open_trm_wrt_trm_1 t1 t2) = open_trm_wrt_trm_1 ([x ~> u]t1) ([x ~> u]t2).
Proof.
  intros. unfold open_trm_wrt_trm_1. generalize 0.
  induction t1; intros; simpl; f_equal*.
  case_nat*. case_var*. apply* open_rec_term.
Qed.

Lemma subst_open_var_2 : forall x y u e, y <> x -> lc_trm u ->
  ([x ~> u]e) ^! y = [x ~> u] (e ^! y).
Proof.
  introv Neq Wu. rewrite* subst_open_2.
  simpl. case_var*.
Qed.

Lemma subst_intro : forall x t u, 
  x \notin (fv t) -> lc_trm u ->
  t ^^ u = [x ~> u](t ^ x).
Proof.
  introv Fr Wu. rewrite* subst_open.
  rewrite* subst_fresh. simpl. case_var*.
Qed.

Lemma subst_intro_2 : forall x t u, 
  x \notin (fv t) -> lc_trm u ->
  t ^^! u = [x ~> u](t ^! x).
Proof.
  introv Fr Wu. rewrite* subst_open_2.
  rewrite* subst_fresh. simpl. case_var*.
Qed.

Ltac cross := 
  rewrite subst_open_var; try cross.

Ltac cross_2 := 
  rewrite subst_open_var_2; try cross_2.

Tactic Notation "cross" "*" := 
  cross; autos*.

Lemma subst_term : forall t z u,
  lc_trm u -> lc_trm t -> lc_trm ([z ~> u]t).
Proof.
  induction 2; simple*.
  case_var*.
  apply_fresh* lc_trm_abs as y. rewrite* subst_open_var.
  apply_fresh* lc_trm_prod as y. rewrite* subst_open_var.
  apply_fresh* lc_trm_sigma as y. rewrite* subst_open_var.
  apply_fresh* lc_trm_pair as y. rewrite* subst_open_var.
Qed.

Lemma open_term : forall t u,
  body t -> lc_trm u -> lc_trm (t ^^ u).
Proof.
  intros. destruct H. pick_fresh y.
  rewrite* (@subst_intro y). apply* subst_term.
Qed.

Hint Resolve subst_term open_term.

Lemma lc_trm_abs1 : forall t2 t1,
  lc_trm (trm_abs t1 t2) -> lc_trm t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_abs2 : forall t1 t2,  
  lc_trm (trm_abs t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma lc_trm_prod1 : forall t2 t1,
  lc_trm (trm_prod t1 t2) -> lc_trm t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_prod2 : forall t1 t2,  
  lc_trm (trm_prod t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma lc_trm_sigma1 : forall t2 t1,
  lc_trm (trm_sigma t1 t2) -> lc_trm t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_sigma2 : forall t1 t2,  
  lc_trm (trm_sigma t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma lc_trm_pair1 : forall e1 e2 t2 t1,
  lc_trm (trm_pair e1 e2 t1 t2) -> lc_trm t1.
Proof.
  intros. inversion* H.
Qed.

Lemma body_pair2 : forall e1 e2 t1 t2,  
  lc_trm (trm_pair e1 e2 t1 t2) -> body t2.
Proof.
  intros. unfold body. inversion* H.
Qed.

Lemma lc_trm_pair3 : forall t1 t2 e2 e1,
  lc_trm (trm_pair e1 e2 t1 t2) -> lc_trm e1.
Proof.
  intros. inversion* H.
Qed.

Lemma lc_trm_pair4 : forall t1 t2 e1 e2,
  lc_trm (trm_pair e1 e2 t1 t2) -> lc_trm e2.
Proof.
  intros. inversion* H.
Qed.

Hint Extern 1 (lc_trm ?t1) =>
  match goal with
  | H: lc_trm (trm_abs t1 ?t2) |- _ => apply (@lc_trm_abs1 t2)
  | H: lc_trm (trm_prod t1 ?t2) |- _ => apply (@lc_trm_prod1 t2)
  | H: lc_trm (trm_sigma t1 ?t2) |- _ => apply (@lc_trm_sigma1 t2)
  | H: lc_trm (trm_pair ?e1 ?e2 t1 ?t2) |- _ => apply (@lc_trm_pair1 e1 e2 t2)
  end.

Hint Extern 3 (body ?t) =>
  match goal with 
  | H: context [trm_abs ?t1 t] |- _ => apply (@body_abs2 t1)
  | H: context [trm_prod ?t1 t] |- _ => apply (@body_prod2 t1) 
  | H: context [trm_sigma ?t1 t] |- _ => apply (@body_sigma2 t1)
  | H: context [trm_pair ?e1 ?e2 ?t1 t] |- _ => apply (@body_pair2 e1 e2 t1)
 end.

Hint Extern 1 (lc_trm ?e) =>
  match goal with
  | H: lc_trm (trm_pair e ?e1 ?t1 ?t2) |- _ => apply (@lc_trm_pair3 t1 t2 e1)
  | H: lc_trm (trm_pair ?e1 e ?t1 ?t2) |- _ => apply (@lc_trm_pair4 t1 t2 e1)
  end.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: context [t ^ _] |- _ =>
      let x := fresh in let Fr := fresh in
      let P := fresh in
      let L := gather_vars in exists L; intros x Fr; 
      lets P: H x __; [ notin_solve 
                      | try destructs P ]
  end.

Lemma lc_trm_abs_prove : forall t1 t2,
  lc_trm t1 -> body t2 -> lc_trm (trm_abs t1 t2).
Proof.
  intros. apply_fresh* lc_trm_abs as x.
Qed.

Lemma lc_trm_prod_prove : forall t1 t2,
  lc_trm t1 -> body t2 -> lc_trm (trm_prod t1 t2).
Proof.
  intros. apply_fresh* lc_trm_prod as x.
Qed.

Lemma lc_trm_sigma_prove : forall t1 t2,
  lc_trm t1 -> body t2 -> lc_trm (trm_sigma t1 t2).
Proof.
  intros. apply_fresh* lc_trm_sigma as x.
Qed.

Lemma lc_trm_pair_prove : forall e1 e2 t1 t2,
  lc_trm e1 -> lc_trm e2 ->
  lc_trm t1 -> body t2 -> lc_trm (trm_pair e1 e2 t1 t2).
Proof.
  intros. apply_fresh* lc_trm_pair as x.
Qed.

Hint Resolve lc_trm_abs_prove lc_trm_prod_prove.
Hint Resolve lc_trm_sigma_prove lc_trm_pair_prove.

Lemma body_prove : forall L t,
  (forall x, x \notin L -> lc_trm (t ^ x)) -> body t.
Proof.
  intros. exists* L.
Qed.

Hint Extern 1 (body ?t) =>
  match goal with 
  | H: forall _, _ \notin ?L -> lc_trm (t ^ _)  |- _ =>
    apply (@body_prove L)
  end. 

Lemma body_subst : forall x t u,
  lc_trm u -> body t -> body ([x ~> u]t).
Proof.
  introv. intros Wu [L Bt].
  exists (\{x} \u L). intros y Fr. cross*.
Qed.

Hint Resolve body_subst.

Lemma open_var_inj : forall x t1 t2, 
  x \notin (fv t1) -> x \notin (fv t2) -> 
  (t1 ^ x = t2 ^ x) -> (t1 = t2).
Proof.
  intros x t1. unfold open_trm_wrt_trm. generalize 0.
  induction t1; intro k; destruct t2; simpl; intros; inversion H1;
  try solve [ f_equal* 
  | do 2 try case_nat; inversions* H1; try notin_false ].
Qed.

Lemma close_var_rec_open : forall x y z t1 i j,
  i <> j -> y <> x -> y \notin (fv t1) ->
    {i ~> trm_var_f y} ({j ~> trm_var_f z} (close_var_rec j x t1) )
  = {j ~> trm_var_f z} (close_var_rec j x ({i ~> trm_var_f y}t1) ).
Proof.
  induction t1; simpl; intros; try solve [ f_equal* ].
  do 2 (case_nat; simpl); try solve [ case_var* | case_nat* ]. 
  case_var*. simpl. case_nat*. 
Qed.

Lemma close_var_fresh : forall x t,
  x \notin fv (close_var x t).
Proof.
  introv. unfold close_var. generalize 0.
  induction t; intros k; simpls; notin_simpl; auto.
  case_var; simple*.
Qed.

Lemma close_var_body : forall x t,
  lc_trm t -> body (close_var x t).
Proof.
  introv W. exists \{x}. intros y Fr.
  unfold open_trm_wrt_trm, close_var. generalize 0. gen y.
  induction W; intros y Fr k; simpls; try solve [autos*].
  case_var; simple*. case_nat*.
  apply_fresh* lc_trm_abs as z.
   unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  apply_fresh* lc_trm_prod as z.
   unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  apply_fresh* lc_trm_sigma as z.
   unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  apply_fresh* lc_trm_pair as z.
   unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
Qed.

Lemma close_var_open : forall x t,
  lc_trm t -> t = (close_var x t) ^ x.
Proof.
  introv W. unfold close_var, open_trm_wrt_trm. generalize 0.
  induction W; intros k; simpls; f_equal*.
  case_var*. simpl. case_nat*.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
  let L := gather_vars in match goal with |- _ = ?t => 
    destruct (var_fresh (L \u fv t)) as [y Fr] end.
  apply* (@open_var_inj y).
  unfolds open_trm_wrt_trm. rewrite* close_var_rec_open.
Qed. 

Lemma close_var_spec : forall t x, lc_trm t -> 
  exists u, t = u ^ x /\ body u /\ x \notin (fv u).
Proof.
  intros. exists (close_var x t). splits 3.
  apply* close_var_open.
  apply* close_var_body.
  apply* close_var_fresh.
Qed. 

Lemma value_is_term : forall t, value t -> lc_trm t.
Proof.
  intros_all. induction* H.
Qed.

Hint Extern 1 (lc_trm ?t) => match goal with
  | H: value t |- _ => apply (value_is_term H) end.

Lemma red_regular_reduct : red_regular reduct.
Proof.
  intros_all. induction* H.
  splits*.
  apply~ lc_trm_castup.
  apply~ lc_trm_castup.
  apply~ lc_trm_app.
  apply~ lc_trm_app.
  inversions H0.
  apply_fresh* lc_trm_abs as x.
  asserts* P: (lc_trm (trm_abs trm_star e ^ x)).
  asserts* W: (trm_abs trm_star e ^ x = trm_abs trm_star (e ^! x)).
  asserts* W2: (trm_abs B e ^ x = trm_abs (B ^ x) (e ^! x)).
  rewrite W2.
  rewrite W in P. inversions P.
  apply_fresh* lc_trm_abs as y.
Qed.

Hint Extern 1 (lc_trm ?t) => match goal with
  | H: reduct t _ |- _ => apply (proj1 (red_regular_reduct H))
  | H: reduct _ t |- _ => apply (proj2 (red_regular_reduct H))
  end.

Hint Extern 1 (lc_trm (trm_abs ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: lc_trm (trm_abs t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_abs t1 t2)) end.

Hint Extern 1 (lc_trm (trm_prod ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: lc_trm (trm_prod t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_prod t1 t2)) end.

Hint Extern 1 (lc_trm (trm_sigma ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: lc_trm (trm_sigma t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_sigma t1 t2)) end.

Hint Extern 1 (lc_trm (trm_pair ([?x ~> ?u]?e1) ([?x ~> ?u]?e2) ([?x ~> ?u]?t1) ([?x ~> ?u]?t2))) =>
  match goal with H: lc_trm (trm_pair e1 e2 t1 t2) |- _ => 
  unsimpl ([x ~> u](trm_pair e1 e2 t1 t2)) end.
 
Lemma regular_sub : forall G e1 e2 A,
  sub G e1 e2 A ->
  (wf G /\ ok G /\ contains_terms G /\ lc_trm e1 /\ lc_trm e2 /\ lc_trm A). 
Proof.
  apply sub_induct with
  (P0 := fun E (_ : wf E) => ok E /\ contains_terms E);
    unfolds contains_terms; intros; splits*.

  destructs H. apply* lc_trm_app.

  destructs H. apply* lc_trm_open.
  apply_fresh* lc_trm_abs as x.
  asserts* W: ((trm_abs trm_star e) ^ x = (trm_abs trm_star (e ^! x))).
  rewrite W. apply_fresh* lc_trm_abs as y.
  lets* P: H0 y __. lets* Q: P x __.

  destructs H. apply* lc_trm_open.
  apply_fresh* lc_trm_abs as x.
  asserts* W: ((trm_abs trm_star e) ^ x = (trm_abs trm_star (e ^! x))).
  rewrite W. apply_fresh* lc_trm_abs as y.
  lets* P: H0 y __. lets* Q: P x __.
  
  intros. false* binds_empty_inv.
  introv Bnd. destruct (binds_push_inv Bnd) as [[? ?]|[? ?]].
    subst*.
    destruct H as (? & ? & HH & ?). apply* HH.
Qed.

Hint Extern 1 (lc_trm ?t) => match goal with
  | H: sub _ t _ _ |- _ => apply (proj31 (proj44 (regular_sub H)))
  | H: sub _ _ t _ |- _ => apply (proj32 (proj44 (regular_sub H)))
  | H: sub _ _ _ t |- _ => apply (proj33 (proj44 (regular_sub H)))
  end.

Lemma ok_from_wf : forall G, wf G -> ok G.
Proof.
  induction 1. auto. autos* (regular_sub H).
Qed.

Hint Extern 1 (ok ?E) => match goal with
  | H: wf E |- _ => apply (ok_from_wf H)
  end.

Hint Extern 1 (wf ?E) => match goal with
  | H: sub E _ _ _ |- _ => apply (proj1 (regular_sub H))
  end.

Lemma wf_push_inv : forall G x A,
  wf (G & x ~ A) -> wf G /\ lc_trm A.
Proof.
  introv W. inversions W. 
  false (empty_push_inv H0).
  destruct (eq_push_inv H) as [? [? ?]]. subst~.
Qed.

Lemma lc_trm_from_binds_in_wf : forall x E T,
  wf E -> binds x T E -> lc_trm T.
Proof.
  introv W Has. gen E. induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv W). 
  destruct (binds_push_inv Has) as [[? ?] | [? ?]].
  subst~.
  apply* IHE.
Qed.

Hint Extern 1 (lc_trm ?t) => match goal with
  | H: binds ?x t ?E |- _ => apply (@lc_trm_from_binds_in_wf x E)
  end.

Lemma wf_left : forall E F : env,
  wf (E & F) -> wf E.
Proof.
  intros. induction F using env_ind.
  rewrite~ concat_empty_r in H.
  rewrite concat_assoc in H.
   inversions H. false (empty_push_inv H1).
   destruct (eq_push_inv H0) as [? [? ?]]. subst~.
Qed.

Implicit Arguments wf_left [E F].

Lemma fv_open_var : forall y x t,
  x <> y -> x \notin fv (t ^ y) -> x \notin fv t.
Proof.
  introv Neq. unfold open_trm_wrt_trm. generalize 0. 
  induction t; simpl; intros; try notin_solve; autos*.
Qed.

Lemma fv_open_var_2 : forall y x t,
  x <> y -> x \notin fv (t ^! y) -> x \notin fv t.
Proof.
  introv Neq. unfold open_trm_wrt_trm_1. generalize 0. 
  induction t; simpl; intros; try notin_solve; autos*.
Qed.

Lemma fresh_false : forall G x (v:trm),
  x # G & x ~ v -> False.
Proof.
  intros. simpl_dom.
  destruct (notin_union x \{x} (dom G)) as [H1 _].
  lets: H1 H.
  destruct H0 as [H3 _].
  apply* notin_same.
Qed.

Lemma and_simpl : forall (A : Prop),
    A -> A /\ A.
Proof.
  intros. auto.
Qed.

Lemma fv_open_term : forall A B x,
  x \notin fv A -> x \notin fv B ->
  x \notin fv (A ^^ B).
Proof.
  intros A. unfold open_trm_wrt_trm. generalize 0. 
  induction A; simpl; intros; try (notin_simpl); autos*.
  case_nat~. 
Qed.

Lemma fv_close_var : forall y x t,
  x <> y -> x \notin fv t -> x \notin fv (t ^ y).
Proof.
  introv Neq. unfold open_trm_wrt_trm. generalize 0. 
  induction t; simpl; intros; try notin_solve; try autos*.
  case_nat~. simpl. auto.
Qed.

Lemma reduct_fresh : forall A B x,
  A --> B -> x \notin fv A -> x \notin fv B.
Proof.
  intros. induction H; simpls; auto.
  apply* fv_open_term. 
Qed.

Lemma sub_fresh : forall G A B C x,
  sub G A B C -> x # G -> (x \notin fv A) /\ (x \notin fv B).
Proof.
  introv Typ.
  induction Typ; simpls; intros; try (apply and_simpl).
  (* case star *)
  auto.
  (* case var *)
  rewrite notin_singleton. intro. subst. applys binds_fresh_inv H0 H1.
  (* case top *)
  splits*. 
  (* case top refl *)
  auto.
  (* case abs *)
  destruct~ IHTyp.
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  (* case app *)
  splits*.
  (* case app_v *)
  splits*.
  (* case prod *)
  destruct~ IHTyp.
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H2 y). auto. auto. auto.
  (* case castup *)
  splits*.
  (* case castdn *)
  splits*.
  (* case sig *)
  destruct~ IHTyp.
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  (* case pair *)
  destruct~ IHTyp1.
  destruct~ IHTyp2.
  destruct~ IHTyp3.
  split; notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  pick_fresh y. apply* (@fv_open_var y).
  destruct (H0 y). auto. auto. auto.
  (* case fst *)
  splits*.
  (* case snd *)
  destruct~ IHTyp.
  (* case open *)
  destruct~ IHTyp1.
  notin_simpl; auto.
  pick_fresh y. apply* (@fv_open_var_2 y).
  pick_fresh z. apply* (@fv_open_var z).
  lets* P: H0 z __. lets* Q: P y __.
  destruct~ Q.
  (* case sub *)
  auto.
Qed.

Lemma notin_fv_from_wf : forall E F x T,
  wf (E & x ~ T & F) -> x \notin fv T.
Proof.
  intros.
  lets W: (wf_left H). inversions W.
  false (empty_push_inv H1). 
  destruct (eq_push_inv H0) as [? [? ?]]. subst~.
  apply* sub_fresh.
Qed.

Lemma notin_fv_from_binds : forall x y U E,
  wf E -> binds y U E -> x # E -> x \notin fv U.
Proof.
  induction E using env_ind; intros.
  false* binds_empty_inv.
  destruct (wf_push_inv H).
    destruct (binds_push_inv H0) as [[? ?] | [? ?]].
    subst. inversions H. false* (empty_push_inv H5).
     destruct (eq_push_inv H4) as [? [? ?]].  subst~. 
     apply* sub_fresh.
     apply* IHE.
Qed.

Lemma notin_fv_from_binds' : forall E F x V y U,
  wf (E & x ~ V & F) -> 
  binds y U E ->
  x \notin fv U.
Proof.
  intros. lets W: (wf_left H). inversions keep W.
  false (empty_push_inv H2). 
  destruct (eq_push_inv H1) as [? [? ?]]. subst~. 
  lets W': (wf_left W). apply* notin_fv_from_binds.
Qed.

Hint Extern 1 (?x \notin fv ?V) => 
  match goal with H: wf (?E & x ~ V & ?F) |- _ =>
    apply (@notin_fv_from_wf E F) end.

Hint Extern 1 (?x \notin fv ?U) => 
  match goal with H: wf (?E & x ~ ?V & ?F), B: binds ?y U ?E |- _ =>
    apply (@notin_fv_from_binds' E F x V y) end.

Lemma open_closed_eq : forall x e,
  lc_trm e -> e = e ^ x.
Proof.
  intros. apply* open_rec_term.
Qed.

Lemma lc_open_eq : forall e1 e2,
  lc_trm e1 -> lc_trm e2 -> e1 ^^ e2 = e1.
Proof.
  intros. pick_fresh x. rewrite* (@subst_intro x).
  rewrite* <- open_closed_eq.
  apply* subst_fresh.
Qed.

Hint Extern 1 (?e1 ^^ ?e2 = ?e1) =>
  match goal with
  | H: lc_trm e1 |- _ => rewrite~ (@lc_open_eq e1 e2)
  end.

Hint Extern 1 (?e1 = ?e1 ^^ ?e2) =>
  match goal with
  | H: lc_trm e1 |- _ => rewrite~ (@lc_open_eq e1 e2)
  end.

Lemma closed_not_open : forall x e,
  lc_trm e ->
  x \notin fv e ->
  x \in fv(e ^ x) -> False.
Proof.
  intros.
  rewrite~ <- open_closed_eq in H1.
Qed.

Lemma open_double_eq: forall v z C,
  lc_trm v -> z \notin fv C ->
  lc_trm (C ^ z) -> C ^^! v = C.
Proof.
  intros.
  rewrite* (@subst_intro_2 z).
  asserts* EQ: (C ^ z = {1 ~> trm_var_f z} C ^ z).
  apply* open_rec_term.
  unfold open_trm_wrt_trm in EQ.
  asserts EQ2: (C = {1 ~> trm_var_f z} C).
  apply open_rec_term_core with (j := 0) (v := trm_var_f z). auto.
  auto. unfold open_trm_wrt_trm_1. rewrite <- EQ2.
  rewrite* subst_fresh.
Qed.

Lemma close_open_any_eq: forall x z P, 
      lc_trm P -> (close_var_rec 1 z P) ^! x = (close_var z P) ^ x.
Proof.
  intros. unfold open_trm_wrt_trm_1.
  unfold open_trm_wrt_trm. unfold close_var.
  generalize 0. inductions H; intros; simpls; try solve [fequals*].
  case_if~. simpls. case_if. case_if. auto.

  fequals*.
  let L := gather_vars in match goal with |- ?t1 = ?t2 => 
    destruct (var_fresh (L \u fv t1 \u fv t2)) as [y Fr] end.
  lets P: H1 y __. auto.
  lets P3: P (S n).
  unfolds open_trm_wrt_trm. rewrite* <- close_var_rec_open in P3.
  rewrite* <- close_var_rec_open in P3.
  apply* open_var_inj.

  fequals*.
  let L := gather_vars in match goal with |- ?t1 = ?t2 => 
    destruct (var_fresh (L \u fv t1 \u fv t2)) as [y Fr] end.
  lets P: H1 y __. auto.
  lets P3: P (S n).
  unfolds open_trm_wrt_trm. rewrite* <- close_var_rec_open in P3.
  rewrite* <- close_var_rec_open in P3.
  apply* open_var_inj.

  fequals*.
  let L := gather_vars in match goal with |- ?t1 = ?t2 => 
    destruct (var_fresh (L \u fv t1 \u fv t2)) as [y Fr] end.
  lets P: H1 y __. auto.
  lets P3: P (S n).
  unfolds open_trm_wrt_trm. rewrite* <- close_var_rec_open in P3.
  rewrite* <- close_var_rec_open in P3.
  apply* open_var_inj.

  fequals*.
  let L := gather_vars in match goal with |- ?t1 = ?t2 => 
    destruct (var_fresh (L \u fv t1 \u fv t2)) as [y Fr] end.
  lets P: H3 y __. auto.
  lets P3: P (S n).
  unfolds open_trm_wrt_trm. rewrite* <- close_var_rec_open in P3.
  rewrite* <- close_var_rec_open in P3.
  apply* open_var_inj.
Qed.

