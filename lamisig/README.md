# Coq Formalization of λIΣ

This repository contains:

* `Main.ott` is the Ott definition of type systems which is written in
  readable ASCII text, including syntax, operational
  semantics and static semantics. For
  details of Ott syntax, please refer to
  its [website](http://www.cl.cam.ac.uk/~pes20/ott/)
  or [developer page](https://github.com/ott-lang/ott).
* `Main_Infra.v` is the infrastructure file, which contains basic
  supporting lemmas for locally nameless representation.
* `Main_Proofs.v` is proofs for the metatheory, including
  transitivity and type safety.

For more information, please refer to the readme file in `lamisub` directory.
