# Archive

Run `make` command to check all Coq proofs (except scripts in `experiments`).

Please refer to the readme document in each directory.

* Formalization of PITS: [pits](pits/)
* Implementation of PITS: [pits-impl](pits-impl/)
* Formalization of λI⩽: [lamisub](lamisub/)
* Formalization of λIΣ: [lamisig](lamisig/)
* Experimental Coq scripts: [experiments](experiments/)
