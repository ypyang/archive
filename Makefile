all:
	make -C pits
	make -C lamisub
	make -C lamisig

clean:
	make -C pits clean
	make -C lamisub clean
	make -C lamisig clean

.PHONY: all clean
